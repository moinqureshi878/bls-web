### Observable to fill dropdown
    typeddl: Observable<any>;
    this.typeddl = this.commonService.getLookUp("?type=moduleType");
    <mat-select placeholder="{{ 'GENERALWORDS.TYPE' | translate }}" formControlName='type'>
    <mat-option *ngFor="let type of typeddl | async" [value]="type.id">
      {{ this.translationService.getSelectedLanguage() =="en" ? type.valueEn : type.valueAr}}
    </mat-option>
    </mat-select>

### Constructor
    private env: EnvService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private translationService: TranslationService,
    private toast: ToastrService,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    private cdr: ChangeDetectorRef,
    public authenticationService: AuthenticationService,
    public moduleService: ModuleService,
    public submoduleService: SubModuleService,
    public pagesService: PagesService,
    public roleService: RoleService,
    public userService: UserService,
    public commonService: CommonService,
    public bnCommonService: BnCommonService,
    public bnService: BnService,
    public configService: ConfigService,
    public masterDataService: MasterDataService,
    public masterTypesService: MasterTypesService,
    private bnService : BnService,
    private jwtTokenService: JwtTokenService,
    private iaService : IAService,

### Imports
    import { Component, OnInit } from '@angular/core';
    import { EnvService } from '../../../core/environment/env.service';
    import { FormBuilder, Validators, FormGroup } from '@angular/forms';
    import { Router } from '@angular/router';
    import { AsyncFormValidatorService } from '../../../core/validator/async-form-validator.service';
    import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
    import { commentValidator, nameArValidator } from '../../../core/validator/form-validators.service';
    import { RoleService, CommonService } from '../../../core/appServices/index.service';
    import { ToastrService } from 'ngx-toastr';
    import { TranslationService } from '@translateService/translation.service';
    import { UserTypeEnum } from '../../../core/_enum/user-type.enum';
    import { Observable } from 'rxjs';
    import { map } from 'rxjs/operators';
    import { ConfigService } from "../../../core/appServices/index.service";

### Get User ID
    import * as jwt_decode from 'jwt-decode';
    let userSession =  JSON.parse(localStorage.getItem("userSession"));
    UserID = jwt_decode(userSession['token']);

### Tranlate to HTML
	  {{ this.translationService.getSelectedLanguage() =="en" ? type.itemEn: type.itemAr}}

### Router Link
    [routerLink]="['/pages/admin/edit-leaves/'+row.id]"
    this.router.navigateByUrl('/pages/master-data');
    this.getHoliday = this.route.snapshot.data.data;

### Date Filter
    {{row.toDate | date:'d/M/yyyy'}} 
    {{row.toDate | date:'d/M/yyyy, h:mm a'}} 

### Inactive Button
    <input type="checkbox" [checked]="row.isActive" (change)="isActive(row.id)">
    <span>{{row.isActive}}</span>

    this.getHoliday = this.route.snapshot.data.data;
    this.holidays = this.getHoliday;
    this.dataSource7 = new MatTableDataSource(this.holidays);

  isActiveBusinessGroup(id) {
    this.changeDataSource(id);
    this.bnCommonService.DeleteBusinessActivity(id).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {

      }
      else {
        this.translationService.getTranslation(String(response.result ? response.result.code : response.code)).subscribe((text: string) => {
              this.toast.error(text);
        });
      }
    });
  }

   changeDataSource(ID: number) {
    let index = this.getBusinessActivityPages.findIndex(x => x.id == ID);
    let isActive: boolean = this.getBusinessActivityPages[index].isActive == true ? false : true;
    this.getBusinessActivityPages[index].isActive = isActive;
  }

### Excel Export
  (click)="exporter.exportTable('xlsx',{fileName:'Holidays', Props: {Author: 'BLS system'}})"
  matTableExporter #exporter="matTableExporter" [hiddenColumns]="[4,5]"
    imports: [
      MatTableExporterModule
    ]

### API Response
  if ((response.result && response.result.code == 200) || (response.code == 200)) {

### Error
  <kt-error [formName]="jobSponsorInfo" [fieldName]="''"></kt-error>
   ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],

### Button
    <button [ngClass]="{'kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light': loading}"
      id="kt_login_signin_submit" class="btn btn-primary btn-elevate kt-login__btn-primary btn-sm">
      {{ 'AUTHMODULE.LOGIN.LoginHeading' | translate }}
    </button>

### Enums
  import { SessionTypeEnum, CommonEnum } from '../../core/_enum/index.enum';
  CommonEnum = CommonEnum;

### ngClass
  [ngClass]="statusClass=='1' ? 'Active' : 
                     statusClass=='2' ? 'AwaitingRequirements':
                     statusClass=='3' ? 'Completed':
                     statusClass=='4' ? 'Deactive':
                     statusClass=='5' ? 'Cancel':
                     statusClass=='6' ? 'Dispute':
                     statusClass=='7' ? 'Unpaid':
                     statusClass=='8' ? 'AwaitingReview':
                     statusClass=='9' ? 'Revoke':
                     statusClass=='10' ? 'Reject':
                     statusClass=='11' ? 'Resolved':
                     statusClass=='12' ? 'Accept':
                     statusClass=='13' ? 'DisputeCancel':
                     'DisputeCancel'"

### Form Submit 
    if (this.forwardForActionForm.valid) {
    
    }
    else {
      this.validationMessages.validateAllFormFields(this.forwardForActionForm);
    }