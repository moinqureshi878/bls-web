// Angular
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
// NgBootstrap
import { NgbDropdownModule, NgbTabsetModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
// Perfect Scrollbar
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
// Core module
import { CoreModule } from '../../core/core.module';
// CRUD Partials
import {
	ActionNotificationComponent,
	AlertComponent,
	DeleteEntityDialogComponent,
	FetchEntityDialogComponent,
	UpdateStatusDialogComponent
} from './content/crud';
// Layout partials
import {
	// ContextMenu2Component,
	// ContextMenuComponent,
	LanguageSelectorComponent,
	NotificationComponent,
	QuickActionComponent,
	// QuickPanelComponent,
	ScrollTopComponent,
	// SearchDefaultComponent,
	// SearchDropdownComponent,
	SearchResultComponent,
	SplashScreenComponent,
	StickyToolbarComponent,
	Subheader1Component,

	UserProfileComponent
} from './layout';
// General
import { NoticeComponent } from './content/general/notice/notice.component';
import { PortletModule } from './content/general/portlet/portlet.module';
// Errpr
import { ErrorComponent } from './content/general/error/error.component';

// SVG inline
import { InlineSVGModule } from 'ng-inline-svg';
// import { CartComponent } from './layout/topbar/cart/cart.component';
import { TranslateModule } from '@ngx-translate/core';
/* UI related Stuff */
import {
	MatAutocompleteModule,
	MatButtonModule,
	MatCardModule,
	MatCheckboxModule,
	MatDatepickerModule,
	MatDialogModule,
	MatIconModule,
	MatInputModule,
	MatMenuModule,
	MatNativeDateModule,
	MatPaginatorModule,
	MatProgressBarModule,
	MatProgressSpinnerModule,
	MatRadioModule,
	MatSelectModule,
	MatSnackBarModule,
	MatSortModule,
	MatTableModule,
	MatTabsModule,
	MatTooltipModule,
	//MatFormFieldModule,
	MatRippleModule,
	MatTreeModule,
	MatStepperModule,
	MatExpansionModule,
	/* Pages Module */
	MatChipsModule,
	MatSliderModule,
	MatSidenavModule,
	MatToolbarModule,
	MatDividerModule,
	MatListModule,
	MatGridListModule,
	MatButtonToggleModule,
	MatBottomSheetModule,
} from '@angular/material';



import { TextMaskModule } from 'angular2-text-mask';
@NgModule({
	declarations: [
		ScrollTopComponent,
		NoticeComponent,
		ActionNotificationComponent,
		DeleteEntityDialogComponent,
		FetchEntityDialogComponent,
		UpdateStatusDialogComponent,
		AlertComponent,

		// topbar components
		// ContextMenu2Component,
		// ContextMenuComponent,
		// QuickPanelComponent,
		ScrollTopComponent,
		SearchResultComponent,
		SplashScreenComponent,
		StickyToolbarComponent,
		Subheader1Component,

		LanguageSelectorComponent,
		NotificationComponent,
		QuickActionComponent,
		// SearchDefaultComponent,
		// SearchDropdownComponent,
		UserProfileComponent,
		// CartComponent,
		ErrorComponent,
	],
	exports: [
		PortletModule,
		CommonModule,
		ScrollTopComponent,
		NoticeComponent,
		ActionNotificationComponent,
		DeleteEntityDialogComponent,
		FetchEntityDialogComponent,
		UpdateStatusDialogComponent,
		AlertComponent,
		// topbar components
		// ContextMenu2Component,
		// ContextMenuComponent,
		// QuickPanelComponent,
		ScrollTopComponent,
		SearchResultComponent,
		SplashScreenComponent,
		StickyToolbarComponent,
		Subheader1Component,
		LanguageSelectorComponent,
		NotificationComponent,
		QuickActionComponent,
		// SearchDefaultComponent,
		// SearchDropdownComponent,
		UserProfileComponent,
		// CartComponent,
		ErrorComponent,
		MatFormFieldModule,
		/* UI Stuff */
		MatButtonModule,
		MatFormFieldModule,
		MatInputModule,
		MatRippleModule,
		MatTableModule,
		MatPaginatorModule,
		MatIconModule,
		MatSelectModule,
		MatRadioModule,
		MatTabsModule,
		MatTreeModule,
		MatCheckboxModule,
		MatDialogModule,
		MatStepperModule,
		MatTooltipModule,
		MatDatepickerModule,
		MatExpansionModule,
		/* Pages Module */
		MatChipsModule,
		MatSliderModule,
		MatSidenavModule,
		MatToolbarModule,
		MatDividerModule,
		MatListModule,
		MatGridListModule,
		MatButtonToggleModule,
		MatBottomSheetModule,
		/* TextMasking */
		TextMaskModule,
		/* Form Module */
		FormsModule,
		ReactiveFormsModule,
	],
	imports: [
		CommonModule,
		RouterModule,
		FormsModule,
		ReactiveFormsModule,
		PerfectScrollbarModule,
		InlineSVGModule,
		CoreModule,
		PortletModule,
		// angular material modules
		MatAutocompleteModule,
		MatButtonModule,
		MatCardModule,
		MatCheckboxModule,
		MatDatepickerModule,
		MatDialogModule,
		MatIconModule,
		MatInputModule,
		MatMenuModule,
		MatNativeDateModule,
		MatPaginatorModule,
		MatProgressBarModule,
		MatProgressSpinnerModule,
		MatRadioModule,
		MatSelectModule,
		MatSnackBarModule,
		MatSortModule,
		MatTableModule,
		MatTabsModule,
		MatTooltipModule,
		//MatFormFieldModule,
		MatRippleModule,
		MatTreeModule,
		MatStepperModule,
		MatExpansionModule,
		/* Pages Module */
		MatChipsModule,
		MatSliderModule,
		MatSidenavModule,
		MatToolbarModule,
		MatDividerModule,
		MatListModule,
		MatGridListModule,
		MatButtonToggleModule,
		MatBottomSheetModule,
		/* TextMasking */
		TextMaskModule,
		// ng-bootstrap modules
		NgbDropdownModule,
		NgbTabsetModule,
		NgbTooltipModule,
		TranslateModule.forChild(),
	]
})
export class PartialsModule {
}
