// Angular
import { Component, HostBinding, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ValidationMessagesService } from '../../../../../core/validator/validation-messages.service';
import { TranslationService } from '../../../../../core/_base/layout/services/translation.service';
@Component({
	selector: 'kt-error',
	templateUrl: './error.component.html',
	styleUrls: ['./error.component.scss']
})
export class ErrorComponent {
	// Public properties
	// type of error template to be used, accepted values; error-v1 | error-v2 | error-v3 | error-v4 | error-v5 | error-v6
	// @Input() type: string = 'error-v1';
	@Input() type: string;
	// full background image
	@Input() image: string;
	// error code, some error types template has it
	@Input() code: string = '404';
	// error title
	@Input() title: string;
	// error subtitle, some error types template has it
	@Input() subtitle: string;
	// error descriptions
	@Input() desc: string = 'Oops! Something went wrong!';
	// return back button title
	@Input() return: string = 'Return back';

	@HostBinding('class') classes: string = 'kt-grid kt-grid--ver kt-grid--root';
	@Input() formName: FormGroup;
	@Input() fieldName: any;
	getFieldName;
	constructor(public formBuilder: FormBuilder,
		public translationService: TranslationService,
		public validationMessages: ValidationMessagesService) {
	}
	ngOnChanges() {
		this.getFieldName = this.validationMessages.validation_messages[this.fieldName];
	}
}
