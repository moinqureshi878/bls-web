// Angular
import { Component, Input, OnInit } from '@angular/core';
import { JwtTokenService, AuthenticationService, CommonService } from '../../../../../core/appServices/index.service';
import { TranslationService } from '../../../../../core/_base/layout';

/*Tooltip */
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl } from '@angular/forms';

@Component({
	selector: 'kt-user-profile',
	styleUrls: ['./user-profile.component.scss'],
	templateUrl: './user-profile.component.html',
})
export class UserProfileComponent implements OnInit {
	// Public properties
	user: any; //Observable<User>;

	@Input() avatar: boolean = true;
	@Input() greeting: boolean = true;
	@Input() badge: boolean;
	@Input() icon: boolean;

	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 */
	constructor(
		private jwtDecodeService: JwtTokenService,
		public translationService: TranslationService,
		private authService: AuthenticationService,
		public commonService: CommonService,
		private jwtTokenService: JwtTokenService,
	) {
	}


	ngOnInit(): void {
		// let userItems;
		// let userSession = JSON.parse(localStorage.getItem("userSession"));
		// if (userSession.length > 1) {
		// 	userItems = userSession[1].token;
		// }
		// else {
		// 	userItems = userSession[0].token;
		// }


		
		let JwtToken = null;
		let UserSession = JSON.parse(localStorage.getItem("userSession"));
		if(UserSession && UserSession.length > 1){
			JwtToken = UserSession[1].token;
		}
		else{
			JwtToken = UserSession[0].token;
		}

		this.user = this.jwtDecodeService.getJwtTokenValueByToken(JwtToken);
		
		this.commonService.getMenuItems().subscribe(data => {
			
			if(data){
				let tokenvalues = data.token;
				this.user = this.jwtDecodeService.getJwtTokenValueByToken(tokenvalues);
			}
		});


		//	this.user = this.jwtDecodeService.getJwtTokenValue();
		//	this.user$ = this.store.pipe(select(currentUser));
	}

	/**
	 * Log out
	 */
	logout() {
		// set menu in subject
		this.authService.logout("logout");
		this.commonService.UpdateMenuFromLocalStorage();
		this.jwtTokenService.updateSessionFromLocalStorage();
	}

	
	/* Tooltip */
	positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
	position = new FormControl(this.positionOptions[3]);


}
