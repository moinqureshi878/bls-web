// Angular
import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ShareableModalComponent } from '../../../../../pages/admin/shareable-modal/shareable-modal.component'
import { JwtTokenService, CommonService } from '../../../../../core/appServices/index.service';
import { CommonEnum } from '../../../../../core/_enum/index.enum';

/*Tooltip */
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl } from '@angular/forms';

@Component({
	selector: 'kt-quick-action',
	templateUrl: './quick-action.component.html',
})
export class QuickActionComponent implements OnInit, AfterViewInit {
	// Public properties
	// Set icon class name
	@Input() icon: string = 'flaticon2-gear';
	@Input() iconType: '' | 'warning';
	// Set true to icon as SVG or false as icon class
	@Input() useSVG: boolean;
	// Set bg image path
	@Input() bgImage: string;
	// Set skin color, default to light
	@Input() skin: 'light' | 'dark' = 'light';
	@Input() gridNavSkin: 'light' | 'dark' = 'light';
	/**
	 * Component constructor
	 */

	userID: number;
	masterUserId: number;
	constructor(
		public dialog: MatDialog,
		public tokenservice: JwtTokenService,
		public commonService: CommonService) {
		this.masterUserId = CommonEnum.masterUserId;
		let userSession = JSON.parse(localStorage.getItem("userSession"));
		let token: string = null;

		if (userSession) {
			if (userSession.length > 1) {

				token = userSession[1].token;
			}
			else {

				token = userSession[0].token;
			}
		}

		this.commonService.getMenuItems().subscribe(value => {

			if (value) {
				let userSession = JSON.parse(localStorage.getItem("userSession"));
				if (userSession) {
					if (userSession.length > 1) {

						token = userSession[1].token;
					}
					else {

						token = userSession[0].token;
					}
				}
			}
			this.userID = +this.tokenservice.getJwtTokenValueByToken(token).UserID;
		});

		this.userID = +this.tokenservice.getJwtTokenValueByToken(token).UserID;
	}
	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */
	/**
	 * After view init
	 */
	ngAfterViewInit(): void {
	}
	/**
	 * On init
	 */
	ngOnInit(): void {
	}
	onSVGInserted(svg) {
		svg.classList.add('kt-svg-icon', 'kt-svg-icon--success', 'kt-svg-icon--lg');
	}
	/* begin:: Shareable Popup */
	openReviewAttachmentDialog() {
		const dialogConfig = new MatDialogConfig();
		dialogConfig.autoFocus = true;
		dialogConfig.minWidth = "30%";
		this.dialog.open(ShareableModalComponent, dialogConfig);
	}
	/* end:: Shareable Popup */

	/* Tooltip */
	positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
	position = new FormControl(this.positionOptions[3]);
}
