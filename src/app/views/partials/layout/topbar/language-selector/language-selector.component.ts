// Angular
import { Component, HostBinding, OnInit, Input, ElementRef, Renderer2, } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
// RxJS
import { filter } from 'rxjs/operators';
// Translate
import { TranslationService } from '../../../../../core/_base/layout';

/*Tooltip */
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl } from '@angular/forms';

interface LanguageFlag {
	lang: string;
	name: string;
	flag: string;
	active?: boolean;
}

@Component({
	selector: 'kt-language-selector',
	templateUrl: './language-selector.component.html',
})
export class LanguageSelectorComponent implements OnInit {
	// Public properties
	@HostBinding('class') classes = '';
	@Input() iconType: '' | 'brand';

	cssLtr: string;
	cssLang: string;
	cssRegisterLtr: string;
	lang: string;
	language: LanguageFlag;
	languages: LanguageFlag[] = [
		{
			lang: 'en',
			name: 'English',
			flag: './assets/media/flags/012-uk.svg'
		},
		{
			lang: 'ar',
			name: 'Arabic',
			flag: './assets/media/flags/021-uae.svg'
		}
	];

	/**
	 * Component constructor
	 *
	 * @param translationService: TranslationService
	 * @param router: Router
	 */
	constructor(
		private el: ElementRef,
		private render: Renderer2,
		private translationService: TranslationService, private router: Router) {
		this.lang = this.translationService.getSelectedLanguage();
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		// this.setSelectedLanguage();
		// this.router.events
		// 	.pipe(filter(event => event instanceof NavigationStart))
		// 	.subscribe(event => {
		// 		this.setSelectedLanguage();
		// 	});
		this.translationService.setLanguage(this.translationService.getSelectedLanguage());
		this.translationCss(this.translationService.getSelectedLanguage());
	}


	/*::begin Translation CSS*/

	private loadCSS(styleUrl: string) {
		return new Promise((resolve, reject) => {
			const styleElement = document.createElement('link');
			styleElement.href = styleUrl;
			styleElement.type = 'text/css';
			styleElement.rel = 'stylesheet';
			styleElement.id = 'loadCss';
			styleElement.onload = resolve;
			this.render.appendChild(this.el.nativeElement, styleElement);
		});
	}
	private registerCSS(styleUrl: string) {
		return new Promise((resolve, reject) => {
			const styleElement = document.createElement('link');
			styleElement.href = styleUrl;
			styleElement.type = 'text/css';
			styleElement.rel = 'stylesheet';
			styleElement.id = 'registerCss';
			styleElement.onload = resolve;
			this.render.appendChild(this.el.nativeElement, styleElement);
		});
	}

	private langCSS(styleUrl: string) {
		return new Promise((resolve, reject) => {
			const styleElement = document.createElement('link');
			styleElement.href = styleUrl;
			styleElement.type = 'text/css';
			styleElement.rel = 'stylesheet';
			styleElement.id = 'langCss';
			styleElement.onload = resolve;
			this.render.appendChild(this.el.nativeElement, styleElement);
		});
	}

	private removeloadCSS() {
		return new Promise((resolve, reject) => {
			const styleElement = document.getElementById('loadCss');
			this.render.removeChild(this.el.nativeElement, styleElement);
		});
	}

	private removeRegisterCSS() {
		return new Promise((resolve, reject) => {
			const styleElement = document.getElementById('registerCss');
			this.render.removeChild(this.el.nativeElement, styleElement);
		});
	}

	private removelangCSS() {
		return new Promise((resolve, reject) => {
			const styleElement = document.getElementById('langCss');
			this.render.removeChild(this.el.nativeElement, styleElement);
		});
	}


	translationCss(lang) {

		if (lang == 'ar') {
			this.cssLtr = '../../assets/css/demo1/style.bundle.rtl.min.css';
			this.cssRegisterLtr = '../../assets/css/demo1/pages/register/register-ar.css';
			this.cssLang = '../../assets/css/demo1/langar/ar.css';
		}
		else {
			this.cssLtr = '../../assets/css/demo1/style.bundle.min.css';
			this.cssRegisterLtr = '../../assets/css/demo1/pages/register/register-en.css';
			this.cssLang = '../../assets/css/demo1/langen/en.css';
		}
		this.loadCSS(this.cssLtr);
		this.registerCSS(this.cssRegisterLtr);
		this.langCSS(this.cssLang);
	}
	/*::end Translation CSS*/

	setLanguage(lang) {
		this.lang = lang;
		this.removeloadCSS();
		this.removeRegisterCSS();
		this.removelangCSS();
		this.translationCss(lang);
		this.languages.forEach((language: LanguageFlag) => {
			if (language.lang === lang) {
				language.active = true;
				this.language = language;
			} else {
				language.active = false;
			}
		});
		this.translationService.setLanguage(lang);
	}

	/**
	 * Set selected language
	 */
	setSelectedLanguage(): any {
		this.setLanguage(this.translationService.getSelectedLanguage());
	}

		/* Tooltip */
		positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
		position = new FormControl(this.positionOptions[3]);
}
