// Angular
import { Component, OnDestroy, ElementRef, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';
// RxJS
import { Observable, Subscription } from 'rxjs';
// Object-Path
import * as objectPath from 'object-path';
// Layout
import { LayoutConfigService, MenuConfigService, PageConfigService, TranslationService } from '../core/_base/layout';
import { HtmlClassService } from './html-class.service';

import { MenuConfig } from '../core/_config/demo1/menu.config';
import { LayoutConfig } from '../core/_config/demo1/layout.config';


interface LanguageFlag {
	lang: string;
	name: string;
	flag: string;
	active?: boolean;
}

@Component({
	selector: 'kt-registration',
	templateUrl: './registration.component.html',
	styleUrls: ['./registration.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class RegistrationComponent implements OnInit {

	// Public variables
	selfLayout: string;
	asideDisplay: boolean;
	asideSecondary: boolean;
	subheaderDisplay: boolean;
	desktopHeaderDisplay: boolean;
	fitTop: boolean;
	fluid: boolean;

	cssLtr: string;
	cssLang: string;
	cssRegisterLtr: string;
	lang: string;
	language: LanguageFlag;
	languages: LanguageFlag[] = [
		{
			lang: 'en',
			name: 'English',
			flag: './assets/media/flags/012-uk.svg'
		},
		{
			lang: 'ar',
			name: 'Arabic',
			flag: './assets/media/flags/021-uae.svg'
		}
	];

	// Private properties
	private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/


	/**
	 * Component constructor
	 * @param el
	 * @param render
	 * @param layoutConfigService: LayoutConfigService
	 * @param menuConfigService: MenuConfifService
	 * @param pageConfigService: PageConfigService
	 * @param htmlClassService: HtmlClassService
	 * @param store
	 * @param permissionsService
	 */
	constructor(
		private el: ElementRef,
		private render: Renderer2,
		private layoutConfigService: LayoutConfigService,
		private menuConfigService: MenuConfigService,
		private translationService: TranslationService,
		private htmlClassService: HtmlClassService) {

		this.lang = this.translationService.getSelectedLanguage();

		// register configs by demos
		this.layoutConfigService.loadConfigs(new LayoutConfig().configs);
		this.menuConfigService.loadConfigs(new MenuConfig().configs);
		// this.pageConfigService.loadConfigs(new PageConfig().configs);

		// setup element classes
		this.htmlClassService.setConfig(this.layoutConfigService.getConfig());

		const subscr = this.layoutConfigService.onConfigUpdated$.subscribe(layoutConfig => {
			// reset body class based on global and page level layout config, refer to html-class.service.ts
			document.body.className = '';
			this.htmlClassService.setConfig(layoutConfig);
		});
		this.unsubscribe.push(subscr);
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {

		const config = this.layoutConfigService.getConfig();
		this.selfLayout = objectPath.get(config, 'self.layout');
		this.asideDisplay = objectPath.get(config, 'aside.self.display');
		this.subheaderDisplay = objectPath.get(config, 'subheader.display');
		this.desktopHeaderDisplay = objectPath.get(config, 'header.self.fixed.desktop');
		this.fitTop = objectPath.get(config, 'content.fit-top');
		this.fluid = objectPath.get(config, 'content.width') === 'fluid';

		// let the layout type change
		const subscr = this.layoutConfigService.onConfigUpdated$.subscribe(cfg => {
			setTimeout(() => {
				this.selfLayout = objectPath.get(cfg, 'self.layout');
			});
		});

		this.translationService.setLanguage(this.translationService.getSelectedLanguage());
		this.unsubscribe.push(subscr);
		this.translationCss(this.translationService.getSelectedLanguage());

	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		this.unsubscribe.forEach(sb => sb.unsubscribe());
	}

	/*::begin Translation CSS*/

	private loadCSS(styleUrl: string) {
		return new Promise((resolve, reject) => {
			const styleElement = document.createElement('link');
			styleElement.href = styleUrl;
			styleElement.type = 'text/css';
			styleElement.rel = 'stylesheet';
			styleElement.id = 'loadCss';
			styleElement.onload = resolve;
			this.render.appendChild(this.el.nativeElement, styleElement);
		});
	}
	private registerCSS(styleUrl: string) {
		return new Promise((resolve, reject) => {
			const styleElement = document.createElement('link');
			styleElement.href = styleUrl;
			styleElement.type = 'text/css';
			styleElement.rel = 'stylesheet';
			styleElement.id = 'registerCss';
			styleElement.onload = resolve;
			this.render.appendChild(this.el.nativeElement, styleElement);
		});
	}

	private langCSS(styleUrl: string) {
		return new Promise((resolve, reject) => {
			const styleElement = document.createElement('link');
			styleElement.href = styleUrl;
			styleElement.type = 'text/css';
			styleElement.rel = 'stylesheet';
			styleElement.id = 'langCss';
			styleElement.onload = resolve;
			this.render.appendChild(this.el.nativeElement, styleElement);
		});
	}

	private removeloadCSS() {
		return new Promise((resolve, reject) => {
			const styleElement = document.getElementById('loadCss');
			this.render.removeChild(this.el.nativeElement, styleElement);
		});
	}

	private removeRegisterCSS() {
		return new Promise((resolve, reject) => {
			const styleElement = document.getElementById('registerCss');
			this.render.removeChild(this.el.nativeElement, styleElement);
		});
	}

	private removelangCSS() {
		return new Promise((resolve, reject) => {
			const styleElement = document.getElementById('langCss');
			this.render.removeChild(this.el.nativeElement, styleElement);
		});
	}


	translationCss(lang) {
	
		if (lang == 'ar') {
			this.cssLtr = '../../assets/css/demo1/style.bundle.rtl.min.css';
			this.cssRegisterLtr = '../../assets/css/demo1/pages/register/register-ar.css';
			this.cssLang = '../../assets/css/demo1/langar/ar.css';
		}
		else {
			this.cssLtr = '../../assets/css/demo1/style.bundle.min.css';
			this.cssRegisterLtr = '../../assets/css/demo1/pages/register/register-en.css';
			this.cssLang = '../../assets/css/demo1/langen/en.css';
		}
		this.loadCSS(this.cssLtr);
		this.registerCSS(this.cssRegisterLtr);
		this.langCSS(this.cssLang);
	}
	/*::end Translation CSS*/

	setLanguage(lang) {
		this.lang = lang;
		this.removeloadCSS();
		this.removeRegisterCSS();
		this.removelangCSS();
		this.translationCss(lang);
		this.languages.forEach((language: LanguageFlag) => {
			if (language.lang === lang) {
				language.active = true;
				this.language = language;
			} else {
				language.active = false;
			}
		});
		this.translationService.setLanguage(lang);
	}

	/**
	 * Set selected language
	 */
	setSelectedLanguage(): any {
		this.setLanguage(this.translationService.getSelectedLanguage());
	}

}
