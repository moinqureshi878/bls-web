import { Component, OnInit, ViewChild, VERSION, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { matchingPasswords, emailValidator, phoneNumberValidator, phoneNumberMasking, emiratesIDMasking, nameValidator, passwordValidator, fileSize, nameArValidator } from '../../core/validator/form-validators.service';
import { AsyncFormValidatorService } from '../../core/validator/async-form-validator.service';
import { ReCaptcha2Component } from 'ngx-captcha';
import { ValidationMessagesService } from '../../core/validator/validation-messages.service';
import { EnvService } from '../../core/environment/env.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatStepper } from '@angular/material';
import { CommonService, UserService } from '../../core/appServices/index.service';
import { of, Observable } from 'rxjs';
import { TranslationService } from '@translateService/translation.service';
import { DomSanitizer } from '@angular/platform-browser';
import { GenderEnum, UserTypeEnum, CommonEnum } from '../../core/_enum/index.enum';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'kt-document-clearance-company',
  templateUrl: './document-clearance-company.component.html',
  styleUrls: ['./document-clearance-company.component.scss']
})
export class DocumentClearanceCompanyComponent implements OnInit {
  //Stepper Document Clearance Company
  basicInfoCompany: FormGroup;
  attachmentsInfo: FormGroup;
  jobSponsorInfo: FormGroup;
  contactInfo: FormGroup;
  address: FormGroup;
  loginCredentials: FormGroup;

  // Dev WOrking
  UserTypes: any;
  ddlNationalities: Observable<any>;
  emirates: Observable<any>;
  mainArea: Observable<any>;
  typeGenderCode: Observable<any>;
  CommonEnum = CommonEnum;


  loading: boolean = false;
  minDate = new Date();
  @ViewChild('captchaElem5', { static: false }) captchaElem5: ReCaptcha2Component;
  siteKey: string;
  @ViewChild('stepperCompany', { static: true }) stepperCompany: MatStepper;

  getAttachmentsType: any;
  isLoading = [];
  isTick = [];
  isDisabled: boolean = false;
  private ngVersion: string = VERSION.full;
  constructor(
    public router: Router,
    private fb: FormBuilder,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    private env: EnvService,
    private _formBuilder: FormBuilder,
    public commonService: CommonService,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    public translationService: TranslationService,
    public userService: UserService,
    private _DomSanitizationService: DomSanitizer,
    public toast: ToastrService
  ) {
    this.minDate.setDate(this.minDate.getDate() + 1);

    this.siteKey = this.env.captchaKey;
  }
  ngAfterViewInit() {
    // this.stepperCompany.selectedIndex = 1;
  }

  generateForm() {

  }

  ngOnInit() {
    this.Step1BasicInfo();
    this.getAttachments();
    this.Step3SponsorInfo();
    this.Step4ContactInfo();
    this.Step5Address();
    this.Step6LoginCredential();

    // fill dropdowns from APIs
    this.getNationalities();
    this.getEmirates();
    this.getMainArea();
    this.getGenderCode();
  }

  Step1BasicInfo() {
    this.basicInfoCompany = this.fb.group({
      licenceNumber: ['', Validators.compose([Validators.required])],
      licenceExpiryDate: ['', Validators.compose([Validators.required])],
      IssuanceEmirates: ['', Validators.compose([Validators.required])],
      BNnameAr: ['', Validators.compose([Validators.required, Validators.maxLength(200), nameArValidator])],
      BNnameEn: ['', Validators.compose([Validators.required, Validators.maxLength(200)])],
      citizenID: ['', Validators.compose([Validators.required]), this.asyncFormValidatorService.citizenIDValidator(0)],
      EmiratesExpiryDate: ['', Validators.compose([Validators.required])],
      nameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      fatherNameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
      grandFatherNameAr: ['', Validators.compose([Validators.maxLength(100), nameArValidator])],
      familyNameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
      fullNameEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      email: ['', Validators.compose([Validators.required, emailValidator]), this.asyncFormValidatorService.emailValidator(0)],
      mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator]), this.asyncFormValidatorService.mobileNoValidator(0)],
      nationality: [CommonEnum.nationality, Validators.compose([Validators.required])],
      passportNo: ['', Validators.compose([Validators.required])],
      passportExpiryDate: ['', Validators.compose([Validators.required])],
      gender: [GenderEnum.male, Validators.required],

    })
  }
  Step2AttachmentsForm() {
    debugger
    this.attachmentsInfo = this._formBuilder.group({
      attachments: this.fb.array([]),
    });

    let control = <FormArray>this.attachmentsInfo.controls.attachments;
    this.getAttachmentsType.forEach(x => {
      control.push(this.fb.group({
        id: [0],
        userId: [0],
        attachmentName: ['', x.isMandatory ? Validators.required : ''],
        attachmentURL: [''],
        attachmentSize: ['', x.isMandatory ? Validators.compose([Validators.required, fileSize]) : Validators.compose([fileSize])],
        attachmentExtension: [''],
        typeCode: [x.valueEn],
        expiryDate: [new Date()],
        isMandatory: [x.isMandatory],
        inputData: ['', x.inputData ? Validators.required : ''],
        attachmentType: [x.id],
      }));
    });
    this.isTick = [];
    this.getAttachmentsType.forEach(att => {
      this.isLoading.push({ loading: false });
      this.isTick.push({ tick: false });
    });
  }
  Step3SponsorInfo() {
    this.jobSponsorInfo = this._formBuilder.group({
      designationTextBox: ['', Validators.compose([Validators.maxLength(100)])],
      occupation: ['', Validators.compose([Validators.maxLength(100)])],
      sponsorname: ['', Validators.compose([Validators.maxLength(100)])],
      sponsorType: [''],
      title: ['', Validators.compose([Validators.maxLength(100)])],
    });
  }

  Step4ContactInfo() {
    this.contactInfo = this._formBuilder.group({
      PhoneResidence: [''],
      PhoneWork: [''],
      SecondaryPhone: [''],
      FaxNo: ['']
    });
  }

  Step5Address() {
    this.address = this._formBuilder.group({
      emirates: [''],
      mainArea: [''],
      subArea: ['', Validators.compose([Validators.maxLength(100)])],
      street: ['', Validators.compose([Validators.maxLength(100)])],
      building: ['', Validators.compose([Validators.maxLength(100)])],
      poBox: ['', Validators.compose([Validators.maxLength(100)])],
      remarks: ['']
    });
  }

  Step6LoginCredential() {
    this.loginCredentials = this._formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameValidator]), this.asyncFormValidatorService.usernameValidator(0)],
      password: ['', Validators.compose([Validators.required, Validators.maxLength(30), passwordValidator])],
      confirmPassword: ['', Validators.required],
      preferedLanguage: ['ar', Validators.required],
      // recaptcha: ['', Validators.required],
      recaptcha: [''],
    }, {
      validator: matchingPasswords('password', 'confirmPassword')
    });
  }
  getFile(event, index) {
    const controlArray = <FormArray>this.attachmentsInfo.get('attachments');
    if (event.target.files[0].size > 2097152) {
      controlArray.controls[index].get('attachmentSize').markAsTouched();
      controlArray.controls[index].get('attachmentSize').markAsDirty();
      controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
    }
    else {
      if (event.target.files[0]) {
        this.isLoading[index].loading = true;
        this.isTick[index].tick = true;
        this.isDisabled = true;
        this.commonService.fileUpload(event.target.files[0]).then(resp => {
          var response = resp;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            controlArray.controls[index].get('attachmentExtension').setValue(event.target.files[0].type);
            controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
            controlArray.controls[index].get('attachmentName').setValue(event.target.files[0].name);

            this.isLoading[index].loading = false;
            this.isDisabled = false;

            this.cdr.markForCheck();
            controlArray.controls[index].get('attachmentURL').setValue(response.attachmentURL);
            controlArray.controls[index].get('attachmentSize').markAsTouched();
            controlArray.controls[index].get('attachmentName').markAsTouched();
          }
          else {
            this.isLoading[index].loading = false;
            this.isDisabled = false;
            this.cdr.markForCheck();
            this.translationService.getTranslation(String(response.result.code)).subscribe((text: string) => {
              this.toast.error(text);
            });
          }
        });
      }
    }
  }
  getAttachments() {
    this.commonService.GetAttachmentByUserTypeCode(UserTypeEnum.Document_Clearance_company).subscribe(res => {
      let response = res;
      if ((response.result && response.result.code == 200) || (response.code == 200)) {
        debugger
        this.getAttachmentsType = response.result.data;
        this.Step2AttachmentsForm();
      }
    });
  }
  // Gets
  getNationalities() {
    this.ddlNationalities = this.commonService.getLookUp("?type=Nationality");
  }
  getEmirates() {
    this.emirates = this.commonService.getLookUp("?type=Emirates");
  }
  getMainArea() {
    this.mainArea = this.commonService.getLookUp("?type=MainAreas");
  }
  getGenderCode() {
    this.typeGenderCode = this.commonService.getLookUp("?type=gender");
  }

  Step1ClickIndividualfill() {
    let today = new Date();
    this.basicInfoCompany.patchValue({

      licenceNumber: "12345678",
      licenceExpiryDate: new Date(today.setDate(today.getDate() + 1)),
      IssuanceEmirates: 25,
      BNnameAr: "business name ar",
      BNnameEn: "business name en",
      citizenID: "111-1111-1111111-2",
      EmiratesExpiryDate: new Date(today.setDate(today.getDate() + 1)),
      nameAr: "ovais hassan",
      fatherNameAr: "muhammad ovais",
      grandFatherNameAr: "muhammad",
      fullNameEn: "muhammad ovais hassan",
      familyNameAr: "family name ar",
      email: "ovaissxs@neusol.com",
      mobileNo: "00971559954787",
      nationality: "1",
      passportNo: "1234",
      passportExpiryDate: new Date(today.setDate(today.getDate() + 1)),
    })
  }


  // Steps Next Button Click
  Step1ClickNext() {

    if (this.basicInfoCompany.valid) {
      this.stepperCompany.selectedIndex = 1;
    }
    else {
      this.validationMessages.validateAllFormFields(this.basicInfoCompany);
    }
  }

  Step2ClickNext() {
    const controlArray = <FormArray>this.attachmentsInfo.get('attachments');
    for (let index = 0; index < controlArray.length; index++) {
      debugger
      if (!this.getAttachmentsType[index].isMandatory) {
        controlArray.controls[index].get('attachmentSize').reset();
      }
    }
    if (this.attachmentsInfo.valid) {
      this.stepperCompany.selectedIndex = 2;
    }
    else {
      this.validationMessages.validateAllFormFields(this.attachmentsInfo);
    }
  }

  Step3ClickNext() {
    if (this.jobSponsorInfo.valid) {
      this.stepperCompany.selectedIndex = 3;
    }
    else {
      this.validationMessages.validateAllFormFields(this.jobSponsorInfo);
    }
  }

  Step4ClickNext() {
    if (this.contactInfo.valid) {
      this.stepperCompany.selectedIndex = 4;
    }
    else {
      this.validationMessages.validateAllFormFields(this.contactInfo);
    }
  }

  Step5ClickNext() {
    if (this.address.valid) {
      this.stepperCompany.selectedIndex = 5;
    }
    else {
      this.validationMessages.validateAllFormFields(this.address);
    }
  }

  Step6ClickNext() {
    if (this.loginCredentials.valid) {

      let documentClearenceJson = {
        "userId": 0,  // not present in form
        "masterId": 0, // not present in form
        "fullNameAr": this.basicInfoCompany.value.nameAr + this.basicInfoCompany.value.fatherNameAr + this.basicInfoCompany.value.grandFatherNameAr + this.basicInfoCompany.value.familyNameAr,
        "fullNameEn": this.basicInfoCompany.value.fullNameEn,
        "userName": this.loginCredentials.value.username,
        "userEmail": this.basicInfoCompany.value.email,
        "password": this.loginCredentials.value.password,
        "type": this.CommonEnum.userTypeLoginVisitorPage,
        "userType": UserTypeEnum.Document_Clearance_company,  // hardcoded for document clearence user
        "mobileNo": this.basicInfoCompany.value.mobileNo,
        "securityToken": "",
        "genderCode": this.basicInfoCompany.value.gender,
        "isConfirmed": false,
        "isActive": true,
        "preferedLanguage": this.loginCredentials.value.preferedLanguage,
        "createdBy": 0,
        "createdAt": "2019-11-18T06:28:20.535Z",
        "modifiedBy": 0,
        "modifiedAt": null,
        "stepCode": 0,
        "loginCounter": 0,
        "lastLogin": "2019-11-18T06:28:20.535Z",
        "userAttributes": {
          "userId": 0,
          "role": "",
          "department": "",
          "nationalityCode": +this.basicInfoCompany.value.nationality,
          "designationCode": null,
          "designation": this.jobSponsorInfo.value.designationTextBox,
          "hrcode": null,
          "businessUnitCode": null,
          "remarks": this.address.value.remarks,
          "citizenId": this.basicInfoCompany.value.citizenID,
          "citizenCode": 24,
          "passportNo": String(this.basicInfoCompany.value.passportNo),
          "passportExpiryAt": this.basicInfoCompany.value.passportExpiryDate,
          "licenseNumber": this.basicInfoCompany.value.licenceNumber,
          "licenseExpiryAt": this.basicInfoCompany.value.licenceExpiryDate,
          "issuanceEmirates": this.address.value.emirates ? this.address.value.emirates : 0,
          "bnnameEn": this.basicInfoCompany.value.BNnameEn,
          "bnnameAr": this.basicInfoCompany.value.BNnameAr,
          "nameAr": this.basicInfoCompany.value.nameAr,
          "fatherNameAr": this.basicInfoCompany.value.fatherNameAr,
          "grandFatherNameAr": this.basicInfoCompany.value.grandFatherNameAr,
          "familyNameAr": this.basicInfoCompany.value.familyNameAr,
          "govtEntityCode": 0,
          "govtEntityNameEn": "",
          "govtEntityNameAr": "",
          "landlineNo": null,
          "emiratesIdexpiry": this.basicInfoCompany.value.emiratesIDExpiry,
          "ocuupation": this.jobSponsorInfo.value.occupation,
          "sponsorName": this.jobSponsorInfo.value.sponsorname,
          "sponsorType": this.jobSponsorInfo.value.sponsorType,
          "title": this.jobSponsorInfo.value.title,
          "phoneResidence": this.contactInfo.value.PhoneResidence,
          "phoneWork": this.contactInfo.value.PhoneWork,
          "secMobileNo": this.contactInfo.value.SecondaryPhone,
          "faxNo": this.contactInfo.value.FaxNo,
          "mainArea": this.address.value.mainArea ? +this.address.value.mainArea : null,
          "subArea": this.address.value.subArea,
          "street": this.address.value.street,
          "building": this.address.value.building,
          "pobox": this.address.value.poBox,
          "emirates": this.address.value.emirates ? +this.address.value.emirates : null
        },
        "attachmentsLog": this.attachmentsInfo.value.attachments,
        "physicalDevices": [],
        "roleUser": [{
          "id": 0,
          "roleId": CommonEnum.DocumentCleranceCompany,
          "userId": 0,
          "isActive": true,
          "createdBy": 0,
          "createdAt": new Date(),
          "modifiedBy": 0,
          "modifiedAt": new Date(),
        }]
      }
      debugger
      this.userService.SignUpUsers(documentClearenceJson)
        .subscribe(data => {
          this.loading = false;
          let response = data;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            this.router.navigate(["/notifications/activation-successful/" + response.refNo])
          }
        })
    }
    else {
      this.validationMessages.validateAllFormFields(this.loginCredentials);
    }
  }
  /* Emirates ID Mask */
  emiratesIDMask = emiratesIDMasking();

  /* Mobile No Mask */
  mobileNoMask = phoneNumberMasking();

}

