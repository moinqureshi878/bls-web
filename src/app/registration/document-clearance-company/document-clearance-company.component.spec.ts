import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentClearanceCompanyComponent } from './document-clearance-company.component';

describe('DocumentClearanceCompanyComponent', () => {
  let component: DocumentClearanceCompanyComponent;
  let fixture: ComponentFixture<DocumentClearanceCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentClearanceCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentClearanceCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
