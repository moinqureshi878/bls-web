// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';


import { OverlayModule } from '@angular/cdk/overlay';

import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonTemplateModule } from '../common-template/common-template.module';
import { PartialsModule } from '../views/partials/partials.module';
import { HtmlClassService } from './html-class.service';

import { RegistrationComponent } from './registration.component';
import { RegisterComponent } from './register/register.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { RegistrationResolver } from '../core/resolvers/regsitration.resolver';
import { UserService } from '../core/appServices/index.service';
import { DocumentClearanceCompanyComponent } from './document-clearance-company/document-clearance-company.component';
const routes: Routes = [{
  path: '',
  component: RegistrationComponent,
  children: [
    {
      path: 'registration',
      component: RegisterComponent,
    },
    {
      path: 'register',
      resolve: { data: RegistrationResolver },
      component: RegisterComponent,
    },

  ],
}];

@NgModule({
  declarations: [RegistrationComponent, RegisterComponent, DocumentClearanceCompanyComponent],
  exports: [RouterModule],
  providers: [HtmlClassService, RegistrationResolver, UserService],
  imports: [
    CommonModule,
    CommonTemplateModule,
    PartialsModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    TranslateModule.forChild(),
    NgbModule.forRoot(),

    OverlayModule,

    NgxCaptchaModule,

  ]
})

export class RegistrationModule { }