import { Component, OnInit, ViewChild, VERSION, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { matchingPasswords, emailValidator, phoneNumberValidator, fileSize, phoneNumberMasking, emiratesIDMasking, nameValidator, passwordValidator, nameArValidator } from '../../core/validator/form-validators.service';
import { AsyncFormValidatorService } from '../../core/validator/async-form-validator.service';
import { ReCaptcha2Component, ReCaptchaV3Service } from 'ngx-captcha';
import { ValidationMessagesService } from '../../core/validator/validation-messages.service';
import { EnvService } from '../../core/environment/env.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatStepper } from '@angular/material';
import { CommonService, UserService } from '../../core/appServices/index.service';
import { Observable } from 'rxjs';
import { TranslationService } from '@translateService/translation.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { GenderEnum, UserTypeEnum, CommonEnum } from '../../core/_enum/index.enum';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'kt-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  closeResult: string; //Bootstrap Modal Popup

  //Stepper Individual
  basicInfoFormIndividual: FormGroup;
  attachmentsInfo: FormGroup;
  jobSponsorInfo: FormGroup;
  contactInfo: FormGroup;
  address: FormGroup;
  loginCredentials: FormGroup;

  // Dev WOrking
  UserTypes: any;
  ddlNationalities: Observable<any>;
  typeGenderCode: Observable<any>;

  emirates: Observable<any>;
  mainArea: Observable<any>;
  loading: boolean = false;

  minDate = new Date();
  @ViewChild('captchaElem', { static: false }) captchaElem: ReCaptcha2Component;
  siteKey: string;
  @ViewChild('stepper', { static: true }) stepper: MatStepper;
  private ngVersion: string = VERSION.full;
  getAttachmentsType: any;
  isLoading = [];
  isTick = [];
  isDisabled: boolean = false;
  setUserTypesValue: number;
  userTypeChangeValue: number;
  userTypeChangeID: number;
  userTypeEnum = UserTypeEnum;
  CommonEnum = CommonEnum;

  constructor(
    private modalService: NgbModal,
    public router: Router,
    private fb: FormBuilder,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    private env: EnvService,
    private _formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private reCaptchaV3Service: ReCaptchaV3Service,
    public commonService: CommonService,
    public translationService: TranslationService,
    private _DomSanitizationService: DomSanitizer,
    public userService: UserService,
    public toast: ToastrService,
    private route: ActivatedRoute) {
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.siteKey = this.env.captchaKey;

    // get data from resolver to fill radio buttons
    this.UserTypes = this.route.snapshot.data.data;
    debugger
    this.setUserTypesValue = this.UserTypes[0].id;
    this.userTypeChangeID = this.UserTypes[0].id;
  }

  ngOnInit() {
    this.Step1BasicInfoFormIndividual();
    this.getAttachments(this.UserTypes[0].id);
    this.Step3SponsorInfo();
    this.Step4ContactInfo();
    this.Step5Address();
    this.Step6LoginCredential();

    // fill dropdowns from APIs
    this.getNationalities();
    this.getEmirates();
    this.getMainArea();
    this.getGenderCode();
  }

  // ---------------------Forms Controls----------------------------------------
  Step1BasicInfoFormIndividual() {
    this.basicInfoFormIndividual = this.fb.group({
      citizenID: ['', Validators.compose([Validators.required]), this.asyncFormValidatorService.citizenIDValidator(0)],
      emiratesIDExpiry: ['', Validators.compose([Validators.required])],
      UID: [null],
      UIDExpiryDate: [null],
      nameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
      fatherNameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
      grandFatherNameAr: ['', Validators.compose([Validators.maxLength(100), nameArValidator])],
      familyNameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
      fullNameEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      email: ['', Validators.compose([Validators.required, emailValidator]), this.asyncFormValidatorService.emailValidator(0)],
      mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator]), this.asyncFormValidatorService.mobileNoValidator(0)],
      nationality: [CommonEnum.nationality, Validators.compose([Validators.required])],
      passportNo: ['', Validators.compose([Validators.required])],
      passportExpiryDate: ['', Validators.compose([Validators.required])],
      gender: [GenderEnum.male, Validators.required],
    });
    this.addOrUpdateValidator();
  }

  Step2AttachmentsForm() {
    this.attachmentsInfo = this._formBuilder.group({
      attachments: this.fb.array([]),
    });

    let control = <FormArray>this.attachmentsInfo.controls.attachments;
    this.getAttachmentsType.forEach(x => {
      control.push(this.fb.group({
        id: [0],
        userId: [0],
        attachmentName: ['', x.isMandatory ? Validators.required : ''],
        attachmentURL: [''],
        attachmentSize: ['', x.isMandatory ? Validators.compose([Validators.required, fileSize]) : Validators.compose([fileSize])],
        attachmentExtension: [''],
        typeCode: [x.valueEn],
        expiryDate: [new Date()],
        isMandatory: [x.isMandatory],
        inputData: ['', x.inputData ? Validators.required : ''],
        attachmentType: [x.id],
      }));
    });
    this.isTick = [];
    this.getAttachmentsType.forEach(att => {
      this.isLoading.push({ loading: false });
      this.isTick.push({ tick: false });
    });
  }

  Step3SponsorInfo() {
    this.jobSponsorInfo = this._formBuilder.group({
      designationTextBox: ['', Validators.compose([Validators.maxLength(100)])],
      occupation: ['', Validators.compose([Validators.maxLength(100)])],
      sponsorname: ['', Validators.compose([Validators.maxLength(100)])],
      sponsorType: [''],
      title: ['', Validators.compose([Validators.maxLength(100)])],
    });
  }

  Step4ContactInfo() {
    this.contactInfo = this._formBuilder.group({
      PhoneResidence: [''],
      PhoneWork: [''],
      SecondaryPhone: [''],
      FaxNo: ['']
    });
  }

  Step5Address() {
    this.address = this._formBuilder.group({
      emirates: [''],
      mainArea: [''],
      subArea: ['', Validators.compose([Validators.maxLength(100)])],
      street: ['', Validators.compose([Validators.maxLength(100)])],
      building: ['', Validators.compose([Validators.maxLength(100)])],
      poBox: ['', Validators.compose([Validators.maxLength(100)])],
      remarks: ['']
    });
  }

  Step6LoginCredential() {
    this.loginCredentials = this._formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameValidator]), this.asyncFormValidatorService.usernameValidator(0)],
      password: ['', Validators.compose([Validators.required, Validators.maxLength(30), passwordValidator])],
      confirmPassword: ['', Validators.required],
      preferedLanguage: ['ar', Validators.required],
      // recaptcha: ['', Validators.required],
      recaptcha: [''],
    }, {
      validator: matchingPasswords('password', 'confirmPassword')
    });
  }
  // ---------------------Forms Controls End ----------------------------------------

  // Steps Next Button Click
  Step1ClickNext() {
    if (this.basicInfoFormIndividual.valid) {
      this.stepper.selectedIndex = 1;
    }
    else {
      this.validationMessages.validateAllFormFields(this.basicInfoFormIndividual);
    }
  }
  Step2ClickNext() {
    const controlArray = <FormArray>this.attachmentsInfo.get('attachments');
    for (let index = 0; index < controlArray.length; index++) {
      debugger
      if (!this.getAttachmentsType[index].isMandatory) {
        controlArray.controls[index].get('attachmentSize').reset();
      }
    }

    if (this.attachmentsInfo.valid) {
      this.stepper.selectedIndex = 2;
    }
    else {
      this.validationMessages.validateAllFormFields(this.attachmentsInfo);
    }
  }
  Step3ClickNext() {
    if (this.jobSponsorInfo.valid) {
      this.stepper.selectedIndex = 3;
    }
    else {
      this.validationMessages.validateAllFormFields(this.jobSponsorInfo);
    }
  }
  Step4ClickNext() {
    if (this.contactInfo.valid) {
      this.stepper.selectedIndex = 4;
    }
    else {
      this.validationMessages.validateAllFormFields(this.contactInfo);
    }
  }
  Step5ClickNext() {
    if (this.address.valid) {
      this.stepper.selectedIndex = 5;
    }
    else {
      this.validationMessages.validateAllFormFields(this.address);
    }
  }
  Step6ClickNext() {
    if (this.loginCredentials.valid) {
      debugger
      let documentClearenceJson = {
        "userId": 0,  // not present in form
        "masterId": 0, // not present in form
        "fullNameAr": this.basicInfoFormIndividual.value.nameAr + this.basicInfoFormIndividual.value.fatherNameAr + this.basicInfoFormIndividual.value.grandFatherNameAr + this.basicInfoFormIndividual.value.familyNameAr,
        "fullNameEn": this.basicInfoFormIndividual.value.fullNameEn,
        "userName": this.loginCredentials.value.username,
        "userEmail": this.basicInfoFormIndividual.value.email,
        "password": this.loginCredentials.value.password,
        "type": this.CommonEnum.userTypeLoginVisitorPage,
        "userType": this.userTypeChangeID,  // radio button id not code on document clearence use 24
        "mobileNo": this.basicInfoFormIndividual.value.mobileNo,
        "securityToken": "",
        "genderCode": this.basicInfoFormIndividual.value.gender,
        "isConfirmed": false,
        "isActive": true,
        "preferedLanguage": this.loginCredentials.value.preferedLanguage,
        "createdBy": 0,
        "createdAt": "2019-11-18T06:28:20.535Z",
        "modifiedBy": 0,
        "modifiedAt": null,
        "stepCode": 0,
        "loginCounter": 0,
        "lastLogin": "2019-11-18T06:28:20.535Z",
        "userAttributes": {
          "userId": 0,
          "role": "",
          "department": "",
          "nationalityCode": +this.basicInfoFormIndividual.value.nationality,
          "designationCode": null,
          "designation": this.jobSponsorInfo.value.designationTextBox,
          "hrcode": null,
          "businessUnitCode": null,
          "remarks": this.address.value.remarks,
          "citizenId": this.basicInfoFormIndividual.value.citizenID ? this.basicInfoFormIndividual.value.citizenID : this.basicInfoFormIndividual.value.UID,
          "citizenCode": this.userTypeChangeID,  // radio button id not code on document clearence use 24
          "passportNo": String(this.basicInfoFormIndividual.value.passportNo),
          "passportExpiryAt": this.basicInfoFormIndividual.value.passportExpiryDate,
          "licenseNumber": this.basicInfoFormIndividual.value.licenceNumber,
          "licenseExpiryAt": this.basicInfoFormIndividual.value.licenceExpiryDate,
          "issuanceEmirates": this.address.value.emirates ? this.address.value.emirates : 0,
          "bnnameEn": this.basicInfoFormIndividual.value.BNnameEn,
          "bnnameAr": this.basicInfoFormIndividual.value.BNnameAr,
          "nameAr": this.basicInfoFormIndividual.value.nameAr,
          "fatherNameAr": this.basicInfoFormIndividual.value.fatherNameAr,
          "grandFatherNameAr": this.basicInfoFormIndividual.value.grandFatherNameAr,
          "familyNameAr": this.basicInfoFormIndividual.value.familyNameAr,
          "govtEntityCode": 0,
          "govtEntityNameEn": "",
          "govtEntityNameAr": "",
          "landlineNo": null,
          "emiratesIdexpiry": this.basicInfoFormIndividual.value.emiratesIDExpiry ? this.basicInfoFormIndividual.value.emiratesIDExpiry : this.basicInfoFormIndividual.value.UIDExpiryDate,
          "ocuupation": this.jobSponsorInfo.value.occupation,
          "sponsorName": this.jobSponsorInfo.value.sponsorname,
          "sponsorType": this.jobSponsorInfo.value.sponsorType,
          "title": this.jobSponsorInfo.value.title,
          "phoneResidence": this.contactInfo.value.PhoneResidence,
          "phoneWork": this.contactInfo.value.PhoneWork,
          "secMobileNo": this.contactInfo.value.SecondaryPhone,
          "faxNo": this.contactInfo.value.FaxNo,
          "mainArea": this.address.value.mainArea ? +this.address.value.mainArea : null,
          "subArea": this.address.value.subArea,
          "street": this.address.value.street,
          "building": this.address.value.building,
          "pobox": this.address.value.poBox,
        },
        "attachmentsLog": this.attachmentsInfo.value.attachments,
        "physicalDevices": [],
        "roleUser": [{
          "id": 0,
          "roleId": CommonEnum.Individual,
          "userId": 0,
          "isActive": true,
          "createdBy": 0,
          "createdAt": new Date(),
          "modifiedBy": 0,
          "modifiedAt": new Date(),
        }]
      }
      debugger
      this.userService.SignUpUsers(documentClearenceJson)
        .subscribe(data => {
          this.loading = false;
          let response = data;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            this.router.navigate(["/notifications/activation-successful/" + + response.refNo])
          }
        })
    }
    else {
      this.validationMessages.validateAllFormFields(this.loginCredentials);
    }
  }
  getFile(event, index) {
    const controlArray = <FormArray>this.attachmentsInfo.get('attachments');
    if (event.target.files[0].size > 20913252) {
      controlArray.controls[index].get('attachmentSize').markAsTouched();
      controlArray.controls[index].get('attachmentSize').markAsDirty();
      controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
    }
    else {
      if (event.target.files[0]) {
        this.isLoading[index].loading = true;
        this.isTick[index].tick = true;
        this.isDisabled = true;
        this.commonService.fileUpload(event.target.files[0]).then(resp => {
          var response = resp;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            controlArray.controls[index].get('attachmentExtension').setValue(event.target.files[0].type);
            controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
            controlArray.controls[index].get('attachmentName').setValue(event.target.files[0].name);

            this.isLoading[index].loading = false;
            this.isDisabled = false;

            this.cdr.markForCheck();
            controlArray.controls[index].get('attachmentURL').setValue(response.attachmentURL);
            controlArray.controls[index].get('attachmentSize').markAsTouched();
            controlArray.controls[index].get('attachmentName').markAsTouched();
          }
          else {
            this.isLoading[index].loading = false;
            this.isDisabled = false;
            this.cdr.markForCheck();
            this.translationService.getTranslation(String(response.result.code)).subscribe((text: string) => {
              this.toast.error(text);
            });
          }
        });
      }
    }
  }
  getAttachments(code) {
    this.commonService.GetAttachmentByUserTypeCode(code).subscribe(res => {
      let response = res;
      if ((response.result && response.result.code == 200) || (response.code == 200)) {
        this.getAttachmentsType = response.result.data;
        this.Step2AttachmentsForm();
      }
    });
  }
  userTypeChange(content, event, value) {
    //  this.userTypeChangeValue= event.value;
    if (this.stepper.selectedIndex == 0) {
      this.userTypeChangeValue = value.id;
      this.userTypeChangeID = value.id;
      debugger
      this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;

      });
      return false;
    }
  }

  // Get Nationality
  getNationalities() {
    this.ddlNationalities = this.commonService.getLookUp("?type=Nationality");
  }
  getEmirates() {
    this.emirates = this.commonService.getLookUp("?type=Emirates");
  }
  getMainArea() {
    this.mainArea = this.commonService.getLookUp("?type=MainAreas");
  }
  getGenderCode() {
    this.typeGenderCode = this.commonService.getLookUp("?type=gender");
  }
  /* Emirates ID Mask */
  emiratesIDMask = emiratesIDMasking();

  /* Mobile No Mask */
  mobileNoMask = phoneNumberMasking();

  //..Modal Popup Start
  private getDismissReason(reason: any): string {
    this.setUserTypesValue = this.setUserTypesValue;
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  @ViewChild('basicInfoForm', { static: false }) basicInfoForm;
  alertClearFormClick() {
    debugger
    this.setUserTypesValue = this.userTypeChangeValue;
    this.getAttachments(this.setUserTypesValue);
    this.modalService.dismissAll();

    this.stepper.reset();
    this.loginCredentials.get('preferedLanguage').setValue('ar');
    // this.basicInfoForm.resetForm();
    setTimeout(() => {
      Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
        item.classList.remove("mat-form-field-invalid");
      });
    }, 100);
    this.addOrUpdateValidator();
  }
  private addOrUpdateValidator() {
    if (this.setUserTypesValue == this.userTypeEnum.UAE_Citizen || this.setUserTypesValue == this.userTypeEnum.Resident_in_UAE) {
      this.basicInfoFormIndividual.controls["citizenID"].setValidators(Validators.required);
      this.basicInfoFormIndividual.controls["citizenID"].setAsyncValidators(this.asyncFormValidatorService.citizenIDValidator(0));
      this.basicInfoFormIndividual.controls["emiratesIDExpiry"].setValidators(Validators.required);
      this.basicInfoFormIndividual.controls["citizenID"].updateValueAndValidity();
      this.basicInfoFormIndividual.controls["emiratesIDExpiry"].updateValueAndValidity();
      this.basicInfoFormIndividual.controls["UID"].clearValidators();
      this.basicInfoFormIndividual.controls["UIDExpiryDate"].clearValidators();
      this.basicInfoFormIndividual.get('UID').updateValueAndValidity();
      this.basicInfoFormIndividual.get('UIDExpiryDate').updateValueAndValidity();
      this.basicInfoFormIndividual.controls['nationality'].setValue(CommonEnum.nationality);
      this.basicInfoFormIndividual.controls['gender'].setValue(GenderEnum.male);
    }
    else if (this.setUserTypesValue == this.userTypeEnum.GCC_Citizen || this.setUserTypesValue == this.userTypeEnum.Visitor_with_UID) {
      this.basicInfoFormIndividual.controls["UID"].setValidators(Validators.required);
      this.basicInfoFormIndividual.controls["UID"].setAsyncValidators(this.asyncFormValidatorService.citizenIDValidator(0));
      this.basicInfoFormIndividual.controls["UIDExpiryDate"].setValidators(Validators.required);
      this.basicInfoFormIndividual.controls["UID"].updateValueAndValidity();
      this.basicInfoFormIndividual.controls["UIDExpiryDate"].updateValueAndValidity();
      this.basicInfoFormIndividual.controls["citizenID"].clearValidators();
      this.basicInfoFormIndividual.controls["emiratesIDExpiry"].clearValidators();
      this.basicInfoFormIndividual.get('citizenID').updateValueAndValidity();
      this.basicInfoFormIndividual.get('emiratesIDExpiry').updateValueAndValidity();
      this.basicInfoFormIndividual.controls['nationality'].setValue(CommonEnum.nationality);
      this.basicInfoFormIndividual.controls['gender'].setValue(GenderEnum.male);
    }
    else {
      this.basicInfoFormIndividual.controls["UID"].setValidators(Validators.required);
      this.basicInfoFormIndividual.controls["UID"].setAsyncValidators(this.asyncFormValidatorService.citizenIDValidator(0));
      this.basicInfoFormIndividual.controls["UIDExpiryDate"].setValidators(Validators.required);
      this.basicInfoFormIndividual.controls["UID"].updateValueAndValidity();
      this.basicInfoFormIndividual.controls["UIDExpiryDate"].updateValueAndValidity();
      this.basicInfoFormIndividual.controls["citizenID"].clearValidators();
      this.basicInfoFormIndividual.controls["emiratesIDExpiry"].clearValidators();
      this.basicInfoFormIndividual.get('citizenID').updateValueAndValidity();
      this.basicInfoFormIndividual.get('emiratesIDExpiry').updateValueAndValidity();
      this.basicInfoFormIndividual.controls['nationality'].setValue(CommonEnum.nationality);
      this.basicInfoFormIndividual.controls['gender'].setValue(GenderEnum.male);
    }
  }

  //..Modal Popup End 
  Step1ClickIndividualfill() {
    let today = new Date();
    this.basicInfoFormIndividual.patchValue({
      nameAr: "ovais hassann",
      fatherNameAr: "muhammad ovaiss",
      grandFatherNameAr: "muhammadd",
      fullNameEn: "muhammad ovais hassann",
      familyNameAr: "family name arr",
      email: "zain@neusol.com",
      mobileNo: "009132559954787",
      nationality: "1",
      passportNo: "1234",
      passportExpiryDate: new Date(today.setDate(today.getDate() + 1)),
    })
  }
}


