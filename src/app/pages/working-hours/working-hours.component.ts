import { Component, OnInit,ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { WorkingHoursService } from '../../core/appServices/index.service';
import { ActivatedRoute, Router } from '@angular/router';
import { workinghoursmodel } from '../../core/appModels/workinghours.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TranslationService } from '../../core/_base/layout';
import { WorkingHoursModule } from './working-hours.module';

@Component({
  selector: 'kt-working-hours',
  templateUrl: './working-hours.component.html',
  styleUrls: ['./working-hours.component.scss']
})
export class WorkingHoursComponent implements OnInit {
  ELEMENT_DATA: workinghoursmodel[] = [];

  displayedColumns7: string[] = ['NameAr', 'NameEn', 'TimeFrom','TimeTo','Year','IsWeekend','editView','isActive'];
  dataSource7 = new MatTableDataSource<workinghoursmodel>(this.ELEMENT_DATA);

  closeResult: string; //Bootstrap Modal Popup

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild('sort7', { static: true }) sort7: MatSort;
  masterType: any;
  constructor( private modalService: NgbModal,
    private workingHoursService: WorkingHoursService,
    private route: ActivatedRoute,
    private router: Router,
    private translationService: TranslationService) { }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
    this.getAll();
  }
  getAll(){
    this.workingHoursService.getAll().toPromise().then(data => {
      debugger;
      this.masterType = data.data;
      this.dataSource7 = new MatTableDataSource<workinghoursmodel>(this.masterType);
      this.filterByModuleName();
    });
    // .(map(resp => resp["data"]))
  }
  filterByModuleName() {
    debugger
     
    //this.dataSource7 = new MatTableDataSource<mastertypesmodel>(filterTypes);
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
    this.applyFilter7();
  }
  applyFilter7() {
   // this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  //..Modal Popup Start
  open(content) {

    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;

    });

  }


  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End
  onEditClick(row) {
    this.router.navigate(['/pages/master-types/add-types'],{ queryParams: { id: row.id } } );
  }
  CheckboxChange(code) {
    this.workingHoursService.DeleteRecord(code)
      .subscribe(data => {
        //console.lo g(data);
      })
  }
  CheckboxChangeWeekend(code) {
   // this.workingHoursService.DeleteRecord(code)
     // .subscribe(data => {
        //console.lo g(data);
     // })
  }
}
