import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBusinessActivityGroupsComponent } from './add-business-activity-groups.component';

describe('AddBusinessActivityGroupsComponent', () => {
  let component: AddBusinessActivityGroupsComponent;
  let fixture: ComponentFixture<AddBusinessActivityGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBusinessActivityGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBusinessActivityGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
