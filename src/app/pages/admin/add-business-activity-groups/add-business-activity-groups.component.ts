import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BnCommonService, CommonService } from '../../../core/appServices/index.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { TranslationService } from '@translateService/translation.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { nameArValidator, alphaNumeric } from '../../../core/validator/form-validators.service';

@Component({
  selector: 'kt-add-business-activity-groups',
  templateUrl: './add-business-activity-groups.component.html',
  styleUrls: ['./add-business-activity-groups.component.scss']
})
export class AddBusinessActivityGroupsComponent implements OnInit {
  businessActivityGroups: FormGroup;
  id: number;
  getBusinessGroup: any;
  loading: boolean = false;
  constructor(public bnCommonService: BnCommonService,
    public commonService: CommonService,
    private fb: FormBuilder,
    public translationService: TranslationService,
    public validationMessages: ValidationMessagesService,
    private router: Router,
    public toast: ToastrService,
    public route: ActivatedRoute,
    private cdr: ChangeDetectorRef) {
    this.getBusinessGroup = this.route.snapshot.data.data;
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      if (this.id) {
        this.InitializeForm();
        this.businessActivityGroups.patchValue(this.getBusinessGroup);
      }
      else {
        this.InitializeForm();
      }
    });
  }

  ngOnInit() {
  }
  InitializeForm() {
    this.businessActivityGroups = this.fb.group({
      "id": [0, Validators.compose([Validators.required])],
      "code": ['', Validators.compose([Validators.required, Validators.maxLength(100), alphaNumeric])],
      "descriptionEn": ['', Validators.compose([Validators.required, Validators.maxLength(2500)])],
      "descriptionAr": ['', Validators.compose([Validators.required, Validators.maxLength(2500), nameArValidator])],
      "isActive": [true],
    });
  }
  businessActivityGroupsFormSubmit() {
    if (this.businessActivityGroups.valid) {
      //Api call
      this.loading = true;
      if (this.id) {
        this.bnCommonService.UpdateBusinessActivity(this.businessActivityGroups.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigate(["/pages/admin/business-activity-groups"]);
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
      else {
        this.bnCommonService.AddBusinessActivity(this.businessActivityGroups.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigate(["/pages/admin/business-activity-groups"]);
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.businessActivityGroups);
    }
  }
}
