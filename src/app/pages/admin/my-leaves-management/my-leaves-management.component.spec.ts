import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyLeavesManagementComponent } from './my-leaves-management.component';

describe('MyLeavesManagementComponent', () => {
  let component: MyLeavesManagementComponent;
  let fixture: ComponentFixture<MyLeavesManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyLeavesManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyLeavesManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
