
import { Component, OnInit, ViewChild, LOCALE_ID } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TdaUsersLinkedRoleComponent } from '../tda-users-linked-role/tda-users-linked-role.component';
import { ActivatedRoute } from '@angular/router';
import { TranslationService } from '@translateService/translation.service';
import { CommonService } from '../../../core/appServices/index.service';
import ar from '@angular/common/locales/ar';
import { registerLocaleData } from '@angular/common';
registerLocaleData(ar);

/** Builds and returns a new User. */
const ELEMENT_DATA = [
	{ nameAr: 'A', nameEn: 'H', username: '1', designation: 'AR', businesUnit: '', editView: '', },
];

@Component({
	selector: 'kt-my-leaves-management',
	templateUrl: './my-leaves-management.component.html',
	styleUrls: ['./my-leaves-management.component.scss'],
	providers: [{ provide: LOCALE_ID, useValue: "ar-AE" }]
})
export class MyLeavesManagementComponent implements OnInit {
	displayedColumns7: string[] = ['Type', 'fromDateTime', 'toDateTime', 'Purpose', 'editView'];
	dataSource7 = new MatTableDataSource(ELEMENT_DATA);
	closeResult: string; //Bootstrap Modal Popup
	@ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
	@ViewChild(MatSort, { static: true }) sort7: MatSort;
	getLeaves: any;
	constructor(public dialog: MatDialog,
		public translationService: TranslationService,
		public commonService: CommonService,
		public route: ActivatedRoute) {

		this.getLeaves = this.route.snapshot.data.data;
		this.dataSource7 = new MatTableDataSource(this.getLeaves);
	}

	ngOnInit() {
		this.dataSource7.paginator = this.paginator7;
		this.dataSource7.sort = this.sort7;
	}

	applyFilter7(filterValue: string) {
		this.dataSource7.filter = filterValue.trim().toLowerCase();
		if (this.dataSource7.paginator) {
			this.dataSource7.paginator.firstPage();
		}
	}

	/* begin:: Linked Role Popup */
	openLinkedUserRoleDialog() {
		const dialogConfig = new MatDialogConfig();
		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.minWidth = "60%";
		this.dialog.open(TdaUsersLinkedRoleComponent, dialogConfig);
	}
	/* end:: Linked Role Popup */
}
