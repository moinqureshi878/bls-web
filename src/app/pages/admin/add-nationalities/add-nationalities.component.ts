
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TdaUsersLinkedRoleComponent } from '../tda-users-linked-role/tda-users-linked-role.component';
import { TranslationService } from 'app/core/_base/layout';
import { ConfigService } from 'app/core/appServices/config.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { CommonService } from 'app/core/appServices/index.service';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';

@Component({
  selector: 'kt-add-nationalities',
  templateUrl: './add-nationalities.component.html',
  styleUrls: ['./add-nationalities.component.scss']
})
export class AddNationalitiesComponent implements OnInit {

  ELEMENT_DATA: any[] = [];

  Nationalitiesddl: Observable<any>;
  NationalitiesFormGroup: FormGroup;
  NationalitiesGrid: any;

  loading: boolean = false;

  displayedColumns7: string[] = ['checkBoxes', 'nationalityAr', 'nationalityEn'];
  dataSource7 = new MatTableDataSource(this.ELEMENT_DATA);



  closeResult: string; //Bootstrap Modal Popup

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  constructor(
    public dialog: MatDialog,
    private commonService: CommonService,
    private translationService: TranslationService,
    public configService: ConfigService,
    private fb: FormBuilder,
    public route: ActivatedRoute,
    private toastrService: ToastrService,
    public router: Router,
    public validationMessages: ValidationMessagesService,
  ) {
    debugger
    this.NationalitiesGrid = this.route.snapshot.data.data;
    this.dataSource7 = new MatTableDataSource(this.NationalitiesGrid);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;

    this.createformNationalities();
    this.getNationalitiesTypeddl();
  }

  NationalitiesddlChange() {
    this.configService.getLookupDetailByID(this.NationalitiesFormGroup.value.nationalitiesddl.id, "nationality")
      .subscribe(data => {
        if (data["result"].code === 200) {
          debugger
          this.NationalitiesGrid = data["data"];
          this.dataSource7 = new MatTableDataSource(this.NationalitiesGrid);
          this.dataSource7.paginator = this.paginator7;
          this.dataSource7.sort = this.sort7;
        }
        else {
          this.loading = false;
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toastrService.success(text);
          });
        }
      })
  }

  createformNationalities() {
    this.NationalitiesFormGroup = this.fb.group({
      nationalitiesddl: ['', Validators.compose([Validators.required])]
    });
  }

  getNationalitiesTypeddl() {
    this.Nationalitiesddl = this.commonService.getLookUp("?type=Country");
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();

    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  submitNationalities() {
    if (this.NationalitiesFormGroup.valid) {
      this.loading = true;
      let AppTypeAttachmentSelected: [] = this.NationalitiesGrid.filter(x => x.isExist == true);
      if (AppTypeAttachmentSelected.length > 0) {

        let model = AppTypeAttachmentSelected.map(item => {
          return {
            "data1": this.NationalitiesFormGroup.value.nationalitiesddl.id,
            "data2": item["id"],
            "data3": 0,
            "type1": this.NationalitiesFormGroup.value.nationalitiesddl.headID,
            "type2": item["headID"],
            "type3": 0,
            "isMandatory": false,
            "inputData": false,
            "isActive": true
          }
        })

        this.configService.addAppTypeAttachment(model)
          .subscribe(data => {
            if (data["result"].code === 200) {
              this.loading = false;
              this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
                this.toastrService.success(text);
              });
              this.router.navigateByUrl('/pages/admin/manage-nationalities');
            }
            else {
              this.loading = false;
              this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
                this.toastrService.error(text);
              });
            }
          })
      }
      else {
        this.loading = false;
        this.translationService.getTranslation('VALIDATIONMESSAGES.NATIONALITYREQUIRED').subscribe((text: string) => {
          this.toastrService.error(text);
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.NationalitiesFormGroup);
    }
  }

  checkBoxChange(...value) {
    debugger
    this.NationalitiesGrid[value[0]].isExist = value[1];
    this.dataSource7 = new MatTableDataSource(this.NationalitiesGrid);
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }

}