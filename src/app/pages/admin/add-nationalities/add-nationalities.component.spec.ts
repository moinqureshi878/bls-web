import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNationalitiesComponent } from './add-nationalities.component';

describe('AddNationalitiesComponent', () => {
  let component: AddNationalitiesComponent;
  let fixture: ComponentFixture<AddNationalitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNationalitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNationalitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
