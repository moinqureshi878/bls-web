import { Component, OnInit } from '@angular/core';
import { EnvService } from '../../../core/environment/env.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AsyncFormValidatorService } from '../../../core/validator/async-form-validator.service';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { commentValidator, nameArValidator } from '../../../core/validator/form-validators.service';
import { RoleService, CommonService } from '../../../core/appServices/index.service';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { UserTypeEnum } from '../../../core/_enum/user-type.enum';
import { TranslationService } from '@translateService/translation.service';


@Component({
  selector: 'kt-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.scss']
})

export class AddRoleComponent implements OnInit {
  addRole: FormGroup;

  loading = false;
  typeddl: Observable<any>;

  UserType = UserTypeEnum;


  constructor(
    private env: EnvService,
    private toastrService: ToastrService,
    public router: Router,
    private fb: FormBuilder,
    private translationService: TranslationService,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    private roleService: RoleService,
    private commonService: CommonService
  ) {
  }

  ngOnInit() {
    this.getType();
    this.createRoleForm();

  }

  getType() {
    this.typeddl = this.commonService.getLookUp("?type=RoleType");
  }

  createRoleForm() {
    this.addRole = this.fb.group({
      roleNameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator]), this.asyncFormValidatorService.roleNameArValidator(0)],
      roleNameEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)]), this.asyncFormValidatorService.roleNameEnValidator(0)],
      type: ['', Validators.required],
      remarks: ['', Validators.compose([Validators.maxLength(500), commentValidator])],
      isActive: [true, Validators.required],
    });
  }
  addRoleFormSubmit() {
    if (this.addRole.valid) {
      this.loading = true;
      this.addRole.value["nameEn"] = this.addRole.value.roleNameEn;
      this.addRole.value["nameAr"] = this.addRole.value.roleNameAr;
      delete this.addRole.value.roleNameAr;
      delete this.addRole.value.roleNameEn;

      this.roleService.AddRoles(this.addRole.value)
        .subscribe(data => {
          if (data["result"].code === 200) {
            this.loading = false;
            this.addRole.reset();
            setTimeout(() => {
              Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
                item.classList.remove("mat-form-field-invalid");
              });
            }, 50);
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toastrService.success(text);
            });
            this.router.navigateByUrl('/pages/admin/roles-definition');
          }
          else {
            this.loading = false;
            this.toastrService.error('Something went wrong. Please try again');
          }
        });
    }
    else {
      this.validationMessages.validateAllFormFields(this.addRole);
    }
  }
}
