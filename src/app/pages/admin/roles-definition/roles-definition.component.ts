import { ActivatedRoute, Router } from '@angular/router';

import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { RolesDefinitionLinkedUsersComponent } from '../roles-definition-linked-users/roles-definition-linked-users.component';
import { TranslationService } from '@translateService/translation.service';
import { RoleService } from '../../../core/appServices/role.service';
/* Tree View */
import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { ToastrService } from 'ngx-toastr';
import { CommonEnum } from '../../../core/_enum/index.enum';

// import { TreeviewItem } from '../../../core/_treeView/treeview-item';
// import { TreeviewConfig } from '../../../core/_treeView/treeview-config';
// 
/*Data Table */
export interface RoleData {
  userNameAr: string;
  userNameEn: string;
  remarks: string;
  type: string;
  linkedUser: string;
  accessRights: string;
  isActive: boolean;
}

@Component({
  selector: 'kt-roles-definition',
  templateUrl: './roles-definition.component.html',
  styleUrls: ['./roles-definition.component.scss'],
  // providers: [ChecklistDatabase]
})
export class RolesDefinitionComponent implements OnInit {
  rolePersmissionLoader: boolean = false;
  /* Table 1 */
  displayedColumns7: string[] = ['nameAr', 'nameEn', 'remarks', 'type', 'linkedUser', 'accessRights', 'isActive'];
  dataSource7 = new MatTableDataSource<RoleData>();
  closeResult: string; //Bootstrap Modal Popup
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  // items: TreeviewItem[];
  // values: number[];
  // config = TreeviewConfig.create({
  //   hasAllCheckBox: false,
  //   hasFilter: false,
  //   hasCollapseExpand: false,
  //   decoupleChildFromParent: false,
  //   maxHeight: 400
  // });
  loadingLinkedUser: boolean = false;
  a = [
    {
      itemEn: "Licensin",
      itemAr: 'LicenceAr',
      value: 1,
      children: [
        {
          itemEn: "Business Names",
          value: 1,
          children: [
            { itemEn: "New Business Names", value: 2 },
            { itemEn: "Extended Business Names", value: 3 },
            { itemEn: "Modify Business Names", value: 4 },
            { itemEn: "Cancel Business Names", value: 5, }]
        }]
    },
    {
      itemEn: "Business License",
      value: 15,
      children: [
        {
          itemEn: "Business Names",
          value: 1,
          children: [
            { itemEn: "middle Business Name", value: 6 },
            { itemEn: "Extended Business Names", value: 7 },
            { itemEn: "Modify Business Names", value: 8 },
            { itemEn: "Cancel Business Names", value: 9 }]
        }]
    },
    {
      itemEn: "Tourism Permit",
      value: 10,
      children: [
        {
          itemEn: "Business Names",
          children: [
            { itemEn: "top Business Names", value: 11 },
            { itemEn: "Extended Business Names", value: 12 },
            { itemEn: "Modify Business Names", value: 13 },
            { itemEn: "aaa Business Names", value: 14 }]
        }]
    }];
  b = [
    {
      value: 11,
    },
    {
      value: 12,
    },
    {
      value: 13,
    },
    {
      value: 14,
    },
    {
      value: 7,
    },
    {
      value: 4,
    }
  ];
  postRolePermission = {
    "roleId": 1,
    "pageIds": [

    ]
  }
  roles: any;
  CommonEnum = CommonEnum;
  roleName;
  constructor(
    private route: ActivatedRoute,
    private modalService: NgbModal,
    public roleService: RoleService,
    public translationService: TranslationService,
    public toastrService: ToastrService,
    public dialog: MatDialog,
    public cdr: ChangeDetectorRef,
    private router: Router, ) {
    console.log(this.route.snapshot.data.data);
    this.roles = this.route.snapshot.data.data;
    this.dataSource7 = new MatTableDataSource<RoleData>(this.roles ? this.roles : []);
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel, this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    // this.dataSource.data = this.a as TodoItemNode[];
  }
  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }
  isActiveRole(id) {
    this.changeDataSource(id);

    this.roleService.DeleteRoles(id).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {

      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toastrService.error(text);
        });
      }
    });
  }
  changeDataSource(ID: number) {
    let index = this.roles.findIndex(x => x.roleId == ID);
    let isActive: boolean = this.roles[index].isActive == true ? false : true;
    this.roles[index].isActive = isActive;
  }
  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  openAccessRightTree(content, row) {
    this.roleName = row;
    row.treeLoading = true;
    this.roleService.GetAccessRights(row.roleId).subscribe(res => {
      let response = res;
      if (response.code == 200 || response.result.code == 200) {
        row.treeLoading = false;
        this.cdr.markForCheck();
        this.dataSource.data = response.data as TodoItemNode[];
        this.postRolePermission.roleId = row.roleId;
        this.postRolePermission.pageIds = [];
        console.log(this.treeControl.dataNodes);
        if (response.module) {
          for (let i = 0; i < response.module.length; i++) {
            this.treeControl.dataNodes.forEach((res, index) => {
              if (response.module[i].value == res.value) {
                this.checklistSelection.toggle(this.treeControl.dataNodes[index]);
              }
            });
          }
        }
        for (let i = 0; i < response.values.length; i++) {
          this.treeControl.dataNodes.forEach((res, index) => {
            if (response.values[i].value == res.value) {
              this.checklistSelection.toggle(this.treeControl.dataNodes[index]);
              this.postRolePermission.pageIds.push({ 'value': response.values[i].value });
            }
          });
        }

        this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
      }
      else {
        row.treeLoading = false;
        this.cdr.markForCheck();
      }
    });

  }
  setRolePermission(node) {
    if (node && node.length > 0) {
      // Filter Level 2 Nodes
      node = node.filter(res => res.level != 1);
      //Filter Matching Page Ids
      let checkedNode;
      if (this.postRolePermission.pageIds.length > 0) {
        checkedNode = node.filter(array => this.postRolePermission.pageIds.map(res => res['value']).includes(array.value));
        node = node.filter(array => !(this.postRolePermission.pageIds.map(res => res['value']).includes(array.value)));
      }
      node = (node.length == 0) ? (node = checkedNode) : node;

      node.forEach(nodeValue => {
        let updateNodesValue = true;
        if ((this.postRolePermission.pageIds.length > 0) && (nodeValue.level == 2)) {
          for (let index = 0; index < this.postRolePermission.pageIds.length; index++) {
            if (this.postRolePermission.pageIds[index].value == nodeValue.value) {
              this.postRolePermission.pageIds.splice(index, 1);
              updateNodesValue = false;
            }
          }
        }
        else if (nodeValue.level != 2) {
          updateNodesValue = false;
        }
        if (updateNodesValue) {
          this.postRolePermission.pageIds.push({ 'value': nodeValue.value });
        }
      });
    }
    else {
      let updateValue = true;
      if (this.postRolePermission.pageIds.length > 0) {
        for (let index = 0; index < this.postRolePermission.pageIds.length; index++) {
          if (this.postRolePermission.pageIds[index].value == node.value) {
            this.postRolePermission.pageIds.splice(index, 1);
            updateValue = false;
          }
        }
      }
      if (updateValue) {
        this.postRolePermission.pageIds.push({ 'value': node.value });
      }
    }
  }
  saveRolePermission() {
    this.rolePersmissionLoader = true;
    this.roleService.AddRolePermission(this.postRolePermission).subscribe(res => {
      let response = res;
      if (response.code == 200 || response.result.code == 200) {
        this.modalService.dismissAll();
        this.rolePersmissionLoader = false;
      }
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  openLinkedRoleUsersDialog(row) {
    row.linkUserLoading = true;
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "60%";

    this.roleService.LinkedUsers(row.roleId).subscribe(res => {
      row.linkUserLoading = false;
      this.cdr.markForCheck();
      let response = res;
      let roleId = row.roleId;
      if (response.code == 200 || response.result.code == 200) {
        let usersList = response.data;
        dialogConfig.data = { usersList, roleId };
        this.dialog.open(RolesDefinitionLinkedUsersComponent, dialogConfig);
      }
    });
  }

  // changeAccessRights(event) {
  //   console.log(event);
  //   this.values = event;
  // }

  /* #region Tree View */
  /* Tree View */
  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();
  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();
  /** A selected parent node to be inserted */
  selectedParent: TodoItemFlatNode | null = null;
  /** The new item's name */
  newItemName = '';
  treeControl: FlatTreeControl<TodoItemFlatNode>;
  treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;
  dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;
  /** The selection for checklist */
  checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);

  /* Tree View */
  getLevel = (node: TodoItemFlatNode) => node.level;
  isExpandable = (node: TodoItemFlatNode) => node.expandable;
  getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;
  hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;
  hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => (_nodeData.itemEn || _nodeData.itemAr) === '';
  /**
   * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
   */
  transformer = (node: TodoItemNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.itemEn === node.itemEn
      ? existingNode
      : new TodoItemFlatNode();
    flatNode.itemEn = node.itemEn;
    flatNode.itemAr = node.itemAr;
    flatNode.value = node.value;
    flatNode.level = level;
    flatNode.expandable = !!node.children;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    return descAllSelected;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: TodoItemFlatNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.setRolePermission(descendants);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);
    // Force update for the parent
    descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    this.checkAllParentsSelection(node);
  }
  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  todoLeafItemSelectionToggle(node): void {
    this.setRolePermission(node);
    this.checklistSelection.toggle(node);
    this.checkAllParentsSelection(node);
  }
  /* Checks all the parents when a leaf node is selected/unselected */
  checkAllParentsSelection(node: TodoItemFlatNode): void {
    let parent: TodoItemFlatNode | null = this.getParentNode(node);
    while (parent) {
      this.checkRootNodeSelection(parent);
      parent = this.getParentNode(parent);
    }
  }
  /** Check root node checked state and change it accordingly */
  checkRootNodeSelection(node: TodoItemFlatNode): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }
  /* Get the parent node of a node */
  getParentNode(node: TodoItemFlatNode): TodoItemFlatNode | null {
    const currentLevel = this.getLevel(node);
    if (currentLevel < 1) {
      return null;
    }
    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;
    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];
      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }
  /* #endregion */
}
export class TodoItemNode {
  children: TodoItemNode[];
  itemEn: string;
  itemAr: string;
  value: number;
}
/** Flat to-do item node with expandable and level information */
export class TodoItemFlatNode {
  itemEn: string;
  itemAr: string;
  level: number;
  expandable: boolean;
  value: number;
}