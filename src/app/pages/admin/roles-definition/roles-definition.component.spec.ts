import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesDefinitionComponent } from './roles-definition.component';

describe('RolesDefinitionComponent', () => {
  let component: RolesDefinitionComponent;
  let fixture: ComponentFixture<RolesDefinitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesDefinitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesDefinitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
