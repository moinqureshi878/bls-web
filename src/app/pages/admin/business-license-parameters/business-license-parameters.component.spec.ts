import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessLicenseParametersComponent } from './business-license-parameters.component';

describe('BusinessLicenseParametersComponent', () => {
  let component: BusinessLicenseParametersComponent;
  let fixture: ComponentFixture<BusinessLicenseParametersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessLicenseParametersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessLicenseParametersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
