import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from 'app/core/_base/layout';
import { CommonService, BnCommonService } from 'app/core/appServices/index.service';
import { Observable } from 'rxjs';
import { BusinessLicenseParameterEnum } from '../../../core/_enum/index.enum';


@Component({
  selector: 'kt-business-license-parameters',
  templateUrl: './business-license-parameters.component.html',
  styleUrls: ['./business-license-parameters.component.scss']
})
export class BusinessLicenseParametersComponent implements OnInit {
  parameterCondition: Number = 199;
  parameterConditionDDL: Observable<any>;
  checkedBLS = true;
  moduleId;

  loading = false;

  ExtensionInDaysArray: any[] = [];
  PeriodInDays: any[] = [];

  // create form
  PeriodPerEachExtensionFormGroup: FormGroup;
  IssuancePeriodFormGroup: FormGroup;
  extensionFormGroup: FormGroup;

  businessLicenseParameterEnum = BusinessLicenseParameterEnum;

  constructor(
    private fb: FormBuilder,
    public validationMessages: ValidationMessagesService,
    public bnCommonService: BnCommonService,
    public toastrService: ToastrService,
    public translationService: TranslationService,
    private cdr: ChangeDetectorRef,
    public commonService: CommonService,
  ) {
    this.GetBusinessLicenseParameters(199);
  }

  ngOnInit() {
    this.GetParameters();
    this.createFormBLP();
  }

  //Radio Button Condtion
  filterByModuleName(fdsa) {
    debugger
  }

  GetParameters() {
    this.parameterConditionDDL = this.commonService.getLookUp("?type=BusinessLicenseParameter");
  }

  GetBusinessLicenseParameters(id: Number) {
    debugger
    this.bnCommonService.GetBusinessLicenseParameters(id)
      .subscribe(data => {
        if (data["result"].code === 200) {
          debugger

          //data["data"].blpid

          this.ExtensionInDaysArray = data["data"].extensionInDays;
          this.PeriodInDays = data["data"].periodInDays;

          this.extensionFormGroup.patchValue({
            allowedExtension: data["data"].limitInDays,
            autoCancellationPeriodReturned: data["data"].cprinDays,
            autoCancellationPeriodPayments: data["data"].cppinDays
          })
        }
        else {
          this.PeriodPerEachExtensionFormGroup.reset();
          this.IssuancePeriodFormGroup.reset();
          this.extensionFormGroup.reset();
          this.PeriodInDays = [];
          this.ExtensionInDaysArray = [];
          setTimeout(() => {
            Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
              item.classList.remove("mat-form-field-invalid");
            });
          }, 50);
        }
      })
  }

  createFormBLP() {
    this.PeriodPerEachExtensionFormGroup = this.fb.group({
      PPEEperiodBLP: ['', Validators.compose([Validators.required])],
    });

    this.IssuancePeriodFormGroup = this.fb.group({
      IssuancePeriodBLP: ['', Validators.compose([Validators.required])],
    });

    this.extensionFormGroup = this.fb.group({
      allowedExtension: ['', Validators.compose([Validators.required])],
      autoCancellationPeriodReturned: ['', Validators.compose([Validators.required])],
      autoCancellationPeriodPayments: ['', Validators.compose([Validators.required])]
    });
  }

  submitPeriodPerEachExtension() {
    if (this.PeriodPerEachExtensionFormGroup.valid) {
      this.ExtensionInDaysArray.push({
        value: this.PeriodPerEachExtensionFormGroup.value.PPEEperiodBLP
      })
      this.PeriodPerEachExtensionFormGroup.reset();
      setTimeout(() => {
        Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
          item.classList.remove("mat-form-field-invalid");
        });
      }, 50);
    }
    else {
      this.validationMessages.validateAllFormFields(this.PeriodPerEachExtensionFormGroup);
    }
  }

  removePeriodPerEachExtension(index: number) {
    this.ExtensionInDaysArray.splice(index, 1);
  }

  submitIssuancePeriod() {
    if (this.IssuancePeriodFormGroup.valid) {
      this.PeriodInDays.push({
        value: this.IssuancePeriodFormGroup.value.IssuancePeriodBLP
      });
      this.IssuancePeriodFormGroup.reset();
      setTimeout(() => {
        Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
          item.classList.remove("mat-form-field-invalid");
        });
      }, 50);
    }
    else {
      this.validationMessages.validateAllFormFields(this.IssuancePeriodFormGroup);
    }
  }

  removeIssuancePeriod(index: number) {
    this.PeriodInDays.splice(index, 1);
  }

  submitExtensions() {
    if (this.extensionFormGroup.valid) {
      let model = {
        "blpid": this.parameterCondition,
        "limitInDays": this.extensionFormGroup.value.allowedExtension,
        "cprinDays": this.extensionFormGroup.value.autoCancellationPeriodReturned,
        "cppinDays": this.extensionFormGroup.value.autoCancellationPeriodPayments,
        "extensionInDays": this.ExtensionInDaysArray,
        "periodInDays": this.PeriodInDays
      }

      this.bnCommonService.AddBusinessLicenseParameters(model)
        .subscribe(data => {
          if (data["result"].code === 200) {
            this.loading = false;
            this.cdr.markForCheck();

            this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
              this.toastrService.success(text);
            });

          }
          else {
            this.loading = false;
            this.cdr.markForCheck();
            this.toastrService.error('Something went wrong. Please try again');
          }
        })
    }
    else {
      this.validationMessages.validateAllFormFields(this.extensionFormGroup);
    }
  }
}
