import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
/* Tree View */
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AsyncFormValidatorService } from '../../../core/validator/async-form-validator.service';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { matchingPasswords, emailValidator, nameValidator, passwordValidator, commentValidator, phoneNumberValidator, macAddressMasking, phoneNumberMasking, nameArValidator } from '../../../core/validator/form-validators.service';
import { CommonService, UserService } from '../../../core/appServices/index.service';
import { Observable } from 'rxjs';
import { TranslationService } from '@translateService/translation.service';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { ToastrService } from 'ngx-toastr';
import { UserTypeEnum, GenderEnum, CommonEnum } from '../../../core/_enum/index.enum';
interface FoodNode {
  name: string;
  children?: FoodNode[];
}

const TREE_DATA: FoodNode[] = [
  {
    name: 'CEO Office',
    children: [
      {
        name: 'Head of Department 1',
        children: [
          {
            name: 'Head of Section 1',
            children: [
              { name: 'Head of Unit A' },
              { name: 'Head of Unit B' },
              { name: 'Head of Unit C' },
            ]

          },

        ]
      },
      {
        name: 'Head of Department 2',
        children: [
          {
            name: 'Head of Section 1',
            children: [
              { name: 'Head of Unit A' },
              { name: 'Head of Unit B' },
              { name: 'Head of Unit C' },
            ]

          },

        ]
      },
      {
        name: 'Head of Department 3',
        children: [
          {
            name: 'Head of Section 1',
            children: [
              { name: 'Head of Unit A' },
              { name: 'Head of Unit B' },
              { name: 'Head of Unit C' },
            ]

          },

        ]
      },
    ]
  },
];


@Component({
  selector: 'kt-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {
  treeValue: number;
  addTDAUser: FormGroup;
  lastIndex: number;
  userId: string;
  typeDesignation: Observable<any>;
  typeNationalities: Observable<any>;
  typeGenderCode: Observable<any>;
  data = {
    physicalDevices: [{
      deviceName: 'atif', macAddress: 'aaa'
    },
    { deviceName: 'ovais', macAddress: 'aaa' },
    ]
  }
  getUserData: any;
  loading: boolean = false;
  businessUnits: any;
  CommonEnum = CommonEnum;

  constructor(private modalService: NgbModal,
    public router: Router,
    public route: ActivatedRoute,
    public commonService: CommonService,
    private translationService: TranslationService,
    public userService: UserService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    public toast: ToastrService) {
    // this.dataSource.data = TREE_DATA;
    this.getUserData = this.route.snapshot.data.data;
    this.route.params.subscribe(params => {
      this.userId = params['id'];
      this.addTDAUserForm();
      if (this.userId) {
        this.setTDAUserForm();
        this.setDeviceFormArray();
      }
      else {
        this.addDevice(undefined);
      }
    });
    // DOM Update After Async Validation Check
    this.addTDAUser.get('fullNameAr').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
    this.addTDAUser.get('fullNameEn').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
    this.addTDAUser.get('username').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
    this.addTDAUser.get('hrCode').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
    this.addTDAUser.get('email').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
    this.addTDAUser.get('mobileNo').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
  }

  ngOnInit() {
    this.getDesignation();
    this.getNationalities();
    this.getGenderCode();
    this.GetOrganizationalStructure();
  }
  GetOrganizationalStructure() {
    this.commonService.GetOrganizationalStructure().subscribe(res => {
      let response = res;
      if ((response.result && response.result.code == 200) || response.code == 200) {
        this.businessUnits = response.data;
        var hierarchicalJson = this.convertToHierarchicalJson(response.data);
        this.dataSource.data = hierarchicalJson;
        this.expandAllNodes();
        if (this.userId) {
          this.treeValue = +this.getUserData.userAttributes.businessUnitCode;
          this.addTDAUser.controls['businessUnitCode'].setValue(Number(this.getUserData.userAttributes.businessUnitCode));
          this.addTDAUser.controls['businessUnitName'].setValue(this.translationService.getSelectedLanguage() == "en" ? this.businessUnits.find(res => res.id == +this.getUserData.userAttributes.businessUnitCode).nameEn : this.businessUnits.find(res => res.id == +this.getUserData.userAttributes.businessUnitCode).nameAr);
        }
      }
    });
  }
  convertToHierarchicalJson(array) {
    var map = {};
    for (var i = 0; i < array.length; i++) {
      var obj = array[i];
      obj.children = [];
      map[obj.id] = obj;
      var parentId = obj.parentId || '-';
      if (!map[parentId]) {
        map[parentId] = {
          children: []
        };
      }
      map[parentId].children.push(obj);
    }
    return map['-'].children;
  }
  private expandAllNodes() {
    for (let i = 0; i < this.treeControl.dataNodes.length; i++) {
      this.treeControl.expand(this.treeControl.dataNodes[i]);
    }
  }
  addTDAUserForm() {
    if (this.userId) {
      this.addTDAUser = this.fb.group({
        fullNameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
        fullNameEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
        nationality: ['', Validators.required],
        gender: [null, Validators.required],
        username: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameValidator]), this.asyncFormValidatorService.usernameValidator(this.userId)],
        password: [{ value: '', disabled: true }, Validators.compose([Validators.required, Validators.maxLength(30), passwordValidator])],
        confirmPassword: [{ value: '', disabled: true }, Validators.required],
        businessUnitCode: ['', Validators.required],
        businessUnitName: [''],
        designation: ['', Validators.required],
        hrCode: ['', Validators.compose([Validators.maxLength(50)]), this.asyncFormValidatorService.hrCodeValidator(this.userId)],
        isActive: [true, Validators.required],
        email: ['', Validators.compose([Validators.required, emailValidator]), this.asyncFormValidatorService.emailValidator(this.userId)],
        mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator]), this.asyncFormValidatorService.mobileNoValidator(this.userId)],
        // mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator])],
        landlineNo: ['', Validators.compose([Validators.required])],
        preferedLanguage: ['', Validators.required],
        physicalDevices: this.fb.array([]),
        remarks: ['', Validators.compose([Validators.maxLength(500), commentValidator])],
      }, { validator: matchingPasswords('password', 'confirmPassword') });
    }
    else {
      this.addTDAUser = this.fb.group({
        fullNameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
        fullNameEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
        nationality: ['', Validators.required],
        gender: [GenderEnum.male, Validators.required],
        username: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameValidator]), this.asyncFormValidatorService.usernameValidator(0)],
        password: ['', Validators.compose([Validators.required, Validators.maxLength(30), passwordValidator])],
        confirmPassword: ['', Validators.required],
        businessUnitCode: ['', Validators.required],
        businessUnitName: [''],
        designation: ['', Validators.required],
        hrCode: ['', Validators.compose([Validators.maxLength(50)]), this.asyncFormValidatorService.hrCodeValidator(0)],
        isActive: [true, Validators.required],
        email: ['', Validators.compose([Validators.required, emailValidator]), this.asyncFormValidatorService.emailValidator(0)],
        mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator]), this.asyncFormValidatorService.mobileNoValidator(0)],
        // mobileNo: ['', Validators.compose([Validators.required,]), this.asyncFormValidatorService.mobileNoValidator(0)],
        landlineNo: ['', Validators.compose([Validators.required]), this.asyncFormValidatorService.landlineNoValidator(0)],
        preferedLanguage: ['ar', Validators.required],
        physicalDevices: this.fb.array([]),
        remarks: ['', Validators.compose([Validators.maxLength(500), commentValidator])],
      }, { validator: matchingPasswords('password', 'confirmPassword') });
    }

    this.lastIndex = this.addTDAUser.value.physicalDevices.length - 1;

  }
  setTDAUserForm() {

    this.addTDAUser.patchValue({
      fullNameAr: this.getUserData.fullNameAr,
      fullNameEn: this.getUserData.fullNameEn,
      nationality: this.getUserData.userAttributes.nationalityCode,
      gender: this.getUserData.genderCode,
      username: this.getUserData.userName,
      password: 'P@ssw0rd',
      confirmPassword: 'P@ssw0rd',
      businessUnitCode: +this.getUserData.userAttributes.businessUnitCode,
      designation: this.getUserData.userAttributes.designationCode,
      hrCode: this.getUserData.userAttributes.hrcode,
      isActive: this.getUserData.isActive,
      email: this.getUserData.userEmail,
      mobileNo: this.getUserData.mobileNo,
      landlineNo: this.getUserData.userAttributes.landlineNo,
      preferedLanguage: this.getUserData.preferedLanguage,
      remarks: this.getUserData.userAttributes.remarks,
    });
  }
  addTDAUserFormSubmit() {
    if (this.addTDAUser.valid) {
      this.loading = true;
      let addtdaUserObj = this.getTDAUserObj()
      if (this.userId) {
        this.userService.EditUser(addtdaUserObj).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.addTDAUser.reset();
            setTimeout(() => {
              Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
                item.classList.remove("mat-form-field-invalid");
              });
            }, 50);
            this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigateByUrl('/pages/admin/tda-users-profile');
          }
        });
      }
      else {
        this.userService.AddUser(addtdaUserObj).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.addTDAUser.reset();
            setTimeout(() => {
              Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
                item.classList.remove("mat-form-field-invalid");
              });
            }, 50);
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigateByUrl('/pages/admin/tda-users-profile');
          }
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.addTDAUser);
    }
  }
  private getTDAUserObj() {
    return {
      "userId": this.userId ? this.getUserData.userId : 0,
      "masterId": 0,
      "fullNameAr": this.addTDAUser.value.fullNameAr,
      "fullNameEn": this.addTDAUser.value.fullNameEn,
      "userName": this.addTDAUser.value.username,
      "userEmail": this.addTDAUser.value.email,
      "password": this.addTDAUser.value.password,
      "type": this.CommonEnum.userTypeLoginPage,
      "userType": UserTypeEnum.TDA_User,
      "mobileNo": this.addTDAUser.value.mobileNo,
      "securityToken": "",
      "genderCode": this.addTDAUser.value.gender,
      "isConfirmed": true,
      "isActive": this.addTDAUser.value.isActive,
      "preferedLanguage": this.addTDAUser.value.preferedLanguage,
      "createdBy": 0,
      "createdAt": "2019-11-11T11:52:25.338Z",
      "modifiedBy": 0,
      "modifiedAt": null,
      "stepCode": 0,
      "loginCounter": 0,
      "lastLogin": new Date(),
      "userAttributes": {
        "userId": this.userId ? this.getUserData.userId : 0,
        "role": "",
        "department": "",
        "nationalityCode": this.addTDAUser.value.nationality,
        "designationCode": this.addTDAUser.value.designation,
        "hrcode": this.addTDAUser.value.hrCode,
        "businessUnitCode": +this.addTDAUser.value.businessUnitCode,
        "remarks": this.addTDAUser.value.remarks,
        "citizenId": "",
        "citizenCode": 0,
        "passportNo": "",
        "passportExpiryAt": null,
        "licenseNumber": "",
        "licenseExpiryAt": null,
        "issuanceEmirates": 0,
        "bnnameEn": "",
        "bnnameAr": "",
        "nameAr": "",
        "fatherNameAr": "",
        "grandFatherNameAr": "",
        "familyNameAr": "",
        "govtEntityCode": 0,
        "govtEntityNameEn": "",
        "govtEntityNameAr": "",
        "landlineNo": this.addTDAUser.value.landlineNo
      },
      "physicalDevices": this.addTDAUser.value.physicalDevices,
      "attachmentsLog": [],
      "roleUser": []
    };
  }

  setDeviceFormArray() {
    let control = <FormArray>this.addTDAUser.controls.physicalDevices;
    this.getUserData.physicalDevices.forEach(x => {
      control.push(this.fb.group({
        id: [0],
        userId: [x.userId],
        deviceName: [x.deviceName, Validators.compose([Validators.required]), this.asyncFormValidatorService.deviceNameValidator(this.userId)],
        deviceType: [x.deviceType],
        macAddress: [x.macAddress, Validators.compose([]), this.asyncFormValidatorService.macAddressValidator(this.userId)],
        isActive: true,
      }))
      this.lastIndex = this.addTDAUser.value.physicalDevices.length - 1;
    });
  }
  createDeviceFormArray() {
    return this.fb.group({
      id: [0],
      userId: [0],
      deviceName: ['', Validators.compose([Validators.required]), this.asyncFormValidatorService.deviceNameValidator(0)],
      deviceType: ["Mobile"],
      macAddress: ['', Validators.compose([]), this.asyncFormValidatorService.macAddressValidator(0)],
      isActive: true,
    });
  }
  addDevice(index) {

    if (index >= 0) {
      const controlArray = <FormArray>this.addTDAUser.get('physicalDevices');
      if (controlArray.valid) {
        (this.addTDAUser.controls['physicalDevices'] as FormArray).push(this.createDeviceFormArray())
        this.lastIndex = this.addTDAUser.value.physicalDevices.length - 1;
      }
      else {
        controlArray.controls[index].get('deviceName').markAsTouched();
        controlArray.controls[index].get('deviceName').markAsDirty();
      }
    }
    else {
      (this.addTDAUser.controls['physicalDevices'] as FormArray).push(this.createDeviceFormArray())
      this.lastIndex = this.addTDAUser.value.physicalDevices.length - 1;
    }
  }
  removeDevice(getIndex) {
    this.removeDeviceAddress.removeAt(getIndex);
    this.lastIndex = this.addTDAUser.value.physicalDevices.length - 1;
  }
  get removeDeviceAddress(): FormArray {
    return this.addTDAUser.get('physicalDevices') as FormArray;
  }
  getDesignation() {
    this.typeDesignation = this.commonService.getLookUp("?type=TDADesignations");
  }
  getGenderCode() {
    this.typeGenderCode = this.commonService.getLookUp("?type=gender");
  }
  getNationalities() {
    this.typeNationalities = this.commonService.getLookUp("?type=Nationality");
  }
  //..Modal Popup Start
  open(content) {
    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  changeNode(id) {

    this.addTDAUser.controls['businessUnitCode'].setValue(id);
    this.addTDAUser.controls['businessUnitName'].setValue(this.translationService.getSelectedLanguage() == "en" ? this.businessUnits.find(res => res.id == id).nameEn : this.businessUnits.find(res => res.id == id).nameAr);
    this.treeValue = id;
  }
  /* Mobile No Mask */
  mobileNoMask = phoneNumberMasking();
  macAddressMask = macAddressMasking();
  private _transformer = (node: OrganizationalUnit, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      nameEn: node.nameEn,
      nameAr: node.nameAr,
      parentId: node.parentId,
      level: level,
    };
  }
  closeResult: string; //Bootstrap Modal Popup
  treeControl = new FlatTreeControl<ExampleFlatNode>(node => node.level, node => node.expandable);
  treeFlattener = new MatTreeFlattener(this._transformer, node => node.level, node => node.expandable, node => node.children);
  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;
}


interface OrganizationalUnit {
  id: number;
  nameEn: string;
  nameAr: string;
  parentId: number;
  children?: OrganizationalUnit[];
}

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  id: number;
  nameEn: string;
  nameAr: string;
  parentId: number;
  level: number;
}