import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectedPagesModalComponent } from './rejected-pages-modal.component';

describe('RejectedPagesModalComponent', () => {
  let component: RejectedPagesModalComponent;
  let fixture: ComponentFixture<RejectedPagesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectedPagesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectedPagesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
