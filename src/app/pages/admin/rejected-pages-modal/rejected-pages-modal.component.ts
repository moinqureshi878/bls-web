
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslationService } from '@translateService/translation.service';

@Component({
  selector: 'kt-rejected-pages-modal',
  templateUrl: './rejected-pages-modal.component.html',
  styleUrls: ['./rejected-pages-modal.component.scss']
})
export class RejectedPagesModalComponent implements OnInit {

  constructor(private translationService: TranslationService,
  ) { }

  ngOnInit() {

  }

  /* Resion List */
  reasons = new FormControl();
  reasonsList: string[] = ['Reason 1', 'Reason 2', 'Reason 3', 'Reason 4', 'Reason 5', 'Reason 6'];
}
