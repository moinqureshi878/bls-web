import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { AuthenticationService } from '../../../core/appServices/auth.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { localStorageSession } from '../../../core/appModels/localStorageSessions.model';
import { phoneNumberValidator, phoneNumberMasking } from '../../../core/validator/form-validators.service';
import { JwtTokenService, CommonService } from '../../../core/appServices/index.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { TranslationService } from '@translateService/translation.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'kt-login-on-behalf',
  templateUrl: './login-on-behalf.component.html',
  styleUrls: ['./login-on-behalf.component.scss']
})
export class LoginOnBehalfComponent implements OnInit {
  PinFormPanel: boolean = false; // hidden by default
  LoginFormPanel: boolean = true;

  loginForm: FormGroup;
  TokenForm: FormGroup;
  loading = false;

  statusCode: string;

  localstorageSessionArray: localStorageSession[] = [];
  localstorageSession: localStorageSession = new localStorageSession();
  blockAsyncCall: boolean = false;

  beneficiaryType: Observable<any>;
  @ViewChild('noPages', { static: false }) content: ElementRef;
  @ViewChild("tb1", { static: false }) tb1: ElementRef;

  closeResult: string; //Bootstrap Modal Popup
  constructor(
    private authentication: AuthenticationService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private jwtTokenService: JwtTokenService,
    private translationService: TranslationService,
    private router: Router,
    private modalService: NgbModal,
    public commonService: CommonService,
    private toastrService: ToastrService,
  ) { }

  ngOnInit() {
    this.initLoginForm();
    this.initTokenForm();
    this.getbeneficiaryType();
  }

  initLoginForm() {
    this.loginForm = this.fb.group({
      beneficiaryType: ['', Validators.compose([Validators.required])],
      mobileNo: ['', Validators.compose([Validators.required, Validators.maxLength(30), phoneNumberValidator])]
    });
  }

  initTokenForm() {
    this.TokenForm = this.fb.group({
      tb1: ['', Validators.compose([Validators.required])],
      tb2: ['', Validators.compose([Validators.required])],
      tb3: ['', Validators.compose([Validators.required])],
      tb4: ['', Validators.compose([Validators.required])]
    });
  }

  loginFormSubmit() {
    if (this.loginForm.valid) {
      this.loading = true;
      let LoginBody = {
        "email": this.jwtTokenService.getJwtTokenValue().email,
        "mobileNo": this.loginForm.value.mobileNo
      }
      this.authentication.getOnBehalfOTP(LoginBody)
        .subscribe(data => {
          debugger
          if (data.code === 200) {
            this.loading = false;
            this.LoginFormPanel = false;
            this.PinFormPanel = true;

            // set beneficary type on localstorage
            localStorage.setItem("beneficaryType", this.loginForm.value.beneficiaryType);
            console.log(data);
          }
          else {

            this.translationService.getTranslation(String(data.code)).subscribe((text: string) => {
              this.toastrService.error(text);
            });

            this.statusCode = String(data.code);
            this.loading = false;
          }
          this.cdr.markForCheck();
        })
    }
  }

  verifyPin() {
    if (this.TokenForm.valid) {
      this.toastrService.clear();
      this.loading = true;
      let token = this.TokenForm.value.tb1 + this.TokenForm.value.tb2 + this.TokenForm.value.tb3 + this.TokenForm.value.tb4;

      let verifyTokenJson = {
        "mobileNo": this.loginForm.value.mobileNo,
        "OTP": token
      }
      if (!this.blockAsyncCall) {
        this.blockAsyncCall = true;

        this.authentication.verifyOnBehalfOTP(verifyTokenJson).subscribe(data => {
          debugger
          if (data.code === 200) {
            // get previous user session values
            debugger
            if (data.data.pages.length > 0) {
              this.localstorageSessionArray = JSON.parse(localStorage.getItem("userSession"));

              this.localstorageSession.token = data["data"].token;
              this.localstorageSession.pages = data["data"].pages;
              this.localstorageSession.sideMenu = data["data"].items;
              this.localstorageSession.sessionID = 0;
              this.localstorageSessionArray.push(this.localstorageSession);

              localStorage.setItem('userSession', JSON.stringify(this.localstorageSessionArray))

              // set menu in subject
              this.commonService.UpdateMenuFromLocalStorage();
              this.jwtTokenService.updateSessionFromLocalStorage();
              
              //localStorage.setItem('token', data["data"].token);
              localStorage.setItem('sideMenu', JSON.stringify(data["data"].items));
              localStorage.setItem('pages', JSON.stringify(data["data"].pages));
              this.router.navigate(["/pages/" + data["data"].pages[0].pageUrl])
            }
            else {
              this.translationService.getTranslation(String(603)).subscribe((text: string) => {
                this.toastrService.error(text);
              });
            }

          }
          else if (data.code == 603) {
            this.modalService.open(this.content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
              this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
              this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
          }
          else {
            this.blockAsyncCall = false;

            this.translationService.getTranslation(String(data.code)).subscribe((text: string) => {
              this.toastrService.error(text);
            });
            // this.toastrService.error('Invalid OTP');

            this.tb1.nativeElement.focus();
            this.TokenForm.reset();

            this.loading = false;
          }
          this.loading = false;
          this.cdr.markForCheck();
        })
      }
    }
  }

  /* Mobile No Mask */
  mobileNoMask = phoneNumberMasking();

  processKeyUp(e, next, prev) {
    if (e.code === "Backspace") {
      if (prev) {
        prev.focus();
      }
    }
    else if (next == undefined) {
      this.verifyPin();
    }
    else {
      next.focus();
    }
  }

  getbeneficiaryType() {
    this.beneficiaryType = this.commonService.getLookUp("?type=BenificiaryTypes");
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  closeNoPagesModal() {
    this.modalService.dismissAll();
    this.LoginFormPanel = true;
    this.PinFormPanel = false;
  }
}
