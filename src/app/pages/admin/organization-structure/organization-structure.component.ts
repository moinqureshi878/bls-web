import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { CommonService } from '../../../core/appServices/index.service';
import { ActivatedRoute } from '@angular/router';
import { TranslationService } from '@translateService/translation.service';
import { ToastrService } from 'ngx-toastr';
import { nameArValidator } from 'app/core/validator/form-validators.service';

@Component({
  selector: 'kt-organization-structure',
  templateUrl: './organization-structure.component.html',
  styleUrls: ['./organization-structure.component.scss']
})
export class OrganizationStructureComponent {
  showContextMenu: boolean = false;
  organizationalUnitForm: FormGroup;
  currentNode: any;
  action: string;
  loading: boolean = false;
  response: any;
  getData: any;
  coordinates = {
    top: null,
    left: null
  }
  // data = [
  //   { "id": 1, "nameEn": "neusol", "nameAr": "neusolAr", "parentId": 0 },
  //   { "id": 2, "nameEn": "atif", "nameAr": "atifAr", "parentId": 1 },
  //   { "id": 3, "nameEn": "moin", "nameAr": "moinAr", "parentId": 1 },
  //   { "id": 4, "nameEn": "ovais", "nameAr": "ovaisAr", "parentId": 2 },
  //   { "id": 5, "nameEn": "waqar", "nameAr": "waqarAr", "parentId": 3 },
  //   { "id": 6, "nameEn": "waqarowais", "nameAr": "waqarowais", "parentId": 4 }
  // ];

  constructor(public elRef: ElementRef, public toast: ToastrService, public translationService: TranslationService, public route: ActivatedRoute, public commonService: CommonService, private modalService: NgbModal, public fb: FormBuilder, public validationMessages: ValidationMessagesService) {
    this.getData = this.route.snapshot.data.data;
    this.organizationalForm();
    var hierarchicalJson = this.convertToHierarchicalJson(this.getData);
    //var hierarchicalJson = this.convertToHierarchicalJson(this.data);
    this.dataSource.data = hierarchicalJson;
    this.expandAllNodes();
  }
  public data: any;

  private expandAllNodes() {
    for (let i = 0; i < this.treeControl.dataNodes.length; i++) {
      this.treeControl.expand(this.treeControl.dataNodes[i]);
    }
  }
  onRightClick(node, event) {

    this.coordinates.top = event.currentTarget.offsetTop + 'px';
    this.coordinates.left = event.currentTarget.offsetLeft + 'px';
    this.showContextMenu = true;
    this.currentNode = node;
    return false;
  }
  deleteNode() {
    this.loading = true;
    this.showContextMenu = false;

    this.commonService.DeleteOrganizationalStructure(this.currentNode.id).subscribe(res => {
      this.loading = false;
      this.response = res;
      let response = res;
      if ((this.response.result && this.response.result.code == 200) || this.response.code == 200) {
        this.dataSource.data = this.convertToHierarchicalJson(this.response.data);
        this.expandAllNodes();
        this.showContextMenu = false;
      }
      else {
        this.translationService.getTranslation(String(response.result.code)).subscribe((text: string) => {
          this.toast.error(text);
        });
      }
    });
  }
  organizationalForm() {
    this.organizationalUnitForm = this.fb.group({
      id: [''],
      organizationUnitNameEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      organizationUnitNameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
      parentId: [''],
    })
  }
  organizationalUnitFormSubmit() {
    this.organizationalUnitForm.value.nameEn = this.organizationalUnitForm.value.organizationUnitNameEn;
    this.organizationalUnitForm.value.nameAr = this.organizationalUnitForm.value.organizationUnitNameAr;
    if (this.organizationalUnitForm.valid) {
      this.loading = true;
      if (this.action == 'add') {
        this.commonService.AddOrganizationalStructure(this.organizationalUnitForm.value).subscribe(res => {
          this.response = res;
          if ((this.response.result && this.response.result.code == 200) || this.response.code == 200) {
            this.dataSource.data = this.convertToHierarchicalJson(this.response.data);
            this.expandAllNodes();
            this.modalService.dismissAll();
            this.organizationalUnitForm.reset();
            this.loading = false;
          }
        });
      }
      else if (this.action == 'edit') {
        this.commonService.UpdateOrganizationalStructure(this.organizationalUnitForm.value).subscribe(res => {
          this.response = res;
          if ((this.response.result && this.response.result.code == 200) || this.response.code == 200) {
            this.dataSource.data = this.convertToHierarchicalJson(this.response.data);
            this.expandAllNodes();
            this.modalService.dismissAll();
            this.organizationalUnitForm.reset();
            this.loading = false;
          }
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.organizationalUnitForm);
    }
  }
  openOrganizationalForm(content, action) {

    this.showContextMenu = false;
    this.action = action;
    if (action == 'add') {
      this.organizationalUnitForm.patchValue({
        id: 0,
        parentId: this.currentNode.id
      });
    }
    else if (action == 'edit') {
      this.organizationalUnitForm.patchValue({
        id: this.currentNode.id,
        organizationUnitNameEn: this.currentNode.nameEn,
        organizationUnitNameAr: this.currentNode.nameAr,
        parentId: this.currentNode.parentId
      });
    }
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    this.organizationalUnitForm.reset();
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  hideShowContextMenu() {
    this.showContextMenu = !this.showContextMenu;
  }
  convertToHierarchicalJson(array) {
    var map = {};
    for (var i = 0; i < array.length; i++) {
      var obj = array[i];
      obj.children = [];
      map[obj.id] = obj;
      var parentId = obj.parentId || '-';
      if (!map[parentId]) {
        map[parentId] = {
          children: []
        };
      }
      map[parentId].children.push(obj);
    }
    return map['-'].children;
  }
  private _transformer = (node: OrganizationalUnit, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      nameEn: node.nameEn,
      nameAr: node.nameAr,
      parentId: node.parentId,
      level: level,
    };
  }
  closeResult: string; //Bootstrap Modal Popup
  treeControl = new FlatTreeControl<ExampleFlatNode>(node => node.level, node => node.expandable);
  treeFlattener = new MatTreeFlattener(this._transformer, node => node.level, node => node.expandable, node => node.children);
  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;
}


interface OrganizationalUnit {
  id: number;
  nameEn: string;
  nameAr: string;
  parentId: number;
  children?: OrganizationalUnit[];
}

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  id: number;
  nameEn: string;
  nameAr: string;
  parentId: number;
  level: number;
}