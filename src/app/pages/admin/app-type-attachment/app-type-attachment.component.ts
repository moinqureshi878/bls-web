
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TdaUsersLinkedRoleComponent } from '../tda-users-linked-role/tda-users-linked-role.component';
import { CommonService } from 'app/core/appServices/index.service';
import { ActivatedRoute } from '@angular/router';
import { ConfigService } from 'app/core/appServices/config.service';
import { TranslationService } from 'app/core/_base/layout';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';

@Component({
  selector: 'kt-app-type-attachment',
  templateUrl: './app-type-attachment.component.html',
  styleUrls: ['./app-type-attachment.component.scss']
})
export class AppTypeAttachmentComponent implements OnInit {

  ELEMENT_DATA: any[] = [];


  appTypeAttachment: [] = [];
  applicationTypeddl: Observable<any>;

  displayedColumns7: string[] = ['applicationType', 'attachmentAr', 'attachmentEn', 'isMandotary', 'inputData', 'isActive'];
  dataSource7 = new MatTableDataSource(this.ELEMENT_DATA);

  applicationTypeSelected: any;

  closeResult: string; //Bootstrap Modal Popup

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  constructor(
    public dialog: MatDialog,
    public route: ActivatedRoute,
    public configService: ConfigService,
    private translationService: TranslationService,
    private toastrService: ToastrService,
    private commonService: CommonService,
  ) {
    debugger
    this.appTypeAttachment = this.route.snapshot.data.data;
    this.dataSource7 = new MatTableDataSource(this.appTypeAttachment);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;

    this.getApplicationTypeDdl();
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();

    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  getApplicationTypeDdl() {
    this.applicationTypeddl = this.commonService.getLookUp("?type=ApplicationTypes");
  }

  editManageApplicationTypeAttachment(...value) {

    let model = {
      "id": value[0],
      "isMandatory": null,
      "inputData": null,
      "isActive": null
    }

    if (value[2] === "isMandatory") {
      model.isMandatory = value[1];
    }
    else if (value[2] === "inputData") {
      model.inputData = value[1];
    }

    else if (value[2] === "isActive") {
      model.isActive = value[1];
    }

    this.configService.editManageApplicationTypeAttachment(model)
      .subscribe(data => {
        if (data["result"].code === 200) {

        }
        else {
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toastrService.success(text);
          });
        }
      })
  }

  ddlApplicationTypeChange(applicationTypeSelected: any) {
    this.dataSource7.filter = applicationTypeSelected.valueEn.trim().toLowerCase() || applicationTypeSelected.valueAr.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

}