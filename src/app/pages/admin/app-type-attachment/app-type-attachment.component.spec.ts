import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppTypeAttachmentComponent } from './app-type-attachment.component';

describe('AppTypeAttachmentComponent', () => {
  let component: AppTypeAttachmentComponent;
  let fixture: ComponentFixture<AppTypeAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppTypeAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppTypeAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
