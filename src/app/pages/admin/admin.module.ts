import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '../../views/partials/partials.module';

import { AdminComponent } from './admin.component';
import { TdaUsersProfileComponent } from './tda-users-profile/tda-users-profile.component';
import { RolesDefinitionComponent } from './roles-definition/roles-definition.component';
import { AddRoleComponent } from './add-role/add-role.component';
import { AddUserComponent } from './add-user/add-user.component';
import { LoginLogoutHistoryComponent } from './login-logout-history/login-logout-history.component';
import { OrganizationStructureComponent } from './organization-structure/organization-structure.component';
import { AddOrganizationUnitComponent } from './add-organization-unit/add-organization-unit.component';
import { LoginOnBehalfComponent } from './login-on-behalf/login-on-behalf.component';
import { RegistrationRequestComponent } from './registration-request/registration-request.component';
import { RejectedPagesModalComponent } from './rejected-pages-modal/rejected-pages-modal.component';
import { ReviewRegistrationRequestComponent } from './review-registration-request/review-registration-request.component';
import { ReviewAttachmentModalComponent } from './review-attachment-modal/review-attachment-modal.component';
import { TdaUsersLinkedRoleComponent } from './tda-users-linked-role/tda-users-linked-role.component';
import { TdaUsersLinkedMoreRoleComponent } from './tda-users-linked-more-role/tda-users-linked-more-role.component';
import { RolesDefinitionLinkedUsersComponent } from './roles-definition-linked-users/roles-definition-linked-users.component';
import { RolesDefinitionLinkedMoreUsersComponent } from './roles-definition-linked-more-users/roles-definition-linked-more-users.component';

import { TreeviewModule } from 'ngx-treeview';

import { ModuleService, SubModuleService, PagesService, RoleService, UserService, BnCommonService } from '../../core/appServices/index.service';
import { JwtTokenService, CommonService } from './../../core/appServices/index.service';
import { AppResolver } from './../../core/resolvers/app.resolver';

import { AuthGuard } from '../../core/auth';

import { IgxTimePickerModule, IgxInputGroupModule } from 'igniteui-angular';

import { MatTableExporterModule } from 'mat-table-exporter';
import { ViewedPagesModalComponent } from './viewed-pages-modal/viewed-pages-modal.component';
import { TranslateModule } from '@ngx-translate/core';
import { PublicHolidaysComponent } from './public-holidays/public-holidays.component';
import { MyLeavesManagementComponent } from './my-leaves-management/my-leaves-management.component';
import { AddLeavesComponent } from './add-leaves/add-leaves.component';
import { AppCatalogDefinitionComponent } from './app-catalog-definition/app-catalog-definition.component';
import { BusinessActivityGroupsComponent } from './business-activity-groups/business-activity-groups.component';
import { AddBusinessActivityGroupsComponent } from './add-business-activity-groups/add-business-activity-groups.component';
import { BusinessActivityPagesComponent } from './business-activity-pages/business-activity-pages.component';
import { AddBusinessActivityPagesComponent } from './add-business-activity-pages/add-business-activity-pages.component';
import { BusinessLicenseParametersComponent } from './business-license-parameters/business-license-parameters.component';
import { AddPublicHolidaysComponent } from './add-public-holidays/add-public-holidays.component';
import { AppTypeAttachmentComponent } from './app-type-attachment/app-type-attachment.component';
import { AddAppTypeAttachmentComponent } from './add-app-type-attachment/add-app-type-attachment.component';
import { ConfigService } from 'app/core/appServices/config.service';
import { ManageNationalitiesComponent } from './manage-nationalities/manage-nationalities.component';
import { AddNationalitiesComponent } from './add-nationalities/add-nationalities.component';
import {
  MatSortModule,
} from '@angular/material';


@NgModule({
  entryComponents: [RejectedPagesModalComponent, ReviewAttachmentModalComponent, TdaUsersLinkedRoleComponent, TdaUsersLinkedMoreRoleComponent, RolesDefinitionLinkedUsersComponent, RolesDefinitionLinkedMoreUsersComponent, ViewedPagesModalComponent],
  declarations: [AdminComponent, TdaUsersProfileComponent, RolesDefinitionComponent, AddRoleComponent, AddUserComponent, LoginLogoutHistoryComponent, OrganizationStructureComponent, AddOrganizationUnitComponent, LoginOnBehalfComponent, RegistrationRequestComponent, RejectedPagesModalComponent, ReviewRegistrationRequestComponent, ReviewAttachmentModalComponent, TdaUsersLinkedRoleComponent, TdaUsersLinkedMoreRoleComponent, RolesDefinitionLinkedUsersComponent, RolesDefinitionLinkedMoreUsersComponent, ViewedPagesModalComponent, PublicHolidaysComponent, MyLeavesManagementComponent, AddLeavesComponent, AppCatalogDefinitionComponent, BusinessActivityGroupsComponent, AddBusinessActivityGroupsComponent, BusinessActivityPagesComponent, AddBusinessActivityPagesComponent, BusinessLicenseParametersComponent, AddPublicHolidaysComponent, AppTypeAttachmentComponent, AddAppTypeAttachmentComponent, ManageNationalitiesComponent, AddNationalitiesComponent],

  imports: [
    // NgxMaterialTimepickerModule,
    PartialsModule,
    NgbModule.forRoot(),
    MatSortModule,
    TreeviewModule.forRoot(),
    MatTableExporterModule,
    IgxTimePickerModule,
    IgxInputGroupModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: AdminComponent
      },
      {
        path: 'tda-users-profile',
        canActivate: [AuthGuard],
        component: TdaUsersProfileComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'edit-user/:id',
        canActivate: [AuthGuard],
        component: AddUserComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'add-user',
        canActivate: [AuthGuard],
        component: AddUserComponent
      },
      {
        path: 'viewed-pages',
        canActivate: [AuthGuard],
        component: ViewedPagesModalComponent
      },
      {
        path: 'roles-definition',
        canActivate: [AuthGuard],
        component: RolesDefinitionComponent,
        resolve: { data: AppResolver }

      },
      {
        path: 'add-role',
        canActivate: [AuthGuard],
        component: AddRoleComponent
      },
      {
        path: 'organization-structure',
        canActivate: [AuthGuard],
        component: OrganizationStructureComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'add-organization-unit',
        canActivate: [AuthGuard],
        component: AddOrganizationUnitComponent
      },
      {
        path: 'login-logout-history',
        canActivate: [AuthGuard],
        component: LoginLogoutHistoryComponent
      },
      {
        path: 'login-on-behalf',
        canActivate: [AuthGuard],
        component: LoginOnBehalfComponent
      },
      {
        path: 'registration-request',
        canActivate: [AuthGuard],
        component: RegistrationRequestComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'review-registration-request/:id',
        canActivate: [AuthGuard],
        component: ReviewRegistrationRequestComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'tda-users-linked-role',
        canActivate: [AuthGuard],
        component: TdaUsersLinkedRoleComponent
      },
      {
        path: 'tda-users-linked-more-role',
        canActivate: [AuthGuard],
        component: TdaUsersLinkedMoreRoleComponent
      },
      {
        path: 'roles-definition-linked-users',
        canActivate: [AuthGuard],
        component: RolesDefinitionLinkedUsersComponent
      },
      {
        path: 'roles-definition-linked-more-users',
        canActivate: [AuthGuard],
        component: RolesDefinitionLinkedMoreUsersComponent
      },
      {
        path: 'public-holidays',
        canActivate: [AuthGuard],
        component: PublicHolidaysComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'add-public-holidays',
        canActivate: [AuthGuard],
        component: AddPublicHolidaysComponent
      },
      {
        path: 'edit-public-holidays/:id',
        canActivate: [AuthGuard],
        component: AddPublicHolidaysComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'my-leaves-management',
        canActivate: [AuthGuard],
        component: MyLeavesManagementComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'add-leaves',
        canActivate: [AuthGuard],
        component: AddLeavesComponent
      },
      {
        path: 'edit-leaves/:id',
        canActivate: [AuthGuard],
        component: AddLeavesComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'app-catalog-definition',
        canActivate: [AuthGuard],
        component: AppCatalogDefinitionComponent
      },
      {
        path: 'business-activity-groups',
        canActivate: [AuthGuard],
        component: BusinessActivityGroupsComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'add-business-activity-groups',
        canActivate: [AuthGuard],
        component: AddBusinessActivityGroupsComponent
      },
      {
        path: 'edit-business-activity-groups/:id',
        canActivate: [AuthGuard],
        component: AddBusinessActivityGroupsComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'business-activity-pages',
        canActivate: [AuthGuard],
        component: BusinessActivityPagesComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'add-business-activity-pages',
        canActivate: [AuthGuard],
        component: AddBusinessActivityPagesComponent
      },
      {
        path: 'edit-business-activity-pages/:id',
        canActivate: [AuthGuard],
        component: AddBusinessActivityPagesComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'business-license-parameters',
        canActivate: [AuthGuard],
        component: BusinessLicenseParametersComponent
      },
      {
        path: 'app-type-attachment',
        canActivate: [AuthGuard],
        component: AppTypeAttachmentComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'add-app-type-attachment',
        canActivate: [AuthGuard],
        component: AddAppTypeAttachmentComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'manage-nationalities',
        canActivate: [AuthGuard],
        component: ManageNationalitiesComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'add-nationalities',
        canActivate: [AuthGuard],
        component: AddNationalitiesComponent,
        resolve: { data: AppResolver }
      },

    ]),
  ],
  providers: [
    AppResolver,
    ModuleService,
    SubModuleService,
    PagesService,
    RoleService,
    JwtTokenService,
    UserService,
    BnCommonService,
    ConfigService,
    // AuthGuard,
  ]
})
export class AdminModule { }
