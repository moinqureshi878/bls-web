import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { timeLessThanWhen, dateLessThanWhenRequired } from '../../../core/validator/form-validators.service';
import { TranslationService } from '@translateService/translation.service';
import { CommonService, UserService } from '../../../core/appServices/index.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'kt-add-leaves',
  templateUrl: './add-leaves.component.html',
  styleUrls: ['./add-leaves.component.scss']
})
export class AddLeavesComponent implements OnInit {
  radioButtonCondition: number = 0;
  leaveType: Observable<any>;
  setLeave: number;
  leaveForm: FormGroup;
  loading: boolean = false;
  getLeaveData: any;
  id: number;
  constructor(
    public commonService: CommonService,
    public userService: UserService,
    private fb: FormBuilder,
    public translationService: TranslationService,
    public validationMessages: ValidationMessagesService,
    private router: Router,
    public toast: ToastrService,
    public route: ActivatedRoute,
    private cdr: ChangeDetectorRef) {
    this.getLeaveData = this.route.snapshot.data.data;
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      if (this.id) {
        this.InitializeForm();
        this.changeLeaveType(this.getLeaveData.typeId);

        this.leaveForm.patchValue({
          id: this.id,
          typeId: this.getLeaveData.typeId,
          fromDate: this.getLeaveData.fromDate,
          toDate: this.getLeaveData.toDate,
          purpose: this.getLeaveData.purpose,
          isActive: true,
          fromTime: new Date(this.getLeaveData.fromDate),
          toTime: new Date(this.getLeaveData.toDate),
          date: this.getLeaveData.fromDate,
        })
      }
      else {
        this.InitializeForm();
      }
    });
  }

  ngOnInit() {
    this.getLeaveType();
  }
  InitializeForm() {

    this.leaveForm = this.fb.group({
      id: [0],
      typeId: [],
      fromDate: [''],
      toDate: [''],
      fromTime: [''],
      toTime: [''],
      date: [''],
      purpose: ['', Validators.compose([Validators.required, Validators.maxLength(1000)])],
    }, {
      validators: [dateLessThanWhenRequired('fromDate', 'toDate'),
      timeLessThanWhen('fromTime', 'toTime')]
    });
  }
  changeLeaveType(typeID) {
    this.leaveForm.reset();
    setTimeout(() => {
      Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
        item.classList.remove("mat-form-field-invalid");
      });
    }, 100);
    this.setLeave = typeID;
    this.leaveForm.controls['typeId'].setValue(this.setLeave);
    if ((195 == typeID) || (196 == typeID)) {
      this.leaveForm.controls["fromDate"].setValidators(Validators.required);
      this.leaveForm.controls["toDate"].setValidators(Validators.required);
      this.leaveForm.controls["fromDate"].updateValueAndValidity();
      this.leaveForm.controls["toDate"].updateValueAndValidity();

      this.leaveForm.controls["fromTime"].setErrors(null);
      this.leaveForm.controls["toTime"].setErrors(null);
      this.leaveForm.controls["date"].setErrors(null);
    }
    else {
      this.leaveForm.controls["fromTime"].setValidators(Validators.required);
      this.leaveForm.controls["toTime"].setValidators(Validators.required);
      this.leaveForm.controls["date"].setValidators(Validators.required);
      this.leaveForm.controls["fromTime"].updateValueAndValidity();
      this.leaveForm.controls["toTime"].updateValueAndValidity();
      this.leaveForm.controls["date"].updateValueAndValidity();

      this.leaveForm.controls["fromDate"].setErrors(null);
      this.leaveForm.controls["toDate"].setErrors(null);
    }
  }
  getLeaveType() {
    this.leaveType = this.commonService.getLookUp("?type=LeaveType");
    this.leaveType.subscribe(res => {
      if (this.id) {
        this.setLeave = this.getLeaveData.typeId;
      }
      else {
        this.setLeave = res[0].id;
        this.changeLeaveType(this.setLeave);
      }
      this.leaveForm.controls['typeId'].setValue(this.setLeave);
    });
  }
  leaveFormSubmit() {
    if (this.leaveForm.valid) {
      this.leaveForm.value.isActive = true;
      this.leaveForm.value.id = this.id ? this.id : 0;
      if (197 == this.setLeave) {
        this.leaveForm.value.fromDate = this.dateDifference(this.leaveForm.value.date, this.leaveForm.value.fromTime);
        this.leaveForm.value.toDate = this.dateDifference(this.leaveForm.value.date, this.leaveForm.value.toTime);
        // JSON.stringify(this.leaveForm.value.fromTime).replace(JSON.stringify(this.leaveForm.value.date).substring(0, 11), JSON.stringify(this.leaveForm.value.fromTime).substring(0, 11))
      }
      this.leaveForm.value.fromDate = this.validationMessages.convertUTCDateToLocalDate(this.leaveForm.value.fromDate);
      this.leaveForm.value.toDate = this.validationMessages.convertUTCDateToLocalDate(this.leaveForm.value.toDate);

      //Api call
      this.loading = true;
      if (this.id) {
        this.userService.UpdateLeave(this.leaveForm.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigate(["/pages/admin/my-leaves-management"]);
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
      else {
        this.userService.AddLeave(this.leaveForm.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigate(["/pages/admin/my-leaves-management"]);
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.leaveForm);
    }
  }
  dateDifference(firstDate, secondDate) {
    var dt1 = new Date(firstDate);
    var dt2 = new Date(secondDate);
    var days = Math.floor((Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) - Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate())) / (1000 * 60 * 60 * 24));
    var updatedDate = this.addDays(dt2, days);
    return updatedDate;
  }
  addDays(theDate, days) {
    return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
  }
}
