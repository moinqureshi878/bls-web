
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from '@translateService/translation.service';
import { CommonService, BnCommonService } from '../../../core/appServices/index.service';
import { ToastrService } from 'ngx-toastr';

export interface UserData {
  fromDate: string;
  toDate: string;
  // titleAr: string;
  titleEn: string;
  edit: string;
  isActive: string;
}
/** Builds and returns a new User. */
const ELEMENT_DATA: UserData[] = [
  { fromDate: '', toDate: '', titleEn: '', edit: '', isActive: '' },
  { fromDate: '', toDate: '', titleEn: '', edit: '', isActive: '' },
  { fromDate: '', toDate: '', titleEn: '', edit: '', isActive: '' },
  { fromDate: '', toDate: '', titleEn: '', edit: '', isActive: '' },
  { fromDate: '', toDate: '', titleEn: '', edit: '', isActive: '' },
];

@Component({
  selector: 'kt-public-holidays',
  templateUrl: './public-holidays.component.html',
  styleUrls: ['./public-holidays.component.scss']
})
export class PublicHolidaysComponent implements OnInit {
  displayedColumns7: string[] = ['titleEn', 'fromDate', 'toDate', 'edit', 'isActive'];
  dataSource7 = new MatTableDataSource<UserData>(ELEMENT_DATA);
  closeResult: string; //Bootstrap Modal Popup
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  getHoliday: any;
  holidays: any;
  year: any;
  years: Number[] = [2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031, 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047, 2048, 2049, 2050, 2051, 2052, 2053, 2054, 2055, 2056, 2057, 2058, 2059, 2060, 2061, 2062, 2063, 2064, 2065, 2066, 2067, 2068, 2069, 2070, 2071, 2072, 2073, 2074, 2075, 2076, 2077, 2078, 2079, 2080, 2081, 2082, 2083, 2084, 2085, 2086, 2087, 2088, 2089, 2090, 2091, 2092, 2093, 2094, 2095, 2096, 2097, 2098, 2099];
  constructor(
    public translationService: TranslationService,
    public commonService: CommonService,
    private router: Router,
    public bnCommonService: BnCommonService,
    public toastrService: ToastrService,
    public route: ActivatedRoute) {
    debugger
    this.getHoliday = this.route.snapshot.data.data;
    this.holidays = this.getHoliday;
    this.dataSource7 = new MatTableDataSource(this.holidays);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }
  filterByYear(year) {
    debugger
    this.holidays = this.getHoliday;
    this.holidays = this.holidays.filter(obj => new Date(obj.fromDate).getFullYear() == year && new Date(obj.toDate).getFullYear() == year);
    this.dataSource7 = new MatTableDataSource(this.holidays);
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }

  onEditClick(id) {
    this.router.navigate(['/pages/admin/edit-public-holidays/' + id]);
  }

  onDelete(id: number) {
    this.bnCommonService.DeletePublicHoliday(id)
      .subscribe(data => {
        let response = data;
        if ((response.result && response.result.code == 200) || response.code == 200) {
          // find index of an element by id and splice from that array so it is better to bind grid again
          debugger
          let index = this.getHoliday.findIndex(x => x.id == id);
          this.getHoliday.splice(index, 1);
          this.dataSource7 = new MatTableDataSource(this.holidays);
          this.dataSource7.paginator = this.paginator7;
          this.dataSource7.sort = this.sort7;
        }
        else {
          this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
            this.toastrService.error(text);
          });
        }
      })
  }

}