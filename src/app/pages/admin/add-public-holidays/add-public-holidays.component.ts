import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { dateLessThanWhenRequired, nameArValidator } from '../../../core/validator/form-validators.service';
import { TranslationService } from '@translateService/translation.service';
import { CommonService, BnCommonService } from '../../../core/appServices/index.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';

@Component({
  selector: 'kt-add-public-holidays',
  templateUrl: './add-public-holidays.component.html',
  styleUrls: ['./add-public-holidays.component.scss']
})
export class AddPublicHolidaysComponent implements OnInit {
  holidayForm: FormGroup;
  loading: boolean = false;
  id: number;
  geHolidayData: any;
  holidays: Observable<any>;
  constructor(public commonService: CommonService,
    public bnCommonService: BnCommonService,
    private fb: FormBuilder,
    public translationService: TranslationService,
    public validationMessages: ValidationMessagesService,
    private router: Router,
    public toast: ToastrService,
    public route: ActivatedRoute,
    private cdr: ChangeDetectorRef) {
    debugger
    this.geHolidayData = this.route.snapshot.data.data;
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      debugger
      if (this.id) {
        this.InitializeForm();

        this.holidayForm.patchValue({
          id: this.id,
          fromDate: this.geHolidayData.fromDate,
          toDate: this.geHolidayData.toDate,
          titleId: this.geHolidayData.titleId,
          color: this.geHolidayData.color,
          isActive: this.geHolidayData.isActive,
          isDeleted: this.geHolidayData.isDeleted,
        })
      }
      else {
        this.InitializeForm();
      }
    });
  }

  ngOnInit() {
    this.holidays = this.commonService.getLookUp("?type=PublicHolidays");
  }

  InitializeForm() {
    this.holidayForm = this.fb.group({
      id: [0],
      fromDate: ['', Validators.required],
      toDate: ['', Validators.required],
      titleId: ['', Validators.compose([Validators.required])],
      color: ["red"],
      isActive: [true],
      isDeleted: [false],
    }, {
      validators: [dateLessThanWhenRequired('fromDate', 'toDate')]
    });
  }

  holidayFormSubmit() {
    if (this.holidayForm.valid) {

      this.holidayForm.value.fromDate = this.validationMessages.convertUTCDateToLocalDate(this.holidayForm.value.fromDate);
      this.holidayForm.value.toDate = this.validationMessages.convertUTCDateToLocalDate(this.holidayForm.value.toDate);

      //Api call
      this.loading = true;
      if (this.id) {
        this.bnCommonService.UpdatePublicHoliday(this.holidayForm.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigate(["/pages/admin/public-holidays"]);
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
      else {
        this.bnCommonService.AddPublicHoliday(this.holidayForm.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigate(["/pages/admin/public-holidays"]);
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.holidayForm);
    }
  }


}
