import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPublicHolidaysComponent } from './add-public-holidays.component';

describe('AddPublicHolidaysComponent', () => {
  let component: AddPublicHolidaysComponent;
  let fixture: ComponentFixture<AddPublicHolidaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPublicHolidaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPublicHolidaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
