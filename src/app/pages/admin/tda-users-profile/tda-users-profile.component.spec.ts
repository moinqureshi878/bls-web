import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TdaUsersProfileComponent } from './tda-users-profile.component';

describe('TdaUsersProfileComponent', () => {
  let component: TdaUsersProfileComponent;
  let fixture: ComponentFixture<TdaUsersProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TdaUsersProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TdaUsersProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
