
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TdaUsersLinkedRoleComponent } from '../tda-users-linked-role/tda-users-linked-role.component';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from '@translateService/translation.service';
import { UserService } from '../../../core/appServices/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'kt-tda-users-profile',
	templateUrl: './tda-users-profile.component.html',
	styleUrls: ['./tda-users-profile.component.scss']
})
export class TdaUsersProfileComponent implements OnInit {

	ELEMENT_DATA: any[] = [];
	displayedColumns7: string[] = ['fullNameAr', 'fullNameEn', 'userName', 'designation', 'businessUnit', 'linkedUser', 'editView', 'isActive'];
	dataSource7 = new MatTableDataSource<any>(this.ELEMENT_DATA);
	closeResult: string; //Bootstrap Modal Popup

	@ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;

	@ViewChild(MatSort, { static: true }) sort7: MatSort;
	userList: any;
	constructor(
		public dialog: MatDialog,
		private route: ActivatedRoute,
		private router: Router,
		private translationService: TranslationService,
		private userService: UserService,
		public toastrService: ToastrService,
		private cdr: ChangeDetectorRef,
	) {
	}
	ngOnInit() {
		this.userList = this.route.snapshot.data.data;
		this.dataSource7 = new MatTableDataSource<any>(this.userList);
		this.dataSource7.paginator = this.paginator7;
		this.dataSource7.sort = this.sort7;
	}
	applyFilter7(filterValue: string) {
		this.dataSource7.filter = filterValue.trim().toLowerCase();
		if (this.dataSource7.paginator) {
			this.dataSource7.paginator.firstPage();
		}
	}

	openLinkedUserRoleDialog(userID, row) {
		row.loadingLinkedUser = true;
		const dialogConfig = new MatDialogConfig();
		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.minWidth = "60%";

		this.userService.getLinkedRoles(userID).subscribe(res => {
			let response = res;
			if (response.code == 200 || response.result.code == 200) {
				let roleList = response.data;
				dialogConfig.data = { roleList, userID };
				row.loadingLinkedUser = false;
				this.cdr.markForCheck();
				this.dialog.open(TdaUsersLinkedRoleComponent, dialogConfig);
			}
			else {
				row.loadingLinkedUser = false;
				this.cdr.markForCheck();
				this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
					this.toastrService.error(text);
				});
			}
		})
	}

	onEditClick(userId) {
		this.router.navigate(['/pages/admin/edit-user/' + userId]);
	}

	CheckboxChange(id, event) {
		this.changeDataSource(id);
		
		this.userService.ActiveDeactiveUsers(id).subscribe(data => {
			let response = data;
			if ((response.result && response.result.code == 200) || response.code == 200) {
				
			}
			else {
				this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
					this.toastrService.error(text);
				});
			}
		});
	}
	changeDataSource(ID: number) {
		
		let index = this.userList.findIndex(x => x.userId == ID);
		let isActive: boolean = this.userList[index].isActive == true ? false : true;
		this.userList[index].isActive = isActive;
	}

}
