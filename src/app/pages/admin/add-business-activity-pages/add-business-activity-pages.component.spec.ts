import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBusinessActivityPagesComponent } from './add-business-activity-pages.component';

describe('AddBusinessActivityPagesComponent', () => {
  let component: AddBusinessActivityPagesComponent;
  let fixture: ComponentFixture<AddBusinessActivityPagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBusinessActivityPagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBusinessActivityPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
