import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BnCommonService, CommonService } from '../../../core/appServices/index.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { TranslationService } from '@translateService/translation.service';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { alphaNumeric, nameArValidator } from 'app/core/validator/form-validators.service';
@Component({
  selector: 'kt-add-business-activity-pages',
  templateUrl: './add-business-activity-pages.component.html',
  styleUrls: ['./add-business-activity-pages.component.scss']
})
export class AddBusinessActivityPagesComponent implements OnInit {
  businessActivityPages: FormGroup;
  id: number;
  getBusinessPages: any;
  loading: boolean = false;
  getActiveBusinessActivity: Observable<any>;
  constructor(public bnCommonService: BnCommonService,
    public commonService: CommonService,
    private fb: FormBuilder,
    public translationService: TranslationService,
    public validationMessages: ValidationMessagesService,
    private router: Router,
    public toast: ToastrService,
    public route: ActivatedRoute,
    private cdr: ChangeDetectorRef) {
    this.getBusinessPages = this.route.snapshot.data.data;
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      if (this.id) {
        this.InitializeForm();
        this.businessActivityPages.patchValue(this.getBusinessPages);
      }
      else {
        this.InitializeForm();
      }
    });
  }

  ngOnInit() {
    this.getActiveBusinessActivityFunction();
  }


  InitializeForm() {
    debugger
    this.businessActivityPages = this.fb.group({
      "id": [0, Validators.compose([Validators.required])],
      "bagid": [0, Validators.compose([Validators.required])],
      "isic4": ['', Validators.compose([Validators.required, Validators.maxLength(100), alphaNumeric])],
      "descriptionEn": ['', Validators.compose([Validators.required, Validators.maxLength(2500)])],
      "descriptionAr": ['', Validators.compose([Validators.required, Validators.maxLength(2500), nameArValidator])],
      "isActive": [true],
      "activityUsageEn": ['', Validators.compose([Validators.required, Validators.maxLength(2500)])],
      "activityUsageAr": ['', Validators.compose([Validators.required, Validators.maxLength(2500), nameArValidator])],
    });
  }
  businessActivityPagesFormSubmit() {
    if (this.businessActivityPages.valid) {
      //Api call
      this.loading = true;
      if (this.id) {
        this.bnCommonService.UpdateBusinessActivityPages(this.businessActivityPages.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigate(["/pages/admin/business-activity-pages"]);
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
      else {
        this.bnCommonService.AddBusinessActivityPages(this.businessActivityPages.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigate(["/pages/admin/business-activity-pages"]);
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.businessActivityPages);
    }
  }
  getActiveBusinessActivityFunction() {
    debugger


    this.getActiveBusinessActivity = this.commonService.getLookUp("?type=BAGroups");  //this.bnCommonService.GetActiveBusinessActivity().pipe(map(resp => resp["data"]));
    this.getActiveBusinessActivity.subscribe(res => {
      if (!this.id) {
        debugger
        if (res && res.length > 0) {
          this.businessActivityPages.controls['bagid'].setValue(res[0].id);
          this.cdr.markForCheck();
        }
      }
      else {
        this.businessActivityPages.controls['bagid'].setValue(this.getBusinessPages.bagid);
      }
    });
  }
}
