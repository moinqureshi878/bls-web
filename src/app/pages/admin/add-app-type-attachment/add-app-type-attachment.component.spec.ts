import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAppTypeAttachmentComponent } from './add-app-type-attachment.component';

describe('AddAppTypeAttachmentComponent', () => {
  let component: AddAppTypeAttachmentComponent;
  let fixture: ComponentFixture<AddAppTypeAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAppTypeAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAppTypeAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
