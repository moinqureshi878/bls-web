
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TdaUsersLinkedRoleComponent } from '../tda-users-linked-role/tda-users-linked-role.component';
import { CommonService } from 'app/core/appServices/index.service';
import { Observable } from 'rxjs';
import { TranslationService } from 'app/core/_base/layout';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from 'app/core/appServices/config.service';
import { ToastrService } from 'ngx-toastr';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';



@Component({
  selector: 'kt-add-app-type-attachment',
  templateUrl: './add-app-type-attachment.component.html',
  styleUrls: ['./add-app-type-attachment.component.scss']
})
export class AddAppTypeAttachmentComponent implements OnInit {

  ELEMENT_DATA: any[] = [];

  ApplicationTypeddl: Observable<any>;
  addAppTypeAttachmentFormGroup: FormGroup;
  AddAppTypeAttachmentGrid: any;

  displayedColumns7: string[] = ['checkBoxes', 'valueAr', 'valueEn', 'isMandotary', 'inputData', 'isActive'];
  dataSource7 = new MatTableDataSource(this.ELEMENT_DATA);

  loading: boolean = false;

  closeResult: string; //Bootstrap Modal Popup

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild('sort7', { static: true }) sort7: MatSort;
  constructor(
    public dialog: MatDialog,
    private commonService: CommonService,
    private translationService: TranslationService,
    public configService: ConfigService,
    private fb: FormBuilder,
    public route: ActivatedRoute,
    private toastrService: ToastrService,
    public router: Router,
    public validationMessages: ValidationMessagesService,
  ) {
    debugger
    this.AddAppTypeAttachmentGrid = this.route.snapshot.data.data;
    this.dataSource7 = new MatTableDataSource(this.AddAppTypeAttachmentGrid);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
    this.getApplicationType();
    this.createformAddAppTypeAttachment();
  }

  ApplicationTypeddlChange() {
    this.configService.getLookupDetailByID(this.addAppTypeAttachmentFormGroup.value.applicationTypeddl.id, "attachments")
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.AddAppTypeAttachmentGrid = data["data"];
          this.dataSource7 = new MatTableDataSource(this.AddAppTypeAttachmentGrid);
          this.dataSource7.paginator = this.paginator7;
          this.dataSource7.sort = this.sort7;
        }
        else {
          this.loading = false;
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toastrService.success(text);
          });
        }
      })
  }

  createformAddAppTypeAttachment() {
    this.addAppTypeAttachmentFormGroup = this.fb.group({
      applicationTypeddl: ['', Validators.compose([Validators.required])]
    });
  }

  getApplicationType() {
    this.ApplicationTypeddl = this.commonService.getLookUp("?type=ApplicationTypes");
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();

    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  submitAddAppTypeAttachments() {
    debugger
    if (this.addAppTypeAttachmentFormGroup.valid) {
      let AppTypeAttachmentSelected: [] = this.AddAppTypeAttachmentGrid.filter(x => x.isExist == true);
      if (AppTypeAttachmentSelected.length > 0) {

        let model = AppTypeAttachmentSelected.map(item => {
          return {
            "data1": this.addAppTypeAttachmentFormGroup.value.applicationTypeddl.id,
            "data2": item["id"],
            "data3": 0,
            "type1": this.addAppTypeAttachmentFormGroup.value.applicationTypeddl.headID,
            "type2": item["headID"],
            "type3": 0,
            "isMandatory": item["isMandatory"] == true ? true : false,
            "inputData": item["inputData"] == true ? true : false,
            "isActive": item["isActive"] == true ? true : false
          }
        })

        this.configService.addAppTypeAttachment(model)
          .subscribe(data => {
            if (data["result"].code === 200) {
              this.loading = false;
              this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
                this.toastrService.success(text);
              });
              this.router.navigateByUrl('/pages/admin/app-type-attachment');
            }
            else {
              this.loading = false;
              this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
                this.toastrService.error(text);
              });
            }
          })
      }
      else {
        this.translationService.getTranslation('VALIDATIONMESSAGES.ATTACHMENTREQUIRED').subscribe((text: string) => {
          this.toastrService.error(text);
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.addAppTypeAttachmentFormGroup);
    }
  }

  checkBoxChange(...value) {
    debugger
    let index = this.AddAppTypeAttachmentGrid.findIndex(x => x.id == value[0]);
    this.AddAppTypeAttachmentGrid[index].isExist = value[1];
    this.dataSource7 = new MatTableDataSource(this.AddAppTypeAttachmentGrid);
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }

  isMandatory(...value) {
    let index = this.AddAppTypeAttachmentGrid.findIndex(x => x.id == value[0]);
    this.AddAppTypeAttachmentGrid[index].isMandatory = value[1];
  }

  inputdata(...value) {
    let index = this.AddAppTypeAttachmentGrid.findIndex(x => x.id == value[0]);
    this.AddAppTypeAttachmentGrid[index].inputData = value[1];
  }

  isActive(...value) {
    let index = this.AddAppTypeAttachmentGrid.findIndex(x => x.id == value[0]);
    this.AddAppTypeAttachmentGrid[index].isActive = value[1];
  }

}