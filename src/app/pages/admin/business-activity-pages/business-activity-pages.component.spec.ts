import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessActivityPagesComponent } from './business-activity-pages.component';

describe('BusinessActivityPagesComponent', () => {
  let component: BusinessActivityPagesComponent;
  let fixture: ComponentFixture<BusinessActivityPagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessActivityPagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessActivityPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
