

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog } from '@angular/material';
import { CommonService, BnCommonService } from '../../../core/appServices/index.service';
import { TranslationService } from '@translateService/translation.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

export interface UserData {
  isic4: string;
  descriptionAr: string;
  descriptionEn: string;
  activityGroup: string;
  isActive: string;
  edit: string;
}

/** Builds and returns a new User. */

const ELEMENT_DATA: UserData[] = [
  { isic4: '', descriptionAr: '', descriptionEn: '', activityGroup: '', edit: '', isActive: '' },
  { isic4: '', descriptionAr: '', descriptionEn: '', activityGroup: '', edit: '', isActive: '' },
  { isic4: '', descriptionAr: '', descriptionEn: '', activityGroup: '', edit: '', isActive: '' },
  { isic4: '', descriptionAr: '', descriptionEn: '', activityGroup: '', edit: '', isActive: '' },
  { isic4: '', descriptionAr: '', descriptionEn: '', activityGroup: '', edit: '', isActive: '' },
];


@Component({
  selector: 'kt-business-activity-pages',
  templateUrl: './business-activity-pages.component.html',
  styleUrls: ['./business-activity-pages.component.scss']
})
export class BusinessActivityPagesComponent implements OnInit {
  displayedColumns7: string[] = ['isic4', 'descriptionAr', 'descriptionEn', 'activityGroup', 'edit', 'isActive'];
  dataSource7 = new MatTableDataSource<UserData>(ELEMENT_DATA);
  closeResult: string; //Bootstrap Modal Popup
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  getBusinessActivityPages: any;
  constructor(public dialog: MatDialog,
    public translationService: TranslationService,
    public commonService: CommonService,
    public bnCommonService: BnCommonService,
    public toast: ToastrService,
    public route: ActivatedRoute) {
    this.getBusinessActivityPages = this.route.snapshot.data.data;
    this.dataSource7 = new MatTableDataSource(this.getBusinessActivityPages);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }
  isActiveBusinessPages(id) {
    this.changeDataSource(id);
    this.bnCommonService.DeleteBusinessActivityPages(id).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {

      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toast.error(text);
        });
      }
    });
  }
  changeDataSource(ID: number) {
    let index = this.getBusinessActivityPages.findIndex(x => x.id == ID);
    let isActive: boolean = this.getBusinessActivityPages[index].isActive == true ? false : true;
    this.getBusinessActivityPages[index].isActive = isActive;
  }
  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }
}