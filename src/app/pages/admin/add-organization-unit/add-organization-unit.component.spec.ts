import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrganizationUnitComponent } from './add-organization-unit.component';

describe('AddOrganizationUnitComponent', () => {
  let component: AddOrganizationUnitComponent;
  let fixture: ComponentFixture<AddOrganizationUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOrganizationUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOrganizationUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
