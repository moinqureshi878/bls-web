import { Component, OnInit } from '@angular/core';
import { TranslationService } from '@translateService/translation.service';

@Component({
  selector: 'kt-add-organization-unit',
  templateUrl: './add-organization-unit.component.html',
  styleUrls: ['./add-organization-unit.component.scss']
})
export class AddOrganizationUnitComponent implements OnInit {

  constructor(private translationService: TranslationService,
  ) { }

  ngOnInit() {
  }

}
