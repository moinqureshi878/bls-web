import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageNationalitiesComponent } from './manage-nationalities.component';

describe('ManageNationalitiesComponent', () => {
  let component: ManageNationalitiesComponent;
  let fixture: ComponentFixture<ManageNationalitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageNationalitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageNationalitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
