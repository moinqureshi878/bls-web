
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TdaUsersLinkedRoleComponent } from '../tda-users-linked-role/tda-users-linked-role.component';
import { ActivatedRoute } from '@angular/router';
import { ConfigService } from 'app/core/appServices/config.service';
import { TranslationService } from 'app/core/_base/layout';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'kt-manage-nationalities',
  templateUrl: './manage-nationalities.component.html',
  styleUrls: ['./manage-nationalities.component.scss']
})
export class ManageNationalitiesComponent implements OnInit {

  ELEMENT_DATA: any[] = [];

  nationalitiesGrid: [] = [];

  displayedColumns7: string[] = ['countries', 'nationalityAr', 'nationalityEn', 'isActive'];
  dataSource7 = new MatTableDataSource(this.ELEMENT_DATA);



  closeResult: string; //Bootstrap Modal Popup

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  constructor(
    public dialog: MatDialog,
    public route: ActivatedRoute,
    public configService: ConfigService,
    private translationService: TranslationService,
    private toastrService: ToastrService
  ) {
    debugger
    this.nationalitiesGrid = this.route.snapshot.data.data;
    this.dataSource7 = new MatTableDataSource(this.nationalitiesGrid);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();

    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  isActiveChanged(...value) {
    let model = {
      "id": value[0],
      "isMandatory": null,
      "inputData": null,
      "isActive": value[1]
    }

    this.configService.editManageApplicationTypeAttachment(model)
      .subscribe(data => {
        if (data["result"].code === 200) {

        }
        else {
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toastrService.success(text);
          });
        }
      })

  }

}