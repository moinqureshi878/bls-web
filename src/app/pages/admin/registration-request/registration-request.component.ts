
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { UserService } from '../../../core/appServices/user.service';
import { ActivatedRoute } from '@angular/router';
import { TranslationService } from '@translateService/translation.service';
import { ToastrService } from 'ngx-toastr';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';

@Component({
  selector: 'kt-registration-request',
  templateUrl: './registration-request.component.html',
  styleUrls: ['./registration-request.component.scss']
})
export class RegistrationRequestComponent implements OnInit {

  displayedColumns7: string[] = ['refNo', 'applicationType', 'userName', 'createdAt', 'userEmail', 'mobileNo', 'view', 'isActive'];
  dataSource7 = new MatTableDataSource();

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  getRegisteredCustomers: any;
  constructor(public userService: UserService, public route: ActivatedRoute,
    public validationMessages: ValidationMessagesService,
    public translateService: TranslationService,
    public toastrService: ToastrService) {

    this.getRegisteredCustomers = this.route.snapshot.data.data;
    this.dataSource7 = new MatTableDataSource(this.getRegisteredCustomers);
   
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }
  isActiveUser(id) {
    this.changeDataSource(id);
    
    this.userService.ActiveDeactiveUsers(id).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {
        
      }
      else {
        this.translateService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toastrService.error(text);
        });
      }
    });
  }
  changeDataSource(ID: number) {
    let index = this.getRegisteredCustomers.findIndex(x => x.userId == ID);
    let isActive: boolean = this.getRegisteredCustomers[index].isActive == true ? false : true;
    this.getRegisteredCustomers[index].isActive = isActive;
  }
  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }
}
