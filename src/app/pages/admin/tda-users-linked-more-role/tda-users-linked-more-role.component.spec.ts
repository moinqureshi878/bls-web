import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TdaUsersLinkedMoreRoleComponent } from './tda-users-linked-more-role.component';

describe('TdaUsersLinkedMoreRoleComponent', () => {
  let component: TdaUsersLinkedMoreRoleComponent;
  let fixture: ComponentFixture<TdaUsersLinkedMoreRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TdaUsersLinkedMoreRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TdaUsersLinkedMoreRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
