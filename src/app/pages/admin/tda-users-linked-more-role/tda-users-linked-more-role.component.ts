
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RoleService } from '../../../core/appServices/role.service';
import { TranslationService } from '@translateService/translation.service';

@Component({
	selector: 'kt-tda-users-linked-more-role',
	templateUrl: './tda-users-linked-more-role.component.html',
	styleUrls: ['./tda-users-linked-more-role.component.scss']
})
export class TdaUsersLinkedMoreRoleComponent implements OnInit {

	userID: number;
	assignUserRole = [];
	loading: boolean = false;
	/* Step Three */
	displayedColumnsstepThree: string[] = ['select', 'nameAr', 'nameEn'];
	stepThree = new MatTableDataSource<any>();
	@ViewChild('matPaginatorstepThree', { static: true }) paginatorstepThree: MatPaginator;
	@ViewChild(MatSort, { static: true }) sortStepThree: MatSort;

	constructor(
		@Inject(MAT_DIALOG_DATA) data,
		private dialogRef: MatDialogRef<TdaUsersLinkedMoreRoleComponent>,
		private translationService: TranslationService,
		private roleService: RoleService
	) {
		this.userID = data.UserID;
		this.stepThree = new MatTableDataSource<any>(data.unlinkedroleList)
	};

	ngOnInit() {
		/* Step Three */
		this.stepThree.paginator = this.paginatorstepThree;
		this.stepThree.sort = this.sortStepThree;

	}

	applyFilterstepThree(filterValue: string) {
		this.stepThree.filter = filterValue.trim().toLowerCase();

		if (this.stepThree.paginator) {
			this.stepThree.paginator.firstPage();
		}
	}


	linkMoreUser(roleID, isActive) {
		let setUserRole = {
			"roleId": roleID,
			"userId": this.userID,
			"isActive": isActive
		}
		let updateValue = true;
		if (this.assignUserRole.length > 0) {
			for (let index = 0; index < this.assignUserRole.length; index++) {
				if ((this.assignUserRole[index].roleId == roleID) && (this.assignUserRole[index].isActive == true)) {
					this.assignUserRole.splice(index, 1);
					updateValue = false;
				}
			}
		}
		if (updateValue) {
			this.assignUserRole.push(setUserRole);
		}
	}

	confirmSelected() {
		this.loading = true;
		if (this.assignUserRole.length > 0) {
			this.roleService.AssignRole(this.assignUserRole).subscribe(res => {
				this.loading = false;
				let response = res;
				if (response.result.code == 200 || response.code == 200) {
					this.dialogRef.close();
				}
			});
		}
		else {
			this.loading = false;
			this.dialogRef.close();
		}
	}
}
