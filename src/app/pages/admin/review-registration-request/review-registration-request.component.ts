import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ReviewAttachmentModalComponent } from '../review-attachment-modal/review-attachment-modal.component';
import { ActivatedRoute } from '@angular/router';
import { emiratesIDMasking, phoneNumberMasking } from '../../../core/validator/form-validators.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { CommonService } from '../../../core/appServices/index.service';
import { TranslationService } from '@translateService/translation.service';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { UserTypeEnum } from '../../../core/_enum/index.enum';


@Component({
  selector: 'kt-review-registration-request',
  templateUrl: './review-registration-request.component.html',
  styleUrls: ['./review-registration-request.component.scss']
})
export class ReviewRegistrationRequestComponent implements OnInit {
  closeResult: string; //Bootstrap Modal Popup

  //Stepper active class
  stepperValue: number = 1;

  //Form Group Stepper
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  userDetail: any;
  /* Emirates ID Mask */
  emiratesIDMask = emiratesIDMasking();
  /* Mobile No Mask */
  mobileNoMask = phoneNumberMasking();

  typeNationalities: Observable<any>;
  attachmentUrl: any;
  emirates: Observable<any>;
  mainArea: Observable<any>;
  userTypeEnum = UserTypeEnum;
  constructor(private _formBuilder: FormBuilder,
    public dialog: MatDialog,
    public route: ActivatedRoute,
    private modalService: NgbModal,
    public commonService: CommonService,
    public validationMessages: ValidationMessagesService,
    public translationService: TranslationService,
  ) {
    this.userDetail = this.route.snapshot.data.data;
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      // firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      // secondCtrl: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      // fourthCtrl: ['', Validators.required]
    });
    this.fifthFormGroup = this._formBuilder.group({
      // fifthCtrl: ['', Validators.required]
    });
    this.sixthFormGroup = this._formBuilder.group({
      // fifthCtrl: ['', Validators.required]
    });
    this.getNationalities();
    this.getEmirates();
    this.getMainArea();
  }

  getNationalities() {
    this.typeNationalities = this.commonService.getLookUp("?type=Nationality");
  }
  getEmirates() {
    this.emirates = this.commonService.getLookUp("?type=Emirates");
  }
  getMainArea() {
    this.mainArea = this.commonService.getLookUp("?type=MainAreas");
  }
  //Stepper Value
  setStepperValue(value: number) {
    this.stepperValue = value;
  }


  //..Modal Popup Start
  open(content, attachment) {
    this.attachmentUrl = attachment.attachmentUrl;
    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End
}