import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewRegistrationRequestComponent } from './review-registration-request.component';

describe('ReviewRegistrationRequestComponent', () => {
  let component: ReviewRegistrationRequestComponent;
  let fixture: ComponentFixture<ReviewRegistrationRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewRegistrationRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewRegistrationRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
