
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog } from '@angular/material';
import { CommonService, BnCommonService } from '../../../core/appServices/index.service';
import { TranslationService } from '@translateService/translation.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
export interface UserData {
  code: string;
  descriptionAr: string;
  descriptionEn: string;
  isActive: string;
  edit: string;

}
/** Builds and returns a new User. */
const ELEMENT_DATA: UserData[] = [
  { code: '', descriptionAr: '', descriptionEn: '', edit: '', isActive: '' },
  { code: '', descriptionAr: '', descriptionEn: '', edit: '', isActive: '' },
  { code: '', descriptionAr: '', descriptionEn: '', edit: '', isActive: '' },
  { code: '', descriptionAr: '', descriptionEn: '', edit: '', isActive: '' },
  { code: '', descriptionAr: '', descriptionEn: '', edit: '', isActive: '' },
];

@Component({
  selector: 'kt-business-activity-groups',
  templateUrl: './business-activity-groups.component.html',
  styleUrls: ['./business-activity-groups.component.scss']
})
export class BusinessActivityGroupsComponent implements OnInit {
  displayedColumns7: string[] = ['code', 'descriptionAr', 'descriptionEn', 'edit', 'isActive'];
  dataSource7 = new MatTableDataSource(ELEMENT_DATA);
  closeResult: string; //Bootstrap Modal Popup
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  getBusinessActivity: any;
  constructor(public dialog: MatDialog,
    public translationService: TranslationService,
    public commonService: CommonService,
    public bnCommonService: BnCommonService,
    public toast: ToastrService,
    public route: ActivatedRoute) {
    this.getBusinessActivity = this.route.snapshot.data.data;
    this.dataSource7 = new MatTableDataSource(this.getBusinessActivity);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }
  isActiveBusinessGroup(id) {
    this.changeDataSource(id);
    this.bnCommonService.DeleteBusinessActivity(id).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {

      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toast.error(text);
        });
      }
    });
  }
  changeDataSource(ID: number) {
    let index = this.getBusinessActivity.findIndex(x => x.id == ID);
    let isActive: boolean = this.getBusinessActivity[index].isActive == true ? false : true;
    this.getBusinessActivity[index].isActive = isActive;
  }
  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }
}