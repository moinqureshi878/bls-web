import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessActivityGroupsComponent } from './business-activity-groups.component';

describe('BusinessActivityGroupsComponent', () => {
  let component: BusinessActivityGroupsComponent;
  let fixture: ComponentFixture<BusinessActivityGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessActivityGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessActivityGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
