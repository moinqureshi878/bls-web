
import { Component, OnInit, ViewChild, Injectable, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, PageEvent } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { Observable } from 'rxjs';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { dateLessThan, timeLessThan, nameValidator, phoneNumberValidator, phoneNumberMasking } from '../../../core/validator/form-validators.service';
import { RoleService } from '../../../core/appServices/role.service';
import { TranslationService } from '@translateService/translation.service';
import { CommonService, AuthenticationService, UserService } from '../../../core/appServices/index.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ViewedPagesModalComponent } from '../viewed-pages-modal/viewed-pages-modal.component';
import { SessionTypeEnum } from '../../../core/_enum/index.enum';

/** Online Users */
export interface OuData {
	userType: string;
	userName: string;
	sessionType: string;
	dateTime: string;
	pages: string;
}
const ELEMENT_OU_DATA: OuData[] = [
	{ userType: 'A', userName: 'a', sessionType: 'Logout Timeout', dateTime: '25/8/2019 9:00AM', pages: '' },
];

/** Government Entity */
export interface GeData {
	department: string;
	userName: string;
	sessionType: string;
	dateTime: string;
	pages: string;
}
const ELEMENT_GE_DATA: GeData[] = [
	{ department: 'A', userName: 'a', sessionType: 'Logout Timeout', dateTime: '25/8/2019 9:00AM', pages: '' },
];

/** Pages */
export interface PagesData {

	pageName: string;
	visitTime: string;
}
const ELEMENT_PAGES_DATA: PagesData[] = [
	{ pageName: 'A', visitTime: '08:00AM' },
];

@Component({
	selector: 'kt-login-logout-history',
	templateUrl: './login-logout-history.component.html',
	styleUrls: ['./login-logout-history.component.scss'],
})
export class LoginLogoutHistoryComponent implements OnInit {
	closeResult: string; //Bootstrap Modal Popup
	/*TDA Staff*/
	displayedColumns7: string[] = ['fullNameEn', 'usernameEn', 'sessionTypeEn', 'userTypeEn', 'dateTime', 'ip', 'sessionHistoryDetail'];
	dataSource7 = new MatTableDataSource();
	@ViewChild('matPaginator7', { static: false }) paginator7: MatPaginator;
	@ViewChild(MatSort, { static: true }) sort7: MatSort;

	/*Online Users*/
	displayedColumnsOU: string[] = ['userTypeEn', 'usernameEn', 'sessionTypeEn', 'dateTime', 'sessionHistoryDetail'];
	dataSourceOU = new MatTableDataSource;
	@ViewChild('matPaginatorOU', { static: false }) paginatorOU: MatPaginator;
	@ViewChild(MatSort, { static: true }) sortOU: MatSort;

	/*Government Entity*/
	displayedColumnsGE: string[] = ['govtNameEn', 'usernameEn', 'sessionTypeEn', 'dateTime', 'sessionHistoryDetail'];
	dataSourceGE = new MatTableDataSource();
	@ViewChild('matPaginatorGE', { static: false }) paginatorGE: MatPaginator;
	@ViewChild(MatSort, { static: true }) sortGE: MatSort;

	//For Pagination
	pageSize: number = 0;
	pageNumber: number;
	totalItems: number;
	pageEvent: PageEvent;

	TDAStaffLoginLogoutHistory: FormGroup;
	onlineUsersLoginLogoutHistory: FormGroup;
	govtEntityLoginLogoutHistory: FormGroup;
	postRolePermission = {
		"pageIds": [
		]
	}
	pageNames = [];
	typeSession: Observable<any>;
	typeBN: Observable<any>;
	typeOnlineUser: Observable<any>;
	typeGovtEntity: any;
	getUserResponse: any;
	tabIndex: number = 0;
	/* Mobile No Mask */
	mobileNoMask = phoneNumberMasking();
	resetForm = {
		"employeeName": "",
		"username": "",
		"mobileNo": "",
		"pageNo": [],
		"sessionType": [],
		"onBehalfUsername": false,
		"fromDate": "",
		"toDate": "",
		"fromTime": "",
		"toTime": "",
		"customerMobileNo": "",
		"beneficiaryType": [],
		"userType": [],
		"govtEntity": [0],
		"type": 0
	};
	loading: boolean = false;
	SessionTypeEnum = SessionTypeEnum;
	constructor(
		private modalService: NgbModal,
		public roleService: RoleService,
		public commonService: CommonService,
		public userService: UserService,
		private fb: FormBuilder,
		public translationService: TranslationService,
		public validationMessages: ValidationMessagesService,
		public authenticationService: AuthenticationService,
		private cdr: ChangeDetectorRef,
		public dialog: MatDialog, ) {
		this.GetAccessRightsTree();
		this.getUserData(1);
		// this.getUserData(136);
		this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel, this.isExpandable, this.getChildren);
		this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
		this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

	}
	ngOnInit() {
		/* TDA Staff */
		this.dataSource7.paginator = this.paginator7;
		this.dataSource7.sort = this.sort7;

		this.TdaStaffForm();
		this.OnlineUsersForm();
		this.GovtEntityForm();

		this.getSessionType();
		this.getBeneficiaryType();
		this.getOnlineuserType();
		this.getGovtEntity();
	}

	TdaStaffForm() {
		this.TDAStaffLoginLogoutHistory = this.fb.group({
			employeeName: ['', Validators.compose([nameValidator])],
			username: ['', Validators.compose([nameValidator])],
			mobileNo: ['', Validators.compose([phoneNumberValidator])],
			pageNo: [new Array],
			sessionType: [new Array],
			onBehalfUsername: [false],
			customerMobileNo: [{ value: '', disabled: true }, Validators.compose([phoneNumberValidator])],
			beneficiaryType: [{ value: new Array, disabled: true }],
			fromDate: [''],
			toDate: [''],
			fromTime: [''],
			toTime: [''],
		}, {
			validators: [dateLessThan('fromDate', 'toDate'),
			timeLessThan('fromTime', 'toTime')]
		});

		this.TDAStaffLoginLogoutHistory.controls['onBehalfUsername'].valueChanges.subscribe(value => {
			if (value) {
				this.TDAStaffLoginLogoutHistory.controls['customerMobileNo'].enable();
				this.TDAStaffLoginLogoutHistory.controls['beneficiaryType'].enable();
			}
			else {
				this.TDAStaffLoginLogoutHistory.controls['customerMobileNo'].disable();
				this.TDAStaffLoginLogoutHistory.controls['beneficiaryType'].disable();
			}
		});
	}
	OnlineUsersForm() {
		this.onlineUsersLoginLogoutHistory = this.fb.group({
			userType: [''],
			username: ['', Validators.compose([nameValidator])],
			mobileNo: ['', Validators.compose([phoneNumberValidator])],
			pageNo: [new Array],
			sessionType: [new Array],
			fromDate: [''],
			toDate: [''],
			fromTime: [''],
			toTime: [''],
		}, {
			validators: [dateLessThan('fromDate', 'toDate'),
			timeLessThan('fromTime', 'toTime')]
		});
	}
	GovtEntityForm() {
		this.govtEntityLoginLogoutHistory = this.fb.group({
			govtEntity: [new Array],
			username: ['', Validators.compose([nameValidator])],
			mobileNo: ['', Validators.compose([phoneNumberValidator])],
			pageNo: [new Array],
			sessionType: [new Array],
			fromDate: [''],
			toDate: [''],
			fromTime: [''],
			toTime: [''],
		}, {
			validators: [dateLessThan('fromDate', 'toDate'),
			timeLessThan('fromTime', 'toTime')]
		});

	}
	TDAStaffLoginLogoutHistorySubmit() {
		if (this.TDAStaffLoginLogoutHistory.value.toDate || this.TDAStaffLoginLogoutHistory.value.fromDate) {
			this.TDAStaffLoginLogoutHistory.controls["toDate"].setValidators(Validators.required);
			this.TDAStaffLoginLogoutHistory.controls["fromDate"].setValidators(Validators.required);

			this.TDAStaffLoginLogoutHistory.controls["toDate"].updateValueAndValidity();
			this.TDAStaffLoginLogoutHistory.controls["fromDate"].updateValueAndValidity();
		}
		if (this.TDAStaffLoginLogoutHistory.value.fromTime || this.TDAStaffLoginLogoutHistory.value.toTime) {
			this.TDAStaffLoginLogoutHistory.controls["fromTime"].setValidators(Validators.required);
			this.TDAStaffLoginLogoutHistory.controls["toTime"].setValidators(Validators.required);

			this.TDAStaffLoginLogoutHistory.controls["fromTime"].updateValueAndValidity();
			this.TDAStaffLoginLogoutHistory.controls["toTime"].updateValueAndValidity();
		}
		if (this.TDAStaffLoginLogoutHistory.valid) {
			if (!this.TDAStaffLoginLogoutHistory.value.onBehalfUsername) {
				this.TDAStaffLoginLogoutHistory.value.customerMobileNo = '';
				this.TDAStaffLoginLogoutHistory.value.beneficiaryType = [];
			}
			//Api call
			for (let [key, formValue] of Object.entries(this.TDAStaffLoginLogoutHistory.value)) {
				let value = formValue;
				if (value != null && value != '' || (value instanceof Array && value.length > 0)) {
					this.TDAStaffLoginLogoutHistory.value.userType = [];
					this.TDAStaffLoginLogoutHistory.value.govtEntity = [];
					this.TDAStaffLoginLogoutHistory.value.type = 1;
					this.loading = true;
					if (this.TDAStaffLoginLogoutHistory.value.toDate || this.TDAStaffLoginLogoutHistory.value.fromDate) {
						// let fromDate = new Date(this.TDAStaffLoginLogoutHistory.value.fromDate);
						// this.TDAStaffLoginLogoutHistory.value.fromDate = new Date(fromDate.setDate(fromDate.getDate() + 1));
						let toDate = new Date(this.TDAStaffLoginLogoutHistory.value.toDate);
						this.TDAStaffLoginLogoutHistory.value.toDate = new Date(toDate.setDate(toDate.getDate() + 1));
					}
					// if (this.TDAStaffLoginLogoutHistory.value.toTime || this.TDAStaffLoginLogoutHistory.value.fromTime) {
					// 	this.TDAStaffLoginLogoutHistory.value.fromTime = this.validationMessages.setTime(this.TDAStaffLoginLogoutHistory.value.fromTime);
					// 	this.TDAStaffLoginLogoutHistory.value.toTime = this.validationMessages.setTime(this.TDAStaffLoginLogoutHistory.value.toTime);
					// }
					debugger
					this.commonService.FilterHistory(this.TDAStaffLoginLogoutHistory.value).subscribe(res => {
						this.loading = false;
						let response = res;
						if ((response.result && response.result.code == 200) || response.code == 200) {
							this.getUserResponse = response.data;
							this.dataSource7 = new MatTableDataSource(this.getUserResponse);
							this.dataSource7.paginator = this.paginator7;
							this.dataSource7.sort = this.sort7;
						}
					},
						error => {
							this.loading = false;
							this.cdr.markForCheck();
						});
					return false;
				}
			}
		}
		else {
			this.validationMessages.validateAllFormFields(this.TDAStaffLoginLogoutHistory);
		}
	}
	onlineUsersLoginLogoutHistorySubmit() {
		if (this.onlineUsersLoginLogoutHistory.value.toDate || this.onlineUsersLoginLogoutHistory.value.fromDate) {
			this.onlineUsersLoginLogoutHistory.controls["toDate"].setValidators(Validators.required);
			this.onlineUsersLoginLogoutHistory.controls["fromDate"].setValidators(Validators.required);

			this.onlineUsersLoginLogoutHistory.controls["toDate"].updateValueAndValidity();
			this.onlineUsersLoginLogoutHistory.controls["fromDate"].updateValueAndValidity();
		}
		if (this.onlineUsersLoginLogoutHistory.value.fromTime || this.onlineUsersLoginLogoutHistory.value.toTime) {
			this.onlineUsersLoginLogoutHistory.controls["fromTime"].setValidators(Validators.required);
			this.onlineUsersLoginLogoutHistory.controls["toTime"].setValidators(Validators.required);

			this.onlineUsersLoginLogoutHistory.controls["fromTime"].updateValueAndValidity();
			this.onlineUsersLoginLogoutHistory.controls["toTime"].updateValueAndValidity();
		}

		if (this.onlineUsersLoginLogoutHistory.valid) {
			//Api call
			for (let [key, formValue] of Object.entries(this.onlineUsersLoginLogoutHistory.value)) {
				let value = formValue;
				if (value != null && value != '' || (value instanceof Array && value.length > 0)) {
					this.onlineUsersLoginLogoutHistory.value.employeeName = '';
					this.onlineUsersLoginLogoutHistory.value.onBehalfUsername = false;
					this.onlineUsersLoginLogoutHistory.value.customerMobileNo = '';
					this.onlineUsersLoginLogoutHistory.value.beneficiaryType = [];
					this.onlineUsersLoginLogoutHistory.value.govtEntity = [];
					this.onlineUsersLoginLogoutHistory.value.type = 3;
					this.loading = true;
					if (this.onlineUsersLoginLogoutHistory.value.toDate || this.onlineUsersLoginLogoutHistory.value.fromDate) {
					// let fromDate = new Date(this.onlineUsersLoginLogoutHistory.value.fromDate);
					// this.onlineUsersLoginLogoutHistory.value.fromDate = new Date(fromDate.setDate(fromDate.getDate() + 1));
					let toDate = new Date(this.onlineUsersLoginLogoutHistory.value.toDate);
					this.onlineUsersLoginLogoutHistory.value.toDate = new Date(toDate.setDate(toDate.getDate() + 1));
					}
					this.commonService.FilterHistory(this.onlineUsersLoginLogoutHistory.value).subscribe(res => {
						this.loading = false;
						let response = res;
						if ((response.result && response.result.code == 200) || response.code == 200) {
							this.getUserResponse = response.data;
							this.dataSourceOU = new MatTableDataSource(this.getUserResponse);
							this.dataSourceOU.paginator = this.paginatorOU;
							this.dataSourceOU.sort = this.sortOU;
						}
					},
						error => {
							this.loading = false;
							this.cdr.markForCheck();
						});
					return false;
				}
			}
		}
		else {
			this.validationMessages.validateAllFormFields(this.onlineUsersLoginLogoutHistory);
		}
	}
	govtEntityLoginLogoutHistorySubmit() {
		if (this.govtEntityLoginLogoutHistory.value.toDate || this.govtEntityLoginLogoutHistory.value.fromDate) {
			this.govtEntityLoginLogoutHistory.controls["toDate"].setValidators(Validators.required);
			this.govtEntityLoginLogoutHistory.controls["fromDate"].setValidators(Validators.required);

			this.govtEntityLoginLogoutHistory.controls["toDate"].updateValueAndValidity();
			this.govtEntityLoginLogoutHistory.controls["fromDate"].updateValueAndValidity();
		}
		if (this.govtEntityLoginLogoutHistory.value.fromTime || this.govtEntityLoginLogoutHistory.value.toTime) {
			this.govtEntityLoginLogoutHistory.controls["fromTime"].setValidators(Validators.required);
			this.govtEntityLoginLogoutHistory.controls["toTime"].setValidators(Validators.required);

			this.govtEntityLoginLogoutHistory.controls["fromTime"].updateValueAndValidity();
			this.govtEntityLoginLogoutHistory.controls["toTime"].updateValueAndValidity();
		}
		if (this.govtEntityLoginLogoutHistory.valid) {
			//Api call
			for (let [key, formValue] of Object.entries(this.govtEntityLoginLogoutHistory.value)) {
				let value = formValue;
				if (value != null && value != '' || (value instanceof Array && value.length > 0)) {
					this.govtEntityLoginLogoutHistory.value.employeeName = '';
					this.govtEntityLoginLogoutHistory.value.onBehalfUsername = false;
					this.govtEntityLoginLogoutHistory.value.customerMobileNo = '';
					this.govtEntityLoginLogoutHistory.value.beneficiaryType = [];
					this.govtEntityLoginLogoutHistory.value.userType = [];
					this.govtEntityLoginLogoutHistory.value.type = 2;
					this.loading = true;
					if (this.govtEntityLoginLogoutHistory.value.toDate || this.govtEntityLoginLogoutHistory.value.fromDate) {
					// let fromDate = new Date(this.govtEntityLoginLogoutHistory.value.fromDate);
					// this.govtEntityLoginLogoutHistory.value.fromDate = new Date(fromDate.setDate(fromDate.getDate() + 1));
					let toDate = new Date(this.govtEntityLoginLogoutHistory.value.toDate);
					this.govtEntityLoginLogoutHistory.value.toDate = new Date(toDate.setDate(toDate.getDate() + 1));
					}
					debugger
					this.commonService.FilterHistory(this.govtEntityLoginLogoutHistory.value).subscribe(res => {
						this.loading = false;
						let response = res;
						if ((response.result && response.result.code == 200) || response.code == 200) {
							this.getUserResponse = response.data;
							this.dataSourceGE = new MatTableDataSource(this.getUserResponse);
							this.dataSourceGE.paginator = this.paginatorGE;
							this.dataSourceGE.sort = this.sortGE;
						}
					},
						error => {
							this.loading = false;
							this.cdr.markForCheck();
						});
					return false;
				}
			}
		}
		else {
			this.validationMessages.validateAllFormFields(this.govtEntityLoginLogoutHistory);
		}
	}
	handlePagination(ev) {

	}
	getUserData(userType: number) {
		debugger
		this.commonService.SessionHistory(userType).subscribe(res => {
			let response = res;
			if ((response.result && response.result.code == 200) || response.code == 200) {
				this.getUserResponse = response.data;
				if (this.tabIndex == 0) {
					this.dataSource7 = new MatTableDataSource(this.getUserResponse);
					this.dataSource7.paginator = this.paginator7;
					this.dataSource7.sort = this.sort7;
				}
				else if (this.tabIndex == 1) {
					this.dataSourceOU = new MatTableDataSource(this.getUserResponse);
					this.dataSourceOU.paginator = this.paginatorOU;
					this.dataSourceOU.sort = this.sortOU;
				}
				else if (this.tabIndex == 2) {
					this.dataSourceGE = new MatTableDataSource(this.getUserResponse);
					this.dataSourceGE.paginator = this.paginatorGE;
					this.dataSourceGE.sort = this.sortGE;
				}
			}
		})
	}

	tabChange(index) {
		this.tabIndex = index;
		if (this.tabIndex == 0) {
			this.TDAStaffLoginLogoutHistory.reset(this.resetForm);
			this.getUserData(1);
		}
		else if (this.tabIndex == 1) {
			this.onlineUsersLoginLogoutHistory.reset(this.resetForm);
			this.getUserData(3);
		}
		else if (this.tabIndex == 2) {
			this.govtEntityLoginLogoutHistory.reset(this.resetForm);
			this.getUserData(2);
		}
		this.pageSize = 5;
		this.totalItems = 0;
		debugger
	}

	/* TDA Staff */
	applyFilter7(filterValue: string) {
		this.dataSource7.filter = filterValue.trim().toLowerCase();
		if (this.dataSource7.paginator) {
			this.dataSource7.paginator.firstPage();
		}
	}

	/* Online Users */
	applyFilterOU(filterValue: string) {
		this.dataSourceOU.filter = filterValue.trim().toLowerCase();
		if (this.dataSourceOU.paginator) {
			this.dataSourceOU.paginator.firstPage();
		}
	}

	/* Goverenment Entity */
	applyFilterGE(filterValue: string) {
		this.dataSourceGE.filter = filterValue.trim().toLowerCase();
		if (this.dataSourceGE.paginator) {
			this.dataSourceGE.paginator.firstPage();
		}
	}
	/* begin:: Viewed Pages Popup */
	openViewedPagesDialog(pages) {
		const dialogConfig = new MatDialogConfig();
		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.minWidth = "30%";
		dialogConfig.data = { pages };
		this.dialog.open(ViewedPagesModalComponent, dialogConfig);
	}
	/* end:: Viewed Pages Popup */
	getSessionType() {
		this.typeSession = this.commonService.getLookUp("?type=SessionTypes");
	}
	getBeneficiaryType() {
		this.typeBN = this.commonService.getLookUp("?type=BenificiaryTypes");
	}
	getOnlineuserType() {
		this.typeOnlineUser = this.commonService.getLookUp("?type=UsersTypes");
	}
	getGovtEntity() {
		this.commonService.getLookUp("?type=IssuanceEntities").subscribe(res => {
			this.typeGovtEntity = res;
		});
	}
	addRolePermission() {
		this.TDAStaffLoginLogoutHistory.patchValue({
			pageNo: this.postRolePermission.pageIds,
		});
		this.onlineUsersLoginLogoutHistory.patchValue({
			pageNo: this.postRolePermission.pageIds,
		});
		this.govtEntityLoginLogoutHistory.patchValue({
			pageNo: this.postRolePermission.pageIds,
		});
		this.modalService.dismissAll();
	}
	GetAccessRightsTree() {
		this.roleService.GetAccessRights(1).subscribe(res => {
			let response = res;
			if ((response.result && response.result.code == 200) || response.code == 200) {
				this.dataSource.data = response.data as TodoItemNode[];
				//this.postRolePermission.roleId = roleId;
				this.treeControl.dataNodes.forEach((res, index) => {
					if (res.level == 2) {
						this.pageNames.push({ 'value': res.value, 'itemEn': res.itemEn, 'itemAr': res.itemAr });
					}
				});
			}
		});
	}
	setRolePermission(node) {
		if (node && node.length > 0) {
			node.forEach(nodeValue => {
				let updateNodesValue = true;
				if ((this.postRolePermission.pageIds.length > 0) && (nodeValue.level == 2)) {
					for (let index = 0; index < this.postRolePermission.pageIds.length; index++) {
						if (this.postRolePermission.pageIds[index] == nodeValue.value) {
							this.postRolePermission.pageIds.splice(index, 1);
							updateNodesValue = false;
						}
					}
				}
				else if (nodeValue.level != 2) {
					updateNodesValue = false;
				}
				if (updateNodesValue) {
					this.postRolePermission.pageIds.push(nodeValue.value);
				}
			});
		}
		else {
			let updateValue = true;
			if (this.postRolePermission.pageIds.length > 0) {
				for (let index = 0; index < this.postRolePermission.pageIds.length; index++) {
					if (this.postRolePermission.pageIds[index] == node.value) {
						this.postRolePermission.pageIds.splice(index, 1);
						updateValue = false;
					}
				}
			}
			if (updateValue) {
				this.postRolePermission.pageIds.push(node.value);
			}
		}
	}
	/* #region Tree View */
	//..Modal Popup Start
	openAccessRightTree(content) {
		this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	}
	//..Modal Popup End
	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}

	/* Tree View */
	/** Map from flat node to nested node. This helps us finding the nested node to be modified */
	flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();
	/** Map from nested node to flattened node. This helps us to keep the same object for selection */
	nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();
	/** A selected parent node to be inserted */
	selectedParent: TodoItemFlatNode | null = null;
	/** The new item's name */
	newItemName = '';
	treeControl: FlatTreeControl<TodoItemFlatNode>;
	treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;
	dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;
	/** The selection for checklist */
	checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);

	/* Tree View */
	getLevel = (node: TodoItemFlatNode) => node.level;
	isExpandable = (node: TodoItemFlatNode) => node.expandable;
	getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;
	hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;
	hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => (_nodeData.itemEn || _nodeData.itemAr) === '';
	/**
	 * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
	 */
	transformer = (node: TodoItemNode, level: number) => {
		const existingNode = this.nestedNodeMap.get(node);
		const flatNode = existingNode && existingNode.itemEn === node.itemEn
			? existingNode
			: new TodoItemFlatNode();
		flatNode.itemEn = node.itemEn;
		flatNode.itemAr = node.itemAr;
		flatNode.value = node.value;
		flatNode.level = level;
		flatNode.expandable = !!node.children;
		this.flatNodeMap.set(flatNode, node);
		this.nestedNodeMap.set(node, flatNode);
		return flatNode;
	}

	/** Whether all the descendants of the node are selected. */
	descendantsAllSelected(node: TodoItemFlatNode): boolean {
		const descendants = this.treeControl.getDescendants(node);
		const descAllSelected = descendants.every(child =>
			this.checklistSelection.isSelected(child)
		);
		return descAllSelected;
	}

	/** Whether part of the descendants are selected */
	descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
		const descendants = this.treeControl.getDescendants(node);
		const result = descendants.some(child => this.checklistSelection.isSelected(child));
		return result && !this.descendantsAllSelected(node);
	}

	/** Toggle the to-do item selection. Select/deselect all the descendants node */
	todoItemSelectionToggle(node: TodoItemFlatNode): void {
		this.checklistSelection.toggle(node);
		const descendants = this.treeControl.getDescendants(node);
		this.setRolePermission(descendants);
		this.checklistSelection.isSelected(node)
			? this.checklistSelection.select(...descendants)
			: this.checklistSelection.deselect(...descendants);
		// Force update for the parent
		descendants.every(child =>
			this.checklistSelection.isSelected(child)
		);
		this.checkAllParentsSelection(node);
	}
	/** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
	todoLeafItemSelectionToggle(node): void {
		this.setRolePermission(node);
		this.checklistSelection.toggle(node);
		this.checkAllParentsSelection(node);
	}
	/* Checks all the parents when a leaf node is selected/unselected */
	checkAllParentsSelection(node: TodoItemFlatNode): void {
		let parent: TodoItemFlatNode | null = this.getParentNode(node);
		while (parent) {
			this.checkRootNodeSelection(parent);
			parent = this.getParentNode(parent);
		}
	}
	/** Check root node checked state and change it accordingly */
	checkRootNodeSelection(node: TodoItemFlatNode): void {
		const nodeSelected = this.checklistSelection.isSelected(node);
		const descendants = this.treeControl.getDescendants(node);
		const descAllSelected = descendants.every(child =>
			this.checklistSelection.isSelected(child)
		);
		if (nodeSelected && !descAllSelected) {
			this.checklistSelection.deselect(node);
		} else if (!nodeSelected && descAllSelected) {
			this.checklistSelection.select(node);
		}
	}
	/* Get the parent node of a node */
	getParentNode(node: TodoItemFlatNode): TodoItemFlatNode | null {
		const currentLevel = this.getLevel(node);
		if (currentLevel < 1) {
			return null;
		}
		const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;
		for (let i = startIndex; i >= 0; i--) {
			const currentNode = this.treeControl.dataNodes[i];
			if (this.getLevel(currentNode) < currentLevel) {
				return currentNode;
			}
		}
		return null;
	}
	/* #endregion */
}
export class TodoItemNode {
	children: TodoItemNode[];
	itemEn: string;
	itemAr: string;
	value: number;
}
/** Flat to-do item node with expandable and level information */
export class TodoItemFlatNode {
	itemEn: string;
	itemAr: string;
	level: number;
	expandable: boolean;
	value: number;
}
