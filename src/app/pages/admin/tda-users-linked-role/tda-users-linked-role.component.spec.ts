import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TdaUsersLinkedRoleComponent } from './tda-users-linked-role.component';

describe('TdaUsersLinkedRoleComponent', () => {
  let component: TdaUsersLinkedRoleComponent;
  let fixture: ComponentFixture<TdaUsersLinkedRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TdaUsersLinkedRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TdaUsersLinkedRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
