
import { Component, OnInit, ViewChild, Inject, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TdaUsersLinkedMoreRoleComponent } from '../tda-users-linked-more-role/tda-users-linked-more-role.component';
import { UserService } from '../../../core/appServices/user.service';
import { TranslationService } from '@translateService/translation.service';
import { ToastrService } from 'ngx-toastr';
import { RoleService } from '../../../core/appServices/role.service';

@Component({
  selector: 'kt-tda-users-linked-role',
  templateUrl: './tda-users-linked-role.component.html',
  styleUrls: ['./tda-users-linked-role.component.scss']
})
export class TdaUsersLinkedRoleComponent implements OnInit {

  /* Step Two */
  displayedColumnsstepTwo: string[] = ['nameAr', 'nameEn', 'remarks', 'type', 'isActive'];
  stepTwo = new MatTableDataSource<any>();

  userID: number;
  loading: boolean = false;
  roleList: any;
  @ViewChild('matPaginatorstepTwo', { static: true }) paginatorstepTwo: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortStepTwo: MatSort;
  constructor(
    public dialog: MatDialog,
    private userService: UserService,
    private cdr: ChangeDetectorRef,
    private translationService: TranslationService,
    public toastrService: ToastrService,
    public roleService: RoleService,
    private dialogRef: MatDialogRef<TdaUsersLinkedRoleComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.userID = data.userID;
    console.group(data.roleList);
    this.roleList = data.roleList;
    this.stepTwo = new MatTableDataSource<any>(this.roleList);
  }

  ngOnInit() {
    this.stepTwo.paginator = this.paginatorstepTwo;
    this.stepTwo.sort = this.sortStepTwo;
  }


  applyFilterstepTwo(filterValue: string) {
    this.stepTwo.filter = filterValue.trim().toLowerCase();
    if (this.stepTwo.paginator) {
      this.stepTwo.paginator.firstPage();
    }
  }

  openLinkedMoreUserRoleDialog() {
    this.loading = true;
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "60%";

    this.userService.getNotLinkedRoles(this.userID).subscribe(res => {
      let response = res;
      let UserID = this.userID;
      if (response.code == 200 || response.result.code == 200) {
        let unlinkedroleList = response.data;
        dialogConfig.data = { unlinkedroleList, UserID };
        this.loading = false;
        this.cdr.markForCheck();
        this.dialogRef.close();
        this.dialog.open(TdaUsersLinkedMoreRoleComponent, dialogConfig);
      }
      else {
        this.loading = false;
        this.cdr.markForCheck();
      }
    })
  }

  isActiveUserRole(row, event) {
    this.changeDataSource(row.id);
    let setUserRole = {
      "roleId": row.roleId,
      "userId": this.userID,
      "isActive": event.srcElement.checked
    }
    this.roleService.SetUserRole(setUserRole).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {
        
      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toastrService.error(text);
        });
      }
    });
  }
  changeDataSource(ID: number) {
    let index = this.roleList.findIndex(x => x.id == ID);
    let isActive: boolean = this.roleList[index].isActive == true ? false : true;
    this.roleList[index].isActive = isActive;
  }
}
