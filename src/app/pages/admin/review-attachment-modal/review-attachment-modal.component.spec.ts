import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewAttachmentModalComponent } from './review-attachment-modal.component';

describe('ReviewAttachmentModalComponent', () => {
  let component: ReviewAttachmentModalComponent;
  let fixture: ComponentFixture<ReviewAttachmentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewAttachmentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewAttachmentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
