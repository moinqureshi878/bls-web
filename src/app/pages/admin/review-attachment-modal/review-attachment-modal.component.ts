import { Component, OnInit } from '@angular/core';
import { TranslationService } from '@translateService/translation.service';

@Component({
  selector: 'kt-review-attachment-modal',
  templateUrl: './review-attachment-modal.component.html',
  styleUrls: ['./review-attachment-modal.component.scss']
})
export class ReviewAttachmentModalComponent implements OnInit {

  constructor(
    private translationService: TranslationService,
  ) { }

  ngOnInit() {
  }

}
