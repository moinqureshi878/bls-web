import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareableModalComponent } from './shareable-modal.component';

describe('ShareableModalComponent', () => {
  let component: ShareableModalComponent;
  let fixture: ComponentFixture<ShareableModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareableModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareableModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
