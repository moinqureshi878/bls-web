import { Component, OnInit } from '@angular/core';
import { phoneNumberMasking } from '../../../core/validator/form-validators.service';
import { TranslationService } from '@translateService/translation.service';

@Component({
  selector: 'kt-shareable-modal',
  templateUrl: './shareable-modal.component.html',
  styleUrls: ['./shareable-modal.component.scss']
})
export class ShareableModalComponent implements OnInit {

  constructor(private translationService: TranslationService,
  ) { }

  ngOnInit() {
  }
  /* Mobile No Mask */
  mobileNoMask = phoneNumberMasking();

}
