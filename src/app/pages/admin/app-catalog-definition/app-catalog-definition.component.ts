
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { Observable } from 'rxjs';
import { CommonService } from 'app/core/appServices/index.service';
import { TranslationService } from 'app/core/_base/layout';
import { BnCommonService } from 'app/core/appServices/bnCommon.service';
import { ToastrService } from 'ngx-toastr';
import { nameArValidator } from 'app/core/validator/form-validators.service';

export interface UserData {
  radioButton: string;
  appTypeAr: string;
  appTypeEn: string;
}

/** Builds and returns a new User. */

const ELEMENT_DATA: UserData[] = [
  { radioButton: '', appTypeAr: 'A', appTypeEn: 'A' },
  { radioButton: '', appTypeAr: 'B', appTypeEn: 'B' },
  { radioButton: '', appTypeAr: 'C', appTypeEn: 'C' },
  { radioButton: '', appTypeAr: 'D', appTypeEn: 'D' },
  { radioButton: '', appTypeAr: 'E', appTypeEn: 'E' },
  { radioButton: '', appTypeAr: 'F', appTypeEn: 'F' },

];

@Component({
  selector: 'kt-app-catalog-definition',
  templateUrl: './app-catalog-definition.component.html',
  styleUrls: ['./app-catalog-definition.component.scss']
})
export class AppCatalogDefinitionComponent implements OnInit {

  appCatalogDefinationForm: FormGroup;
  businessTypesDDL: Observable<any>;

  displayedColumns7: string[] = ['radioButton', 'appTypeAr', 'appTypeEn'];
  dataSource7 = new MatTableDataSource<UserData>(ELEMENT_DATA);

  loading = false;


  closeResult: string; //Bootstrap Modal Popup

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  constructor(
    private fb: FormBuilder,
    public validationMessages: ValidationMessagesService,
    public commonService: CommonService,
    public translationService: TranslationService,
    public bnCommonService: BnCommonService,
    public toastrService: ToastrService,
    private cdr: ChangeDetectorRef,
  ) {
    this.createFormPages();
    this.getBusinessTypes();
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }

  createFormPages() {
    this.appCatalogDefinationForm = this.fb.group({

      businessTypesddl: ['', Validators.compose([Validators.required])],

      grp1id: [0],
      grp1TitleEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      grp1TitleAr: ['', Validators.compose([Validators.required, nameArValidator, Validators.maxLength(100)])],
      grp1DescriptionEn: ['', Validators.compose([Validators.required, Validators.maxLength(2500)])],
      grp1DescriptionAr: ['', Validators.compose([Validators.required, nameArValidator, Validators.maxLength(2500)])],

      grp2id: [0],
      grp2TitleEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      grp2TitleAr: ['', Validators.compose([Validators.required, nameArValidator, Validators.maxLength(100)])],
      grp2DescriptionEn: ['', Validators.compose([Validators.required, Validators.maxLength(2500)])],
      grp2DescriptionAr: ['', Validators.compose([Validators.required, nameArValidator, Validators.maxLength(2500)])],

      grp3id: [0],
      grp3TitleEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      grp3TitleAr: ['', Validators.compose([Validators.required, nameArValidator, Validators.maxLength(100)])],
      grp3DescriptionEn: ['', Validators.compose([Validators.required, Validators.maxLength(2500)])],
      grp3DescriptionAr: ['', Validators.compose([Validators.required, nameArValidator, Validators.maxLength(2500)])],

      grp4id: [0],
      grp4TitleEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      grp4TitleAr: ['', Validators.compose([Validators.required, nameArValidator, Validators.maxLength(100)])],
      grp4DescriptionEn: ['', Validators.compose([Validators.required, Validators.maxLength(2500)])],
      grp4DescriptionAr: ['', Validators.compose([Validators.required, nameArValidator, Validators.maxLength(2500)])],
    });
  }

  getBusinessTypes() {
    this.businessTypesDDL = this.commonService.getLookUp("?type=ApplicationTypes");
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();

    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  onSubmit() {
    if (this.appCatalogDefinationForm.valid) {
      this.loading = true;
      let modelJson = [
        {
          "id": this.appCatalogDefinationForm.value.grp1id,
          "appTypes": this.appCatalogDefinationForm.value.businessTypesddl,
          "titleEn": this.appCatalogDefinationForm.value.grp1TitleEn,
          "titleAr": this.appCatalogDefinationForm.value.grp1TitleAr,
          "descriptionEn": this.appCatalogDefinationForm.value.grp1DescriptionEn,
          "descriptionAr": this.appCatalogDefinationForm.value.grp1DescriptionAr,
          "isActive": true,
        },
        {
          "id": this.appCatalogDefinationForm.value.grp2id,
          "appTypes": this.appCatalogDefinationForm.value.businessTypesddl,
          "titleEn": this.appCatalogDefinationForm.value.grp2TitleEn,
          "titleAr": this.appCatalogDefinationForm.value.grp2TitleAr,
          "descriptionEn": this.appCatalogDefinationForm.value.grp2DescriptionEn,
          "descriptionAr": this.appCatalogDefinationForm.value.grp2DescriptionAr,
          "isActive": true,
        },
        {
          "id": this.appCatalogDefinationForm.value.grp3id,
          "appTypes": this.appCatalogDefinationForm.value.businessTypesddl,
          "titleEn": this.appCatalogDefinationForm.value.grp3TitleEn,
          "titleAr": this.appCatalogDefinationForm.value.grp3TitleAr,
          "descriptionEn": this.appCatalogDefinationForm.value.grp3DescriptionEn,
          "descriptionAr": this.appCatalogDefinationForm.value.grp3DescriptionAr,
          "isActive": true,
        },
        {
          "id": this.appCatalogDefinationForm.value.grp4id,
          "appTypes": this.appCatalogDefinationForm.value.businessTypesddl,
          "titleEn": this.appCatalogDefinationForm.value.grp4TitleEn,
          "titleAr": this.appCatalogDefinationForm.value.grp4TitleAr,
          "descriptionEn": this.appCatalogDefinationForm.value.grp4DescriptionEn,
          "descriptionAr": this.appCatalogDefinationForm.value.grp4DescriptionAr,
          "isActive": true,
        }
      ]

      debugger
      if (modelJson[0].id > 0) {
        debugger
        this.bnCommonService.UpdateppTypesCatalog(modelJson)
          .subscribe(data => {
            if (data["result"].code === 200) {
              this.loading = false;
              this.cdr.markForCheck();
              this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
                this.toastrService.success(text);
              });

            }
            else {
              this.loading = false;
              this.cdr.markForCheck();
              this.toastrService.error('Something went wrong. Please try again');
            }
          })
      }
      else {
        debugger
        this.bnCommonService.AddAppTypesCatalog(modelJson)
          .subscribe(data => {
            if (data["result"].code === 200) {
              this.loading = false;
              this.cdr.markForCheck();

              this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
                this.toastrService.success(text);
              });

            }
            else {
              this.loading = false;
              this.cdr.markForCheck();
              this.toastrService.error('Something went wrong. Please try again');
            }
          })
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.appCatalogDefinationForm);
    }

  }

  onBusinessTypeDDLChange() {
    debugger
    var id = this.appCatalogDefinationForm.value.businessTypesddl;
    this.bnCommonService.getAppTypesCatalogByID(id)
      .subscribe(data => {
        debugger
        if (data["data"].length > 0) {
          this.appCatalogDefinationForm.patchValue({

            grp1id: data["data"][0].id,
            grp1TitleEn: data["data"][0].titleEn,
            grp1TitleAr: data["data"][0].titleAr,
            grp1DescriptionEn: data["data"][0].descriptionEn,
            grp1DescriptionAr: data["data"][0].descriptionAr,

            grp2id: data["data"][1].id,
            grp2TitleEn: data["data"][1].titleEn,
            grp2TitleAr: data["data"][1].titleAr,
            grp2DescriptionEn: data["data"][1].descriptionEn,
            grp2DescriptionAr: data["data"][1].descriptionAr,

            grp3id: data["data"][2].id,
            grp3TitleEn: data["data"][2].titleEn,
            grp3TitleAr: data["data"][2].titleAr,
            grp3DescriptionEn: data["data"][2].descriptionEn,
            grp3DescriptionAr: data["data"][2].descriptionAr,

            grp4id: data["data"][3].id,
            grp4TitleEn: data["data"][3].titleEn,
            grp4TitleAr: data["data"][3].titleAr,
            grp4DescriptionEn: data["data"][3].descriptionEn,
            grp4DescriptionAr: data["data"][3].descriptionAr,

          })
        }
        else {
          this.appCatalogDefinationForm.patchValue({

            grp1id: 0,
            grp1TitleEn: null,
            grp1TitleAr: null,
            grp1DescriptionEn: null,
            grp1DescriptionAr: null,

            grp2id: 0,
            grp2TitleEn: null,
            grp2TitleAr: null,
            grp2DescriptionEn: null,
            grp2DescriptionAr: null,

            grp3id: 0,
            grp3TitleEn: null,
            grp3TitleAr: null,
            grp3DescriptionEn: null,
            grp3DescriptionAr: null,

            grp4id: 0,
            grp4TitleEn: null,
            grp4TitleAr: null,
            grp4DescriptionEn: null,
            grp4DescriptionAr: null,

          })

          this.appCatalogDefinationForm.markAsUntouched();
          this.appCatalogDefinationForm.markAsPristine();
          setTimeout(() => {
            Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
              item.classList.remove("mat-form-field-invalid");
            });
          }, 50);
        }
      })
  }

}