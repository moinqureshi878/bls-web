import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppCatalogDefinitionComponent } from './app-catalog-definition.component';

describe('AppCatalogDefinitionComponent', () => {
  let component: AppCatalogDefinitionComponent;
  let fixture: ComponentFixture<AppCatalogDefinitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppCatalogDefinitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppCatalogDefinitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
