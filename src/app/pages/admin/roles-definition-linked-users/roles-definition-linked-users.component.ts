import { Component, OnInit, ViewChild, Injectable, Inject } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { RolesDefinitionLinkedMoreUsersComponent } from '../roles-definition-linked-more-users/roles-definition-linked-more-users.component';
import { TranslationService } from '@translateService/translation.service';
import { RoleService } from '../../../core/appServices/role.service';
import { ToastrService } from 'ngx-toastr';
/* Step Two Date */
export interface stepTwoData {
  fullNameAr: string;
  fullNameEn: string;
  userName: string;
  type: string;
  isActive: boolean;
}

@Component({
  selector: 'kt-roles-definition-linked-users',
  templateUrl: './roles-definition-linked-users.component.html',
  styleUrls: ['./roles-definition-linked-users.component.scss']
})
export class RolesDefinitionLinkedUsersComponent implements OnInit {
  /* Step Two */
  displayedColumnsstepTwo: string[] = ['userNameAr', 'userNameEn', 'username', 'type', 'isActive'];
  stepTwo = new MatTableDataSource<stepTwoData>();
  roleId: number;
  usersList: any;
  @ViewChild('matPaginatorstepTwo', { static: true }) paginatorstepTwo: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortStepTwo: MatSort;
  loading: boolean = false;
  constructor(public dialog: MatDialog,
    public translationService: TranslationService,
    public toastrService: ToastrService,
    public roleService: RoleService,
    private dialogRef: MatDialogRef<RolesDefinitionLinkedUsersComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.roleId = data.roleId;
    this.usersList = data.usersList;
    this.stepTwo = new MatTableDataSource<stepTwoData>(this.usersList);
  }

  ngOnInit() {
    /* Step Two */
    this.stepTwo.paginator = this.paginatorstepTwo;
    this.stepTwo.sort = this.sortStepTwo;
  }


  applyFilterstepTwo(filterValue: string) {
    this.stepTwo.filter = filterValue.trim().toLowerCase();
    if (this.stepTwo.paginator) {
      this.stepTwo.paginator.firstPage();
    }
  }
  isActiveUserRole(userId, event) {

    this.changeDataSource(userId);
    
    let setUserRole = {
      "roleId": this.roleId,
      "userId": userId,
      "isActive": event.srcElement.checked
    }
    this.roleService.SetUserRole(setUserRole).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {
        
      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toastrService.error(text);
        });
      }
    });
  }
  changeDataSource(ID: number) {
    let index = this.usersList.findIndex(x => x.userId == ID);
    let isActive: boolean = this.usersList[index].isActive == true ? false : true;
    this.usersList[index].isActive = isActive;
  }
  /* begin:: Linked Users Role Popup */
  openLinkedRoleMoreUsersDialog(roleId) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "60%";

    this.loading = true;
    this.roleService.NotLinkedUsers(roleId).subscribe(res => {
      this.loading = false;
      let response = res;
      if (response.result.code == 200) {

        this.dialogRef.close();
        dialogConfig.data = { usersList: response.data, roleId: this.roleId };
        this.dialog.open(RolesDefinitionLinkedMoreUsersComponent, dialogConfig);
      }
    });

  }
  /* end:: Linked Users Role Popup */
}
