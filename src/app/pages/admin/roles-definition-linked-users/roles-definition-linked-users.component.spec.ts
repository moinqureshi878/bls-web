import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesDefinitionLinkedUsersComponent } from './roles-definition-linked-users.component';

describe('RolesDefinitionLinkedUsersComponent', () => {
  let component: RolesDefinitionLinkedUsersComponent;
  let fixture: ComponentFixture<RolesDefinitionLinkedUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesDefinitionLinkedUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesDefinitionLinkedUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
