import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesDefinitionLinkedMoreUsersComponent } from './roles-definition-linked-more-users.component';

describe('RolesDefinitionLinkedMoreUsersComponent', () => {
  let component: RolesDefinitionLinkedMoreUsersComponent;
  let fixture: ComponentFixture<RolesDefinitionLinkedMoreUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesDefinitionLinkedMoreUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesDefinitionLinkedMoreUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
