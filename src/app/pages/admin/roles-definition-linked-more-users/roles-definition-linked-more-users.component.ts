import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslationService } from '@translateService/translation.service';
import { RoleService } from '../../../core/appServices/role.service';


/* Step Three Date */
export interface stepThreeData {
  select: string;
  fullNameAr: string;
  fullNameEn: string;
  userEmail: string;
  mobileNo: string;

}

@Component({
  selector: 'kt-roles-definition-linked-more-users',
  templateUrl: './roles-definition-linked-more-users.component.html',
  styleUrls: ['./roles-definition-linked-more-users.component.scss']
})
export class RolesDefinitionLinkedMoreUsersComponent implements OnInit {


  /* Step Three */
  displayedColumnsstepThree: string[] = ['select', 'fullNameAr', 'fullNameEn', 'userEmail', 'mobileNo'];
  stepThree = new MatTableDataSource<stepThreeData>();
  @ViewChild('matPaginatorstepThree', { static: true }) paginatorstepThree: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortStepThree: MatSort;
  assignUserRole = [];
  roleId: number;
  loading: boolean = false;
  constructor(public translationService: TranslationService,
    public roleService: RoleService,
    private dialogRef: MatDialogRef<RolesDefinitionLinkedMoreUsersComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.roleId = data.roleId;
    this.stepThree = new MatTableDataSource<stepThreeData>(data.usersList);
  }

  ngOnInit() {
    /* Step Three */
    this.stepThree.paginator = this.paginatorstepThree;
    this.stepThree.sort = this.sortStepThree;
  }
  linkMoreUser(userId, isActive) {
    let setUserRole = {
      "roleId": this.roleId,
      "userId": userId,
      "isActive": isActive
    }
    let updateValue = true;
    if (this.assignUserRole.length > 0) {
      for (let index = 0; index < this.assignUserRole.length; index++) {
        if ((this.assignUserRole[index].userId == userId) && (this.assignUserRole[index].isActive == true)) {
          this.assignUserRole.splice(index, 1);
          updateValue = false;
        }
      }
    }
    if (updateValue) {
      this.assignUserRole.push(setUserRole);
    }
  }
  confirmSelected() {
    this.loading = true;
    if (this.assignUserRole.length > 0) {
      this.roleService.AssignRole(this.assignUserRole).subscribe(res => {
        this.loading = false;
        let response = res;
        if (response.result.code == 200 || response.code == 200) {
          this.dialogRef.close();
        }
      });
    }
    else {
      this.loading = false;
      this.dialogRef.close();
    }
  }
  applyFilterstepThree(filterValue: string) {
    this.stepThree.filter = filterValue.trim().toLowerCase();
    if (this.stepThree.paginator) {
      this.stepThree.paginator.firstPage();
    }
  }
}
