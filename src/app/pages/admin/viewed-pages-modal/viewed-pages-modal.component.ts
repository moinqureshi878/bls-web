
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { TranslationService } from '@translateService/translation.service';

/* Step Two Date */
export interface stepTwoData {
  pageName: string;
  visitTime: string;

}

/** Builds and returns a new User. */

const STEP_TWO_DATA: stepTwoData[] = [
  { pageName: 'B 1', visitTime: 'Ba edf' },
  { pageName: 'B 3', visitTime: 'Ba 3df' },
  { pageName: 'B 4', visitTime: 'Ba 5df' },
  { pageName: 'B 1', visitTime: 'Ba 4df' },
  { pageName: 'B 16', visitTime: 'Ba 3df4' },
  { pageName: 'B 17', visitTime: 'Ba df3' },
  { pageName: 'B 18', visitTime: 'Ba df3' },
  { pageName: 'B 19', visitTime: 'Ba dfe' },
  { pageName: 'B 10', visitTime: 'Ba dfh' },
  { pageName: 'B 17', visitTime: 'Ba dfh' },
  { pageName: 'B 15', visitTime: 'Ba dfh' },
  { pageName: 'B 14', visitTime: 'Ba dfgg' },
  { pageName: 'B 13', visitTime: 'Ba dff' },
  { pageName: 'B 12', visitTime: 'Ba ddf' },
  { pageName: 'B 12', visitTime: 'Ba dsf' },
  { pageName: 'B 12', visitTime: 'Ba adf' },


];

@Component({
  selector: 'kt-viewed-pages-modal',
  templateUrl: './viewed-pages-modal.component.html',
  styleUrls: ['./viewed-pages-modal.component.scss']
})
export class ViewedPagesModalComponent implements OnInit {

  /* Step Two */
  displayedColumnsstepTwo: string[] = ['pageName', 'dateTime'];
  stepTwo = new MatTableDataSource<stepTwoData>(STEP_TWO_DATA);

  @ViewChild('matPaginatorstepTwo', { static: true }) paginatorstepTwo: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortStepTwo: MatSort;
  constructor(public dialog: MatDialog, public validationMessages: ValidationMessagesService, private dialogRef: MatDialogRef<ViewedPagesModalComponent>,
    private translationService: TranslationService,
    @Inject(MAT_DIALOG_DATA) data) {
    let pages = data.pages;
    console.table(pages);
    this.stepTwo = new MatTableDataSource(pages);
  }

  ngOnInit() {
    /* Step Two */
    this.stepTwo.paginator = this.paginatorstepTwo;
    this.stepTwo.sort = this.sortStepTwo;
  }


  applyFilterstepTwo(filterValue: string) {
    this.stepTwo.filter = filterValue.trim().toLowerCase();
    if (this.stepTwo.paginator) {
      this.stepTwo.paginator.firstPage();
    }
  }

}
