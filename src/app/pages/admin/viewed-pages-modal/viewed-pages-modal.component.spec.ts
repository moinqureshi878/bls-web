import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewedPagesModalComponent } from './viewed-pages-modal.component';

describe('ViewedPagesModalComponent', () => {
  let component: ViewedPagesModalComponent;
  let fixture: ComponentFixture<ViewedPagesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewedPagesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewedPagesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
