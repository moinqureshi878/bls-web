import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageSubUsersComponent } from './manage-sub-users.component';

describe('ManageSubUsersComponent', () => {
  let component: ManageSubUsersComponent;
  let fixture: ComponentFixture<ManageSubUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageSubUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageSubUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
