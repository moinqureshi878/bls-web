
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { GovernmentLinkedUsersComponent } from '../government-linked-users/government-linked-users.component';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../core/appServices/user.service';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from '@translateService/translation.service';
import { JwtTokenService } from '../../../core/appServices/jwt.decode.service';

@Component({
	selector: 'kt-manage-sub-users',
	templateUrl: './manage-sub-users.component.html',
	styleUrls: ['./manage-sub-users.component.scss']
})
export class ManageSubUsersComponent implements OnInit {

	ELEMENT_DATA: any[] = [];
	displayedColumns7: string[] = ['fullNameAr', 'fullNameEn', 'userName', 'editView', 'isActive'];
	dataSource7 = new MatTableDataSource<any>(this.ELEMENT_DATA);
	closeResult: string; //Bootstrap Modal Popup
	GovtEntityNameEn: string;
	GovtEntityNameAr: string;
	@ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;

	@ViewChild(MatSort, { static: true }) sort7: MatSort;
	userList: any;
	constructor(
		public modalService: NgbModal,
		public dialog: MatDialog,
		public route: ActivatedRoute,
		public router: Router,
		public userService: UserService,
		public translationService: TranslationService,
		public toastrService: ToastrService,
		public jwtService: JwtTokenService
	) {

		this.userList = this.route.snapshot.data.data;
		this.dataSource7 = new MatTableDataSource<any>(this.userList);

		let userSession = JSON.parse(localStorage.getItem("userSession"));

		let currentSessionToken: string;
		if (userSession && userSession.length > 1) {
			currentSessionToken = userSession[1].token;
		}
		else {
			currentSessionToken = userSession[0].token;
		}

		this.GovtEntityNameEn = this.jwtService.getJwtTokenValueByToken(currentSessionToken).GovtEntityNameEn;
		this.GovtEntityNameAr = this.jwtService.getJwtTokenValueByToken(currentSessionToken).GovtEntityNameAr;

	}
	ngOnInit() {
		this.dataSource7.paginator = this.paginator7;
		this.dataSource7.sort = this.sort7;
	}
	applyFilter7(filterValue: string) {
		this.dataSource7.filter = filterValue.trim().toLowerCase();
		if (this.dataSource7.paginator) {
			this.dataSource7.paginator.firstPage();
		}
	}
	//..Modal Popup Start
	open(content) {
		//const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
		this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	}
	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}
	//..Modal Popup End

	openDialog() {
		const dialogConfig = new MatDialogConfig();
		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.minWidth = "60%";
		this.dialog.open(GovernmentLinkedUsersComponent, dialogConfig);
	}

	onEditClick(userId) {
		this.router.navigate(['/pages/government/edit-sub-users/' + userId]);
	}
	CheckboxChange(id, event) {
		this.changeDataSource(id);

		this.userService.ActiveDeactiveUsers(id).subscribe(data => {
			let response = data;
			if ((response.result && response.result.code == 200) || response.code == 200) {

			}
			else {
				this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
					this.toastrService.error(text);
				});
			}
		});
	}
	changeDataSource(ID: number) {
		let index = this.userList.findIndex(x => x.userId == ID);
		let isActive: boolean = this.userList[index].isActive == true ? false : true;
		this.userList[index].isActive = isActive;
	}
}
