import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGovtUsersComponent } from './add-govt-users.component';

describe('AddGovtUsersComponent', () => {
  let component: AddGovtUsersComponent;
  let fixture: ComponentFixture<AddGovtUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGovtUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGovtUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
