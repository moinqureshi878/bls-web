import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AsyncFormValidatorService } from '../../../core/validator/async-form-validator.service';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { passwordValidator, nameValidator, emailValidator, matchingPasswords, fileSize, phoneNumberValidator, phoneNumberMasking, emiratesIDMasking, nameArValidator } from '../../../core/validator/form-validators.service';
import { CommonService, UserService } from '../../../core/appServices/index.service';
import { TranslationService } from '@translateService/translation.service';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { UserTypeEnum, GenderEnum, CommonEnum } from '../../../core/_enum/index.enum';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'kt-add-govt-users',
  templateUrl: './add-govt-users.component.html',
  styleUrls: ['./add-govt-users.component.scss']
})
export class AddGovtUsersComponent implements OnInit {

  addGovtUserForm: FormGroup;
  userId: any;
  getAttachmentsType: any;

  isLoading = [];
  loading: boolean = false;
  isDisabled: boolean = false;
  minDate = new Date();
  typeGenderCode: Observable<any>;

  // isEmiratesId: boolean = false;
  typeGovtEntity: any;
  getUserResponse: any;
  CommonEnum = CommonEnum;

  constructor(public router: Router,
    public commonService: CommonService,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    private _DomSanitizationService: DomSanitizer,
    private cdr: ChangeDetectorRef,
    public toast: ToastrService,
    public userService: UserService,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public translationService: TranslationService,
    public validationMessages: ValidationMessagesService) {
    this.getAttachmentsType = this.route.snapshot.data.attachmentData;

    this.getUserResponse = this.route.snapshot.data.userData;
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.route.params.subscribe(params => {

      this.userId = Number(params['id']);
      if (this.userId) {
        this.addGovtUser();
        this.setGovtUserForm(this.getUserResponse);
      }
      else {
        this.addGovtUser();
      }
    });
    // Change Dropdown(Govt Entity) to fill Textboxes
    this.addGovtUserForm.controls['govtEntity'].valueChanges.subscribe(value => {
      let typeGovtEntity = this.typeGovtEntity.filter(res => res.id == value);
      if (value) {
        this.addGovtUserForm.patchValue({
          govtEntityNameAr: typeGovtEntity[0].nameAr,
          govtEntityNameEn: typeGovtEntity[0].nameEn,
        });
      }
    });
    // Change Async Validator to update DOM
    this.addGovtUserForm.get('username').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
    this.addGovtUserForm.get('citizenID').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
    this.addGovtUserForm.get('email').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
    this.addGovtUserForm.get('mobileNo').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
  }
  ngOnInit() {
    this.getGovtEntity();
    this.getGenderCode();
  }
  setGovtUserForm(response) {
    this.addGovtUserForm.patchValue({
      govtEntity: response.userAttributes.govtEntityCode,
      govtEntityNameAr: response.userAttributes.govtEntityNameAr,
      govtEntityNameEn: response.userAttributes.govtEntityNameEn,
      username: response.userName,
      password: 'P@ssw0rd',
      confirmPassword: 'P@ssw0rd',
      citizenID: response.userAttributes.citizenId,
      emiratesIDExpiry: response.userAttributes.emiratesIdexpiry,
      nameAr: response.fullNameAr,
      nameEn: response.fullNameEn,
      representativeRole: response.userAttributes.role,
      isActive: String(response.isActive),
      department: response.userAttributes.department,
      email: response.userEmail,
      mobileNo: response.mobileNo,
      preferedLanguage: response.preferedLanguage,
      gender: response.genderCode
    });
  }

  addGovtUserFormSubmit() {
    const controlArray = <FormArray>this.addGovtUserForm.get('attachments');
    for (let index = 0; index < controlArray.length; index++) {
      if (this.userId) {
        if (!this.getUserResponse.attachmentsLog[index].isMandatory) {
          controlArray.controls[index].get('attachmentSize').reset();
        }
        // else {
        //   if ((!this.getAttachmentsType[index]) && (!this.getAttachmentsType[index].isMandatory)) {
        //     controlArray.controls[index].get('attachmentSize').reset();
        //   }
        // }
      }
    }
    debugger
    if (this.addGovtUserForm.valid) {
      this.loading = true;
      let addGovtUserFormObj = this.createUserJson(this.addGovtUserForm.value);
      if (this.userId) {
        this.userService.EditUser(addGovtUserFormObj).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            this.router.navigateByUrl('/pages/government/manage-users');
            this.addGovtUserForm.reset();
            setTimeout(() => {
              Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
                item.classList.remove("mat-form-field-invalid");
              });
            }, 50);
            this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
              this.toast.success(text);
            });
          }
        });
      }
      else {
        this.userService.AddUser(addGovtUserFormObj).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            this.addGovtUserForm.reset();
            setTimeout(() => {
              Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
                item.classList.remove("mat-form-field-invalid");
              });
            }, 50);
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigateByUrl('/pages/government/manage-users');
          }
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.addGovtUserForm);
    }
  }

  addGovtUser() {
    if (this.userId) {
      this.addGovtUserForm = this.fb.group({
        attachments: this.fb.array([]),
        govtEntity: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
        govtEntityNameAr: [''],
        govtEntityNameEn: [''],
        // username: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameValidator])],
        username: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameValidator]), this.asyncFormValidatorService.usernameValidator(this.userId)],
        password: [{ value: '', disabled: true }, Validators.compose([Validators.required, Validators.maxLength(30), passwordValidator])],
        confirmPassword: [{ value: '', disabled: true }, Validators.required],
        citizenID: ['', Validators.compose([Validators.required]), this.asyncFormValidatorService.citizenIDValidator(this.userId)],
        emiratesIDExpiry: [null],
        nameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
        nameEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
        representativeRole: ['', Validators.maxLength(100)],
        isActive: [null, Validators.required],
        department: ['', Validators.maxLength(100)],
        // email: ['', Validators.compose([Validators.required, emailValidator])],
        email: ['', Validators.compose([Validators.required, emailValidator]), this.asyncFormValidatorService.emailValidator(this.userId)],
        // mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator])],
        mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator]), this.asyncFormValidatorService.mobileNoValidator(this.userId)],
        preferedLanguage: ['ar', Validators.required],
        gender: [null, Validators.required],
      }, { validator: matchingPasswords('password', 'confirmPassword') });
      let control = <FormArray>this.addGovtUserForm.controls.attachments;
      debugger
      this.getUserResponse.attachmentsLog.forEach(x => {
        control.push(this.fb.group({
          id: [x.id],
          userId: [x.userId],
          attachmentName: [x.attachmentName, x.isMandatory ? Validators.required : ''],
          attachmentURL: [x.attachmentUrl],
          attachmentSize: [2097151, x.isMandatory ? Validators.compose([Validators.required, fileSize]) : Validators.compose([fileSize])],
          attachmentExtension: [x.attachmentExtension],
          typeCode: [x.valueEn],
          expiryDate: [new Date()],
          isMandatory: [x.isMandatory],
          inputData: [x.inputData, x.isInputData ? Validators.required : ''],
          attachmentType: [x.attachmentType],
        }))
      });
    }
    else {
      this.addGovtUserForm = this.fb.group({
        attachments: this.fb.array([]),
        govtEntity: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
        govtEntityNameAr: [''],
        govtEntityNameEn: [''],
        username: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameValidator]), this.asyncFormValidatorService.usernameValidator(0)],
        password: ['', Validators.compose([Validators.required, Validators.maxLength(30), passwordValidator])],
        confirmPassword: ['', Validators.required],
        citizenID: ['', Validators.compose([Validators.required]), this.asyncFormValidatorService.citizenIDValidator(0)],
        emiratesIDExpiry: [null],
        nameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
        nameEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
        representativeRole: ['', Validators.maxLength(100)],
        isActive: ['true', Validators.required],
        department: ['', Validators.maxLength(100)],
        email: ['', Validators.compose([Validators.required, emailValidator]), this.asyncFormValidatorService.emailValidator(0)],
        mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator]), this.asyncFormValidatorService.mobileNoValidator(0)],
        // mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator]), this.asyncFormValidatorService.mobileNoValidator()],
        preferedLanguage: ['ar', Validators.required],
        gender: [GenderEnum.male, Validators.required],
      }, { validator: matchingPasswords('password', 'confirmPassword') });
      let control = <FormArray>this.addGovtUserForm.controls.attachments;
      this.getAttachmentsType.forEach((x, index) => {
        control.push(this.fb.group({
          id: [0],
          userId: [0],
          attachmentName: ['', x.isMandatory ? Validators.required : ''],
          attachmentURL: [''],
          attachmentSize: ['', x.isMandatory ? Validators.compose([Validators.required, fileSize]) : Validators.compose([fileSize])],
          attachmentExtension: [''],
          typeCode: [x.valueEn],
          expiryDate: [new Date()],
          isMandatory: [x.isMandatory],
          inputData: ['', x.inputData ? Validators.required : ''],
          attachmentType: [x.id],
        }));
      });
    }
    debugger
    if (this.userId) {
      this.getUserResponse.attachmentsLog.forEach(att => {
        this.isLoading.push({ loading: false });
      });
    }
    else {
      this.getAttachmentsType.forEach(att => {
        this.isLoading.push({ loading: false });
      });
    }
  }
  getFile(event, index) {
    debugger
    const controlArray = <FormArray>this.addGovtUserForm.get('attachments');
    if (event.target.files[0].size > 2097152) {
      controlArray.controls[index].get('attachmentSize').markAsTouched();
      controlArray.controls[index].get('attachmentSize').markAsDirty();
      controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
    }
    else {
      if (event.target.files[0]) {
        this.isLoading[index].loading = true;
        this.isDisabled = true;
        this.commonService.fileUpload(event.target.files[0]).then(resp => {
          var response = resp;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            controlArray.controls[index].get('attachmentExtension').setValue(event.target.files[0].type);
            controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
            controlArray.controls[index].get('attachmentName').setValue(event.target.files[0].name);

            this.isLoading[index].loading = false;
            this.isDisabled = false;

            this.cdr.markForCheck();
            controlArray.controls[index].get('attachmentURL').setValue(response.attachmentURL);
            controlArray.controls[index].get('attachmentSize').markAsTouched();
            controlArray.controls[index].get('attachmentName').markAsTouched();
          }
          else {
            this.isLoading[index].loading = false;
            this.isDisabled = false;
            this.cdr.markForCheck();
            this.translationService.getTranslation(String(response.result.code)).subscribe((text: string) => {
              this.toast.error(text);
            });
          }
        });
      }
    }
  }

  createUserJson(govtUser) {
    let addtdaUserObj = {
      "userId": this.userId ? this.userId : 0,
      "masterId": 0,
      "fullNameAr": govtUser.nameAr,
      "fullNameEn": govtUser.nameEn,
      "userName": govtUser.username,
      "userEmail": govtUser.email,
      "password": govtUser.password,
      "type": this.CommonEnum.userTypeLoginPage,
      "userType": UserTypeEnum.Govt_User,
      "mobileNo": govtUser.mobileNo,
      "securityToken": "",
      "genderCode": govtUser.gender,
      "isConfirmed": true,
      "isActive": Boolean(govtUser.isActive),
      "preferedLanguage": govtUser.preferedLanguage,
      "createdBy": 0,
      "createdAt": new Date(),
      "modifiedBy": 0,
      "modifiedAt": null,
      "stepCode": 0,
      "loginCounter": 0,
      "lastLogin": new Date(),
      "userAttributes": {
        "userId": this.userId ? this.userId : 0,
        "role": govtUser.representativeRole,
        "department": govtUser.department,
        "nationalityCode": null,
        "designationCode": null,
        "hrcode": null,
        "businessUnitCode": null,
        "remarks": null,
        "citizenId": govtUser.citizenID,
        "citizenCode": 0,
        "passportNo": "",
        "passportExpiryAt": null,
        "licenseNumber": "",
        "licenseExpiryAt": null,
        "issuanceEmirates": 0,
        "bnnameEn": "",
        "bnnameAr": "",
        "nameAr": "",
        "fatherNameAr": "",
        "grandFatherNameAr": "",
        "familyNameAr": "",
        "govtEntityCode": govtUser.govtEntity,
        "govtEntityNameEn": govtUser.govtEntityNameEn,
        "govtEntityNameAr": govtUser.govtEntityNameAr,
        "landlineNo": null,
        "emiratesIdexpiry": govtUser.emiratesIDExpiry//this.validationMessages.setDate(govtUser.emiratesIDExpiry)
      },
      "attachmentsLog": this.addGovtUserForm.value.attachments,
      "physicalDevices": [],
      "roleUser": []
    }
    return addtdaUserObj;
  }

  getGovtEntity() {
    this.userService.GetGovtEntity().subscribe(res => {

      this.typeGovtEntity = res["data"];
    });
  }
  getGenderCode() {
    this.typeGenderCode = this.commonService.getLookUp("?type=gender");
  }
  /* Emirates ID Mask */
  emiratesIDMask = emiratesIDMasking();
  /* Mobile No Mask */
  mobileNoMask = phoneNumberMasking();

}
