import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GovernmentLinkedUsersComponent } from './government-linked-users.component';

describe('GovernmentLinkedUsersComponent', () => {
  let component: GovernmentLinkedUsersComponent;
  let fixture: ComponentFixture<GovernmentLinkedUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GovernmentLinkedUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GovernmentLinkedUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
