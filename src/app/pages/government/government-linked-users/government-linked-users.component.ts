

import { Component, OnInit, ViewChild, Inject, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl } from '@angular/forms';
import { UserService } from '../../../core/appServices/user.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from '@translateService/translation.service';

@Component({
  selector: 'kt-government-linked-users',
  templateUrl: './government-linked-users.component.html',
  styleUrls: ['./government-linked-users.component.scss']
})
export class GovernmentLinkedUsersComponent implements OnInit {
  @ViewChild('matPaginatorstepTwo', { static: true }) paginatorstepTwo: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortStepTwo: MatSort;

  STEP_TWO_DATA: any;
  displayedColumnsstepTwo: string[] = ['fullNameAr', 'fullNameEn', 'roleName', 'isActive'];
  stepTwo = new MatTableDataSource<any>(this.STEP_TWO_DATA);

  userID: number;
  rolesList: Observable<any>;

  userRolesAdd: any = [];
  rolesIDsArray: any = [];

  loading: boolean = false;
  userList: any;
  constructor(
    public userService: UserService,
    private dialogRef: MatDialogRef<GovernmentLinkedUsersComponent>,
    private cdr: ChangeDetectorRef,
    private translationService: TranslationService,
    public toastrService: ToastrService,
    @Inject(MAT_DIALOG_DATA) data) {

    this.userID = data.userID;
    this.userList = data.userList;
    this.stepTwo = new MatTableDataSource<any>(this.userList);
  }

  ngOnInit() {
    this.stepTwo.paginator = this.paginatorstepTwo;
    this.stepTwo.sort = this.sortStepTwo;
    this.getGovermentRole();
  }
  applyFilterstepTwo(filterValue: string) {
    this.stepTwo.filter = filterValue.trim().toLowerCase();
    if (this.stepTwo.paginator) {
      this.stepTwo.paginator.firstPage();
    }
  }

  getGovermentRole() {
    this.rolesList = this.userService.GetRoleByGovernment().pipe(map(resp => resp["data"]));
  }

  rolesChanged(event, userid) {
    if (this.userRolesAdd.length > 0) {
      if (this.userRolesAdd.find(x => x.userId === userid)) {
        let roleIds: any = [];
        let itemIndex = this.userRolesAdd.indexOf(this.userRolesAdd.find(x => x.userId === userid));
        this.userRolesAdd[itemIndex].roleIds = [];
        event.forEach(element => {
          this.userRolesAdd[itemIndex].roleIds.push({ roleId: element });
        });
      }
      else {
        let RolesIDsArray = {
          roleIds: [],
          userId: userid
        }
        event.forEach(element => {
          RolesIDsArray.roleIds.push({ roleId: element });
        });
        this.userRolesAdd.push(RolesIDsArray);
      }
    }
    else {
      let RolesIDsArray = {
        roleIds: [],
        userId: userid
      }
      event.forEach(element => {
        RolesIDsArray.roleIds.push({ roleId: element });
      });
      this.userRolesAdd.push(RolesIDsArray);
    }
  }

  CheckboxChange(id) {
    this.changeDataSource(id);
    
    this.userService.ActiveDeactiveUsers(id).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {
        
      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toastrService.error(text);
        });
      }
    });
  }
  changeDataSource(ID: number) {
    let index = this.userList.findIndex(x => x.userId == ID);
    let isActive: boolean = this.userList[index].isActive == true ? false : true;
    this.userList[index].isActive = isActive;
  }

  confirmSelected() {
    this.loading = true;
    this.userService.addRolesSubUsers(this.userRolesAdd).subscribe(data => {
      if (data.code == 200 || data.result.code == 200) {
        this.dialogRef.close();
        this.loading = false;
      }
      else {
        this.loading = false;
      }
      this.cdr.markForCheck();
    })
  }

}
