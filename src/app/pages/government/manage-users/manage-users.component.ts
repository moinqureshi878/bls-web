
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { GovernmentLinkedUsersComponent } from '../government-linked-users/government-linked-users.component';
import { UserService } from '../../../core/appServices/index.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from '@translateService/translation.service';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'kt-manage-users',
	templateUrl: './manage-users.component.html',
	styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit {

	ELEMENT_DATA: any[] = [];
	displayedColumns7: string[] = ['fullNameAr', 'fullNameEn', 'userName', 'linkedUser', 'editView', 'isActive'];
	dataSource7 = new MatTableDataSource<any>(this.ELEMENT_DATA);
	closeResult: string; //Bootstrap Modal Popup

	userModel: any;
	usersData: any;
	@ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
	@ViewChild(MatSort, { static: true }) sort7: MatSort;
	constructor(
		private modalService: NgbModal,
		public dialog: MatDialog,
		public userService: UserService,
		public route: ActivatedRoute,
		public router: Router,
		public cdr: ChangeDetectorRef,
		private translationService: TranslationService,
		public toastrService: ToastrService
	) {
		let data = this.route.snapshot.data.data;
		data.linkedUserloading = false;
		this.usersData = data;
		this.dataSource7 = new MatTableDataSource<any>(data);
	}

	ngOnInit() {
		this.dataSource7.paginator = this.paginator7;
		this.dataSource7.sort = this.sort7;
	}

	applyFilter7(filterValue: string) {
		this.dataSource7.filter = filterValue.trim().toLowerCase();
		if (this.dataSource7.paginator) {
			this.dataSource7.paginator.firstPage();
		}
	}

	open(content) {
		this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}

	openLinkedRoleUsersDialog(row, userID) {
		row.linkedUserloading = true;
		const dialogConfig = new MatDialogConfig();
		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.minWidth = "60%";

		this.userService.GetLinkedUserFromMasterUser(userID).subscribe(res => {
			let response = res;
			if (response.code == 200 || response.result.code == 200) {
				let userList = response.data;
				dialogConfig.data = { userList, userID };
				row.linkedUserloading = false;
				this.cdr.markForCheck();
				this.dialog.open(GovernmentLinkedUsersComponent, dialogConfig);
			}
			else {
				row.linkedUserloading = false;
				this.cdr.markForCheck();
				this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
					this.toastrService.error(text);
				});
			}
		})
	}

	onEditClick(row) {
		this.router.navigate(['/pages/government/edit-govt-users/' + row.userId]);
	}
	CheckboxChange(id) {
		this.changeDataSource(id);
		
		this.userService.ActiveDeactiveUsers(id).subscribe(data => {
			let response = data;
			if ((response.result && response.result.code == 200) || response.code == 200) {
				
			}
			else {
				this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
					this.toastrService.error(text);
				});
			}
		});
	}
	changeDataSource(ID: number) {
		let index = this.usersData.findIndex(x => x.userId == ID);
		let isActive: boolean = this.usersData[index].isActive == true ? false : true;
		this.usersData[index].isActive = isActive;
	}
}
