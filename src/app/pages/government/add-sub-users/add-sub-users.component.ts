import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AsyncFormValidatorService } from '../../../core/validator/async-form-validator.service';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { passwordValidator, nameValidator, emailValidator, commentValidator, matchingPasswords, fileSize, phoneNumberValidator, phoneNumberMasking, emiratesIDMasking, nameArValidator } from '../../../core/validator/form-validators.service';
import { CommonService, UserService, JwtTokenService } from '../../../core/appServices/index.service';
import { TranslationService } from '@translateService/translation.service';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { UserTypeEnum, GenderEnum, CommonEnum } from '../../../core/_enum/index.enum';

export interface Roles {
  id: number;
  name: string;
}

@Component({
  selector: 'kt-add-sub-users',
  templateUrl: './add-sub-users.component.html',
  styleUrls: ['./add-sub-users.component.scss']
})
export class AddSubUsersComponent implements OnInit {

  addSubUserForm: FormGroup;
  attachmentsData = {
    attachments: [
      {
        attachmentName: "atif",
        attachmentURL: "atif.jpg",
      }, {
        attachmentName: "ovais",
        attachmentURL: "ovais.jpg",
      },
    ]
  }
  userId: number;
  userRole: any;
  getAttachmentsType: any;
  isLoading = [];
  loading: boolean = false;
  isDisabled: boolean = false;
  minDate = new Date();
  getUserResponse: any;
  typeGenderCode: Observable<any>;
  CommonEnum = CommonEnum;

  constructor(
    public router: Router,
    public commonService: CommonService,
    private cdr: ChangeDetectorRef,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public userService: UserService,
    public jwtTokenService: JwtTokenService,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public translationService: TranslationService,
    public validationMessages: ValidationMessagesService,
    public toast: ToastrService) {
    debugger
    this.userRole = this.route.snapshot.data.userRole;
    this.getAttachmentsType = this.route.snapshot.data.attachmentData;
    this.getUserResponse = this.route.snapshot.data.userData;

    this.minDate.setDate(this.minDate.getDate() + 1);
    this.route.params.subscribe(params => {
      this.userId = Number(params['id']);
      if (this.userId) {
        this.addSubUser();
        this.setSubUserForm(this.getUserResponse);
      }
      else {
        this.addSubUser();
      }
    });
    // DOM Update After Async Validation Check
    this.addSubUserForm.get('username').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
    this.addSubUserForm.get('citizenID').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
    this.addSubUserForm.get('email').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
    this.addSubUserForm.get('mobileNo').statusChanges.subscribe(status => {
      this.cdr.markForCheck();
    });
  }

  ngOnInit() {
    this.getGenderCode();
  }

  setSubUserForm(response) {
    let roleUser = response.roleUser.map(element => {
      return element.roleId;
    });
    this.addSubUserForm.patchValue({
      username: response.userName,
      password: 'P@ssw0rd',
      confirmPassword: 'P@ssw0rd',
      citizenID: response.userAttributes.citizenId,
      emiratesIDExpiry: response.userAttributes.emiratesIdexpiry,
      userRole: roleUser,
      nameAr: response.fullNameAr,
      nameEn: response.fullNameEn,
      representativeRole: response.userAttributes.role,
      isActive: response.isActive,
      department: response.userAttributes.department,
      email: response.userEmail,
      mobileNo: response.mobileNo,
      preferedLanguage: response.preferedLanguage,
      gender: response.genderCode
    });
  }

  addSubUser() {

    if (this.userId) {
      this.addSubUserForm = this.fb.group({
        attachments: this.fb.array([]),
        username: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameValidator]), this.asyncFormValidatorService.usernameValidator(this.userId)],
        password: [{ value: '', disabled: true }, Validators.compose([Validators.required, Validators.maxLength(30), passwordValidator])],
        confirmPassword: [{ value: '', disabled: true }, Validators.required],
        userRole: ['', Validators.required],
        nameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
        nameEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
        citizenID: ['', Validators.compose([Validators.required]), this.asyncFormValidatorService.citizenIDValidator(this.userId)],
        emiratesIDExpiry: [null],
        representativeRole: ['', Validators.compose([Validators.maxLength(100)])],
        isActive: [true, Validators.required],
        department: ['', Validators.compose([Validators.maxLength(100)])],
        email: ['', Validators.compose([Validators.required, emailValidator]), this.asyncFormValidatorService.emailValidator(this.userId)],
        // mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator]), this.asyncFormValidatorService.mobileNoValidator()],
        mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator]), this.asyncFormValidatorService.mobileNoValidator(this.userId)],
        preferedLanguage: ['ar', Validators.required],
        gender: [null, Validators.required],
      }, { validator: matchingPasswords('password', 'confirmPassword') });

      let control = <FormArray>this.addSubUserForm.controls.attachments;
      this.getUserResponse.attachmentsLog.forEach(x => {
        control.push(this.fb.group({
          id: [x.id],
          userId: [x.userId],
          attachmentName: [x.attachmentName, x.isMandatory ? Validators.required : ''],
          attachmentURL: [x.attachmentUrl],
          attachmentSize: [2097151, x.isMandatory ? Validators.compose([Validators.required, fileSize]) : Validators.compose([fileSize])],
          attachmentExtension: [x.attachmentExtension],
          typeCode: [x.valueEn],
          expiryDate: [new Date()],
          isMandatory: [x.isMandatory],
          inputData: [x.inputData, x.isInputData ? Validators.required : ''],
          attachmentType: [x.attachmentType],
        }))
      });
    }
    else {
      this.addSubUserForm = this.fb.group({
        attachments: this.fb.array([]),
        username: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameValidator]), this.asyncFormValidatorService.usernameValidator(0)],
        password: ['', Validators.compose([Validators.required, Validators.maxLength(30), passwordValidator])],
        confirmPassword: ['', Validators.required],
        citizenID: ['', Validators.compose([Validators.required]), this.asyncFormValidatorService.citizenIDValidator(0)],
        emiratesIDExpiry: [null],
        userRole: ['', Validators.required],
        nameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
        nameEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
        representativeRole: ['', Validators.compose([Validators.maxLength(100)])],
        isActive: [true, Validators.required],
        department: ['', Validators.compose([Validators.maxLength(100)])],
        email: ['', Validators.compose([Validators.required, emailValidator]), this.asyncFormValidatorService.emailValidator(0)],
        mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator]), this.asyncFormValidatorService.mobileNoValidator(0)],
        // mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator]), this.asyncFormValidatorService.mobileNoValidator()],
        preferedLanguage: ['ar', Validators.required],
        gender: [GenderEnum.male, Validators.required],
      }, { validator: matchingPasswords('password', 'confirmPassword') });

      let control = <FormArray>this.addSubUserForm.controls.attachments;
      debugger
      this.getAttachmentsType.forEach((x, index) => {
        control.push(this.fb.group({
          id: [0],
          userId: [0],
          attachmentName: ['', x.isMandatory ? Validators.required : ''],
          attachmentURL: [''],
          attachmentSize: ['', x.isMandatory ? Validators.compose([Validators.required, fileSize]) : Validators.compose([fileSize])],
          attachmentExtension: [''],
          typeCode: [x.valueEn],
          expiryDate: [new Date()],
          isMandatory: [x.isMandatory],
          inputData: ['', x.inputData ? Validators.required : ''],
          attachmentType: [x.id],
        }));
      });
    }
    if (this.userId) {
      this.getUserResponse.attachmentsLog.forEach(att => {
        this.isLoading.push({ loading: false });
      });
    }
    else {
      this.getAttachmentsType.forEach(att => {
        this.isLoading.push({ loading: false });
      });
    }
  }

  addSubUserFormSubmit() {
    const controlArray = <FormArray>this.addSubUserForm.get('attachments');
    for (let index = 0; index < controlArray.length; index++) {
      if (this.userId) {
        if (!this.getUserResponse.attachmentsLog[index].isMandatory) {
          controlArray.controls[index].get('attachmentSize').setValue(2097151);
        }
        // else {
        //   if ((!this.getAttachmentsType[index]) && (!this.getAttachmentsType[index].isMandatory)) {
        //     controlArray.controls[index].get('attachmentSize').setValue(2097151);
        //   }
        // }
      }
    }
    debugger
    if (this.addSubUserForm.valid) {
      this.loading = true;
      let addGovtSubUserFormObj = this.createUserJson(this.addSubUserForm.value);
      if (this.userId) {
        this.userService.EditUser(addGovtSubUserFormObj).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            this.addSubUserForm.reset();
            setTimeout(() => {
              Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
                item.classList.remove("mat-form-field-invalid");
              });
            }, 50);
            this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigateByUrl('/pages/government/manage-sub-users');
          }
        });
      }
      else {
        this.userService.AddUser(addGovtSubUserFormObj).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            this.addSubUserForm.reset();
            setTimeout(() => {
              Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
                item.classList.remove("mat-form-field-invalid");
              });
            }, 50);
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigateByUrl('/pages/government/manage-sub-users');
          }
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.addSubUserForm);
    }
  }
  getFile(event, index) {
    const controlArray = <FormArray>this.addSubUserForm.get('attachments');
    if (event.target.files[0].size > 2097152) {
      controlArray.controls[index].get('attachmentSize').markAsTouched();
      controlArray.controls[index].get('attachmentSize').markAsDirty();
      controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
      //set default attachment name
      // if (this.userId) {
      //   controlArray.controls[index].get('attachmentName').setValue('atiffff');
      // }
    }
    else {
      if (event.target.files[0]) {
        this.isLoading[index].loading = true;
        this.isDisabled = true;
        this.commonService.fileUpload(event.target.files[0]).then(resp => {
          var response = resp;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            controlArray.controls[index].get('attachmentExtension').setValue(event.target.files[0].type);
            controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
            controlArray.controls[index].get('attachmentName').setValue(event.target.files[0].name);

            this.isLoading[index].loading = false;
            this.isDisabled = false;

            this.cdr.markForCheck();
            controlArray.controls[index].get('attachmentURL').setValue(response.attachmentURL);
            controlArray.controls[index].get('attachmentSize').markAsTouched();
            controlArray.controls[index].get('attachmentName').markAsTouched();
          }
          else {
            this.isLoading[index].loading = false;
            this.isDisabled = false;
            this.cdr.markForCheck();
            this.translationService.getTranslation(String(response.result.code)).subscribe((text: string) => {
              this.toast.error(text);
            });
          }
        });
      }
    }
  }
  createUserJson(govtSubUser) {
    let roleUser = [];
    if (govtSubUser.userRole.length > 0) {
      govtSubUser.userRole.forEach(element => {
        let role;
        // if (this.userId) {
        //   role = this.getUserResponse.roleUser.filter(res => {
        //     return res.roleId == element
        //   });
        // }
        // else {
        role = this.userRole.filter(res => {
          return res.id == element
        });
        //}
        roleUser.push({
          "id": 0,
          "roleId": role[0].id,
          "userId": this.userId ? this.getUserResponse.roleUser[0].userId : 0,
          "isActive": true,
          "createdBy": 0,
          "createdAt": new Date(),
          "modifiedBy": 0,
          "modifiedAt": new Date(),
        });
      });

    }

    let addSubUserFormObj = {
      "userId": this.userId ? this.userId : 0,
      "masterId": Number(this.jwtTokenService.getJwtTokenValue().UserID),
      "fullNameAr": govtSubUser.nameAr,
      "fullNameEn": govtSubUser.nameEn,
      "userName": govtSubUser.username,
      "userEmail": govtSubUser.email,
      "password": govtSubUser.password,
      "type": this.CommonEnum.userTypeLoginPage,
      "userType": UserTypeEnum.Govt_Sub_User,
      "mobileNo": govtSubUser.mobileNo,
      "securityToken": "",
      "genderCode": govtSubUser.gender,
      "isConfirmed": true,
      "isActive": Boolean(govtSubUser.isActive),
      "preferedLanguage": govtSubUser.preferedLanguage,
      "createdBy": 0,
      "createdAt": new Date(),
      "modifiedBy": 0,
      "modifiedAt": null,
      "stepCode": 0,
      "loginCounter": 0,
      "lastLogin": new Date(),
      "userAttributes": {
        "userId": this.userId ? this.userId : 0,
        "role": govtSubUser.representativeRole,
        "department": govtSubUser.department,
        "nationalityCode": null,
        "designationCode": null,
        "hrcode": null,
        "businessUnitCode": null,
        "remarks": null,
        "citizenId": govtSubUser.citizenID,
        "citizenCode": 0,
        "passportNo": "",
        "passportExpiryAt": null,
        "licenseNumber": "",
        "licenseExpiryAt": null,
        "issuanceEmirates": 0,
        "bnnameEn": "",
        "bnnameAr": "",
        "nameAr": "",
        "fatherNameAr": "",
        "grandFatherNameAr": "",
        "familyNameAr": "",
        "govtEntityCode": govtSubUser.govtEntity,
        "govtEntityNameEn": govtSubUser.govtEntityNameEn,
        "govtEntityNameAr": govtSubUser.govtEntityNameAr,
        "landlineNo": null,
        "emiratesIdexpiry": this.validationMessages.setDate(govtSubUser.emiratesIDExpiry)
      },
      "attachmentsLog": this.addSubUserForm.value.attachments,
      "physicalDevices": [],
      "roleUser": roleUser
    }
    return addSubUserFormObj;
  }
  getGenderCode() {
    this.typeGenderCode = this.commonService.getLookUp("?type=gender");
  }
  /* Emirates ID Mask */
  emiratesIDMask = emiratesIDMasking();

  /* Mobile No Mask */
  mobileNoMask = phoneNumberMasking();
}
