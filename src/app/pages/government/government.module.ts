import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '../../views/partials/partials.module';

import { GovernmentComponent } from './government.component';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { AddGovtUsersComponent } from './add-govt-users/add-govt-users.component';
import { GovernmentLinkedUsersComponent } from './government-linked-users/government-linked-users.component';
import { ManageSubUsersComponent } from './manage-sub-users/manage-sub-users.component';
import { AddSubUsersComponent } from './add-sub-users/add-sub-users.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
 
import { UserService, ModuleService, SubModuleService, PagesService } from '../../core/appServices/index.service';
import { GovermentResolver } from '../../core/resolvers/goverment.resolver';
import { CommonResolver } from '../../core/resolvers/common.resolver';
import { AppResolver } from '../../core/resolvers/app.resolver';
import { AuthGuard } from '../../core/auth';

import { MatTableExporterModule } from 'mat-table-exporter';
import { TranslateModule } from '@ngx-translate/core';
import {
  MatSortModule,
} from '@angular/material';

@NgModule({
  entryComponents: [GovernmentLinkedUsersComponent],
  declarations: [GovernmentComponent, ManageUsersComponent, AddGovtUsersComponent, GovernmentLinkedUsersComponent, ManageSubUsersComponent, AddSubUsersComponent],

  imports: [
    PartialsModule,
    FormsModule,
    MatSortModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    MatTableExporterModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: GovernmentComponent
      },
      {
        path: 'manage-users',
        canActivate: [AuthGuard],
        component: ManageUsersComponent,
        resolve: { data: GovermentResolver }
      },
      {
        path: 'add-govt-users',
        canActivate: [AuthGuard],
        component: AddGovtUsersComponent,
        resolve: { attachmentData: CommonResolver }
      },
      {
        path: 'edit-govt-users/:id',
        canActivate: [AuthGuard],
        component: AddGovtUsersComponent,
        resolve: {
          userData: GovermentResolver,
          attachmentData: CommonResolver,
        }
      },
      {
        path: 'manage-sub-users',
        canActivate: [AuthGuard],
        component: ManageSubUsersComponent,
        resolve: { data: GovermentResolver }
      },
      {
        path: 'add-sub-users',
        canActivate: [AuthGuard],
        component: AddSubUsersComponent,
        resolve: {
          userRole: AppResolver,
          attachmentData: CommonResolver
        }
      },
      {
        path: 'edit-sub-users/:id',
        canActivate: [AuthGuard],
        component: AddSubUsersComponent,
        resolve: {
          userRole: AppResolver,
          attachmentData: CommonResolver,
          userData: GovermentResolver,
        }
      },
    ]),
  ],
  providers: [
    GovermentResolver,
    UserService,
    CommonResolver,
    AppResolver,
    ModuleService,
    SubModuleService,
    PagesService
  ]
})
export class GovernmentModule { }

