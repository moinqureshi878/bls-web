import { Component, OnInit,ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NationalitiesService } from '../../core/appServices/index.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Nationalitymodel } from '../../core/appModels/Nationalities.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TranslationService } from '../../core/_base/layout';
import { NationaltiesModule } from './nationalties.module';
@Component({
  selector: 'kt-nationalities',
  templateUrl: './nationalities.component.html',
  styleUrls: ['./nationalities.component.scss']
})
export class NationalitiesComponent implements OnInit {
  ELEMENT_DATA: Nationalitymodel[] = [];

  displayedColumns7: string[] = ['Code', 'NationalityAr', 'NationalityEn','CountryAr', 'CountryEn','editView','isActive'];
  dataSource7 = new MatTableDataSource<Nationalitymodel>(this.ELEMENT_DATA);

  closeResult: string; //Bootstrap Modal Popup

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  nationality: any;
  constructor( private modalService: NgbModal,
    private nationalitiesService: NationalitiesService,
    private route: ActivatedRoute,
    private router: Router,
    private translationService: TranslationService) { }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
    this.getAllNates();
  }
  getAllNates(){
    this.nationalitiesService.getAllNats().toPromise().then(data => {
      debugger;
      this.nationality = data.data;
      this.dataSource7 = new MatTableDataSource<Nationalitymodel>(this.nationality);
      this.filterByModuleName();
    });
    // .(map(resp => resp["data"]))
  }
  filterByModuleName() {
    debugger
     
    //this.dataSource7 = new MatTableDataSource<Nationalitymodel>(filterTypes);
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
    this.applyFilter7();
  }
  applyFilter7() {
   // this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  //..Modal Popup Start
  open(content) {

    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;

    });

  }


  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End
  onEditClick(row) {
    this.router.navigate(['/pages/nationalities/add-nationality'],{ queryParams: { id: row.id } } );
  }
  CheckboxChange(code) {
    this.nationalitiesService.DeleteNat(code)
      .subscribe(data => {
        //console.lo g(data);
      })
  }
}

