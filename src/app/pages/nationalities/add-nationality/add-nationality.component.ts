import { Component, OnInit } from '@angular/core';


 
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AsyncFormValidatorService } from '../../../core/validator/async-form-validator.service';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { NationalitiesService } from '../../../core/appServices/index.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Nationalitymodel } from '../../../core/appModels/Nationalities.model';
import { TranslationService } from '../../../core/_base/layout';

@Component({
  selector: 'kt-add-nationality',
  templateUrl: './add-nationality.component.html',
  styleUrls: ['./add-nationality.component.scss']
})
 
export class AddNationalityComponent implements OnInit {
  ID: number;
  nationalityEdit: Nationalitymodel;

  addForm: FormGroup;

  //moduleddl: Observable<any>;

  loading = false;
  country: Observable<any>;
  nationlity: any;
  typeId: number;
  constructor(public router: Router,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    public toastrService: ToastrService,
    private nationalitiesService: NationalitiesService,
    private translationService: TranslationService) {
      this.route
      .queryParams
      .subscribe(params => {
        this.ID = +params['id'];
      });
     }

     ngOnInit() {
       
      this.createSubModuleForm();
      if (this.ID) {
        this.getNatById(this.ID);
       // this.onEdit();
      }
    }
    
    getNatById(id: number){
       this.nationalitiesService.getNatById(id)
      .toPromise().then(data => {
        this.nationalityEdit = data.data;
        this.onEdit();
       
      });
    }
    createSubModuleForm() {
      this.addForm = this.fb.group({       
        code: ['', Validators.required],
        nationalityAr: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
        nationalityEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
        countryAr: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
        countryEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
        isActive: [true, Validators.required],
      });
      //this.lastIndex = this.addSubmodule.value.devices.length - 1;
    }
    addFormSubmit() {
      if (this.addForm.valid) {
        this.loading = true;
  
        let masterJson = {
          "code":this.addForm.value.code,
          "nationalityAr": this.addForm.value.nationalityAr,
          "nationalityEn": this.addForm.value.nationalityEn,
          "isActive": this.addForm.value.isActive,
          "countryAr": this.addForm.value.countryAr,
          "countryEn": this.addForm.value.countryEn,
          "id":0,
          "createdBy": 0,
          "createdAt": "2019-11-25T09:07:59.455Z",      
        }
   
        if (this.ID) {
          this.updateMethod(masterJson);
  
        }
        else {
          this.addMethod(masterJson);
        }
      }
      else {
        this.validationMessages.validateAllFormFields(this.addForm);
      }
    }
    addMethod(natJson: any) {
      this.nationalitiesService.AddNat(natJson)
        .subscribe(data => {
          if (data["result"].code === 200) {
            this.loading = false;
            this.router.navigateByUrl('/pages/nationalities');
          }
          else {
            this.loading = false;
            this.toastrService.error('Something went wrong. Please try again');
          }
        })
    }
  
    updateMethod(natJson: any) {
      this.nationalitiesService.UpdateNat(natJson, this.ID)
        .subscribe(data => {
          if (data["result"].code === 200) {
            this.loading = false;
            this.router.navigateByUrl('/pages/nationalities');
          }
          else {
            this.loading = false;
            this.toastrService.error('Something went wrong. Please try again');
          }
        })
    }
    onEdit() {
  
      if (this.nationalityEdit) {
        this.addForm.patchValue({
          nationalityAr: this.nationalityEdit[0].nationalityAr,
          nationalityEn: this.nationalityEdit[0].nationalityEn,
          countryAr: this.nationalityEdit[0].countryAr,
          countryEn: this.nationalityEdit[0].countryEn,
          code: this.nationalityEdit[0].code,
          isActive: this.nationalityEdit[0].isActive,
        })
      }
    }

}
