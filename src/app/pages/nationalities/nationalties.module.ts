import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NationalitiesComponent } from './nationalities.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatTableModule,
  MatPaginatorModule,
  MatIconModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatTreeModule,
  MatCheckboxModule,
  MatTooltipModule,
  MatSortModule,
} from '@angular/material';


import { PagesService, UserService, NationalitiesService } from '../../core/appServices/index.service';
import { AppResolver } from '../../core/resolvers/app.resolver';
import { TextMaskModule } from 'angular2-text-mask';
import { MatTableExporterModule } from 'mat-table-exporter';
import { AddNationalityComponent } from './add-nationality/add-nationality.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [NationalitiesComponent, AddNationalityComponent],

  exports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatTooltipModule
  ],
  imports: [

    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatTreeModule,
    MatCheckboxModule,
    MatTooltipModule,
    TextMaskModule,
    MatSortModule,
    MatTableExporterModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: NationalitiesComponent
      }
      ,
      {
        path: 'add-nationality',
        component: AddNationalityComponent
      },

    ]),
  ],
  providers: [
    AppResolver,
    PagesService,
    UserService,
    NationalitiesService,
  ]
})

export class NationaltiesModule { }