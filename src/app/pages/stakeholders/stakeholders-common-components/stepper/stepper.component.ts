import { Component, OnInit, ViewChild, VERSION, ChangeDetectorRef, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { matchingPasswords, emailValidator, phoneNumberValidator, fileSize, phoneNumberMasking, emiratesIDMasking, nameValidator, passwordValidator, nameArValidator } from '../../../../core/validator/form-validators.service';
import { AsyncFormValidatorService } from '../../../../core/validator/async-form-validator.service';
// import { ReCaptcha2Component, ReCaptchaV3Service } from 'ngx-captcha';
import { ValidationMessagesService } from '../../../../core/validator/validation-messages.service';
import { EnvService } from '../../../../core/environment/env.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatStepper } from '@angular/material';
import { CommonService, UserService } from '../../../../core/appServices/index.service';
import { Observable } from 'rxjs';
import { TranslationService } from '@translateService/translation.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { GenderEnum, UserTypeEnum, CommonEnum } from '../../../../core/_enum/index.enum';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'kt-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {
  closeResult: string; //Bootstrap Modal Popup

  //Stepper Individual
  basicInfoFormIndividual: FormGroup;
  attachmentsInfo: FormGroup;
  jobSponsorInfo: FormGroup;
  contactInfo: FormGroup;
  address: FormGroup;
  // loginCredentials: FormGroup;

  // Dev WOrking
  UserTypes: any;
  ddlNationalities: Observable<any>;
  typeGenderCode: Observable<any>;

  emirates: Observable<any>;
  mainArea: Observable<any>;
  loading: boolean = false;

  minDate = new Date();
  // @ViewChild('captchaElem', { static: false }) captchaElem: ReCaptcha2Component;
  siteKey: string;
  @ViewChild('stepper', { static: true }) stepper: MatStepper;
  private ngVersion: string = VERSION.full;
  getAttachmentsType: any;
  isLoading = [];
  isTick = [];
  isDisabled: boolean = false;
  setUserTypesValue: number;
  userTypeChangeValue: number;
  userTypeChangeID: number;
  userTypeEnum = UserTypeEnum;
  @Input('userDetail') userDetail: any;
  currentURL: string;
  stakeholderType: number;
  userId: number;
  CommonEnum = CommonEnum;
  stakeholderData: any;
  isUpdatedUserType: boolean = false;
  constructor(
    private modalService: NgbModal,
    public router: Router,
    private fb: FormBuilder,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    private env: EnvService,
    private _formBuilder: FormBuilder,
    private toast: ToastrService,
    private cdr: ChangeDetectorRef,
    // private reCaptchaV3Service: ReCaptchaV3Service,
    public commonService: CommonService,
    public translationService: TranslationService,
    private _DomSanitizationService: DomSanitizer,
    public userService: UserService,
    private route: ActivatedRoute) {
    this.minDate.setDate(this.minDate.getDate() + 1);
    // Set UserType
    this.currentURL = this.router.url;
    if (this.currentURL.match('add-stakeholders-individual') || this.currentURL.match('edit-stakeholders-individual')) {
      this.stakeholderType = UserTypeEnum.stakeholderIndividual;
    }
    else {
      this.stakeholderType = UserTypeEnum.stakeholderInheritanceRepresentative;
    }
    // this.siteKey = this.env.captchaKey;
    // find IsEdit or Add form
    this.route.params.subscribe(params => this.userId = Number(params['id']));

    // get data from resolver to fill radio buttons
    this.UserTypes = this.route.snapshot.data.data;
    this.stakeholderData = this.route.snapshot.data.stakeholderData;
    if (this.userId && !this.stakeholderData.isRegistered) {
      this.setUserTypesValue = this.stakeholderData.userTypeNavigation.id;
      this.userTypeChangeID = this.stakeholderData.userTypeNavigation.id;
    }
    else {
      this.setUserTypesValue = this.UserTypes[0].id;
      this.userTypeChangeID = this.UserTypes[0].id;
    }
  }
  ngOnChanges() {
    // Use when click Add Registered User as Stakeholder
    if (this.userDetail) {
      this.setUserTypesValue = this.userDetail.userTypeNavigation.id;
      this.userTypeChangeID = this.userDetail.userTypeNavigation.id;
      this.Step1BasicInfoFormIndividual();
    }
  }

  ngOnInit() {
    this.Step1BasicInfoFormIndividual();
    this.getAttachments(this.UserTypes[0].id);
    this.Step3SponsorInfo();
    this.Step4ContactInfo();
    this.Step5Address();
    if (this.userId && !this.stakeholderData.isRegistered) {
      this.fillAllForms(this.stakeholderData);
    }
    // fill dropdowns from APIs
    this.getNationalities();
    this.getEmirates();
    this.getMainArea();
    this.getGenderCode();
  }

  // ---------------------Forms Controls----------------------------------------
  Step1BasicInfoFormIndividual() {
    this.basicInfoFormIndividual = this.fb.group({
      citizenID: [{ value: this.userDetail ? this.userDetail.userAttributes.citizenId : '', disabled: this.userDetail }],
      emiratesIDExpiry: [{ value: this.userDetail ? this.userDetail.userAttributes.emiratesIdexpiry : '', disabled: this.userDetail }],
      UID: [{ value: this.userDetail ? this.userDetail.userAttributes.citizenId : null, disabled: this.userDetail }],
      UIDExpiryDate: [{ value: this.userDetail ? this.userDetail.userAttributes.emiratesIdexpiry : null, disabled: this.userDetail }],
      nameAr: [{ value: this.userDetail ? this.userDetail.userAttributes.nameAr : '', disabled: this.userDetail }, Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
      fatherNameAr: [{ value: this.userDetail ? this.userDetail.userAttributes.fatherNameAr : '', disabled: this.userDetail }, Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
      grandFatherNameAr: [{ value: this.userDetail ? this.userDetail.userAttributes.grandFatherNameAr : '', disabled: this.userDetail }, Validators.compose([Validators.maxLength(100), nameArValidator])],
      familyNameAr: [{ value: this.userDetail ? this.userDetail.userAttributes.familyNameAr : '', disabled: this.userDetail }, Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
      fullNameEn: [{ value: this.userDetail ? this.userDetail.fullNameEn : '', disabled: this.userDetail }, Validators.compose([Validators.required, Validators.maxLength(100)])],
      email: [{ value: this.userDetail ? this.userDetail.userEmail : '', disabled: this.userDetail }, Validators.compose([Validators.required, emailValidator]), this.userId ? this.asyncFormValidatorService.emailValidator(this.userId) : this.asyncFormValidatorService.emailValidator(0)],
      mobileNo: [{ value: this.userDetail ? this.userDetail.mobileNo : '', disabled: this.userDetail }, Validators.compose([Validators.required, phoneNumberValidator]), this.userId ? this.asyncFormValidatorService.mobileNoValidator(this.userId) : this.asyncFormValidatorService.mobileNoValidator(0)],
      nationality: [{ value: this.userDetail ? this.userDetail.userAttributes.nationalityCode : CommonEnum.nationality, disabled: this.userDetail }, Validators.compose([Validators.required])],
      passportNo: [{ value: this.userDetail ? this.userDetail.userAttributes.passportNo : '', disabled: this.userDetail }, Validators.compose([Validators.required])],
      passportExpiryDate: [{ value: this.userDetail ? this.userDetail.userAttributes.passportExpiryAt : '', disabled: this.userDetail }, Validators.compose([Validators.required])],
      gender: [{ value: this.userDetail ? this.userDetail.genderCode : GenderEnum.male, disabled: this.userDetail }, Validators.required],
    });
    this.addOrUpdateValidator();
  }
  Step2AttachmentsForm() {
    this.attachmentsInfo = this._formBuilder.group({
      attachments: this.fb.array([]),
    });
    let control = <FormArray>this.attachmentsInfo.controls.attachments;
    if (!this.isUpdatedUserType) {
      if (this.userId && !this.stakeholderData.isRegistered) {
        this.stakeholderData.attachmentsLog.forEach(x => {
          control.push(this.fb.group({
            id: [x.id],
            userId: [x.userId],
            attachmentName: [x.attachmentName, x.isMandatory ? Validators.required : ''],
            attachmentURL: [x.attachmentUrl],
            attachmentSize: [2097151, x.isMandatory ? Validators.compose([Validators.required, fileSize]) : Validators.compose([fileSize])],
            attachmentExtension: [x.attachmentExtension],
            typeCode: [x.valueEn],
            expiryDate: [new Date()],
            isMandatory: [x.isMandatory],
            inputData: [x.inputData, x.isInputData ? Validators.required : ''],
            attachmentType: [x.attachmentType],
          }))
        });
      }
    }
    else {
      this.getAttachmentsType.forEach(x => {
        control.push(this.fb.group({
          id: [0],
          userId: [0],
          attachmentName: ['', x.isMandatory ? Validators.required : ''],
          attachmentURL: [''],
          attachmentSize: ['', x.isMandatory ? Validators.compose([Validators.required, fileSize]) : Validators.compose([fileSize])],
          attachmentExtension: [''],
          typeCode: [x.valueEn],
          expiryDate: [new Date()],
          isMandatory: [x.isMandatory],
          inputData: ['', x.inputData ? Validators.required : ''],
          attachmentType: [x.id],
        }));
      });
    }
    this.isTick = [];

    this.getAttachmentsType.forEach(att => {
      this.isLoading.push({ loading: false });
      this.isTick.push({ tick: false });
    });
  }

  Step3SponsorInfo() {
    this.jobSponsorInfo = this._formBuilder.group({
      designationTextBox: ['', Validators.compose([Validators.maxLength(100)])],
      occupation: ['', Validators.compose([Validators.maxLength(100)])],
      sponsorname: ['', Validators.compose([Validators.maxLength(100)])],
      sponsorType: [''],
      title: ['', Validators.compose([Validators.maxLength(100)])],
    });
  }

  Step4ContactInfo() {
    this.contactInfo = this._formBuilder.group({
      PhoneResidence: [''],
      PhoneWork: [''],
      SecondaryPhone: [''],
      FaxNo: ['']
    });
  }

  Step5Address() {
    this.address = this._formBuilder.group({
      emirates: [''],
      mainArea: [''],
      subArea: ['', Validators.compose([Validators.maxLength(100)])],
      street: ['', Validators.compose([Validators.maxLength(100)])],
      building: ['', Validators.compose([Validators.maxLength(100)])],
      poBox: ['', Validators.compose([Validators.maxLength(100)])],
      remarks: ['']
    });
  }

  // Steps Next Button Click
  Step1ClickNext() {
    if (this.basicInfoFormIndividual.valid) {
      this.stepper.selectedIndex = 1;
    }
    else {
      this.validationMessages.validateAllFormFields(this.basicInfoFormIndividual);
    }
  }
  Step2ClickNext() {
    const controlArray = <FormArray>this.attachmentsInfo.get('attachments');
    for (let index = 0; index < controlArray.length; index++) {
      debugger
      if (!this.getAttachmentsType[index].isMandatory) {
        controlArray.controls[index].get('attachmentSize').reset();
      }
    }

    if (this.attachmentsInfo.valid) {
      this.stepper.selectedIndex = 2;
    }
    else {
      this.validationMessages.validateAllFormFields(this.attachmentsInfo);
    }
  }
  Step3ClickNext() {
    if (this.jobSponsorInfo.valid) {
      this.stepper.selectedIndex = 3;
    }
    else {
      this.validationMessages.validateAllFormFields(this.jobSponsorInfo);
    }
  }
  Step4ClickNext() {
    if (this.contactInfo.valid) {
      this.stepper.selectedIndex = 4;
    }
    else {
      this.validationMessages.validateAllFormFields(this.contactInfo);
    }
  }
  Step5ClickNext() {
    if (this.address.valid) {
      // this.stepper.selectedIndex = 5;
      let documentClearenceJson = {
        "userId": this.userId ? this.userId : 0, // not present in form
        "masterId": 0, // not present in form
        "fullNameAr": this.basicInfoFormIndividual.value.nameAr + this.basicInfoFormIndividual.value.fatherNameAr + this.basicInfoFormIndividual.value.grandFatherNameAr + this.basicInfoFormIndividual.value.familyNameAr,
        "fullNameEn": this.basicInfoFormIndividual.value.fullNameEn,
        "userName": null,
        "userEmail": this.basicInfoFormIndividual.value.email,
        "password": null,
        "type": this.CommonEnum.userTypeLoginVisitorPage,
        "userType": this.userTypeChangeID,   // radio button id not code on document clearence use 24
        "mobileNo": this.basicInfoFormIndividual.value.mobileNo,
        "securityToken": "",
        "genderCode": this.basicInfoFormIndividual.value.gender,
        "isConfirmed": false,
        "isActive": true,
        "preferedLanguage": 'en',
        "createdBy": 0,
        "createdAt": "2019-11-18T06:28:20.535Z",
        "modifiedBy": 0,
        "modifiedAt": null,
        "stepCode": 0,
        "loginCounter": 0,
        "lastLogin": "2019-11-18T06:28:20.535Z",
        "stakeHolderType": this.stakeholderType,
        "userAttributes": {
          "userId": this.userId ? this.userId : 0,
          "role": "",
          "department": "",
          "nationalityCode": +this.basicInfoFormIndividual.value.nationality,
          "designationCode": null,
          "designation": this.jobSponsorInfo.value.designationTextBox,
          "hrcode": null,
          "businessUnitCode": null,
          "remarks": this.address.value.remarks,
          "citizenId": this.basicInfoFormIndividual.value.citizenID ? this.basicInfoFormIndividual.value.citizenID : this.basicInfoFormIndividual.value.UID,
          "citizenCode": this.userTypeChangeID,  // radio button id not code on document clearence use 24
          "passportNo": String(this.basicInfoFormIndividual.value.passportNo),
          "passportExpiryAt": this.basicInfoFormIndividual.value.passportExpiryDate,
          "licenseNumber": this.basicInfoFormIndividual.value.licenceNumber,
          "licenseExpiryAt": this.basicInfoFormIndividual.value.licenceExpiryDate,
          "issuanceEmirates": this.address.value.emirates ? this.address.value.emirates : 0,
          "bnnameEn": this.basicInfoFormIndividual.value.BNnameEn,
          "bnnameAr": this.basicInfoFormIndividual.value.BNnameAr,
          "nameAr": this.basicInfoFormIndividual.value.nameAr,
          "fatherNameAr": this.basicInfoFormIndividual.value.fatherNameAr,
          "grandFatherNameAr": this.basicInfoFormIndividual.value.grandFatherNameAr,
          "familyNameAr": this.basicInfoFormIndividual.value.familyNameAr,
          "govtEntityCode": 0,
          "govtEntityNameEn": "",
          "govtEntityNameAr": "",
          "landlineNo": null,
          "emiratesIdexpiry": this.basicInfoFormIndividual.value.emiratesIDExpiry ? this.basicInfoFormIndividual.value.emiratesIDExpiry : this.basicInfoFormIndividual.value.UIDExpiryDate,
          "ocuupation": this.jobSponsorInfo.value.occupation,
          "sponsorName": this.jobSponsorInfo.value.sponsorname,
          "sponsorType": this.jobSponsorInfo.value.sponsorType,
          "title": this.jobSponsorInfo.value.title,
          "phoneResidence": this.contactInfo.value.PhoneResidence,
          "phoneWork": this.contactInfo.value.PhoneWork,
          "secMobileNo": this.contactInfo.value.SecondaryPhone,
          "faxNo": this.contactInfo.value.FaxNo,
          "mainArea": this.address.value.mainArea ? +this.address.value.mainArea : null,
          "subArea": this.address.value.subArea,
          "street": this.address.value.street,
          "building": this.address.value.building,
          "pobox": this.address.value.poBox,
        },
        "attachmentsLog": this.attachmentsInfo.value.attachments,
        "physicalDevices": [],
        "roleUser": []
      }
      debugger
      if (this.userId) {
        this.userService.EditUser(documentClearenceJson).subscribe(data => {
          this.loading = false;
          let response = data;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            if (this.currentURL.match('add-stakeholders-individual') || this.currentURL.match('edit-stakeholders-individual')) {
              this.router.navigateByUrl('/pages/stakeholders/individual-stakeholders');
            }
            else {
              this.router.navigateByUrl('/pages/stakeholders/representative-stakeholders');
            }
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
          }
        });
      }
      else {
        this.userService.SignUpUsers(documentClearenceJson).subscribe(data => {
          this.loading = false;
          let response = data;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            if (this.currentURL.match('add-stakeholders-individual') || this.currentURL.match('edit-stakeholders-individual')) {
              this.router.navigateByUrl('/pages/stakeholders/individual-stakeholders');
            }
            else {
              this.router.navigateByUrl('/pages/stakeholders/representative-stakeholders');
            }
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
          }
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.address);
    }
  }
  getFile(event, index) {
    const controlArray = <FormArray>this.attachmentsInfo.get('attachments');
    if (event.target.files[0].size > 20913252) {
      controlArray.controls[index].get('attachmentSize').markAsTouched();
      controlArray.controls[index].get('attachmentSize').markAsDirty();
      controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
    }
    else {
      if (event.target.files[0]) {
        this.isLoading[index].loading = true;
        this.isTick[index].tick = true;
        this.isDisabled = true;
        this.commonService.fileUpload(event.target.files[0]).then(resp => {
          var response = resp;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            controlArray.controls[index].get('attachmentExtension').setValue(event.target.files[0].type);
            controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
            controlArray.controls[index].get('attachmentName').setValue(event.target.files[0].name);

            this.isLoading[index].loading = false;
            this.isDisabled = false;

            this.cdr.markForCheck();
            controlArray.controls[index].get('attachmentURL').setValue(response.attachmentURL);
            controlArray.controls[index].get('attachmentSize').markAsTouched();
            controlArray.controls[index].get('attachmentName').markAsTouched();
          }
          else {
            this.isLoading[index].loading = false;
            this.isDisabled = false;
            this.cdr.markForCheck();
            this.translationService.getTranslation(String(response.result.code)).subscribe((text: string) => {
              this.toast.error(text);
            });
          }
        });
      }
    }
  }
  getAttachments(id) {
    this.commonService.GetAttachmentByUserTypeCode(id).subscribe(res => {
      let response = res;
      if ((response.result && response.result.code == 200) || (response.code == 200)) {
        this.getAttachmentsType = response.result.data;
        this.Step2AttachmentsForm();
      }
    });
  }
  userTypeChange(content, event, value) {
    //  this.userTypeChangeValue= event.value;
    if (this.stepper.selectedIndex == 0) {
      this.userTypeChangeValue = value.id;
      this.userTypeChangeID = value.id;
      this.isUpdatedUserType = true;
      debugger
      this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;

      });
      return false;
    }
  }

  // Get Nationality
  getNationalities() {
    this.ddlNationalities = this.commonService.getLookUp("?type=Nationality");
  }
  getEmirates() {
    this.emirates = this.commonService.getLookUp("?type=Emirates");
  }
  getMainArea() {
    this.mainArea = this.commonService.getLookUp("?type=MainAreas");
  }
  getGenderCode() {
    this.typeGenderCode = this.commonService.getLookUp("?type=gender");
  }
  /* Emirates ID Mask */
  emiratesIDMask = emiratesIDMasking();

  /* Mobile No Mask */
  mobileNoMask = phoneNumberMasking();

  //..Modal Popup Start
  private getDismissReason(reason: any): string {
    this.setUserTypesValue = this.setUserTypesValue;
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  @ViewChild('basicInfoForm', { static: false }) basicInfoForm;
  alertClearFormClick() {
    debugger
    this.setUserTypesValue = this.userTypeChangeValue;
    this.getAttachments(this.setUserTypesValue);
    this.modalService.dismissAll();

    this.stepper.reset();
    // this.loginCredentials.get('preferedLanguage').setValue('ar');
    // this.basicInfoForm.resetForm();
    setTimeout(() => {
      Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
        item.classList.remove("mat-form-field-invalid");
      });
    }, 100);
    this.addOrUpdateValidator();
  }
  addOrUpdateValidator() {
    if (this.setUserTypesValue == this.userTypeEnum.UAE_Citizen || this.setUserTypesValue == this.userTypeEnum.Resident_in_UAE) {
      this.basicInfoFormIndividual.controls["citizenID"].setValidators(Validators.required);
      this.basicInfoFormIndividual.controls["citizenID"].setAsyncValidators(this.userId ? this.asyncFormValidatorService.citizenIDValidator(this.userId) : this.asyncFormValidatorService.citizenIDValidator(0));
      this.basicInfoFormIndividual.controls["emiratesIDExpiry"].setValidators(Validators.required);
      this.basicInfoFormIndividual.controls["citizenID"].updateValueAndValidity();
      this.basicInfoFormIndividual.controls["emiratesIDExpiry"].updateValueAndValidity();

      this.basicInfoFormIndividual.controls["UID"].clearValidators();
      this.basicInfoFormIndividual.controls["UIDExpiryDate"].clearValidators();
      this.basicInfoFormIndividual.get('UID').updateValueAndValidity();
      this.basicInfoFormIndividual.get('UIDExpiryDate').updateValueAndValidity();

      this.basicInfoFormIndividual.controls['nationality'].setValue(CommonEnum.nationality);
      this.basicInfoFormIndividual.controls['gender'].setValue(GenderEnum.male);
    }
    else if (this.setUserTypesValue == this.userTypeEnum.GCC_Citizen || this.setUserTypesValue == this.userTypeEnum.Visitor_with_UID) {
      this.basicInfoFormIndividual.controls["UID"].setValidators(Validators.required);
      this.basicInfoFormIndividual.controls["UID"].setAsyncValidators(this.userId ? this.asyncFormValidatorService.citizenIDValidator(this.userId) : this.asyncFormValidatorService.citizenIDValidator(0));
      this.basicInfoFormIndividual.controls["UIDExpiryDate"].setValidators(Validators.required);
      this.basicInfoFormIndividual.controls["UID"].updateValueAndValidity();
      this.basicInfoFormIndividual.controls["UIDExpiryDate"].updateValueAndValidity();

      this.basicInfoFormIndividual.controls["citizenID"].clearValidators();
      this.basicInfoFormIndividual.controls["emiratesIDExpiry"].clearValidators();
      this.basicInfoFormIndividual.get('citizenID').updateValueAndValidity();
      this.basicInfoFormIndividual.get('emiratesIDExpiry').updateValueAndValidity();

      this.basicInfoFormIndividual.controls['nationality'].setValue(CommonEnum.nationality);
      this.basicInfoFormIndividual.controls['gender'].setValue(GenderEnum.male);
    }
    else {
      this.basicInfoFormIndividual.controls["UID"].setValidators(Validators.required);
      this.basicInfoFormIndividual.controls["UID"].setAsyncValidators(this.userId ? this.asyncFormValidatorService.citizenIDValidator(this.userId) : this.asyncFormValidatorService.citizenIDValidator(0));
      this.basicInfoFormIndividual.controls["UIDExpiryDate"].setValidators(Validators.required);
      this.basicInfoFormIndividual.controls["UID"].updateValueAndValidity();
      this.basicInfoFormIndividual.controls["UIDExpiryDate"].updateValueAndValidity();

      this.basicInfoFormIndividual.controls["citizenID"].clearValidators();
      this.basicInfoFormIndividual.controls["emiratesIDExpiry"].clearValidators();
      this.basicInfoFormIndividual.get('citizenID').updateValueAndValidity();
      this.basicInfoFormIndividual.get('emiratesIDExpiry').updateValueAndValidity();

      this.basicInfoFormIndividual.controls['nationality'].setValue(CommonEnum.nationality);
      this.basicInfoFormIndividual.controls['gender'].setValue(GenderEnum.male);
    }
  }
  addStakeholder() {
    debugger
    this.loading = true;
    this.userService.AddUserAsStakeHolder(this.userDetail.userId).subscribe(res => {
      let response = res;
      this.loading = false;
      this.cdr.markForCheck();
      if ((response.result && response.result.code == 200) || response.code == 200) {
        this.router.navigateByUrl('/pages/stakeholders/individual-stakeholders');
        this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
          this.toast.success(text);
        });
      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toast.error(text);
        });
      }
    });
  }
  fillAllForms(userDetailData: any) {
    let userDetail = userDetailData;
    this.basicInfoFormIndividual.patchValue({
      "citizenID": userDetail.userAttributes.citizenId,
      "emiratesIDExpiry": userDetail.userAttributes.emiratesIdexpiry,
      "UID": userDetail.userAttributes.citizenId,
      "UIDExpiryDate": userDetail.userAttributes.emiratesIdexpiry,
      "nameAr": userDetail.userAttributes.nameAr,
      "fatherNameAr": userDetail.userAttributes.fatherNameAr,
      "grandFatherNameAr": userDetail.userAttributes.grandFatherNameAr,
      "familyNameAr": userDetail.userAttributes.familyNameAr,
      "fullNameEn": userDetail.fullNameEn,
      "email": userDetail.userEmail,
      "mobileNo": userDetail.mobileNo,
      "nationality": userDetail.userAttributes.nationalityCode,
      "passportNo": userDetail.userAttributes.passportNo,
      "passportExpiryDate": userDetail.userAttributes.emiratesIdexpiry,
      "gender": userDetail.genderCode
    });
    this.jobSponsorInfo.patchValue({
      "designationTextBox": userDetail.userAttributes.designation,
      "occupation": userDetail.userAttributes.ocuupation,
      "sponsorname": userDetail.userAttributes.sponsorName,
      "sponsorType": userDetail.userAttributes.sponsorType,
      "title": userDetail.userAttributes.title
    });
    this.contactInfo.patchValue({
      "PhoneResidence": userDetail.userAttributes.phoneResidence,
      "PhoneWork": userDetail.userAttributes.phoneWork,
      "SecondaryPhone": userDetail.userAttributes.secMobileNo,
      "FaxNo": userDetail.userAttributes.faxNo
    });
    this.address.patchValue({
      "emirates": userDetail.userAttributes.issuanceEmirates,
      "mainArea": userDetail.userAttributes.mainArea,
      "subArea": userDetail.userAttributes.subArea,
      "street": userDetail.userAttributes.street,
      "building": userDetail.userAttributes.building,
      "poBox": userDetail.userAttributes.pobox,
      "remarks": userDetail.userAttributes.remarks
    });
  }
}