import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndividualStakeholdersComponent } from './individual-stakeholders.component';

describe('IndividualStakeholdersComponent', () => {
  let component: IndividualStakeholdersComponent;
  let fixture: ComponentFixture<IndividualStakeholdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndividualStakeholdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndividualStakeholdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
