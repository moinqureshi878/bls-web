import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddStakeholdersIndividualComponent } from './add-stakeholders-individual.component';

describe('AddStakeholdersIndividualComponent', () => {
  let component: AddStakeholdersIndividualComponent;
  let fixture: ComponentFixture<AddStakeholdersIndividualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddStakeholdersIndividualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddStakeholdersIndividualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
