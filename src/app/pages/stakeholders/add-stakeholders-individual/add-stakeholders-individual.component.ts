import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { phoneNumberMasking, emiratesIDMasking, phoneNumberValidator } from 'app/core/validator/form-validators.service';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { UserService } from 'app/core/appServices/index.service';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from 'app/core/_base/layout';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'kt-add-stakeholders-individual',
  templateUrl: './add-stakeholders-individual.component.html',
  styleUrls: ['./add-stakeholders-individual.component.scss']
})
export class AddStakeholdersIndividualComponent implements OnInit {
  stakeHolderType: string;
  emiratesIDMask = emiratesIDMasking();
  mobileNoMask = phoneNumberMasking();
  registeredStakeholderForm: FormGroup;
  loading: boolean = false;
  userDetail: any;
  getUserById: any;
  constructor(public fb: FormBuilder,
    public validationMessages: ValidationMessagesService,
    private translationService: TranslationService,
    private toast: ToastrService,
    public userService: UserService,
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute
  ) {
    this.getUserById = this.route.snapshot.data.stakeholderData;

  }

  ngOnInit() {

    if (this.getUserById && this.getUserById.isRegistered) {
      this.registeredStakeholderForm = this.fb.group({
        citizenID: [this.getUserById.userAttributes.citizenId, Validators.compose([Validators.required])],
        mobileNo: [this.getUserById.mobileNo, Validators.compose([Validators.required, phoneNumberValidator])],
      });
      this.stakeHolderType = 'addRegisteredUserAsStakeholder';
      this.userDetail = this.getUserById;
    }
    else {
      this.registeredStakeholderForm = this.fb.group({
        citizenID: ['', Validators.compose([Validators.required])],
        mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator])],
      });
      this.stakeHolderType = 'addNewStakeholder';
    }
  }
  setStakeholdersType(type) {
    debugger
    if (this.getUserById) {
      return false;
    }
    else {
      this.stakeHolderType = type;
    }
  }
  registeredStakeholderFormSubmit() {
    if (this.registeredStakeholderForm.valid) {
      this.loading = true;
      this.userService.GetUserByID(this.registeredStakeholderForm.value.citizenID, this.registeredStakeholderForm.value.mobileNo).subscribe(res => {
        debugger
        let response = res;
        this.loading = false;
        this.cdr.markForCheck();
        if ((response.result && response.result.code == 200) || (response.code == 200)) {
          debugger
          this.stakeHolderType = 'addRegisteredUserAsStakeholder';
          this.userDetail = response.data;
        }
        else {
          this.translationService.getTranslation(String(response.result.code)).subscribe((text: string) => {
            this.toast.error(text);
          });
        }
      });
    }
    else {
      this.validationMessages.validateAllFormFields(this.registeredStakeholderForm);
    }
  }
}
