import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'kt-add-representative-stakeholders',
  templateUrl: './add-representative-stakeholders.component.html',
  styleUrls: ['./add-representative-stakeholders.component.scss']
})
export class AddRepresentativeStakeholdersComponent implements OnInit {

  emCopy: string = "ID front side";
  embCopy: string = "ID back side";
  passportCopy: string = "Passport info pages";
  visaCopy: string = "Visa page";


  //Stepper active class
  stepperValue: number = 1;

  //Form Group Stepper
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;

  radioButtonCondition: number = 0;

  constructor(private _formBuilder: FormBuilder, ) { }

  ngOnInit() {

    this.firstFormGroup = this._formBuilder.group({
      // firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      // secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      // thirdCtrl: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      // fourthCtrl: ['', Validators.required]
    });
    this.fifthFormGroup = this._formBuilder.group({
      // fifthCtrl: ['', Validators.required]
    });
    this.sixthFormGroup = this._formBuilder.group({
      // sixthCtrl: ['', Validators.required]
    });

  }
  //Radio Button Condtion
  setRadioButtonCondition(value: number) {
    this.radioButtonCondition = value;
  }


  getEmCopy(event) {
    this.emCopy = event.target.files[0].name.substring(0, 15);
  }
  getEmbId(event) {
    this.embCopy = event.target.files[0].name.substring(0, 15);
  }
  getPassportCopy(event) {
    this.passportCopy = event.target.files[0].name.substring(0, 15);
  }
  getVisa(event) {
    this.visaCopy = event.target.files[0].name.substring(0, 15);
  }


  /* Emirates ID Mask */
  emiratesIDMask = [/[0-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/]

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);

}