import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRepresentativeStakeholdersComponent } from './add-representative-stakeholders.component';

describe('AddRepresentativeStakeholdersComponent', () => {
  let component: AddRepresentativeStakeholdersComponent;
  let fixture: ComponentFixture<AddRepresentativeStakeholdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRepresentativeStakeholdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRepresentativeStakeholdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
