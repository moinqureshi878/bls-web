import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepresentativeStakeholdersComponent } from './representative-stakeholders.component';

describe('RepresentativeStakeholdersComponent', () => {
  let component: RepresentativeStakeholdersComponent;
  let fixture: ComponentFixture<RepresentativeStakeholdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepresentativeStakeholdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepresentativeStakeholdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
