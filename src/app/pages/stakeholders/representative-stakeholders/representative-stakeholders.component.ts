
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from 'app/core/_base/layout';

/** Builds and returns a new User. */
const ELEMENT_DATA = [
  { nameAr: 'Atif', nameEn: 'عاطف', emid: '000-0000-0000000-0', nationality: 'UAE', mobile: '', editView: '', },
  { nameAr: 'Ovais', nameEn: 'اویس', emid: '000-0000-0000000-0', nationality: 'Pakistani', mobile: '', editView: '', },
  { nameAr: 'Moeen', nameEn: 'معین', emid: '000-0000-0000000-0', nationality: 'Turkish', mobile: '', editView: '', },
  { nameAr: 'Atif', nameEn: 'عاطف', emid: '000-0000-0000000-0', nationality: 'UAE', mobile: '', editView: '', },
  { nameAr: 'Ovais', nameEn: 'اویس', emid: '000-0000-0000000-0', nationality: 'Pakistani', mobile: '', editView: '', },
];

@Component({
  selector: 'kt-representative-stakeholders',
  templateUrl: './representative-stakeholders.component.html'
})
export class RepresentativeStakeholdersComponent implements OnInit {
  displayedColumns7: string[] = ['nameAr', 'nameEn', 'emid', 'nationality', 'mobile', 'editView'];
  dataSource7 = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private translationService: TranslationService,
    private toast: ToastrService,) {
      debugger
    let userData = this.route.snapshot.data.data;
    this.dataSource7 = new MatTableDataSource(userData);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

}
