import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '../../views/partials/partials.module';

import { StakeholdersComponent } from './stakeholders.component';
import { AddStakeholdersIndividualComponent } from './add-stakeholders-individual/add-stakeholders-individual.component';
import { IndividualStakeholdersComponent } from './individual-stakeholders/individual-stakeholders.component';
import { RepresentativeStakeholdersComponent } from './representative-stakeholders/representative-stakeholders.component';
import { TranslateModule } from '@ngx-translate/core';
import { AddRepresentativeStakeholdersComponent } from './add-representative-stakeholders/add-representative-stakeholders.component';
import { StepperComponent } from './stakeholders-common-components/stepper/stepper.component';
import { RegistrationResolver } from 'app/core/resolvers/regsitration.resolver';
import { AuthGuard } from 'app/core/auth';
import { StakeholderResolver } from 'app/core/resolvers/stakeholder.resolver';
import { MatTableExporterModule } from 'mat-table-exporter';
import {
  MatSortModule,
} from '@angular/material';
@NgModule({
  declarations: [StakeholdersComponent, AddStakeholdersIndividualComponent, IndividualStakeholdersComponent, RepresentativeStakeholdersComponent, AddRepresentativeStakeholdersComponent, StepperComponent],
  imports: [
    PartialsModule,
    MatTableExporterModule,
    MatSortModule,
    NgbModule.forRoot(),
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: StakeholdersComponent
      },
      {
        path: 'individual-stakeholders',
        component: IndividualStakeholdersComponent,
        canActivate: [AuthGuard],
        resolve: { data: StakeholderResolver },
      },
      {
        path: 'add-stakeholders-individual',
        component: AddStakeholdersIndividualComponent,
        resolve: { data: RegistrationResolver },
        canActivate: [AuthGuard]
      },
      {
        path: 'edit-stakeholders-individual/:id',
        component: AddStakeholdersIndividualComponent,
        resolve: {
          data: RegistrationResolver,
          stakeholderData: StakeholderResolver,
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'representative-stakeholders',
        component: RepresentativeStakeholdersComponent,
        canActivate: [AuthGuard],
        resolve: { data: StakeholderResolver },
      },
      {
        path: 'add-representative-stakeholders',
        component: AddRepresentativeStakeholdersComponent,
        resolve: { data: RegistrationResolver },
        canActivate: [AuthGuard]
      },
      {
        path: 'edit-representative-stakeholders/:id',
        component: AddRepresentativeStakeholdersComponent,
        resolve: {
          data: RegistrationResolver,
          stakeholderData: StakeholderResolver,
        },
        canActivate: [AuthGuard]
      },
      {
        path: 'stepper',
        component: StepperComponent

      }

    ]),
  ]
})
export class StakeholdersModule { }