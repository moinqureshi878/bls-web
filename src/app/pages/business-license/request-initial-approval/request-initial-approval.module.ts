import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '../../../views/partials/partials.module';

import { RequestInitialApprovalComponent } from './request-initial-approval.component';
import { AddInitialApprovalComponent } from './add-initial-approval/add-initial-approval.component';
import { InitialApprovalsComponent } from './initial-approvals/initial-approvals.component';

// import { RequestBusinessNameComponent } from './request-business-name.component';
// import { BusinessNamesComponent } from './business-names/business-names.component';
// import { ExtendBusinessNamesComponent } from './model/extend-business-names/extend-business-names.component';
// import { BusinessNamesModificationListComponent } from './model/business-names-modification-list/business-names-modification-list.component';
// import { CancelBusinessNameComponent } from './model/cancel-business-name/cancel-business-name.component';
// import { BusinessNameModificationComponent } from './model/business-name-modification/business-name-modification.component';
// import { AddStakeholdersModalComponent } from './model/add-stakeholders-modal/add-stakeholders-modal.component';
// import { ExtendBusinessExtensionComponent } from './model/extend-business-extension/extend-business-extension.component';
// import { AddBusinessNamesComponent } from './add-business-names/add-business-names.component';
import { BusinessActivityComponent } from '../bn-common-components/business-activity/business-activity.component';
// import { BusinessNamesAvailabilityComponent } from '../bn-common-components/business-names-availability/business-names-availability.component';
import { BnCommonComponentsModule } from '../bn-common-components/bn-common-components.module';
import { ExtendInitialApprovalsComponent } from './model/extend-initial-approvals/extend-initial-approvals.component';
import { InitialApprovalsModificationListComponent } from './model/initial-approvals-modification-list/initial-approvals-modification-list.component';
import { CancelInitialApprovalsComponent } from './model/cancel-initial-approvals/cancel-initial-approvals.component';
import { ExtendInitialApprovalsExtensionComponent } from './model/extend-initial-approvals-extension/extend-initial-approvals-extension.component';
import { InitialApprovalsModificationComponent } from './model/initial-approvals-modification/initial-approvals-modification.component';
import { ViewedModifiedCustomerIaComponent } from './viewed-modified-customer-ia/viewed-modified-customer-ia.component';
import { AuthGuard } from 'app/core/auth';
import { IAResolver } from 'app/core/resolvers/ia.resolver';
// import { ViewedModifiedCustomerBnComponent } from './viewed-modified-customer-bn/viewed-modified-customer-bn.component';
import {
  MatSortModule,
} from '@angular/material';


@NgModule({
  entryComponents: [
    ExtendInitialApprovalsComponent, InitialApprovalsModificationListComponent, CancelInitialApprovalsComponent,
    ExtendInitialApprovalsExtensionComponent, InitialApprovalsModificationComponent
  ],
  declarations: [
    RequestInitialApprovalComponent, AddInitialApprovalComponent, InitialApprovalsComponent, ExtendInitialApprovalsComponent, InitialApprovalsModificationListComponent, CancelInitialApprovalsComponent, ExtendInitialApprovalsExtensionComponent, InitialApprovalsModificationComponent, ViewedModifiedCustomerIaComponent
  ],
  exports: [],
  imports: [
    PartialsModule,
    MatSortModule,
    BnCommonComponentsModule,
    NgbModule.forRoot(),
    RouterModule.forChild([
      {
        path: '',
        component: InitialApprovalsComponent
      },
      {
        path: 'initial-approvals',
        canActivate: [AuthGuard],
        component: InitialApprovalsComponent,
        resolve: { data: IAResolver }
      },
      {
        path: 'add-initial-approval',
        component: AddInitialApprovalComponent
      },
      {
        path: 'viewed-modified-customer-ia',
        component: ViewedModifiedCustomerIaComponent
      },
      {
        path: 'extend-initial-approvals-extension',
        component: ExtendInitialApprovalsExtensionComponent
      },
      {
        path: 'extend-initial-approvals',
        component: ExtendInitialApprovalsComponent
      },
      {
        path: 'cancel-initial-approvals',
        component: CancelInitialApprovalsComponent
      },
      {
        path: 'initial-approvals-modification-list',
        component: InitialApprovalsModificationListComponent
      },
      {
        path: 'initial-approvals-modification',
        component: InitialApprovalsModificationComponent
      },

    ]),
  ],

})
export class RequestInitialApprovalModule { }







