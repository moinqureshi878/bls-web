import { Component, OnInit, } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { TooltipPosition } from '@angular/material/tooltip';


@Component({
  selector: 'kt-initial-approvals-modification',
  templateUrl: './initial-approvals-modification.component.html',
  styleUrls: ['./initial-approvals-modification.component.scss']
})
export class InitialApprovalsModificationComponent implements OnInit {

  closeResult: string; //Bootstrap Modal Popup
  emCopy: string = "ID front side";
  embCopy: string = "ID back side";
  passportCopy: string = "Passport info pages";
  visaCopy: string = "Visa page";
  //Stepper active class
  stepperValue: number = 1;

  //Form Group Stepper
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  eighthFormGroup: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
    });
    this.secondFormGroup = this._formBuilder.group({
    });
    this.sixthFormGroup = this._formBuilder.group({
    });
    this.eighthFormGroup = this._formBuilder.group({
    });
  }

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);

  getEmCopy(event) {
    this.emCopy = event.target.files[0].name.substring(0, 15);
    // + "." + event.target.files[0].name.split('.')[1]
  }
  getEmbId(event) {
    this.embCopy = event.target.files[0].name.substring(0, 15);
  }
  getPassportCopy(event) {
    this.passportCopy = event.target.files[0].name.substring(0, 15);
  }
  getVisa(event) {
    this.visaCopy = event.target.files[0].name.substring(0, 15);
  }

  //Stepper Value
  setStepperValue(value: number) {
    this.stepperValue = value;
  }

  //..Modal Popup Start
  open(content) {

    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

}