import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitialApprovalsModificationComponent } from './initial-approvals-modification.component';

describe('InitialApprovalsModificationComponent', () => {
  let component: InitialApprovalsModificationComponent;
  let fixture: ComponentFixture<InitialApprovalsModificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitialApprovalsModificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitialApprovalsModificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
