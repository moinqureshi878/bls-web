import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendInitialApprovalsComponent } from './extend-initial-approvals.component';

describe('ExtendInitialApprovalsComponent', () => {
  let component: ExtendInitialApprovalsComponent;
  let fixture: ComponentFixture<ExtendInitialApprovalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtendInitialApprovalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendInitialApprovalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
