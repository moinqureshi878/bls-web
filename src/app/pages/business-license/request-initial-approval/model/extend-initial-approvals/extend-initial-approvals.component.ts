import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { FormControl } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';


/* Step Two Date */
export interface stepTwoData {
  applicationNo: string;
  extendDays: string;
  requestStatus: string;
  requestDate: string;
  editView: string;
}

/** Builds and returns a new User. */
const STEP_TWO_DATA: stepTwoData[] = [
  { applicationNo: 'IAE-1', extendDays: '90', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'IAE-2', extendDays: '30', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'IAE-3', extendDays: '30', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'IAE-4', extendDays: '60', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'IAE-5', extendDays: '30', requestStatus: 'Submitted', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'IAE-6', extendDays: '90', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'IAE-7', extendDays: '90', requestStatus: 'Submitted', requestDate: '12/12/2019', editView: '' }
];

@Component({
  selector: 'kt-extend-initial-approvals',
  templateUrl: './extend-initial-approvals.component.html',
  styleUrls: ['./extend-initial-approvals.component.scss']
})
export class ExtendInitialApprovalsComponent implements OnInit {

  /* Step Two */
  displayedColumnsstepTwo: string[] = ['applicationNo', 'extendDays', 'requestStatus', 'requestDate', 'editView'];
  stepTwo = new MatTableDataSource<stepTwoData>(STEP_TWO_DATA);

  @ViewChild('matPaginatorstepTwo', { static: true }) paginatorstepTwo: MatPaginator;
  @ViewChild('sortStepTwo', { static: true }) sortStepTwo: MatSort;
  constructor(public dialog: MatDialog) {

  }

  ngOnInit() {

    /* Step Two */
    this.stepTwo.paginator = this.paginatorstepTwo;
    this.stepTwo.sort = this.sortStepTwo;
  }

  applyFilterstepTwo(filterValue: string) {
    this.stepTwo.filter = filterValue.trim().toLowerCase();

    if (this.stepTwo.paginator) {
      this.stepTwo.paginator.firstPage();
    }
  }


  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

}