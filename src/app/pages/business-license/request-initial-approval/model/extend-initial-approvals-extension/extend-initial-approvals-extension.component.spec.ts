import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendInitialApprovalsExtensionComponent } from './extend-initial-approvals-extension.component';

describe('ExtendInitialApprovalsExtensionComponent', () => {
  let component: ExtendInitialApprovalsExtensionComponent;
  let fixture: ComponentFixture<ExtendInitialApprovalsExtensionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtendInitialApprovalsExtensionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendInitialApprovalsExtensionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
