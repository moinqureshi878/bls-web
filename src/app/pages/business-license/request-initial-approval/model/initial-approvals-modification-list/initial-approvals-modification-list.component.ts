import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormControl } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';



/* Step Two Date */
export interface stepTwoData {
  applicationNo: string;
  submissionReason: string;
  requestStatus: string;
  requestDate: string;
  editView: string;
}

/** Builds and returns a new User. */
const STEP_TWO_DATA: stepTwoData[] = [
  { applicationNo: 'IAM-1', submissionReason: '', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'IAM-2', submissionReason: '', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'IAM-3', submissionReason: '', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'IAM-4', submissionReason: '', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'IAM-5', submissionReason: 'BN Modified', requestStatus: 'Submitted', requestDate: '12/12/2019', editView: '' },
];

@Component({
  selector: 'kt-initial-approvals-modification-list',
  templateUrl: './initial-approvals-modification-list.component.html',
  styleUrls: ['./initial-approvals-modification-list.component.scss']
})
export class InitialApprovalsModificationListComponent implements OnInit {

  /* Step Two */
  displayedColumnsstepTwo: string[] = ['applicationNo', 'submissionReason', 'requestStatus', 'requestDate', 'editView'];
  stepTwo = new MatTableDataSource<stepTwoData>(STEP_TWO_DATA);

  @ViewChild('matPaginatorstepTwo', { static: true }) paginatorstepTwo: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortStepTwo: MatSort;
  constructor() {

  }

  ngOnInit() {

    /* Step Two */
    this.stepTwo.paginator = this.paginatorstepTwo;
    this.stepTwo.sort = this.sortStepTwo;
  }

  applyFilterstepTwo(filterValue: string) {
    this.stepTwo.filter = filterValue.trim().toLowerCase();

    if (this.stepTwo.paginator) {
      this.stepTwo.paginator.firstPage();
    }
  }

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

}