import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitialApprovalsModificationListComponent } from './initial-approvals-modification-list.component';

describe('InitialApprovalsModificationListComponent', () => {
  let component: InitialApprovalsModificationListComponent;
  let fixture: ComponentFixture<InitialApprovalsModificationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitialApprovalsModificationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitialApprovalsModificationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
