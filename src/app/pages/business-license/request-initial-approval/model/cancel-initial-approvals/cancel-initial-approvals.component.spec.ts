import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelInitialApprovalsComponent } from './cancel-initial-approvals.component';

describe('CancelInitialApprovalsComponent', () => {
  let component: CancelInitialApprovalsComponent;
  let fixture: ComponentFixture<CancelInitialApprovalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelInitialApprovalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelInitialApprovalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
