import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ViewSubmittedIaRequestComponent } from '../../bn-common-components/model/view-submitted-ia-request/view-submitted-ia-request.component';
import { ViewSubmittedBnRequestComponent } from "../../bn-common-components/model/view-submitted-bn-request/view-submitted-bn-request.component";

@Component({
  selector: 'kt-viewed-modified-customer-ia',
  templateUrl: './viewed-modified-customer-ia.component.html',
  styleUrls: ['./viewed-modified-customer-ia.component.scss']
})
export class ViewedModifiedCustomerIaComponent implements OnInit {
  panelOpenState = false;
  emCopy: string = "ID front side";
  embCopy: string = "ID back side";
  passportCopy: string = "Passport info pages";
  visaCopy: string = "Visa page";
  stepperValue: number = 1;

  constructor(
    private _formBuilder: FormBuilder,
    public dialog: MatDialog,
  ) { }

  ngOnInit() { }

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);

  getEmCopy(event) {
    this.emCopy = event.target.files[0].name.substring(0, 15);
  }
  getEmbId(event) {
    this.embCopy = event.target.files[0].name.substring(0, 15);
  }
  getPassportCopy(event) {
    this.passportCopy = event.target.files[0].name.substring(0, 15);
  }
  getVisa(event) {
    this.visaCopy = event.target.files[0].name.substring(0, 15);
  }
  //Stepper Value
  setStepperValue(value: number) {
    this.stepperValue = value;
  }


  /* begin:: Display ExtendGrid */
  openBusinessModifications() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewSubmittedIaRequestComponent, dialogConfig);

  }
  /* end:: Display ExtendGrid */

  /* begin:: Open Business Request */
  openSubmittedBNRequest() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);

  }
  /* end:: Open Business Request */


}