import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewedModifiedCustomerIaComponent } from './viewed-modified-customer-ia.component';

describe('ViewedModifiedCustomerIaComponent', () => {
  let component: ViewedModifiedCustomerIaComponent;
  let fixture: ComponentFixture<ViewedModifiedCustomerIaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewedModifiedCustomerIaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewedModifiedCustomerIaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
