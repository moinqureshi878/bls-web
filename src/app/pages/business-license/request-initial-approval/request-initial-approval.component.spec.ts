import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestInitialApprovalComponent } from './request-initial-approval.component';

describe('RequestInitialApprovalComponent', () => {
  let component: RequestInitialApprovalComponent;
  let fixture: ComponentFixture<RequestInitialApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestInitialApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestInitialApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
