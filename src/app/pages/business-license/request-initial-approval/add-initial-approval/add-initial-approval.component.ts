import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig, MatStepper } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
import { ViewSubmittedBnRequestComponent } from '../../bn-common-components/model/view-submitted-bn-request/view-submitted-bn-request.component';
import { termsConditionValidator, fileSize } from 'app/core/validator/form-validators.service';
import { Observable } from 'rxjs';
import { JwtTokenService, CommonService } from 'app/core/appServices/index.service';
import { BusinessLicenseService } from 'app/core/appServices/business-license/business-license.service';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { IAService } from 'app/core/appServices/ia.service';
import { TranslationService } from 'app/core/_base/layout';
import { ToastrService } from 'ngx-toastr';
import { ApplicationTypesEnum } from 'app/core/_enum/applicationTypes.enum';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';



@Component({
  selector: 'kt-add-initial-approval',
  templateUrl: './add-initial-approval.component.html',
  styleUrls: ['./add-initial-approval.component.scss']
})
export class AddInitialApprovalComponent implements OnInit {

  // enums declaration
  applicationTypeEnum = ApplicationTypesEnum;

  closeResult: string; //Bootstrap Modal Popup
  panelOpenState = false;
  radioButtonCondition: number = 0;
  emCopy: string = "ID front side";
  embCopy: string = "ID back side";
  passportCopy: string = "Passport info pages";
  visaCopy: string = "Visa page";
  //Stepper active class
  stepperValue: number = 1;

  //Form Group Stepper
  firstServiceInfoForm: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  seventhFormGroup: FormGroup;
  /* Checkbox Value Set to true */
  checkedBLS = true;

  loading = false;

  JsonModel: any = {
    "iaRequest": {
      "id": 0,
      "userId": null,
      "onBehalfUid": null,
      "iarefNo": null,
      "bnid": null,
      "bnrefNo": null,
      "blno": null,
      "applicationTypeId": 141,
      "reqStatusId": 172,
      "reasonId": null,
      "mainAreaId": null,
      "subAreaId": null,
      "street": null,
      "address": null,
      "parcelId": null,
      "submissionReasonId": null,
      "branchNameEn": null,
      "branchNameAr": null,
      "latitude": null,
      "longitude": null,
      "makaniNo": null,
      "building": null,
      "units": null,
      "stepNo": 1,
      "rating": null,
      "remarks": null,
      "isActive": true
    },
    "iaService": [],
    "iaAttachments": []
  };

  IAInfoModel: any = {
    refNo: null,
    id: null,
    bnID: null,
    bnRef: null,
    bnNameEn: null,
    bnNameAr: null
  };
  serviceInfoAndUserInfo: Observable<any>;

  getAttachmentsType: any;
  isTick = [];
  isLoading = [];
  isDisabled: boolean = false;

  @ViewChild('IAStepper', { static: true }) IAStepper: MatStepper;

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private jwtTokenService: JwtTokenService,
    public businessLicenseService: BusinessLicenseService,
    public validationMessages: ValidationMessagesService,
    private iaService: IAService,
    private cdr: ChangeDetectorRef,
    private translationService: TranslationService,
    private toast: ToastrService,
    private commonService: CommonService,
    private _DomSanitizationService: DomSanitizer,
    public router: Router,
    public dialog: MatDialog) { }

  ngOnInit() {

    this.fillUserIDOnModel();
    this.getAttachments(this.applicationTypeEnum.IAN)

    this.firstServiceInfoForm = this.fb.group({
      termsCondition: [false, Validators.compose([Validators.required, termsConditionValidator])]
    });
    this.secondFormGroup = this.fb.group({
    });
    this.thirdFormGroup = this.fb.group({
    });
    this.fourthFormGroup = this.fb.group({
    });
    this.seventhFormGroup = this.fb.group({
    });


  }

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);


  getEmCopy(event) {
    this.emCopy = event.target.files[0].name.substring(0, 15);
  }
  getEmbId(event) {
    this.embCopy = event.target.files[0].name.substring(0, 15);
  }
  getPassportCopy(event) {
    this.passportCopy = event.target.files[0].name.substring(0, 15);
  }
  getVisa(event) {
    this.visaCopy = event.target.files[0].name.substring(0, 15);
  }

  //Stepper Value
  setStepperValue(value: number) {
    this.stepperValue = value;
  }

  fillUserIDOnModel() {
    let userSession = JSON.parse(localStorage.getItem("userSession"));
    let sessionID = 0;

    let currentSessionToken: string;
    let behalfUserID: number;
    let UserID: number;
    if (userSession && userSession.length > 1) {
      currentSessionToken = userSession[0].token;
      behalfUserID = +this.jwtTokenService.getJwtTokenValueByToken(userSession[1].token).UserID;
      UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
      this.JsonModel["iaRequest"].onBehalfUid = behalfUserID;
    }
    else {
      currentSessionToken = userSession[0].token;
      UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
    }

    this.jwtTokenService.getSessionSubject().subscribe(value => {
      if (value) {
        if (value && value.length > 1) {
          currentSessionToken = value[0].token;
          behalfUserID = +this.jwtTokenService.getJwtTokenValueByToken(value[1].token).UserID;
          UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
          this.JsonModel["iaRequest"].onBehalfUid = behalfUserID;
        }
        else {
          currentSessionToken = value[0].token;
          UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
        }
      }
    });

    this.getServiceInfo(behalfUserID ? behalfUserID : 0, UserID);

    this.JsonModel["iaRequest"].userId = UserID;
  }

  AttachmentsForm() {
    this.seventhFormGroup = this.fb.group({
      attachments: this.fb.array([]),
    });

    let control = <FormArray>this.seventhFormGroup.controls.attachments;
    this.getAttachmentsType.forEach(x => {
      control.push(this.fb.group({
        id: [0],
        userId: [this.JsonModel.iaRequest.userId],
        // bnid: [this.JsonModel.bnRequest.id],
        // bnrefNo: [this.JsonModel.bnRequest.bnrefNo],
        attachmentName: ['', x.isMandatory ? Validators.required : ''],
        attachmentURL: [''],
        attachmentSize: ['', x.isMandatory ? Validators.compose([Validators.required, fileSize]) : Validators.compose([fileSize])],
        attachmentExtension: [''],
        typeCode: [x.valueEn],
        expiryDate: [new Date()],
        isMandatory: [x.isMandatory],
        inputData: ['', x.inputData ? Validators.required : ''],
        attachmentType: [x.id],
      }));
    });
    this.isTick = [];
    this.getAttachmentsType.forEach(att => {
      this.isLoading.push({ loading: false });
      this.isTick.push({ tick: false });
    });
  }

  getAttachments(code) {
    this.commonService.GetAttachmentByUserTypeCode(code).subscribe(res => {
      let response = res;
      if ((response.result && response.result.code == 200) || (response.code == 200)) {
        this.getAttachmentsType = response.result.data;
        this.AttachmentsForm();
      }
    });
  }

  getFile(event, index) {
    const controlArray = <FormArray>this.seventhFormGroup.get('attachments');
    if (event.target.files[0].size > 20913252) {
      controlArray.controls[index].get('attachmentSize').markAsTouched();
      controlArray.controls[index].get('attachmentSize').markAsDirty();
      controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
    }
    else {
      if (event.target.files[0]) {
        this.isLoading[index].loading = true;
        this.isTick[index].tick = true;
        this.isDisabled = true;
        this.commonService.fileUpload(event.target.files[0]).then(resp => {
          var response = resp;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            controlArray.controls[index].get('attachmentExtension').setValue(event.target.files[0].type);
            controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
            controlArray.controls[index].get('attachmentName').setValue(event.target.files[0].name);

            this.isLoading[index].loading = false;
            this.isDisabled = false;

            this.cdr.markForCheck();
            controlArray.controls[index].get('attachmentURL').setValue(response.attachmentURL);
            controlArray.controls[index].get('attachmentSize').markAsTouched();
            controlArray.controls[index].get('attachmentName').markAsTouched();
          }
          else {
            this.isLoading[index].loading = false;
            this.isDisabled = false;
            this.cdr.markForCheck();
            this.translationService.getTranslation(String(response.result.code)).subscribe((text: string) => {
              this.toast.error(text);
            });
          }
        });
      }
    }
  }

  getServiceInfo(loginOnBehalfID: number, UserID: number) {
    this.serviceInfoAndUserInfo = this.businessLicenseService.getServiceInfobyTypeID(loginOnBehalfID, UserID, this.applicationTypeEnum.IAN);
  }


  //..Modal Popup Start
  open(content) {
    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

  /* begin:: Open Business Request */
  openSubmittedBNRequest() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);

  }
  /* end:: Open Business Request */


  bnListValuesClick(event) {
    this.JsonModel["iaRequest"].bnid = event.bnid;
    this.JsonModel["iaRequest"].bnrefNo = event.bnrefNo;
  }

  // steps form submit
  firstServiceInfoFormSubmit() {
    if (this.firstServiceInfoForm.valid) {
      this.loading = true;
      this.serviceInfoAndUserInfo.subscribe(
        data => {
          data["serviceInfo"].forEach(item => {
            this.JsonModel.iaService.push({
              "id": 0,
              "iaid": 0,
              "bnrefNo": null,
              "serviceId": item.id,
              "applicationTypeId": 226,
              "businessNameRequest": null
            })
          })
          this.JsonModel["iaRequest"].stepNo = 1;
          this.AddIAAPI(this.JsonModel, 1);
        })
    }
    else {
      this.validationMessages.validateAllFormFields(this.firstServiceInfoForm);
    }
  }
  secondSubmitterInfoFormSubmit() {
    this.JsonModel["iaRequest"].stepNo = 2;
    this.AddIAAPI(this.JsonModel, 2);
  }
  thirdBNListSubmit() {

    if (this.JsonModel["iaRequest"].bnid) {
      this.JsonModel["iaRequest"].stepNo = 3;
      this.AddIAAPI(this.JsonModel, 3);
    }
    else {
      this.translationService.getTranslation('VALIDATIONMESSAGES.BUSINESSNAMESLISTREQUIRED').subscribe((text: string) => {
        this.toast.error(text);
      });
    }
  }
  fourthIAInfoSubmit() {
    if (this.fourthFormGroup.valid) {
      debugger
      this.JsonModel["iaRequest"].mainAreaId = this.fourthFormGroup.value.mainArea.id;
      this.JsonModel["iaRequest"].subAreaId = this.fourthFormGroup.value.subArea.id;
      this.JsonModel["iaRequest"].street = this.fourthFormGroup.value.street;
      this.JsonModel["iaRequest"].address = (this.translationService.getSelectedLanguage() == 'en' ? (this.fourthFormGroup.value.mainArea.valueEn + ',' + this.fourthFormGroup.value.subArea.valueEn + ',' + this.fourthFormGroup.value.street) : (this.fourthFormGroup.value.mainArea.valueAr + ',' + this.fourthFormGroup.value.subArea.valueAr + ',' + this.fourthFormGroup.value.street));
      this.JsonModel["iaRequest"].parcelId = this.fourthFormGroup.value.parcelID;
      this.JsonModel["iaRequest"].submissionReasonId = this.fourthFormGroup.value.reason.id;
      this.JsonModel["iaRequest"].branchNameEn = this.fourthFormGroup.value.branchNameEn;
      this.JsonModel["iaRequest"].branchNameAr = this.fourthFormGroup.value.branchNameAr;
      this.JsonModel["iaRequest"].latitude = String(this.fourthFormGroup.value.latitude);
      this.JsonModel["iaRequest"].longitude = String(this.fourthFormGroup.value.longitude);
      this.JsonModel["iaRequest"].makaniNo = this.fourthFormGroup.value.makhniNo;
      this.JsonModel["iaRequest"].building = this.fourthFormGroup.value.building;
      this.JsonModel["iaRequest"].units = this.fourthFormGroup.value.unit;

      this.JsonModel["iaRequest"].stepNo = 4;
      this.AddIAAPI(this.JsonModel, 4);
    }
    else {
      this.validationMessages.validateAllFormFields(this.fourthFormGroup);
    }
  }
  seventhAttachmentSubmit() {
    const controlArray = <FormArray>this.seventhFormGroup.get('attachments');
    for (let index = 0; index < controlArray.length; index++) {
      if (!this.getAttachmentsType[index].isMandatory) {
        controlArray.controls[index].get('attachmentSize').reset();
      }
    }
    if (this.seventhFormGroup.valid) {
      let attachmentValues = this.seventhFormGroup.value.attachments;
      attachmentValues.forEach(data => {
        data.iaid = this.JsonModel.iaRequest.id,
          data.bnrefNo = this.JsonModel.iaRequest.bnrefNo
      });
      this.JsonModel.iaAttachments = attachmentValues;
      this.JsonModel["iaRequest"].stepNo = 5;

      this.AddIAAPI(this.JsonModel, 5);
    }
    else {
      this.validationMessages.validateAllFormFields(this.seventhFormGroup);
    }
  }


  AddIAAPI(model, stepno: number) {
    this.loading = true;
    this.iaService.addIARequest(model)
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.loading = false;
          this.cdr.markForCheck();
          if (stepno == 1) {
            this.JsonModel["iaRequest"].iarefNo = data.refNo;
            this.JsonModel["iaRequest"].id = data.id;
            this.IAStepper.selectedIndex = stepno; // redirecting on next step
          }
          else if (stepno == 3) {
            this.IAInfoModel.refNo = data.refNo;
            this.IAInfoModel.id = data.id;
            this.IAInfoModel.bnID = data.bnID;
            this.IAInfoModel.bnRef = data.bnRef;
            this.IAInfoModel.bnNameEn = data.bnNameEn;
            this.IAInfoModel.bnNameAr = data.bnNameAr;
            this.IAStepper.selectedIndex = stepno;
            //this.openHappinessMeterModel("happinessMeterModal");
          }
          else if (stepno == 5) {
            this.router.navigateByUrl('/pages/business-license/request-initial-approval/initial-approvals');
          }
          else {
            this.IAStepper.selectedIndex = stepno; // redirecting on next step
          }

        }
        else {
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toast.error(text);
          });
          this.loading = false;
          this.cdr.markForCheck();
        }
      })
  }

}