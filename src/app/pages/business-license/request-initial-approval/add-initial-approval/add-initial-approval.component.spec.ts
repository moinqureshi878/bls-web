import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInitialApprovalComponent } from './add-initial-approval.component';

describe('AddInitialApprovalComponent', () => {
  let component: AddInitialApprovalComponent;
  let fixture: ComponentFixture<AddInitialApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInitialApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInitialApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
