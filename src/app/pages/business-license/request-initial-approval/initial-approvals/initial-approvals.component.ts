
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';
import { ExtendInitialApprovalsComponent } from '../model/extend-initial-approvals/extend-initial-approvals.component';
import { InitialApprovalsModificationListComponent } from '../model/initial-approvals-modification-list/initial-approvals-modification-list.component';
import { CancelInitialApprovalsComponent } from '../model/cancel-initial-approvals/cancel-initial-approvals.component';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { WorkflowStatusEnum } from 'app/core/_enum/workflowStatus.enum';
import { TranslationService } from 'app/core/_base/layout';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';

@Component({
  selector: 'kt-initial-approvals',
  templateUrl: './initial-approvals.component.html',
  styleUrls: ['./initial-approvals.component.scss']
})
export class InitialApprovalsComponent implements OnInit {
  workflowStatusEnum = WorkflowStatusEnum;

  requestInitialApproval: any;
  cancelInitialApproval: any;

  isShown: boolean = false; // hidden by default
  isHidden: boolean = true;

  displayedColumns7: string[] = ['iarefNo', 'businessNameAr', 'businessNameEn', 'blno', 'status', 'daysLeft', 'editView'];
  dataSource7 = new MatTableDataSource<any>(this.requestInitialApproval);
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;

  closeResult: string; //Bootstrap Modal Popup

  displayedColumnsOU: string[] = ['iarefNo', 'businessNameAr', 'businessNameEn', 'bnType', 'submissionDate', 'status', 'statusDate', 'editView'];
  dataSourceOU = new MatTableDataSource<any>(this.cancelInitialApproval);
  @ViewChild('matPaginatorOU', { static: true }) paginatorOU: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortOU: MatSort;
  constructor(
    private modalService: NgbModal,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private translationService: TranslationService,
    public validationMessages: ValidationMessagesService,
  ) {

    debugger
    this.requestInitialApproval = this.route.snapshot.data.data.requestInitialApproval;
    this.cancelInitialApproval = this.route.snapshot.data.data.expiredInitialApproval;

    this.dataSource7 = new MatTableDataSource<any>(this.requestInitialApproval);
    this.dataSourceOU = new MatTableDataSource<any>(this.cancelInitialApproval);

  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;

    this.dataSourceOU.paginator = this.paginatorOU;
    this.dataSourceOU.sort = this.sortOU;
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();

    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  applyFilterOU(filterValue: string) {
    this.dataSourceOU.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceOU.paginator) {
      this.dataSourceOU.paginator.firstPage();
    }
  }




  //..Modal Popup Start
  open(content) {
    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 


  /* begin:: Display ExtendGrid */
  openExtendInitialApprovals() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "60%";
    dialogConfig.maxWidth = "60%";
    this.dialog.open(ExtendInitialApprovalsComponent, dialogConfig);

  }
  /* end:: Display ExtendGrid */

  /* begin:: Display Cancel */
  openCancelInitialApprovals() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "60%";
    dialogConfig.maxWidth = "60%";
    this.dialog.open(CancelInitialApprovalsComponent, dialogConfig);

  }
  /* end:: Display Cancel */

  /* begin:: Display ExtendGrid */
  openModifyInitialApprovals() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "60%";
    dialogConfig.maxWidth = "60%";
    this.dialog.open(InitialApprovalsModificationListComponent, dialogConfig);

  }
  /* end:: Display ExtendGrid */

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

  toggleShow() {
    this.isHidden = !this.isHidden;
    this.isShown = !this.isShown;

  }

}