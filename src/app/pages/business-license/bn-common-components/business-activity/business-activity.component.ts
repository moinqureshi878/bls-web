import { Component, ViewChild, OnInit, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { BnCommonService, CommonService } from 'app/core/appServices/index.service';
import { TranslationService } from 'app/core/_base/layout';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';


export interface UserData {
  isic4: string;
  arDescription: string;
  enDescription: string;
  activityGroup: string;
  editView: string;
}
/** Builds and returns a new User. */
const ELEMENT_DATA: UserData[] = [
  { isic4: '1', arDescription: '1', enDescription: '1', activityGroup: '', editView: 'Remove' },
  { isic4: '2', arDescription: '2', enDescription: '2', activityGroup: '', editView: 'Remove' },
  { isic4: '3', arDescription: '3', enDescription: '3', activityGroup: '', editView: 'Remove' },
  { isic4: '5', arDescription: '5', enDescription: '5', activityGroup: '', editView: 'Remove' },
  { isic4: '6', arDescription: '6', enDescription: '6', activityGroup: '', editView: 'Remove' },
];
/* Add Business Activity */
/* Step Three Date */
export interface stepThreeData {
  select: string;
  isic4: string;
  arDescription: string;
  enDescription: string;
  activityGroup: string;
}
/** Builds and returns a new User. */
const STEP_THREE_DATA: stepThreeData[] = [

  { select: '', isic4: '1', arDescription: '1', activityGroup: '', enDescription: '1' },
  { select: '', isic4: '2', arDescription: '2', activityGroup: '', enDescription: '2' },
  { select: '', isic4: '3', arDescription: '3', activityGroup: '', enDescription: '3' },
  { select: '', isic4: '4', arDescription: '4', activityGroup: '', enDescription: '4' },
  { select: '', isic4: '5', arDescription: '5', activityGroup: '', enDescription: '5' },
];
@Component({
  selector: 'kt-business-activity',
  templateUrl: './business-activity.component.html',
  styleUrls: ['./business-activity.component.scss']
})
export class BusinessActivityComponent implements OnInit {
  closeResult: string; //Bootstrap Modal Popup
  selectedBusinessActivities: any[] = [];

  displayedColumns7: string[] = ['isic4', 'descriptionAr', 'descriptionEn', 'activityGroup', 'editView'];
  dataSource7 = new MatTableDataSource<any>(this.selectedBusinessActivities);
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;

  /* Step Three */
  displayedColumnsstepThree: string[] = ['select', 'isic4', 'descriptionAr', 'descriptionEn', 'activityGroup'];
  stepThree = new MatTableDataSource();
  @ViewChild('matPaginatorstepThree', { static: true }) paginatorstepThree: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortStepThree: MatSort;

  businessActivity: Observable<any>;
  businessActivities: any;
  selectedGroupId: number;
  loading: boolean = false;

  @Output() BusinessActivityValues = new EventEmitter<any>();

  constructor(
    private modalService: NgbModal,
    public bnCommonService: BnCommonService,
    private commonService: CommonService,
    public translationService: TranslationService,
    private cdr: ChangeDetectorRef,
    private toast: ToastrService
  ) {

  }
  ngOnInit() {
    /* Data Table */
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
    /* Step Three */
    this.stepThree.paginator = this.paginatorstepThree;
    this.stepThree.sort = this.sortStepThree;
    // this.BusinessActivity();
  }
  BusinessActivity(content) {
    this.loading = true;
    this.commonService.getLookUp("?type=BAgroups")
      .subscribe(data => {
        this.loading = false;
        this.businessActivity = data;
        this.filterByGroup(this.businessActivity[0].id);
        this.selectedGroupId = this.businessActivity[0].id;
        this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
      })
  }
  private filterByGroup(selectedGroupId) {
    this.bnCommonService.GetBusinessActivityPagesByBAGID(selectedGroupId).subscribe(resp => {
      let response = resp;
      if ((response.result && response.result.code == 200) || response.code == 200) {
        response.data.map(respn => respn['isActive'] = false);
        this.businessActivities = response.data;
        debugger
        if (this.selectedBusinessActivities.length > 0) {
          this.businessActivities.forEach(selectedArray => this.selectedBusinessActivities.map(map => map['id']).includes(selectedArray.id) ? selectedArray['isActive'] = true : selectedArray['isActive'] = false);
        }
        this.stepThree = new MatTableDataSource(this.businessActivities);
        this.stepThree.paginator = this.paginatorstepThree;
        this.stepThree.sort = this.sortStepThree;
      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toast.error(text);
        });
      }
    });
  }
  isActiveBusinessActivity(id, checked) {
    if (checked) {
      debugger
      this.selectedBusinessActivities.push(this.businessActivities.find(res => res.id == id));
      this.selectedBusinessActivities.find(res => res.id == id).isActive = true;
    }
    else {
      this.selectedBusinessActivities.splice(this.selectedBusinessActivities.findIndex(x => x.id == id), 1);
    }
    this.dataSource7 = new MatTableDataSource(this.selectedBusinessActivities);
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }
  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }
  applyFilterstepThree(filterValue: string) {
    this.stepThree.filter = filterValue.trim().toLowerCase();
    if (this.stepThree.paginator) {
      this.stepThree.paginator.firstPage();
    }
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  btnNextClickBA() {
    debugger
    let JsonModel = [];
    if (this.selectedBusinessActivities.length > 0) {
      this.selectedBusinessActivities.forEach(item => {
        debugger
        JsonModel.push({
          "id": 0,
          "bnid": 0,
          "bnrefNo": null,
          "activityId": item.id,
          "createdAt": null,
          "createdBy": null,
          "isActive": true,
          "isDeleted": false,
          "isModified": false,
          "bn": null
        })
      })
      this.BusinessActivityValues.emit(JsonModel);
    }
    else{
      this.translationService.getTranslation('VALIDATIONMESSAGES.BUSINESSNAMESTAKEHOLDER').subscribe((text: string) => {
        this.toast.error(text);
      });
    }
    
  }

}
