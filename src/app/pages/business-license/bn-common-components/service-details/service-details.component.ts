import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslationService } from 'app/core/_base/layout';
@Component({
  selector: 'kt-service-details',
  templateUrl: './service-details.component.html',
  styleUrls: ['./service-details.component.scss']
})
export class ServiceDetailsComponent implements OnInit {
  @Input() termsConditionForm: FormGroup;
  @Input('serviceInfo') serviceInfo: any;
  constructor(private translationService: TranslationService) {

  }

  ngOnInit() {

  }
  // ngOnChanges(changes) {
  //   console.log('ngOnChanges', changes);
  // }

}
