import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotesHistoryModalComponent } from './notes-history-modal.component';

describe('NotesHistoryModalComponent', () => {
  let component: NotesHistoryModalComponent;
  let fixture: ComponentFixture<NotesHistoryModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotesHistoryModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesHistoryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
