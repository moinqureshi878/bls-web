
import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { TranslationService } from 'app/core/_base/layout';
export interface UserData {
  noteID: string;
  noteFrom: string;
  noteTo: string;
  notes: string;
  dateTime: string;
}
/** Builds and returns a new User. */
const ELEMENT_DATA: UserData[] = [
  { noteID: 'N-1', noteFrom: 'Najm al-Din', noteTo: 'Salim', dateTime: '12/08/2019 10:00AM', notes: 'Emirates ID Copy, Business License Copy', },
  { noteID: 'N-2', noteFrom: 'Najm al-Din', noteTo: 'Salim', dateTime: '13/08/2019 10:00AM', notes: 'Emirates ID Copy, Business License Copy', },
  { noteID: 'N-3', noteFrom: 'Najm al-Din', noteTo: 'Salim', dateTime: '14/08/2019 10:00AM', notes: 'Emirates ID Copy, Business License Copy', },
  { noteID: 'N-4', noteFrom: 'Najm al-Din', noteTo: 'Salim', dateTime: '15/08/2019 10:00AM', notes: 'Emirates ID Copy, Business License Copy', },
  { noteID: 'N-5', noteFrom: 'Najm al-Din', noteTo: 'Salim', dateTime: '16/08/2019 10:00AM', notes: 'Emirates ID Copy, Business License Copy', },
];

@Component({
  selector: 'kt-notes-history-modal',
  templateUrl: './notes-history-modal.component.html',
  styleUrls: ['./notes-history-modal.component.scss']
})
export class NotesHistoryModalComponent implements OnInit {
  panelOpenState = false;

  displayedColumns7: string[] = ['noteID', 'noteFrom', 'noteTo', 'dateTime', 'notes'];
  dataSource7 = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;

  @Output() notesEvent: EventEmitter<string> = new EventEmitter();
  @Input() stickyNotes: any;
  @Input() notes: any;

  constructor(private translationService: TranslationService) {

  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }
  ngOnChanges() {
    this.dataSource7 = new MatTableDataSource(this.notes ? this.notes : new Array);
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
    this.stickyNotes
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }
  allowMaxCharacter(event) {
    debugger
    if (event.currentTarget.textLength > 2500) {
      return false;
    }
  }
}
