import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyIaInfoComponent } from './modify-ia-info.component';

describe('ModifyIaInfoComponent', () => {
  let component: ModifyIaInfoComponent;
  let fixture: ComponentFixture<ModifyIaInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyIaInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyIaInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
