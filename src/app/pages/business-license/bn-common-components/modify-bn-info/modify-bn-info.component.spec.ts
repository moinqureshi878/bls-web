import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyBnInfoComponent } from './modify-bn-info.component';

describe('ModifyBnInfoComponent', () => {
  let component: ModifyBnInfoComponent;
  let fixture: ComponentFixture<ModifyBnInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyBnInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyBnInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
