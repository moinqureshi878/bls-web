import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BnListComponent } from './bn-list.component';

describe('BnListComponent', () => {
  let component: BnListComponent;
  let fixture: ComponentFixture<BnListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BnListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BnListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
