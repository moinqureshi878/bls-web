import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { IAService } from 'app/core/appServices/ia.service';
import { WorkflowStatusEnum } from 'app/core/_enum/workflowStatus.enum';
import { TranslationService } from 'app/core/_base/layout';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'kt-bn-list',
  templateUrl: './bn-list.component.html',
  styleUrls: ['./bn-list.component.scss']
})

export class BnListComponent implements OnInit {

  ELEMENT_BUSINESS_DATA: any;
  displayedColumns8: string[] = ['bnrefNo', 'businessNameAr', 'businessNameEn', 'ianumber', 'blno', 'status', 'daysLeft', 'editView'];
  dataSource8 = new MatTableDataSource<any>(this.ELEMENT_BUSINESS_DATA);
  @ViewChild('matPaginator8', { static: true }) paginator8: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort8: MatSort;

  @Output() bnListValues = new EventEmitter<any>();


  workflowStatusEnum = WorkflowStatusEnum;

  constructor(
    private iaService: IAService,
    private translationService: TranslationService,
  ) { }

  ngOnInit() {

    this.getApprovedBNList();
    /* Data Table */
    this.dataSource8.paginator = this.paginator8;
    this.dataSource8.sort = this.sort8;
  }

  getApprovedBNList() {
    this.iaService.GetApproveBusinessName().subscribe(data => {
      debugger
      this.ELEMENT_BUSINESS_DATA = data["data"];
      this.dataSource8 = new MatTableDataSource<any>(this.ELEMENT_BUSINESS_DATA);
      this.dataSource8.paginator = this.paginator8;
      this.dataSource8.sort = this.sort8;
    })
  }
  applyFilter8(eve) { }
  selectedBN(row: any) {
    let model = {
      "bnid": row.id,
      "bnrefNo": row.bnrefNo,
    }
    this.bnListValues.emit(model);
  }

}
