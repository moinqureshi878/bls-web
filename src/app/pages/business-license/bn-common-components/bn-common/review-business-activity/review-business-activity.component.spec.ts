import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewBusinessActivityComponent } from './review-business-activity.component';

describe('ReviewBusinessActivityComponent', () => {
  let component: ReviewBusinessActivityComponent;
  let fixture: ComponentFixture<ReviewBusinessActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewBusinessActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewBusinessActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
