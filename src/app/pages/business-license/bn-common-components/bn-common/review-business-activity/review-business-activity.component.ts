import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { BusinessLicenseService } from 'app/core/appServices/business-license/business-license.service';
import { TranslationService } from 'app/core/_base/layout';
@Component({
  selector: 'kt-review-business-activity',
  templateUrl: './review-business-activity.component.html',
  styleUrls: ['./review-business-activity.component.scss']
})
export class ReviewBusinessActivityComponent implements OnInit {

  displayedColumns7: string[] = ['isic4', 'arDescription', 'enDescription', 'activityGroup', 'editView'];
  dataSource7 = new MatTableDataSource();
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;

  @Input('getBusinessActivity') getBusinessActivity: any;
  @Output() updateBusinessActivityData: EventEmitter<string> = new EventEmitter();
  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    public businessLicenseService: BusinessLicenseService,
    private translationService: TranslationService,
    private router: Router) {
  }
  ngOnChanges() {
    if (this.getBusinessActivity && this.getBusinessActivity.isHide) {
      this.displayedColumns7 = ['isic4', 'arDescription', 'enDescription', 'activityGroup'];
    }
    this.dataSource7 = new MatTableDataSource(this.getBusinessActivity);
  }
  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }
  updatebusinessActivityValue(id: number) {
    let index = this.getBusinessActivity.findIndex(x => x.id == id);
    let isChecked: boolean = this.getBusinessActivity[index].isChecked == true ? false : true;
    this.getBusinessActivity[index].isChecked = isChecked;
    this.updateBusinessActivityData.emit(this.getBusinessActivity);
  }
  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }
}
