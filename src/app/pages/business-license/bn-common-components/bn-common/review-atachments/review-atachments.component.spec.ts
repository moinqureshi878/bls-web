import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewAtachmentsComponent } from './review-atachments.component';

describe('ReviewAtachmentsComponent', () => {
  let component: ReviewAtachmentsComponent;
  let fixture: ComponentFixture<ReviewAtachmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewAtachmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewAtachmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
