import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslationService } from 'app/core/_base/layout';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { ReviewAttachmentModalComponent } from '../../model/review-attachment-modal/review-attachment-modal.component';

@Component({
  selector: 'kt-review-atachments',
  templateUrl: './review-atachments.component.html',
  styleUrls: ['./review-atachments.component.scss']
})
export class ReviewAtachmentsComponent implements OnInit {

  @Input('getAttachmentsData') getAttachmentsData: any;
  @Output() updateAttachmentsData: EventEmitter<string> = new EventEmitter();
  constructor(private translationService: TranslationService,
    public dialog: MatDialog) { }

  ngOnInit() {
  }
  updateAttachmentInfoValue(id: number) {
    let index = this.getAttachmentsData.findIndex(x => x.id == id);
    let isChecked: boolean = this.getAttachmentsData[index].isChecked == true ? false : true;
    this.getAttachmentsData[index].isChecked = isChecked;
    this.updateAttachmentsData.emit(this.getAttachmentsData);
  }

  openDialog(...dialogName) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";

    if (dialogName[0] == 'attachment') {
      dialogConfig.data = dialogName[1];
      this.dialog.open(ReviewAttachmentModalComponent, dialogConfig);
    }
  }
}
