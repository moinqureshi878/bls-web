import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewStakeholderComponent } from './review-stakeholder.component';

describe('ReviewStakeholderComponent', () => {
  let component: ReviewStakeholderComponent;
  let fixture: ComponentFixture<ReviewStakeholderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewStakeholderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewStakeholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
