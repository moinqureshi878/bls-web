import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, TooltipPosition } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { BusinessLicenseService } from 'app/core/appServices/business-license/business-license.service';
import { TranslationService } from 'app/core/_base/layout';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'kt-review-stakeholder',
  templateUrl: './review-stakeholder.component.html',
  styleUrls: ['./review-stakeholder.component.scss']
})
export class ReviewStakeholderComponent implements OnInit {

  stakeHoldersdisplayedColumns7: string[] = ['orderNo', 'type', 'relationship', 'nameAr', 'nameEn', 'share', 'registered', 'editView'];
  stakeHoldersdataSource7 = new MatTableDataSource();
  @ViewChild('stakeHoldersmatPaginator7', { static: true }) stakeHolderspaginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) stakeHolderssort7: MatSort;

  @Input('getStakeholder') getStakeholder: any;
  @Output() updateStakeholdersData: EventEmitter<string> = new EventEmitter();
  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    public businessLicenseService: BusinessLicenseService,
    private translationService: TranslationService,
    private router: Router) {
  }
  ngOnChanges() {
    if (this.getStakeholder && this.getStakeholder.isHide) {
      this.stakeHoldersdisplayedColumns7 = ['orderNo', 'type', 'relationship', 'nameAr', 'nameEn', 'share', 'registered'];
    }
    this.stakeHoldersdataSource7 = new MatTableDataSource(this.getStakeholder);
  }
  ngOnInit() {
    this.stakeHoldersdataSource7.paginator = this.stakeHolderspaginator7;
    this.stakeHoldersdataSource7.sort = this.stakeHolderssort7;
  }
  updateStakeholderInfoValue(id: number) {
    let index = this.getStakeholder.findIndex(x => x.id == id);
    let isChecked: boolean = this.getStakeholder[index].isChecked == true ? false : true;
    this.getStakeholder[index].isChecked = isChecked;
    this.updateStakeholdersData.emit(this.getStakeholder);
  }
  stakeHoldersapplyFilter7(filterValue: string) {
    this.stakeHoldersdataSource7.filter = filterValue.trim().toLowerCase();
    if (this.stakeHoldersdataSource7.paginator) {
      this.stakeHoldersdataSource7.paginator.firstPage();
    }
  }
  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);
}
