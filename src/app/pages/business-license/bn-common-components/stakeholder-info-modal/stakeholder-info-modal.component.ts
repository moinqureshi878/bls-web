import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog } from '@angular/material';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService, UserService, BnCommonService } from 'app/core/appServices/index.service';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { StakeholderTypesEnum } from 'app/core/_enum/stakeholderTypes.enum';
import { TranslationService } from 'app/core/_base/layout';
import { BusinessNamesTypesEnum } from 'app/core/_enum/businessNamesTypes.enum';
import { ToastrService } from 'ngx-toastr';
import { nameArValidator } from 'app/core/validator/form-validators.service';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';

@Component({
  selector: 'kt-stakeholder-info-modal',
  templateUrl: './stakeholder-info-modal.component.html',
  styleUrls: ['./stakeholder-info-modal.component.scss']
})
export class StakeholderInfoModalComponent implements OnInit {

  @Output() StakeHolderInfoModelValues = new EventEmitter<any>();

  stakeholderEnum = StakeholderTypesEnum;
  businessNamesTypesEnum = BusinessNamesTypesEnum;

  closeResult: string; //Bootstrap Modal Popup
  radioButtonCondition: number;
  branchCondition: number = 0;

  multipleLookups: Observable<any>;
  stakeholderTypes: Observable<any>;
  stakeholderRelationship: Observable<any>;
  govtEntity: Observable<any>;
  individuals: Observable<any>;
  inheritenceRepresentative: Observable<any>;
  businessNameTypes: Observable<any>;
  issueanceAuthority: Observable<any>;

  StakeHolderForm: FormGroup;

  constructor(private modalService: NgbModal,
    private dialogRef: MatDialog,
    private commonService: CommonService,
    private fb: FormBuilder,
    private translationService: TranslationService,
    public userService: UserService,
    public bnCommonService: BnCommonService,
    private toast: ToastrService,
    public validationMessages: ValidationMessagesService,
    public router: Router, ) { }

  ngOnInit() {
    this.fillDropdowns();
    this.initializeForm();

    this.radioButtonCondition = this.businessNamesTypesEnum.New;
  }

  fillDropdowns() {
    this.multipleLookups = this.commonService.getMultipleLookups("?types=StakeholderTypes,StakeholderOwnershipTypes,GovernmentAuthorities,BNCategorries,IssuanceEntities");
    this.stakeholderTypes = this.multipleLookups.pipe(map(resp => resp["StakeholderTypes"]));
    this.stakeholderRelationship = this.multipleLookups.pipe(map(resp => resp["StakeholderOwnershipTypes"]));
    this.govtEntity = this.multipleLookups.pipe(map(resp => resp["GovernmentAuthorities"]));
    this.multipleLookups.subscribe(
      data => {
        this.businessNameTypes = data["BNCategorries"];
      }
    )
    this.issueanceAuthority = this.multipleLookups.pipe(map(resp => resp["IssuanceEntities"]));
  }

  initializeForm() {
    // this.StakeHolderForm = this.fb.group({
    //   "type": [null, Validators.compose([Validators.required])],
    //   "ddlStakeHolderRelationshipType": [null, Validators.compose([Validators.required, Validators.maxLength(50)])],
    //   "shares": [null, Validators.compose([Validators.required])],
    //   "govtEntity": [null, Validators.compose([Validators.required])],
    //   "licenceNumber": new FormControl(null),
    // });
    this.StakeHolderForm = this.fb.group({
      "type": [null, Validators.compose([Validators.required])],
      "ddlStakeHolderRelationshipType": [null, Validators.compose([Validators.required, Validators.maxLength(50)])],
      "shares": [null, Validators.compose([Validators.required])],
      "govtEntity": [null],
      "individualEntities": [null],
      'InheritanceRepresentative': [null],
      "licenceNumber": [null],
      "businessNamesTypes": [null],
      "licenceIssueDate": [null],
      "licenceExpiryDate": [null],
      "ddlIssueanceAuthority": [null],
      "BNnameEn": [null],
      "BNnameAr": [null]
    });
  }

  getLicenseInfo() {

  }

  onStakeholderTypesChanged() {
    this.getIndividualAndInheritenceRepresentativeData(this.StakeHolderForm.value.type);
    if (this.StakeHolderForm.value.type == this.stakeholderEnum.License) {
      this.StakeHolderForm = this.fb.group({
        "type": [this.StakeHolderForm.value.type, Validators.compose([Validators.required])],
        "ddlStakeHolderRelationshipType": [null, Validators.compose([Validators.required])],
        "shares": [null, Validators.compose([Validators.required])],
        "govtEntity": [null],
        "individualEntities": [null],
        "InheritanceRepresentative": [null],
        "licenceNumber": [null, Validators.compose([Validators.required])],
        "businessNamesTypes": [this.businessNamesTypesEnum.New],
        "licenceIssueDate": [null, Validators.compose([Validators.required])],
        "licenceExpiryDate": [null, Validators.compose([Validators.required])],
        "ddlIssueanceAuthority": ['', Validators.compose([Validators.required, Validators.maxLength(200)])],
        "BNnameEn": ['', Validators.compose([Validators.required, Validators.maxLength(200)])],
        "BNnameAr": ['', Validators.compose([Validators.required, Validators.maxLength(200), nameArValidator])]
      });
    }
    else if (this.StakeHolderForm.value.type == this.stakeholderEnum.GovAuthority) {
      this.StakeHolderForm = this.fb.group({
        "type": [this.StakeHolderForm.value.type, Validators.compose([Validators.required])],
        "ddlStakeHolderRelationshipType": [null, Validators.compose([Validators.required])],
        "shares": [null, Validators.compose([Validators.required])],
        "govtEntity": [null, Validators.compose([Validators.required])],
        "individualEntities": [null],
        "InheritanceRepresentative": [null],
        "licenceNumber": [null],
        "businessNamesTypes": [null],
        "licenceIssueDate": [null],
        "licenceExpiryDate": [null],
        "ddlIssueanceAuthority": [null],
        "BNnameEn": [null],
        "BNnameAr": [null]
      });
    }
    else if (this.StakeHolderForm.value.type == this.stakeholderEnum.Individual) {
      this.StakeHolderForm = this.fb.group({
        "type": [this.StakeHolderForm.value.type, Validators.compose([Validators.required])],
        "ddlStakeHolderRelationshipType": [null, Validators.compose([Validators.required])],
        "shares": [null, Validators.compose([Validators.required])],
        "govtEntity": [null, Validators.compose([Validators.required])],
        "individualEntities": [null, Validators.compose([Validators.required])],
        "InheritanceRepresentative": [null],
        "licenceNumber": [null],
        "businessNamesTypes": [null],
        "licenceIssueDate": [null],
        "licenceExpiryDate": [null],
        "ddlIssueanceAuthority": [null],
        "BNnameEn": [null],
        "BNnameAr": [null]
      });
    }
    else if (this.StakeHolderForm.value.type == this.stakeholderEnum.InehritanceRepresentative) {
      this.StakeHolderForm = this.fb.group({
        "type": [this.StakeHolderForm.value.type, Validators.compose([Validators.required])],
        "ddlStakeHolderRelationshipType": [null, Validators.compose([Validators.required])],
        "shares": [null, Validators.compose([Validators.required])],
        "govtEntity": [null],
        "individualEntities": [null],
        "InheritanceRepresentative": [null],
        "licenceNumber": [null],
        "businessNamesTypes": [null],
        "licenceIssueDate": [null],
        "licenceExpiryDate": [null],
        "ddlIssueanceAuthority": [null],
        "BNnameEn": [null],
        "BNnameAr": [null]
      });
    }
  }

  setRadioButtonCondition() {

  }

  getIndividualAndInheritenceRepresentativeData(id: number) {
    if (id == this.stakeholderEnum.InehritanceRepresentative) {
      this.inheritenceRepresentative = this.userService.GetStakeHolderByType(id).pipe(map(resp => resp["data"]));
    }
    else if (id == this.stakeholderEnum.Individual) {
      this.individuals = this.userService.GetStakeHolderByType(id).pipe(map(resp => resp["data"]));
    }
  }

  setBranchCondition(value: number) {

    this.branchCondition = value;
  }

  addstakeholder() {
    if (this.StakeHolderForm.valid) {
      console.log(this.StakeHolderForm.value);
      this.StakeHolderInfoModelValues.emit(this.StakeHolderForm.value);
    }
    else {
      this.validationMessages.validateAllFormFields(this.StakeHolderForm);
    }
  }

  getHeadOfficeLicenseNoData() {
    this.bnCommonService.getLicenseDetail(this.StakeHolderForm.value.licenceNumber)
      .subscribe(data => {
        if (data["result"].code === 200) {
          console.log(data);
        }
        else {
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toast.error(text);
          });
        }
      })
  }

  /* Emirates ID Mask */
  emiratesIDMask = [/[0-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/]

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);


  //..Modal Popup Start
  open(content) {
    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 

  closeAllMatDialog() {
    this.dialogRef.closeAll();
  }
}
