import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StakeholderInfoModalComponent } from './stakeholder-info-modal.component';

describe('StakeholderInfoModalComponent', () => {
  let component: StakeholderInfoModalComponent;
  let fixture: ComponentFixture<StakeholderInfoModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StakeholderInfoModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StakeholderInfoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
