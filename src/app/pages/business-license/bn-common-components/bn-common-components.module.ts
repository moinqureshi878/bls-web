import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceDetailsComponent } from '../bn-common-components/service-details/service-details.component';
import { PartialsModule } from 'app/views/partials/partials.module';
import { SubmittedInfoComponent } from './submitted-info/submitted-info.component';
import { StakeholderInfoComponent } from './stakeholder-info/stakeholder-info.component';
import { StakeholderInfoModalComponent } from './stakeholder-info-modal/stakeholder-info-modal.component';
// import { AddStakeholdersModalComponent } from '../request-business-name/model/add-stakeholders-modal/add-stakeholders-modal.component';
import { ApplicationInfoComponent } from './application-info/application-info.component';
import { BusinessNamesAvailabilityComponent } from './business-names-availability/business-names-availability.component';
import { NotesHistoryModalComponent } from '../bn-common-components/notes-history-modal/notes-history-modal.component';
import { RequestForInspectionComponent } from './model/request-for-inspection/request-for-inspection.component';
import { ViewActionsComponent } from './model/view-actions/view-actions.component';
import { ViewSubmittedBnRequestComponent } from './model/view-submitted-bn-request/view-submitted-bn-request.component';
import { FwdForActionComponent } from './model/fwd-for-action/fwd-for-action.component';
import { ReturnBackNotesComponent } from './model/return-back-notes/return-back-notes.component';
import { RejectedBusinessModalComponent } from './model/rejected-business-modal/rejected-business-modal.component';
import { ReviewAttachmentModalComponent } from './model/review-attachment-modal/review-attachment-modal.component';
import { FilterBlComponent } from './filter-bl/filter-bl.component';
import { ViewSubmittedIaRequestComponent } from './model/view-submitted-ia-request/view-submitted-ia-request.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ReviewAtachmentsComponent } from './bn-common/review-atachments/review-atachments.component';
import { ReviewBusinessActivityComponent } from './bn-common/review-business-activity/review-business-activity.component';
import { ReviewStakeholderComponent } from './bn-common/review-stakeholder/review-stakeholder.component';
import { BnAttachmentsComponent } from './bn-attachments/bn-attachments.component';
import { MatTableExporterModule } from 'mat-table-exporter';
import { MatSortModule } from '@angular/material';
import { BnListComponent } from './bn-list/bn-list.component';
import { RouterModule } from '@angular/router';
import { IaInfoComponent } from './ia-info/ia-info.component';
import { ModifyBlStatusInfoComponent } from './modify-bl-status-info/modify-bl-status-info.component';
import { ModifyBnInfoComponent } from './modify-bn-info/modify-bn-info.component';
import { ModifyIaInfoComponent } from './modify-ia-info/modify-ia-info.component';
import { ModifyBlInfoComponent } from './modify-bl-info/modify-bl-info.component';

@NgModule({
  declarations: [
    ServiceDetailsComponent,
    SubmittedInfoComponent,
    StakeholderInfoComponent,
    StakeholderInfoModalComponent,
    ApplicationInfoComponent,
    BusinessNamesAvailabilityComponent,
    NotesHistoryModalComponent,
    RequestForInspectionComponent,
    ViewActionsComponent,
    ViewSubmittedBnRequestComponent,
    FwdForActionComponent,
    ReturnBackNotesComponent,
    RejectedBusinessModalComponent,
    ReviewAttachmentModalComponent,
    FilterBlComponent,
    ViewSubmittedIaRequestComponent,
    ReviewAtachmentsComponent,
    ReviewBusinessActivityComponent,
    ReviewStakeholderComponent,
    BnAttachmentsComponent,
    BnListComponent,
    IaInfoComponent,
    ModifyBlStatusInfoComponent,
    ModifyBnInfoComponent,
    ModifyIaInfoComponent,
    ModifyBlInfoComponent,
  ],
  entryComponents: [
    StakeholderInfoModalComponent,
    ViewSubmittedBnRequestComponent,
    ViewActionsComponent,
    FwdForActionComponent,
    ReturnBackNotesComponent,
    RejectedBusinessModalComponent,
    ReviewAttachmentModalComponent,
    FwdForActionComponent,
    ReturnBackNotesComponent,
    RejectedBusinessModalComponent,
    RequestForInspectionComponent,
    ViewSubmittedIaRequestComponent,
  ],
  exports: [
    ServiceDetailsComponent,
    SubmittedInfoComponent,
    StakeholderInfoComponent,
    ApplicationInfoComponent,
    BusinessNamesAvailabilityComponent,
    NotesHistoryModalComponent,
    ViewActionsComponent,
    ViewSubmittedBnRequestComponent,
    FwdForActionComponent,
    ReturnBackNotesComponent,
    RejectedBusinessModalComponent,
    ReviewAttachmentModalComponent,
    FilterBlComponent,
    RequestForInspectionComponent,
    ViewSubmittedIaRequestComponent,
    ReviewAtachmentsComponent,
    ReviewBusinessActivityComponent,
    ReviewStakeholderComponent,
    BnAttachmentsComponent,
    BnListComponent,
    IaInfoComponent,
    ModifyBlStatusInfoComponent,
    ModifyBnInfoComponent,
    ModifyIaInfoComponent,
    ModifyBlInfoComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    PartialsModule,
    MatSortModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableExporterModule
  ]
})
export class BnCommonComponentsModule { }
