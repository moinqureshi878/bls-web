import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { nameArValidator } from 'app/core/validator/form-validators.service';
import { CommonService } from 'app/core/appServices/index.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TranslationService } from 'app/core/_base/layout';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { ViewSubmittedBnRequestComponent } from '../model/view-submitted-bn-request/view-submitted-bn-request.component';

@Component({
  selector: 'kt-ia-info',
  templateUrl: './ia-info.component.html',
  styleUrls: ['./ia-info.component.scss']
})
export class IaInfoComponent implements OnInit {

  @Input() model: any;
  @Input() InitialInfoForm: FormGroup;

  multipleLookups: Observable<any>;

  mainArea: Observable<any>;
  subArea: Observable<any>;
  reasonsType: Observable<any>;

  constructor(
    public commonService: CommonService,
    public translationService: TranslationService,
    public validationMessages: ValidationMessagesService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.initializeForm();
    this.fillDropdownValues();
  }

  initializeForm() {

    this.InitialInfoForm.addControl("mainArea", new FormControl(null, Validators.compose([Validators.required, Validators.maxLength(50)])));
    this.InitialInfoForm.addControl("subArea", new FormControl(null, Validators.compose([Validators.required, Validators.maxLength(50)])));
    this.InitialInfoForm.addControl("street", new FormControl(null, Validators.compose([Validators.maxLength(100)])));
    this.InitialInfoForm.addControl("address", new FormControl(null));
    this.InitialInfoForm.addControl("parcelID", new FormControl(null, Validators.compose([Validators.required, Validators.maxLength(50)])));
    this.InitialInfoForm.addControl("reason", new FormControl(null, Validators.compose([Validators.required, Validators.maxLength(50)])));
    this.InitialInfoForm.addControl("branchNameAr", new FormControl(null, Validators.compose([Validators.required, nameArValidator])));
    this.InitialInfoForm.addControl("branchNameEn", new FormControl(null, Validators.compose([Validators.required])));

    this.InitialInfoForm.addControl("latitude", new FormControl(null, Validators.compose([Validators.required])));
    this.InitialInfoForm.addControl("longitude", new FormControl(null, Validators.compose([Validators.required])));

    this.InitialInfoForm.addControl("makhniNo", new FormControl(null, Validators.compose([Validators.required])));
    this.InitialInfoForm.addControl("building", new FormControl(null, Validators.compose([Validators.required, Validators.maxLength(100)])));
    this.InitialInfoForm.addControl("unit", new FormControl(null, Validators.compose([Validators.required])));


    //mainArea
    //subArea
    //reason



  }

  fillDropdownValues() {
    this.multipleLookups = this.commonService.getMultipleLookups("?types=MainAreas,SubArea,ReasonTypes");

    this.mainArea = this.multipleLookups.pipe(map(resp => resp["MainAreas"]));
    this.subArea = this.multipleLookups.pipe(map(resp => resp["SubArea"]));
    this.reasonsType = this.multipleLookups.pipe(map(resp => resp["ReasonTypes"]));

  }

  openSubmittedBNRequest() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);

  }

}
