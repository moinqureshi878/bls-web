import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IaInfoComponent } from './ia-info.component';

describe('IaInfoComponent', () => {
  let component: IaInfoComponent;
  let fixture: ComponentFixture<IaInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IaInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IaInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
