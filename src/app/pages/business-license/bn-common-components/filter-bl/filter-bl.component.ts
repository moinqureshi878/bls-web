import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { dateLessThanWhenRequired } from 'app/core/validator/form-validators.service';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';

@Component({
  selector: 'kt-filter-bl',
  templateUrl: './filter-bl.component.html',
  styleUrls: ['./filter-bl.component.scss']
})
export class FilterBlComponent implements OnInit {
  @Output() getFilters: EventEmitter<string> = new EventEmitter();
  blFilterForm: FormGroup;
  resetFormObj = {
    "requestDateFrom": "",
    "requestDateTo": "",
    "delayed": [],
    "expiringSoon": [],
    "Status": [],
    "statusDateFrom": "",
    "statusDateTo": "",
  };
  loading: boolean = false;
  resetLoading: boolean = false;
  constructor(public fb: FormBuilder,
    public cdr: ChangeDetectorRef,
    public validationMessages: ValidationMessagesService) {
    this.blFilterForm = this.fb.group({
      requestDateFrom: [''],
      requestDateTo: [''],
      delayed: [new Array],
      expiringSoon: [new Array],
      status: [new Array],
      statusDateFrom: [''],
      statusDateTo: [''],
    }, {
      validators: [
        dateLessThanWhenRequired('requestDateFrom', 'requestDateTo'),
        dateLessThanWhenRequired('statusDateFrom', 'statusDateTo')
      ]
    });
  }

  ngOnInit() {
  }

  resetForm() {
    this.blFilterForm.reset(this.resetFormObj);

    this.blFilterForm.controls["requestDateTo"].clearValidators();
    this.blFilterForm.controls["requestDateFrom"].clearValidators();
    this.blFilterForm.controls["requestDateTo"].updateValueAndValidity();
    this.blFilterForm.controls["requestDateFrom"].updateValueAndValidity();

    this.blFilterForm.controls["statusDateTo"].clearValidators();
    this.blFilterForm.controls["statusDateFrom"].clearValidators();
    this.blFilterForm.controls["statusDateTo"].updateValueAndValidity();
    this.blFilterForm.controls["statusDateFrom"].updateValueAndValidity();
  }
  resetGridAndForm() {
    this.resetForm();
    this.resetLoading = true;
    this.getFilters.emit('resetForm');
  }
  blFilterFormSubmit() {
    if (this.blFilterForm.value.requestDateFrom || this.blFilterForm.value.requestDateTo) {
      this.blFilterForm.controls["requestDateTo"].setValidators(Validators.required);
      this.blFilterForm.controls["requestDateFrom"].setValidators(Validators.required);

      this.blFilterForm.controls["requestDateTo"].updateValueAndValidity();
      this.blFilterForm.controls["requestDateFrom"].updateValueAndValidity();
    }
    if (this.blFilterForm.value.statusDateFrom || this.blFilterForm.value.statusDateTo) {
      this.blFilterForm.controls["statusDateTo"].setValidators(Validators.required);
      this.blFilterForm.controls["statusDateFrom"].setValidators(Validators.required);

      this.blFilterForm.controls["statusDateTo"].updateValueAndValidity();
      this.blFilterForm.controls["statusDateFrom"].updateValueAndValidity();
    }
    if (this.blFilterForm.value.requestDateFrom && this.blFilterForm.value.requestDateTo) {
      this.blFilterForm.value.requestDateFrom = this.validationMessages.convertUTCDateToLocalDate(this.blFilterForm.value.requestDateFrom);
      this.blFilterForm.value.requestDateTo = this.validationMessages.convertUTCDateToLocalDate(this.blFilterForm.value.requestDateTo);
    }
    if (this.blFilterForm.value.statusDateFrom || this.blFilterForm.value.statusDateTo) {
      this.blFilterForm.value.statusDateFrom = this.validationMessages.convertUTCDateToLocalDate(this.blFilterForm.value.statusDateFrom);
      this.blFilterForm.value.statusDateTo = this.validationMessages.convertUTCDateToLocalDate(this.blFilterForm.value.statusDateTo);
    }
    if (this.blFilterForm.valid) {
      for (let [key, formValue] of Object.entries(this.blFilterForm.value)) {
        let value = formValue;
        if (value != null && value != '' || (value instanceof Array && value.length > 0)) {
          this.loading = true;
          this.getFilters.emit(this.blFilterForm.value);
          return false;
        }
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.blFilterForm);
    }
  }
  closeLoader(){
    this.resetLoading=false;
    this.loading = false;
    this.cdr.markForCheck();
	}
}
