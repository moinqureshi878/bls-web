import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterBlComponent } from './filter-bl.component';

describe('FilterBlComponent', () => {
  let component: FilterBlComponent;
  let fixture: ComponentFixture<FilterBlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterBlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterBlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
