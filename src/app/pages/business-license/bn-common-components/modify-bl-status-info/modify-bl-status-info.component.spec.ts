import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyBlStatusInfoComponent } from './modify-bl-status-info.component';

describe('ModifyBlStatusInfoComponent', () => {
  let component: ModifyBlStatusInfoComponent;
  let fixture: ComponentFixture<ModifyBlStatusInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyBlStatusInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyBlStatusInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
