import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessNamesAvailabilityComponent } from './business-names-availability.component';

describe('BusinessNamesAvailabilityComponent', () => {
  let component: BusinessNamesAvailabilityComponent;
  let fixture: ComponentFixture<BusinessNamesAvailabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessNamesAvailabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessNamesAvailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
