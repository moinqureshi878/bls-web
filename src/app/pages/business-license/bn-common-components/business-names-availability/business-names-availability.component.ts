import { Component, OnInit, Input, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { nameArValidator } from 'app/core/validator/form-validators.service';
import { AsyncFormValidatorService } from 'app/core/validator/async-form-validator.service';
import { TranslationService } from 'app/core/_base/layout';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'kt-business-names-availability',
  templateUrl: './business-names-availability.component.html',
  styleUrls: ['./business-names-availability.component.scss']
})
export class BusinessNamesAvailabilityComponent implements OnInit {

  checkAvailablityarray: any[] = [];

  @Input() businessNamesAvailablityForm: FormGroup;
  @Input('BusinessNamesData') BusinessNamesData: any;
  @Output() BusinessNameAvailabilityValues = new EventEmitter<any>();
  constructor(
    public validationMessages: ValidationMessagesService,
    public asyncFormValidatorService: AsyncFormValidatorService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private translationService: TranslationService,
    private toast: ToastrService,
  ) {

  }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {

    this.businessNamesAvailablityForm.addControl("BNArabic1", new FormControl({ value: this.BusinessNamesData ? this.BusinessNamesData.businessNames[0].businessNameAr : null, disabled: this.BusinessNamesData }, Validators.compose([Validators.required, Validators.maxLength(50), nameArValidator]), this.asyncFormValidatorService.BNArabicValidator(0)));
    this.businessNamesAvailablityForm.addControl("BNArabic2", new FormControl({ value: this.BusinessNamesData ? this.BusinessNamesData.businessNames[1].businessNameAr : null, disabled: this.BusinessNamesData }, Validators.compose([Validators.required, Validators.maxLength(50), nameArValidator]), this.asyncFormValidatorService.BNArabicValidator(0)));
    this.businessNamesAvailablityForm.addControl("BNArabic3", new FormControl({ value: this.BusinessNamesData ? this.BusinessNamesData.businessNames[2].businessNameAr : null, disabled: this.BusinessNamesData }, Validators.compose([Validators.required, Validators.maxLength(50), nameArValidator]), this.asyncFormValidatorService.BNArabicValidator(0)));
    this.businessNamesAvailablityForm.addControl("BNEnglish1", new FormControl({ value: this.BusinessNamesData ? this.BusinessNamesData.businessNames[0].businessNameEn : null, disabled: this.BusinessNamesData }, Validators.compose([Validators.required, Validators.maxLength(50)]), this.asyncFormValidatorService.BNEnglishValidator(0)));
    this.businessNamesAvailablityForm.addControl("BNEnglish2", new FormControl({ value: this.BusinessNamesData ? this.BusinessNamesData.businessNames[1].businessNameEn : null, disabled: this.BusinessNamesData }, Validators.compose([Validators.required, Validators.maxLength(50)]), this.asyncFormValidatorService.BNEnglishValidator(0)));
    this.businessNamesAvailablityForm.addControl("BNEnglish3", new FormControl({ value: this.BusinessNamesData ? this.BusinessNamesData.businessNames[2].businessNameEn : null, disabled: this.BusinessNamesData }, Validators.compose([Validators.required, Validators.maxLength(50)]), this.asyncFormValidatorService.BNEnglishValidator(0)));
    this.businessNamesAvailablityForm.addControl("BNShortNameArabic", new FormControl({ value: this.BusinessNamesData ? this.BusinessNamesData.shortNameAr : null, disabled: this.BusinessNamesData }, Validators.compose([Validators.maxLength(100), nameArValidator])));
    this.businessNamesAvailablityForm.addControl("BNShortNameEnglish", new FormControl({ value: this.BusinessNamesData ? this.BusinessNamesData.shortNameEn : null, disabled: this.BusinessNamesData }, Validators.compose([Validators.maxLength(100)])));
  }

  checkAvailablity(index: number) {
    this.checkAvailablityarray[index] = true;
  }
}
