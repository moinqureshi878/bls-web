import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslationService } from 'app/core/_base/layout';
import { Observable } from 'rxjs';
import { CommonService, BnCommonService } from 'app/core/appServices/index.service';
import { BusinessNamesTypesEnum } from 'app/core/_enum/businessNamesTypes.enum';
import { map, filter } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { nameArValidator } from 'app/core/validator/form-validators.service';
import { AsyncFormValidatorService } from 'app/core/validator/async-form-validator.service';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
@Component({
  selector: 'kt-application-info',
  templateUrl: './application-info.component.html',
  styleUrls: ['./application-info.component.scss']
})
export class ApplicationInfoComponent implements OnInit {

  businessNamesTypesEnum = BusinessNamesTypesEnum;

  @Input('businessNameTypes') businessNameTypes: any;
  @Input('BNData') BNData: any;
  @Input() ApplicationInfoForm: FormGroup;
  // @Output() ApplicationInfoFormOutput = new EventEmitter<FormGroup>();

  bnTypes: Observable<any>;
  legalType: Observable<any>;
  licenseCategory: Observable<any>;
  licenseType: Observable<any>;
  countries: Observable<any>;
  issueanceAuthority: Observable<any>;

  radioButtonCondition: number;

  multipleLookups: Observable<any>;
  loading: boolean = false;

  constructor(private translationService: TranslationService,
    public bnCommonService: BnCommonService,
    private toast: ToastrService,
    private fb: FormBuilder,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    private commonService: CommonService) {

  }
  ngOnChanges() {
    this.radioButtonCondition = (this.BNData && this.BNData.isHide) ? this.BNData.bncategoryId : this.businessNamesTypesEnum.New;
  }
  initializeForm() {
    this.ApplicationInfoForm.addControl("businessNamesTypes", new FormControl({ value: this.BNData ? this.BNData.bncategoryId : this.businessNamesTypesEnum.New, disabled: this.BNData }));
    this.ApplicationInfoForm.addControl("licenceNumber", new FormControl({ value: this.BNData ? this.BNData.holicenseNo : null, disabled: this.BNData }));
    this.ApplicationInfoForm.addControl("licenceIssueDate", new FormControl({ value: this.BNData ? this.BNData.issueDate : null, disabled: this.BNData }));
    this.ApplicationInfoForm.addControl("licenceExpiryDate", new FormControl({ value: this.BNData ? this.BNData.expiredDate : null, disabled: this.BNData }));
    this.ApplicationInfoForm.addControl("BNTypes", new FormControl({ value: this.BNData ? this.BNData.bntypeId : null, disabled: this.BNData }, Validators.compose([Validators.required])));
    this.ApplicationInfoForm.addControl("legalTypeddl", new FormControl({ value: this.BNData ? this.BNData.legalTypeId : null, disabled: this.BNData }, Validators.compose([Validators.required])));
    this.ApplicationInfoForm.addControl("ddlLicenseCategory", new FormControl({ value: this.BNData ? this.BNData.licenseCategoryId : null, disabled: this.BNData }, Validators.compose([Validators.required])));
    this.ApplicationInfoForm.addControl("ddlLicenseType", new FormControl({ value: this.BNData ? this.BNData.licenseTypeId : null, disabled: this.BNData }, Validators.compose([Validators.required])));
    this.ApplicationInfoForm.addControl("reservationPeriod", new FormControl(60, Validators.compose([Validators.required])));
    this.ApplicationInfoForm.addControl("ddlIssueanceAuthority", new FormControl({ value: this.BNData ? this.BNData.licenseIssuanceAuthorityId : null, disabled: this.BNData }));
    this.ApplicationInfoForm.addControl("ddlCountry", new FormControl({ value: this.BNData ? this.BNData.countryId : null, disabled: this.BNData }));

    // this.ApplicationInfoForm = this.fb.group({
    //   "businessNamesTypes": [{ value: this.BNData ? this.BNData.bncategoryId : this.businessNamesTypesEnum.New, disabled: this.BNData }],
    //   "licenceNumber": [{ value: this.BNData ? this.BNData.holicenseNo : null, disabled: this.BNData }],
    //   "licenceIssueDate": [{ value: this.BNData ? this.BNData.issueDate : null, disabled: this.BNData }],
    //   "licenceExpiryDate": [{ value: this.BNData ? this.BNData.expiredDate : null, disabled: this.BNData }],
    //   "BNTypes": [{ value: this.BNData ? this.BNData.bntypeId : null, disabled: this.BNData }, Validators.compose([Validators.required])],
    //   "legalTypeddl": [{ value: this.BNData ? this.BNData.legalTypeId : null, disabled: this.BNData }, Validators.compose([Validators.required])],
    //   "ddlLicenseCategory": [{ value: this.BNData ? this.BNData.licenseCategoryId : null, disabled: this.BNData }, Validators.compose([Validators.required])],
    //   "ddlLicenseType": [{ value: this.BNData ? this.BNData.licenseTypeId : null, disabled: this.BNData }, Validators.compose([Validators.required])],
    //   "reservationPeriod": [60, Validators.compose([Validators.required])],
    //   "ddlIssueanceAuthority": [{ value: this.BNData ? this.BNData.licenseIssuanceAuthorityId : null, disabled: this.BNData }],
    //   "ddlCountry": [{ value: this.BNData ? this.BNData.countryId : null, disabled: this.BNData }],
    // });
  }


  ngOnInit() {
    this.initializeForm();
    this.getMultipleLookups();

    // setting new radio button select by default
    this.radioButtonCondition = this.businessNamesTypesEnum.New;
  }

  getMultipleLookups() {
    this.multipleLookups = this.commonService.getMultipleLookups("?types=BusinessNamesTypes,LegalTypes,LicenseCategory,LicenseTypes,Country,IssuanceEntities");
    this.legalType = this.multipleLookups.pipe(map(resp => resp["LegalTypes"]));
    this.bnTypes = this.multipleLookups.pipe(map(resp => resp["BusinessNamesTypes"]));
    this.licenseCategory = this.multipleLookups.pipe(map(resp => resp["LicenseCategory"]));
    this.licenseType = this.multipleLookups.pipe(map(resp => resp["LegalTypes"]));
    this.countries = this.multipleLookups.pipe(map(resp => resp["Country"]));
    this.issueanceAuthority = this.multipleLookups.pipe(map(resp => resp["IssuanceEntities"]));
  }

  setRadioButtonCondition(value: number) {
    if (this.BNData) {
      return false;
    }
    else {
      debugger
      this.ApplicationInfoForm.reset();
      this.ApplicationInfoForm.markAsUntouched();
      this.initializeForm();
      this.ApplicationInfoForm.patchValue({
        "reservationPeriod": 60
      })
      if (value != this.businessNamesTypesEnum.New) {
        this.ApplicationInfoForm.controls.licenceNumber.setValidators([Validators.required]);
        this.ApplicationInfoForm.controls.licenceNumber.updateValueAndValidity();
        this.ApplicationInfoForm.controls.licenceIssueDate.setValidators([Validators.required]);
        this.ApplicationInfoForm.controls.licenceIssueDate.updateValueAndValidity();
        this.ApplicationInfoForm.controls.licenceExpiryDate.setValidators([Validators.required]);
        this.ApplicationInfoForm.controls.licenceExpiryDate.updateValueAndValidity();


        this.ApplicationInfoForm.controls.ddlIssueanceAuthority.setValidators(null);
        this.ApplicationInfoForm.controls.ddlIssueanceAuthority.updateValueAndValidity();
        this.ApplicationInfoForm.controls.ddlCountry.setValidators(null);
        this.ApplicationInfoForm.controls.ddlCountry.updateValueAndValidity();

        if (value == this.businessNamesTypesEnum.LocalTDABranch) {
          this.ApplicationInfoForm.controls.licenceIssueDate.disable();
          this.ApplicationInfoForm.controls.licenceExpiryDate.disable();

        }
        else {
          this.ApplicationInfoForm.controls.licenceIssueDate.enable();
          this.ApplicationInfoForm.controls.licenceExpiryDate.enable();
        }
      }
      else {
        this.ApplicationInfoForm.controls.licenceNumber.setValidators(null);
        this.ApplicationInfoForm.controls.licenceNumber.updateValueAndValidity();

        this.ApplicationInfoForm.controls.licenceIssueDate.setValidators(null);
        this.ApplicationInfoForm.controls.licenceIssueDate.updateValueAndValidity();

        this.ApplicationInfoForm.controls.licenceExpiryDate.setValidators(null);
        this.ApplicationInfoForm.controls.licenceExpiryDate.updateValueAndValidity();

        this.ApplicationInfoForm.controls.ddlIssueanceAuthority.setValidators(null);
        this.ApplicationInfoForm.controls.ddlIssueanceAuthority.updateValueAndValidity();

        this.ApplicationInfoForm.controls.ddlCountry.setValidators(null);
        this.ApplicationInfoForm.controls.ddlCountry.updateValueAndValidity();
      }

      if ((value == this.businessNamesTypesEnum.OtherEmiratesBranch) || (value == this.businessNamesTypesEnum.BranchForRAKFreeZoneCompany)) {
        this.ApplicationInfoForm.controls.ddlIssueanceAuthority.setValidators([Validators.required]);
        this.ApplicationInfoForm.controls.ddlIssueanceAuthority.updateValueAndValidity();
      }

      if ((value == this.businessNamesTypesEnum.BranchForAForeignCompany) || (value == this.businessNamesTypesEnum.BranchForAGCCCompany)) {
        this.ApplicationInfoForm.controls.ddlCountry.setValidators([Validators.required]);
        this.ApplicationInfoForm.controls.ddlCountry.updateValueAndValidity();
      }
      this.radioButtonCondition = value;
    }
  }

  getHeadOfficeLicenseNoData() {
    this.bnCommonService.getLicenseDetail(this.ApplicationInfoForm.value.licenceNumber)
      .subscribe(data => {
        if (data["result"].code === 200) {
          console.log(data);
          this.ApplicationInfoForm.patchValue({
            "licenceIssueDate": new Date(data["data"].issueDate),
            "licenceExpiryDate": new Date(data["data"].expiredDate)
          })
        }
        else {
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toast.error(text);
          });
        }
      })
  }

  // thirdApplicationInfoSubmit() {
  //   if (this.ApplicationInfoForm.valid) {
  //     this.ApplicationInfoFormOutput.emit(this.ApplicationInfoForm);
  //   }
  //   else {
  //     this.validationMessages.validateAllFormFields(this.ApplicationInfoForm);
  //   }
  // }
}
