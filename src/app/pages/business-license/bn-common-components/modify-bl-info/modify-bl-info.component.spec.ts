import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyBlInfoComponent } from './modify-bl-info.component';

describe('ModifyBlInfoComponent', () => {
  let component: ModifyBlInfoComponent;
  let fixture: ComponentFixture<ModifyBlInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyBlInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyBlInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
