import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { TranslationService } from '@translateService/translation.service';
import { CommonService } from 'app/core/appServices/index.service';
import { fileSize } from '../../../../core/validator/form-validators.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'kt-bn-attachments',
  templateUrl: './bn-attachments.component.html',
  styleUrls: ['./bn-attachments.component.scss']
})
export class BnAttachmentsComponent implements OnInit {

  @Input() AttachmentFormGroup: FormGroup;


  getAttachmentsType: any;
  isTick = [];
  isLoading = [];
  isDisabled: boolean = false;

  constructor(
    public translationService: TranslationService,
    private _formBuilder: FormBuilder,
    public commonService: CommonService,
    private _DomSanitizationService: DomSanitizer,
    private cdr: ChangeDetectorRef,
    public toast: ToastrService,
  ) { }

  ngOnInit() {
    this.getAttachments(137)
  }

  AttachmentsForm() {
debugger
    this.AttachmentFormGroup.addControl("attachments", new FormControl(this._formBuilder.array([])));

    // this.AttachmentFormGroup = this._formBuilder.group({
    //   attachments: this._formBuilder.array([]),
    // });

    let control = <FormArray>this.AttachmentFormGroup.controls.attachments;
    debugger
    this.getAttachmentsType.forEach(x => {
      control.push(this._formBuilder.group({
        id: [0],
        userId: [0],
        attachmentName: ['', x.isMandatory ? Validators.required : ''],
        attachmentURL: [''],
        attachmentSize: ['', x.isMandatory ? Validators.compose([Validators.required, fileSize]) : Validators.compose([fileSize])],
        attachmentExtension: [''],
        typeCode: [x.valueEn],
        expiryDate: [new Date()],
        isMandatory: [x.isMandatory],
        inputData: ['', x.inputData ? Validators.required : ''],
        attachmentType: [x.id],
      }));
    });
    this.isTick = [];
    this.getAttachmentsType.forEach(att => {
      this.isLoading.push({ loading: false });
      this.isTick.push({ tick: false });
    });
  }

  getAttachments(code) {
    this.commonService.GetAttachmentByUserTypeCode(code).subscribe(res => {
      debugger
      let response = res;
      if ((response.result && response.result.code == 200) || (response.code == 200)) {
        this.getAttachmentsType = response.result.data;
        this.AttachmentsForm();
      }
    });
  }

  getFile(event, index) {
    const controlArray = <FormArray>this.AttachmentFormGroup.get('attachments');
    if (event.target.files[0].size > 20913252) {
      controlArray.controls[index].get('attachmentSize').markAsTouched();
      controlArray.controls[index].get('attachmentSize').markAsDirty();
      controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
    }
    else {
      if (event.target.files[0]) {
        this.isLoading[index].loading = true;
        this.isTick[index].tick = true;
        this.isDisabled = true;
        this.commonService.fileUpload(event.target.files[0]).then(resp => {
          var response = resp;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            controlArray.controls[index].get('attachmentExtension').setValue(event.target.files[0].type);
            controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
            controlArray.controls[index].get('attachmentName').setValue(event.target.files[0].name);

            this.isLoading[index].loading = false;
            this.isDisabled = false;

            this.cdr.markForCheck();
            controlArray.controls[index].get('attachmentURL').setValue(response.attachmentURL);
            controlArray.controls[index].get('attachmentSize').markAsTouched();
            controlArray.controls[index].get('attachmentName').markAsTouched();
          }
          else {
            this.isLoading[index].loading = false;
            this.isDisabled = false;
            this.cdr.markForCheck();
            this.translationService.getTranslation(String(response.result.code)).subscribe((text: string) => {
              this.toast.error(text);
            });
          }
        });
      }
    }
  }

}
