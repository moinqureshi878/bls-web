import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BnAttachmentsComponent } from './bn-attachments.component';

describe('BnAttachmentsComponent', () => {
  let component: BnAttachmentsComponent;
  let fixture: ComponentFixture<BnAttachmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BnAttachmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BnAttachmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
