import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { TooltipPosition } from '@angular/material/tooltip';
import { BusinessLicenseService } from 'app/core/appServices/business-license/business-license.service';

@Component({
  selector: 'kt-view-submitted-bn-request',
  templateUrl: './view-submitted-bn-request.component.html',
  styleUrls: ['./view-submitted-bn-request.component.scss']
})
export class ViewSubmittedBnRequestComponent implements OnInit {

  //Stepper active class
  stepperValue: number = 1;

  //Form Group Stepper
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  seventhFormGroup: FormGroup;
  eighthFormGroup: FormGroup;

  getBNData: any;
  constructor(
    @Inject(MAT_DIALOG_DATA) data,
    private _formBuilder: FormBuilder,
    private modalService: NgbModal,
    public businessLicenseService: BusinessLicenseService,
    public dialog: MatDialog) {
    // debugger
    let BNData = data && data.bnData;
    BNData.applicationData.isHide = true;
    BNData.businessActivity.isHide = true;
    BNData.businessName.isHide = false;
    BNData.stakeHolders.isHide = true;
    BNData.attachments.isHide = false;
    this.getBNData = BNData;
  }

  ngOnInit() {
    this.secondFormGroup = this._formBuilder.group({});
    this.thirdFormGroup = this._formBuilder.group({});
    this.fifthFormGroup = this._formBuilder.group({});
    this.fourthFormGroup = this._formBuilder.group({});
    this.seventhFormGroup = this._formBuilder.group({});
    this.eighthFormGroup = this._formBuilder.group({});

  }
  //Stepper Value
  setStepperValue(value: number) {
    this.stepperValue = value;
  }
}