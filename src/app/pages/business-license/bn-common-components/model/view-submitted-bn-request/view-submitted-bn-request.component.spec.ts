import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSubmittedBnRequestComponent } from './view-submitted-bn-request.component';

describe('ViewSubmittedBnRequestComponent', () => {
  let component: ViewSubmittedBnRequestComponent;
  let fixture: ComponentFixture<ViewSubmittedBnRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSubmittedBnRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSubmittedBnRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
