import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslationService } from 'app/core/_base/layout';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { CommonService, UserService } from 'app/core/appServices/index.service';
import { WorkFlowCode } from 'app/core/_enum/workflowCodes.enum';
import { commentValidator } from 'app/core/validator/form-validators.service';

@Component({
  selector: 'kt-request-for-inspection',
  templateUrl: './request-for-inspection.component.html',
  styleUrls: ['./request-for-inspection.component.scss']
})
export class RequestForInspectionComponent implements OnInit {
  requestForInspectionForm: FormGroup;
  loading: boolean = false;
  dialogData: any;
 
  workFlowCode = WorkFlowCode;

  postJson: any;

  @Output() action: EventEmitter<string> = new EventEmitter();
  constructor(@Inject(MAT_DIALOG_DATA) data,
  public dialogRef: MatDialogRef<RequestForInspectionComponent>,
  private translationService: TranslationService,
  private toast: ToastrService,
  private router: Router,
  public validationMessages: ValidationMessagesService,
  public commonService: CommonService,
  public userService: UserService,
  public fb: FormBuilder) { 
    this.initializeForm();
    this.postJson = data;
  }

  ngOnInit() {
  }
  initializeForm() {
    this.requestForInspectionForm = this.fb.group({
      remarks: [null, Validators.compose([Validators.maxLength(500), commentValidator])],
    });
  }
  requestForInspectionFormSubmit() {
    if (this.requestForInspectionForm.valid) {
      this.loading = true;
      // this.postJson.workFlow.code = this.requestForInspectionForm.value.forwardForAction;
      this.postJson.workFlow.remarks = this.requestForInspectionForm.value.remarks;
      this.dialogData = {
        dialogName: 'requestForInspection',
        postJson: this.postJson
      }
      this.action.emit(this.dialogData);

      // this.dialogRef.close(this.dialogData);
      // this.router.navigateByUrl('pages/business-license/business-names/manage-business-names');
    }
    else {
      this.validationMessages.validateAllFormFields(this.requestForInspectionForm);
    }
  }
}
