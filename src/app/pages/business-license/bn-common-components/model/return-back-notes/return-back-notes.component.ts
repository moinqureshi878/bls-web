import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslationService } from 'app/core/_base/layout';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { WorkFlowCode } from 'app/core/_enum/workflowCodes.enum';
@Component({
  selector: 'kt-return-back-notes',
  templateUrl: './return-back-notes.component.html',
  styleUrls: ['./return-back-notes.component.scss']
})
export class ReturnBackNotesComponent implements OnInit {

  returnNotesForm: FormGroup;
  loading: boolean = false;
  dialogData: any;
  postJson: any;
  workFlowCode = WorkFlowCode;

  @Output() action: EventEmitter<string> = new EventEmitter();
  constructor(@Inject(MAT_DIALOG_DATA) data,
    public dialogRef: MatDialogRef<ReturnBackNotesComponent>,
    private translationService: TranslationService,
    private router: Router,
    private toast: ToastrService,
    public validationMessages: ValidationMessagesService,
    public fb: FormBuilder) {
    debugger
    // this.getNotes = data;
    this.postJson = data;
    this.initializeForm();
  }

  ngOnInit() {
  }
  initializeForm() {
    this.returnNotesForm = this.fb.group({
      notes: ['', Validators.compose([Validators.required, Validators.maxLength(30)])],
    });
  }
  returnNotesFormSubmit() {
    if (this.returnNotesForm.valid) {
      this.loading = true;
      this.dialogData = {
        dialogName: 'returnNotes',
        postJson: this.postJson
      }
      this.postJson.workFlow.code = this.workFlowCode.ToCustomer;
      this.postJson.workFlow.remarks = this.returnNotesForm.value.notes;
      this.action.emit(this.dialogData);
      // this.dialogRef.close(this.dialogData);
      // this.router.navigateByUrl('pages/business-license/business-names/manage-business-names');
    }
    else {
      this.validationMessages.validateAllFormFields(this.returnNotesForm);
    }
  }
}
