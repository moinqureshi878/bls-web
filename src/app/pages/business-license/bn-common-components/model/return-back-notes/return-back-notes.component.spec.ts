import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnBackNotesComponent } from './return-back-notes.component';

describe('ReturnBackNotesComponent', () => {
  let component: ReturnBackNotesComponent;
  let fixture: ComponentFixture<ReturnBackNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReturnBackNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnBackNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
