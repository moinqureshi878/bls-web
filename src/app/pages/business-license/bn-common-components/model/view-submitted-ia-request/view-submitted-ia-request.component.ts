import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { termsConditionValidator } from 'app/core/validator/form-validators.service';
import { Observable } from 'rxjs';
import { JwtTokenService } from 'app/core/appServices/index.service';
import { BusinessLicenseService } from 'app/core/appServices/business-license/business-license.service';

@Component({
  selector: 'kt-view-submitted-ia-request',
  templateUrl: './view-submitted-ia-request.component.html',
  styleUrls: ['./view-submitted-ia-request.component.scss']
})
export class ViewSubmittedIaRequestComponent implements OnInit {
  closeResult: string; //Bootstrap Modal Popup
  panelOpenState = false;
  emCopy: string = "ID front side";
  embCopy: string = "ID back side";
  passportCopy: string = "Passport info pages";
  visaCopy: string = "Visa page";
  //Stepper active class
  stepperValue: number = 1;

  //Form Group Stepper
  firstServiceInfoForm: FormGroup;
  secondFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  eighthFormGroup: FormGroup;
  /* Checkbox Value Set to true */
  checkedBLS = true;
  serviceInfoAndUserInfo: Observable<any>;
  constructor(
    private _formBuilder: FormBuilder,
    private jwtTokenService: JwtTokenService,
    public businessLicenseService: BusinessLicenseService,
    private modalService: NgbModal,
  ) { }

  ngOnInit() {
    this.fillUserIDOnModel();
    this.firstServiceInfoForm = this._formBuilder.group({
      termsCondition: [true, Validators.compose([Validators.required, termsConditionValidator])]
    });
    this.secondFormGroup = this._formBuilder.group({});
    this.sixthFormGroup = this._formBuilder.group({});
    this.eighthFormGroup = this._formBuilder.group({});
  }
  fillUserIDOnModel() {
    let userSession = JSON.parse(localStorage.getItem("userSession"));
    let sessionID = 0;

    let currentSessionToken: string;
    let behalfUserID: number;
    let UserID: number;
    if (userSession && userSession.length > 1) {
      currentSessionToken = userSession[0].token;
      behalfUserID = +this.jwtTokenService.getJwtTokenValueByToken(userSession[1].token).UserID;
      UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
      // this.JsonModel["bnRequest"].onBehalfUid = behalfUserID;
    }
    else {
      currentSessionToken = userSession[0].token;
      UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
    }

    this.jwtTokenService.getSessionSubject().subscribe(value => {
      if (value) {
        if (value && value.length > 1) {
          currentSessionToken = value[0].token;
          behalfUserID = +this.jwtTokenService.getJwtTokenValueByToken(value[1].token).UserID;
          UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
          // this.JsonModel["bnRequest"].onBehalfUid = behalfUserID;
        }
        else {
          currentSessionToken = value[0].token;
          UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
        }
      }
    });

    this.getServiceInfo(behalfUserID ? behalfUserID : 0, UserID);

    // this.JsonModel["bnRequest"].userId = UserID;
  }
  getServiceInfo(loginOnBehalfID: number, UserID: number) {
    this.serviceInfoAndUserInfo = this.businessLicenseService.GetServiceInfo(loginOnBehalfID, UserID);
  }
  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);

  getEmCopy(event) {
    this.emCopy = event.target.files[0].name.substring(0, 15);
  }
  getEmbId(event) {
    this.embCopy = event.target.files[0].name.substring(0, 15);
  }
  getPassportCopy(event) {
    this.passportCopy = event.target.files[0].name.substring(0, 15);
  }
  getVisa(event) {
    this.visaCopy = event.target.files[0].name.substring(0, 15);
  }

  //Stepper Value
  setStepperValue(value: number) {
    this.stepperValue = value;
  }

  //..Modal Popup Start
  open(content) {
    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 
}