import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSubmittedIaRequestComponent } from './view-submitted-ia-request.component';

describe('ViewSubmittedIaRequestComponent', () => {
  let component: ViewSubmittedIaRequestComponent;
  let fixture: ComponentFixture<ViewSubmittedIaRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSubmittedIaRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSubmittedIaRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
