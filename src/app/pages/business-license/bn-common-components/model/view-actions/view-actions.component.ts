
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MAT_DIALOG_DATA } from '@angular/material';

/** Builds and returns a new User. */
const STEP_THREE_DATA = [
  { ver: '1', userType: 'Individual Customer', actionBy: 'Ahmed Salem', actionDateTime: '10-Nov-2019 10:12 AM', status: 'Submitted', internalNotes: '-', customerNotes: '-' },
  { ver: '1', userType: 'TDA Staff', actionBy: 'Shahi Ali', actionDateTime: '10-Nov-2019 10:45 AM', status: 'Under Process', internalNotes: '-', customerNotes: '-' },
  { ver: '1', userType: 'TDA Staff', actionBy: 'Shahi Ali', actionDateTime: '10-Nov-2019 1:45 PM', status: 'Send to Manager', internalNotes: 'Please check Business Type', customerNotes: '-' },
  { ver: '1', userType: 'TDA Manager', actionBy: 'Abdallah Sharaf', actionDateTime: '11-Nov-2019 9:45 AM', status: 'Returned Back', internalNotes: 'Checked and get update from Customer', customerNotes: '-' },
  { ver: '1', userType: 'TDA Staff', actionBy: 'Shahi Ali', actionDateTime: '11-Nov-2019 10:10 AM', status: 'Send to Customer', internalNotes: '-', customerNotes: 'Please update Business Type' },
  { ver: '2', userType: 'Individual Customer', actionBy: 'Ahmed Salem', actionDateTime: '12-Nov-2019 11:12 AM', status: 'Re-Submitted', internalNotes: '-', customerNotes: 'Updated Please' },
  { ver: '2', userType: 'TDA Staff', actionBy: 'Shahi Ali', actionDateTime: '12-Nov-2019 11:30 AM', status: 'Under Process', internalNotes: '-', customerNotes: '-' },
  { ver: '2', userType: 'TDA Staff', actionBy: 'Shahi Ali', actionDateTime: '12-Nov-2019 11:45 AM', status: 'Send to Consultant', internalNotes: 'Please Verify business type', customerNotes: '-' },
  { ver: '2', userType: 'TDA Consultant', actionBy: 'Majid Almulla', actionDateTime: '12-Nov-2019 1:55 PM', status: 'Returned Back', internalNotes: 'All OK', customerNotes: '-' },
  { ver: '2', userType: 'TDA Staff', actionBy: 'Shahi Ali', actionDateTime: '12-Nov-2019 2:22 PM', status: 'Approved', internalNotes: '-', customerNotes: '-' },
];

@Component({
  selector: 'kt-view-actions',
  templateUrl: './view-actions.component.html',
  styleUrls: ['./view-actions.component.scss']
})
export class ViewActionsComponent implements OnInit {

  /* Step Three */
  displayedColumnsstepThree: string[] = ['ver', 'userType', 'actionBy', 'actionDateTime', 'status', 'internalNotes', 'customerNotes'];
  stepThree = new MatTableDataSource(STEP_THREE_DATA);
  @ViewChild('matPaginatorstepThree', { static: true }) paginatorstepThree: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortStepThree: MatSort;

  constructor(@Inject(MAT_DIALOG_DATA) data) {

  }

  ngOnInit() {
    this.stepThree.paginator = this.paginatorstepThree;
    this.stepThree.sort = this.sortStepThree;
  }

  applyFilterstepThree(filterValue: string) {
    this.stepThree.filter = filterValue.trim().toLowerCase();
    if (this.stepThree.paginator) {
      this.stepThree.paginator.firstPage();
    }
  }
}