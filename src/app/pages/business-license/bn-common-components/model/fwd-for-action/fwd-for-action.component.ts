import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslationService } from 'app/core/_base/layout';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { CommonService, UserService } from 'app/core/appServices/index.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { WorkFlowCode } from 'app/core/_enum/workflowCodes.enum';
import { commentValidator } from 'app/core/validator/form-validators.service';

@Component({
  selector: 'kt-fwd-for-action',
  templateUrl: './fwd-for-action.component.html',
  styleUrls: ['./fwd-for-action.component.scss']
})
export class FwdForActionComponent implements OnInit {

  forwardForActionForm: FormGroup;
  loading: boolean = false;
  dialogData: any;
  designations: Observable<any> = this.commonService.getLookUp("?type=TDADesignations");
  tdaUsers: Observable<any> = this.userService.GetInternalUser().pipe(map(resp => resp['data']));
  workFlowCode = WorkFlowCode;

  postJson: any;
  managerOrConsultant: string;
  @Output() action: EventEmitter<string> = new EventEmitter();
  constructor(@Inject(MAT_DIALOG_DATA) data,
    public dialogRef: MatDialogRef<FwdForActionComponent>,
    private translationService: TranslationService,
    private toast: ToastrService,
    private router: Router,
    public validationMessages: ValidationMessagesService,
    public commonService: CommonService,
    public userService: UserService,
    public fb: FormBuilder) {
    this.postJson = data.postJson;
    this.managerOrConsultant = data.managerOrConsultant;
    this.initializeForm();
  }

  ngOnInit() {

  }

  initializeForm() {
    this.forwardForActionForm = this.fb.group({
      forwardForAction: [(this.managerOrConsultant == 'manager' ? this.workFlowCode.ToConsultant : this.workFlowCode.ToManager)],
      tdaUser: [null, Validators.compose([Validators.required])],
      designation: [null, Validators.required],
      remarks: [null, Validators.compose([Validators.maxLength(500), commentValidator])],
    });
  }
  forwardForActionFormSubmit() {
    if (this.forwardForActionForm.valid) {
      this.loading = true;
      this.postJson.workFlow.code = this.forwardForActionForm.value.forwardForAction;
      this.postJson.workFlow.userId = this.forwardForActionForm.value.tdaUser;
      this.postJson.workFlow.designation = this.forwardForActionForm.value.designation;
      this.postJson.workFlow.remarks = this.forwardForActionForm.value.remarks;
      this.dialogData = {
        dialogName: 'fwdAction',
        postJson: this.postJson
      }
      this.action.emit(this.dialogData);

      // this.dialogRef.close(this.dialogData);
      // this.router.navigateByUrl('pages/business-license/business-names/manage-business-names');
    }
    else {
      this.validationMessages.validateAllFormFields(this.forwardForActionForm);
    }
  }
}
