import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwdForActionComponent } from './fwd-for-action.component';

describe('FwdForActionComponent', () => {
  let component: FwdForActionComponent;
  let fixture: ComponentFixture<FwdForActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwdForActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwdForActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
