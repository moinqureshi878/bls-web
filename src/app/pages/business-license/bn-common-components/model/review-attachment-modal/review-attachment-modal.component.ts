import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { TranslationService } from 'app/core/_base/layout';

@Component({
  selector: 'kt-review-attachment-modal',
  templateUrl: './review-attachment-modal.component.html',
  styleUrls: ['./review-attachment-modal.component.scss']
})
export class ReviewAttachmentModalComponent implements OnInit {
  url: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) data,
    private translateService: TranslationService,
    ) {
    debugger
    this.url = data;
  }

  ngOnInit() {
  }

}
