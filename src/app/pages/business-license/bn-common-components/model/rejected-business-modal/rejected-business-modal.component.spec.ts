import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectedBusinessModalComponent } from './rejected-business-modal.component';

describe('RejectedBusinessModalComponent', () => {
  let component: RejectedBusinessModalComponent;
  let fixture: ComponentFixture<RejectedBusinessModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectedBusinessModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectedBusinessModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
