import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslationService } from 'app/core/_base/layout';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { ToastrService } from 'ngx-toastr';
import { BnCommonService } from 'app/core/appServices/index.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { WorkFlowCode } from 'app/core/_enum/workflowCodes.enum';
import { commentValidator } from 'app/core/validator/form-validators.service';

@Component({
  selector: 'kt-rejected-business-modal',
  templateUrl: './rejected-business-modal.component.html',
  styleUrls: ['./rejected-business-modal.component.scss']
})
export class RejectedBusinessModalComponent implements OnInit {

  rejectForm: FormGroup;
  loading: boolean = false;
  cancelReasons: Observable<any> = this.bnCommonService.GetCancelReason(137);
  dialogData: any;
  workFlowCode = WorkFlowCode;
  postJson: any;

  @Output() action: EventEmitter<string> = new EventEmitter();
  constructor(@Inject(MAT_DIALOG_DATA) data,
    public dialogRef: MatDialogRef<RejectedBusinessModalComponent>,
    private translationService: TranslationService,
    private toast: ToastrService,
    public validationMessages: ValidationMessagesService,
    public bnCommonService: BnCommonService,
    private router: Router,
    public fb: FormBuilder) {
    debugger
    this.postJson = data;

  }

  ngOnInit() {
    this.initializeForm();
  }
  initializeForm() {
    this.rejectForm = this.fb.group({
      reason: [new Array, Validators.compose([Validators.required])],
      remarks: [null, Validators.compose([Validators.maxLength(500), commentValidator])],
    });
  }
  rejectFormSubmit() {
    debugger
    if (this.rejectForm.valid) {
      this.loading = true;
      this.postJson.workFlow.code = this.workFlowCode.Rejected;
      this.postJson.workFlow.remarks = this.rejectForm.value.remarks;
      this.postJson.workFlow.reasonIds = this.rejectForm.value.reason;
      this.dialogData = {
        dialogName: 'reject',
        postJson: this.postJson
      }
      this.action.emit(this.dialogData);
      // this.dialogRef.close(this.dialogData);
      // this.router.navigateByUrl('pages/business-license/business-names/manage-business-names');
    }
    else {
      this.validationMessages.validateAllFormFields(this.rejectForm);
    }
  }

}
