import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StakeholderInfoComponent } from './stakeholder-info.component';

describe('StakeholderInfoComponent', () => {
  let component: StakeholderInfoComponent;
  let fixture: ComponentFixture<StakeholderInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StakeholderInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StakeholderInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
