import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { MatDialogConfig, MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { StakeholderInfoModalComponent } from '../stakeholder-info-modal/stakeholder-info-modal.component';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { StakeholderTypesEnum } from 'app/core/_enum/stakeholderTypes.enum';
import { BusinessNamesTypesEnum } from 'app/core/_enum/businessNamesTypes.enum';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CommonService, UserService, BnCommonService } from 'app/core/appServices/index.service';
import { TranslationService } from 'app/core/_base/layout';
import { ToastrService } from 'ngx-toastr';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { Router } from '@angular/router';
import { map, filter } from 'rxjs/operators';
import { nameArValidator } from 'app/core/validator/form-validators.service';


@Component({
  selector: 'kt-stakeholder-info',
  templateUrl: './stakeholder-info.component.html',
  styleUrls: ['./stakeholder-info.component.scss']
})
export class StakeholderInfoComponent implements OnInit {

  gridStakeHolder: any[] = [];
  stakeHoldersdataSource7 = new MatTableDataSource<any>(this.gridStakeHolder);
  stakeHoldersdisplayedColumns7: string[] = ['orderNo', 'type', 'relationShipTypeEn', 'nameAr', 'nameEn', 'sharePrct', 'isRegistered', 'Action'];
  closeResult: string; //Bootstrap Modal Popup

  @ViewChild('stakeHoldersmatPaginator7', { static: true }) stakeHolderspaginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) stakeHolderssort7: MatSort;
  @Output() StakeHolderInfoModelValues = new EventEmitter<any>();

  stakeholderEnum = StakeholderTypesEnum;
  businessNamesTypesEnum = BusinessNamesTypesEnum;
  radioButtonCondition: number;
  branchCondition: number = 0;
  userDetail: any;

  multipleLookups: Observable<any>;
  stakeholderTypes: Observable<any>;
  stakeholderRelationship: Observable<any>;
  govtEntity: Observable<any>;
  individuals: Observable<any>;
  inheritenceRepresentative: Observable<any>;
  businessNameTypes: Observable<any>;
  issueanceAuthority: Observable<any>;
  nationalities: Observable<any>;

  StakeHolderForm: FormGroup;

  emiratesIDMask = [/[0-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/]

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);

  constructor(
    public dialog: MatDialog,
    private modalService: NgbModal,
    private dialogRef: MatDialog,
    private commonService: CommonService,
    private fb: FormBuilder,
    private translationService: TranslationService,
    public userService: UserService,
    public bnCommonService: BnCommonService,
    private toast: ToastrService,
    public validationMessages: ValidationMessagesService,
    public router: Router,
  ) { }

  ngOnInit() {
    this.stakeHoldersdataSource7.paginator = this.stakeHolderspaginator7;
    this.stakeHoldersdataSource7.sort = this.stakeHolderssort7;
  }

  fillDropdowns() {
    this.multipleLookups = this.commonService.getMultipleLookups("?types=StakeholderTypes,StakeholderOwnershipTypes,GovernmentAuthorities,BNCategorries,IssuanceEntities,Nationality");
    this.stakeholderTypes = this.multipleLookups.pipe(map(resp => resp["StakeholderTypes"]));
    this.stakeholderRelationship = this.multipleLookups.pipe(map(resp => resp["StakeholderOwnershipTypes"]));
    this.govtEntity = this.multipleLookups.pipe(map(resp => resp["GovernmentAuthorities"]));
    this.multipleLookups.subscribe(
      data => {
        this.businessNameTypes = data["BNCategorries"];
      }
    )
    this.issueanceAuthority = this.multipleLookups.pipe(map(resp => resp["IssuanceEntities"]));
    this.nationalities = this.multipleLookups.pipe(map(resp => resp["Nationality"]));
  }

  initializeForm() {
    // this.StakeHolderForm = this.fb.group({
    //   "type": [null, Validators.compose([Validators.required])],
    //   "ddlStakeHolderRelationshipType": [null, Validators.compose([Validators.required, Validators.maxLength(50)])],
    //   "shares": [null, Validators.compose([Validators.required])],
    //   "govtEntity": [null, Validators.compose([Validators.required])],
    //   "licenceNumber": new FormControl(null),
    // });
    this.StakeHolderForm = this.fb.group({
      "type": [this.stakeholderEnum.Individual, Validators.compose([Validators.required])],
      "ddlStakeHolderRelationshipType": [null, Validators.compose([Validators.required, Validators.maxLength(50)])],
      "shares": [null, Validators.compose([Validators.required])],
      "govtEntity": [null],
      "individualEntities": [null],
      'InheritanceRepresentative': [null],
      "licenceNumber": [null],
      "businessNamesTypes": [null],
      "licenceIssueDate": [null],
      "licenceExpiryDate": [null],
      "ddlIssueanceAuthority": [null],
      "BNnameEn": [null],
      "BNnameAr": [null]
    });
  }

  onStakeholderTypesChanged() {
    this.userDetail = null;
    this.getIndividualAndInheritenceRepresentativeData(this.StakeHolderForm.value.type.id);
    if (this.StakeHolderForm.value.type.id == this.stakeholderEnum.License) {
      this.StakeHolderForm = this.fb.group({
        "type": [this.StakeHolderForm.value.type, Validators.compose([Validators.required])],
        "ddlStakeHolderRelationshipType": [null, Validators.compose([Validators.required])],
        "shares": [null, Validators.compose([Validators.required])],
        "govtEntity": [null],
        "individualEntities": [null],
        "InheritanceRepresentative": [null],
        "licenceNumber": [null, Validators.compose([Validators.required])],
        "businessNamesTypes": [this.businessNamesTypesEnum.New],
        "licenceIssueDate": [null, Validators.compose([Validators.required])],
        "licenceExpiryDate": [null, Validators.compose([Validators.required])],
        "ddlIssueanceAuthority": ['', Validators.compose([Validators.required, Validators.maxLength(200)])],
        "BNnameEn": ['', Validators.compose([Validators.required, Validators.maxLength(200)])],
        "BNnameAr": ['', Validators.compose([Validators.required, Validators.maxLength(200), nameArValidator])]
      });
    }
    else if (this.StakeHolderForm.value.type.id == this.stakeholderEnum.GovAuthority) {
      this.StakeHolderForm = this.fb.group({
        "type": [this.StakeHolderForm.value.type, Validators.compose([Validators.required])],
        "ddlStakeHolderRelationshipType": [null, Validators.compose([Validators.required])],
        "shares": [null, Validators.compose([Validators.required])],
        "govtEntity": [null, Validators.compose([Validators.required])],
        "individualEntities": [null],
        "InheritanceRepresentative": [null],
        "licenceNumber": [null],
        "businessNamesTypes": [null],
        "licenceIssueDate": [null],
        "licenceExpiryDate": [null],
        "ddlIssueanceAuthority": [null],
        "BNnameEn": [null],
        "BNnameAr": [null]
      });
    }
    else if (this.StakeHolderForm.value.type.id == this.stakeholderEnum.Individual) {
      this.StakeHolderForm = this.fb.group({
        "type": [this.StakeHolderForm.value.type, Validators.compose([Validators.required])],
        "ddlStakeHolderRelationshipType": [null, Validators.compose([Validators.required])],
        "shares": [null, Validators.compose([Validators.required])],
        "govtEntity": [null],
        "individualEntities": [null, Validators.compose([Validators.required])],
        "InheritanceRepresentative": [null],
        "licenceNumber": [null],
        "businessNamesTypes": [null],
        "licenceIssueDate": [null],
        "licenceExpiryDate": [null],
        "ddlIssueanceAuthority": [null],
        "BNnameEn": [null],
        "BNnameAr": [null]
      });
    }
    else if (this.StakeHolderForm.value.type.id == this.stakeholderEnum.InehritanceRepresentative) {
      this.StakeHolderForm = this.fb.group({
        "type": [this.StakeHolderForm.value.type, Validators.compose([Validators.required])],
        "ddlStakeHolderRelationshipType": [null, Validators.compose([Validators.required])],
        "shares": [null, Validators.compose([Validators.required])],
        "govtEntity": [null],
        "individualEntities": [null],
        "InheritanceRepresentative": [null, Validators.compose([Validators.required])],
        "licenceNumber": [null],
        "businessNamesTypes": [null],
        "licenceIssueDate": [null],
        "licenceExpiryDate": [null],
        "ddlIssueanceAuthority": [null],
        "BNnameEn": [null],
        "BNnameAr": [null]
      });
    }
  }

  getIndividualAndInheritenceRepresentativeData(id: number) {
    if (id == this.stakeholderEnum.InehritanceRepresentative) {
      this.inheritenceRepresentative = this.userService.GetStakeHolderByType(id).pipe(map(resp => resp["data"]));
    }
    else if (id == this.stakeholderEnum.Individual) {
      this.individuals = this.userService.GetStakeHolderByType(id).pipe(map(resp => resp["data"]));
    }
  }

  setBranchCondition(value: number) {

    this.branchCondition = value;
  }

  addstakeholder(type: string) {
    if (this.StakeHolderForm.valid) {
      debugger

      let model = {
        "id": 0,
        "bnrefNo": "BNM-1",
        "stakeHolderTypeId": this.StakeHolderForm.value.type.id,
        "stakeHolderTypeEn": this.StakeHolderForm.value.type.valueEn,
        "stakeHolderTypeAr": this.StakeHolderForm.value.type.valueAr,
        "relationShipTypeId": this.StakeHolderForm.value.ddlStakeHolderRelationshipType.id,
        "relationShipTypeEn": this.StakeHolderForm.value.ddlStakeHolderRelationshipType.valueEn,
        "relationShipTypeAr": this.StakeHolderForm.value.ddlStakeHolderRelationshipType.valueAr,
        "stakeHolderUserId": (this.StakeHolderForm.value.type.id == this.stakeholderEnum.Individual) ? this.StakeHolderForm.value.individualEntities.stakeHolderId : (this.StakeHolderForm.value.type.id == this.stakeholderEnum.InehritanceRepresentative) ? this.StakeHolderForm.value.InheritanceRepresentative.stakeHolderId : null,
        "govtEntityId": (this.StakeHolderForm.value.type.id == this.stakeholderEnum.GovAuthority) ? this.StakeHolderForm.value.govtEntity.id : null,
        "nameEn": (this.StakeHolderForm.value.type.id == this.stakeholderEnum.GovAuthority) ? this.StakeHolderForm.value.govtEntity.valueEn : (this.StakeHolderForm.value.type.id == this.stakeholderEnum.Individual) ? this.StakeHolderForm.value.individualEntities.fullNameEn : (this.StakeHolderForm.value.type.id == this.stakeholderEnum.InehritanceRepresentative) ? this.StakeHolderForm.value.InheritanceRepresentative.fullNameEn : (this.StakeHolderForm.value.type.id == this.stakeholderEnum.License) ? this.StakeHolderForm.value.BNnameEn : null,
        "nameAr": (this.StakeHolderForm.value.type.id == this.stakeholderEnum.GovAuthority) ? this.StakeHolderForm.value.govtEntity.valueAr : (this.StakeHolderForm.value.type.id == this.stakeholderEnum.Individual) ? this.StakeHolderForm.value.individualEntities.fullNameAr : (this.StakeHolderForm.value.type.id == this.stakeholderEnum.InehritanceRepresentative) ? this.StakeHolderForm.value.InheritanceRepresentative.fullNameAr : (this.StakeHolderForm.value.type.id == this.stakeholderEnum.License) ? this.StakeHolderForm.value.BNnameAr : null,
        "sharePrct": this.StakeHolderForm.value.shares,
        "isRegistered": (this.StakeHolderForm.value.type.id == this.stakeholderEnum.Individual) ? this.userDetail.isRegistered : (this.StakeHolderForm.value.type.id == this.stakeholderEnum.InehritanceRepresentative) ? this.userDetail.isRegistered : (this.StakeHolderForm.value.type.id == this.stakeholderEnum.GovAuthority) ? true : (this.StakeHolderForm.value.type.id == this.stakeholderEnum.License) ? true : false,
        "licenseNumber": this.StakeHolderForm.value.licenceNumber,
        "issueDate": this.StakeHolderForm.value.licenceIssueDate,
        "expiryDate": this.StakeHolderForm.value.licenceExpiryDate,
        "businessNameAr": (this.StakeHolderForm.value.type.id == this.stakeholderEnum.License) ? this.StakeHolderForm.value.BNnameAr : null,
        "businessNameEn": (this.StakeHolderForm.value.type.id == this.stakeholderEnum.License) ? this.StakeHolderForm.value.BNnameEn : null,
        "issuingAuthorityId": this.StakeHolderForm.value.ddlIssueanceAuthority
      }

      if (this.gridStakeHolder.some(x => JSON.stringify(x, ["stakeHolderTypeId", "govtEntityId", "stakeHolderUserId", "licenseNumber"]) == JSON.stringify(model, ["stakeHolderTypeId", "govtEntityId", "stakeHolderUserId", "licenseNumber"]))) {
        this.translationService.getTranslation('VALIDATIONMESSAGES.BUSINESSNAMESTAKEHOLDERALREADYEXIST').subscribe((text: string) => {
          this.toast.error(text);
        });
      }
      else {
        this.gridStakeHolder.push(model);
        this.stakeHoldersdataSource7 = new MatTableDataSource<any>(this.gridStakeHolder);
        this.stakeHoldersdataSource7.paginator = this.stakeHolderspaginator7;
        this.stakeHoldersdataSource7.sort = this.stakeHolderssort7;
      }

      if (type == 'saveandclose') {
        this.modalService.dismissAll();
      }


      this.StakeHolderInfoModelValues.emit(model);
    }
    else {
      this.validationMessages.validateAllFormFields(this.StakeHolderForm);
    }
  }

  openAddStakeHolders(modelName) {
    this.fillDropdowns();
    this.initializeForm();

    this.radioButtonCondition = this.businessNamesTypesEnum.New;

    this.modalService.open(modelName, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  onInheritanceRepresentativeChanged() {
    if (this.StakeHolderForm.value.InheritanceRepresentative.stakeHolderId > 0) {
      this.getUserDetail(this.StakeHolderForm.value.InheritanceRepresentative.stakeHolderId);
    }
  }

  onIndividualEntitiesChanged() {
    this.getUserDetail(this.StakeHolderForm.value.individualEntities.stakeHolderId);
  }

  getUserDetail(id: number) {
    this.userService.GetUser(id)
      .subscribe(data => {
        if (data.code === 200) {
          console.log(data);
          this.userDetail = data["data"];
        }
        else {

        }
      })
  }

  stakeHoldersapplyFilter7(filterValue: string) {
    this.stakeHoldersdataSource7.filter = filterValue.trim().toLowerCase();
    if (this.stakeHoldersdataSource7.paginator) {
      this.stakeHoldersdataSource7.paginator.firstPage();
    }
  }

  getHeadOfficeLicenseNoData() {
    this.bnCommonService.getLicenseDetail(this.StakeHolderForm.value.licenceNumber)
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.StakeHolderForm.patchValue({
            "licenceIssueDate": new Date(data["data"].issueDate),
            "licenceExpiryDate": new Date(data["data"].expiredDate),
            "BNnameEn": data["data"].businessNameEn,
            "BNnameAr": data["data"].businessNameAr
          })
        }
        else {
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toast.error(text);
          });
        }
      })
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  submitStakeHolderInfo() {


    let JsonArray: any[] = [];

    this.gridStakeHolder.forEach(x => {

      let JsonValue = {
        "id": 0,
        "bnid": 1,
        "bnrefNo": "BNR-1",
        "stakeHolderTypeId": x.stakeHolderTypeId,
        "stakeHolderUserId": x.stakeHolderUserId,
        "sharePrct": x.sharePrct,
        "relationShipTypeId": x.relationShipTypeId,
        "govtEntityId": x.govtEntityId,
        "licenseNumber": x.licenseNumber,
        "issuingAuthorityId": x.issuingAuthorityId,
        "issueDate": x.issueDate,
        "expiryDate": x.expiryDate,
        "businessNameAr": x.businessNameAr,
        "businessNameEn": x.businessNameEn,
        "isRegistered": x.isRegistered,
        "businessNameRequest": null
      }

      JsonArray.push(JsonValue);


    });

    if (JsonArray && JsonArray.length > 0) {

    }
    else {
      this.translationService.getTranslation('VALIDATIONMESSAGES.BUSINESSNAMESTAKEHOLDER').subscribe((text: string) => {
        this.toast.error(text);
      });
    }
    this.StakeHolderInfoModelValues.emit(JsonArray);


  }

  setRadioButtonCondition(value: number) {
    debugger

    this.StakeHolderForm.controls['govtEntity'].reset();
    this.StakeHolderForm.controls['individualEntities'].reset();

    this.StakeHolderForm.patchValue({
      "InheritanceRepresentative": 0
    })

    this.StakeHolderForm.controls['licenceNumber'].reset();
    this.StakeHolderForm.controls['licenceIssueDate'].reset();
    this.StakeHolderForm.controls['licenceExpiryDate'].reset();
    this.StakeHolderForm.controls['ddlIssueanceAuthority'].reset();
    this.StakeHolderForm.controls['BNnameEn'].reset();
    this.StakeHolderForm.controls['BNnameAr'].reset();


    if (value == this.businessNamesTypesEnum.LocalTDABranch) {
      this.StakeHolderForm.controls.licenceIssueDate.disable();
      this.StakeHolderForm.controls.licenceExpiryDate.disable();
      this.StakeHolderForm.controls.BNnameAr.disable();
      this.StakeHolderForm.controls.BNnameEn.disable();
    }
    else {
      this.StakeHolderForm.controls.licenceIssueDate.enable();
      this.StakeHolderForm.controls.licenceExpiryDate.enable();
      this.StakeHolderForm.controls.BNnameAr.enable();
      this.StakeHolderForm.controls.BNnameEn.enable();
    }
  }

  deleteStakeHolder(index: number) {
    this.gridStakeHolder.splice(index, 1);
    this.stakeHoldersdataSource7 = new MatTableDataSource<any>(this.gridStakeHolder);
    this.stakeHoldersdataSource7.paginator = this.stakeHolderspaginator7;
    this.stakeHoldersdataSource7.sort = this.stakeHolderssort7;
  }

}
