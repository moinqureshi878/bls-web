import { Component, OnInit, Input } from '@angular/core';
import { TranslationService } from 'app/core/_base/layout';
import { Observable } from 'rxjs';
import { CommonService } from 'app/core/appServices/index.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'kt-submitted-info',
  templateUrl: './submitted-info.component.html',
  styleUrls: ['./submitted-info.component.scss']
})
export class SubmittedInfoComponent implements OnInit {
  nationalities: Observable<any>;
  multipleLookups: Observable<any>;

  @Input('user') user: any;
  @Input('loginOnBehalf') loginOnBehalf: any;
  constructor(
    public translationService: TranslationService,
    private commonService: CommonService,
  ) {
    this.fillDropdowns();
  }

  fillDropdowns() {
    this.multipleLookups = this.commonService.getMultipleLookups("?types=Nationality");
    this.nationalities = this.multipleLookups.pipe(map(resp => resp["Nationality"]));
  }

  ngOnInit() {
  }

  ngOnChanges() {
  }
}
