import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewRenewBlRequestDetailComponent } from './review-renew-bl-request-detail.component';

describe('ReviewRenewBlRequestDetailComponent', () => {
  let component: ReviewRenewBlRequestDetailComponent;
  let fixture: ComponentFixture<ReviewRenewBlRequestDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewRenewBlRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewRenewBlRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
