import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { TooltipPosition } from '@angular/material/tooltip';
// import { RejectedBlModalComponent } from '../rejected-bl-modal/rejected-bl-modal.component';
// import { ViewActionsComponent } from '../view-actions/view-actions.component';
// import { ViewSubmittedIaRequestComponent } from '../view-submitted-ia-request/view-submitted-ia-request.component';
// import { FwdForActionComponent } from '../fwd-for-action/fwd-for-action.component';
// import { RequestForInspectionComponent } from '../request-for-inspection/request-for-inspection.component';
// import { ReturnBackNotesComponent } from "../return-back-notes/return-back-notes.component";
// import { ViewSubmittedBnRequestComponent } from "../view-submitted-bn-request/view-submitted-bn-request.component";
// import { ViewSubmittedBlRequestComponent } from '../view-submitted-bl-request/view-submitted-bl-request.component';

import { RejectedBusinessModalComponent } from '../../bn-common-components/model/rejected-business-modal/rejected-business-modal.component';
import { ViewSubmittedBnRequestComponent } from '../../bn-common-components/model/view-submitted-bn-request/view-submitted-bn-request.component';
import { ViewActionsComponent } from '../../bn-common-components/model/view-actions/view-actions.component';
import { FwdForActionComponent } from '../../bn-common-components/model/fwd-for-action/fwd-for-action.component';

/* Inspection */
export interface UserData {
  reportNo: string;
  relatedDocs: string;
  pending: string;
  action: string;
}

const ELEMENT_DATA: UserData[] = [
  { reportNo: '', relatedDocs: '', pending: '', action: '', },
  { reportNo: '', relatedDocs: '', pending: '', action: '', },
  { reportNo: '', relatedDocs: '', pending: '', action: '', },
  { reportNo: '', relatedDocs: '', pending: '', action: '', },
  { reportNo: '', relatedDocs: '', pending: '', action: '', },
];


/* Violation */
export interface ViolationsData {
  refNo: string;
  type: string;
  descAr: string;
  descEn: string;
  fineAmount: string;
  dueDate: string;
  payLinks: string;
}

const ELEMENT_DATA_VIOLATION: ViolationsData[] = [
  { refNo: '001', type: 'A', descAr: 'عربی تفصیل', descEn: 'English Desc', fineAmount: '100', dueDate: '20-20-2020', payLinks: 'Click for Payment' },
  { refNo: '002', type: 'B', descAr: 'عربی تفصیل', descEn: 'English Desc', fineAmount: '-', dueDate: '-', payLinks: '' },
  { refNo: '003', type: 'C', descAr: 'عربی تفصیل', descEn: 'English Desc', fineAmount: '600', dueDate: '20-20-2020', payLinks: 'Click for Payment' },
  { refNo: '004', type: 'D', descAr: 'عربی تفصیل', descEn: 'English Desc', fineAmount: '-', dueDate: '-', payLinks: '' },
  { refNo: '005', type: 'E', descAr: 'عربی تفصیل', descEn: 'English Desc', fineAmount: '250', dueDate: '20-20-2020', payLinks: 'Click for Payment' },
];

/* Government Entity Name */
export interface EntityData {
  refNo: string;
  govtEntity: string;
  notesAr: string;
  notesEn: string;
  fineAmount: string;
  payLinks: string;
}

const ELEMENT_DATA_ENTITY: EntityData[] = [
  { refNo: '1', govtEntity: 'ABC', notesAr: 'ملاحظات عربية', notesEn: 'Arabic Notes', fineAmount: '20', payLinks: 'Click for Payment' },
  { refNo: '2', govtEntity: 'ABC', notesAr: 'ملاحظات عربية', notesEn: 'Arabic Notes', fineAmount: '-', payLinks: '-' },
  { refNo: '3', govtEntity: 'XYZ', notesAr: 'ملاحظات عربية', notesEn: 'Arabic Notes', fineAmount: '100', payLinks: 'Click for Payment' },
  { refNo: '4', govtEntity: 'XYZ', notesAr: 'ملاحظات عربية', notesEn: 'Arabic Notes', fineAmount: '-', payLinks: '-' },
  { refNo: '5', govtEntity: 'ABC', notesAr: 'ملاحظات عربية', notesEn: 'Arabic Notes', fineAmount: '300', payLinks: 'Click for Payment' },
];


@Component({
  selector: 'kt-review-renew-bl-request-detail',
  templateUrl: './review-renew-bl-request-detail.component.html',
  styleUrls: ['./review-renew-bl-request-detail.component.scss']
})
export class ReviewRenewBlRequestDetailComponent implements OnInit {
  closeResult: string; //Bootstrap Modal Popup
  partial: number = 0;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;

  displayedColumns7: string[] = ['reportNo', 'action', 'relatedDocs', 'pending',];
  dataSource7 = new MatTableDataSource<UserData>(ELEMENT_DATA);
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild('sort7', { static: true }) sort7: MatSort;


  displayedColumns8: string[] = ['refNo', 'type', 'descAr', 'descEn', 'fineAmount', 'dueDate', 'payLinks',];
  dataSource8 = new MatTableDataSource<ViolationsData>(ELEMENT_DATA_VIOLATION);
  @ViewChild('matPaginator8', { static: true }) paginator8: MatPaginator;
  @ViewChild('sort8', { static: true }) sort8: MatSort;


  displayedColumns9: string[] = ['refNo', 'govtEntity', 'notesAr', 'notesEn', 'fineAmount', 'payLinks',];
  dataSource9 = new MatTableDataSource<EntityData>(ELEMENT_DATA_ENTITY);
  @ViewChild('matPaginator9', { static: true }) paginator9: MatPaginator;
  @ViewChild('sort9', { static: true }) sort9: MatSort;


  constructor(private _formBuilder: FormBuilder, public dialog: MatDialog, private modalService: NgbModal, ) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });

    this.fourthFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });

    this.sixthFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });

    /* Data Table */

    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;

    this.dataSource8.paginator = this.paginator8;
    this.dataSource8.sort = this.sort8;

    this.dataSource9.paginator = this.paginator9;
    this.dataSource9.sort = this.sort9;

  }

  /* Checkbox Value Set to true */
  checkedBLS = true;

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);

  /* begin:: Rejected Pages Popup */
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    this.dialog.open(RejectedBusinessModalComponent, dialogConfig);

  }
  /* end:: Rejected Pages Popup */


  /* begin:: Business Modification */
  openBusinessModifications() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    // this.dialog.open(ViewSubmittedIaRequestComponent, dialogConfig);

  }
  /* end:: Business Modification */


  /* begin:: Business Modification */
  openBusinessLicenseSubmit() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    // this.dialog.open(ViewSubmittedBlRequestComponent, dialogConfig);

  }
  /* end:: Business Modification */

  /* begin:: Open Business Request */
  openSubmittedBNRequest() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);

  }
  /* end:: Open Business Request */

  /* begin:: View Actions */
  openViewActions() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewActionsComponent, dialogConfig);

  }
  /* end:: View Action */

  /* begin:: Forward for Action Popup */
  openFwdActionDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    this.dialog.open(FwdForActionComponent, dialogConfig);

  }
  /* end:: Forward for Action Popup */

  /* begin:: Request for Inspection Popup */
  openRequestActionDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    // this.dialog.open(RequestForInspectionComponent, dialogConfig);

  }
  /* end:: Request for Inspection Popup */


  /* begin:: Return Back Notes Popup */
  openReturnBackNotesDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    // this.dialog.open(ReturnBackNotesComponent, dialogConfig);

  }
  /* end:: Return Back Notes Popup */

  //..Modal Popup Start
  open(content) {

    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 


  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  applyFilter8(filterValue: string) {
    this.dataSource8.filter = filterValue.trim().toLowerCase();
    if (this.dataSource8.paginator) {
      this.dataSource8.paginator.firstPage();
    }
  }


  applyFilter9(filterValue: string) {
    this.dataSource9.filter = filterValue.trim().toLowerCase();
    if (this.dataSource9.paginator) {
      this.dataSource9.paginator.firstPage();
    }
  }


  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

  partialValue(value: number) {
    this.partial = value;
  }
}