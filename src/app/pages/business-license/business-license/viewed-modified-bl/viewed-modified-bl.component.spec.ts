import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewedModifiedBlComponent } from './viewed-modified-bl.component';

describe('ViewedModifiedBlComponent', () => {
  let component: ViewedModifiedBlComponent;
  let fixture: ComponentFixture<ViewedModifiedBlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewedModifiedBlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewedModifiedBlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
