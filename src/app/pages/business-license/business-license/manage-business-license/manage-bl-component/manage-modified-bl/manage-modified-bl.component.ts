import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl } from '@angular/forms';

/* Expired Soon */
export interface OuData {

  bnBnr: string;
  bnAr: string;
  bnEn: string;
  submissionDate: string;
  status: string;
  statusDate: string;
  expCanDate: string;
  isBNModified: string;
  editView: string;

}

const ELEMENT_OU_DATA: OuData[] = [
  { bnBnr: 'BLM-10-1', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', isBNModified: 'Yes', status: 'Modified BN', statusDate: '12/10/2019', expCanDate: '', editView: 'Rejected' },
  { bnBnr: 'BLM-12-1', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', isBNModified: 'No', status: 'Under Process', statusDate: '03/08/2019', expCanDate: '', editView: 'Cancelled' },
  { bnBnr: 'BLM-13-2', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', isBNModified: 'No', status: 'Pending at Customere', statusDate: '12/10/2019', expCanDate: '', editView: 'Expired' },
  { bnBnr: 'BLM-15-2', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', isBNModified: 'No', status: 'Pending at Manager', statusDate: '16/09/2019', expCanDate: '', editView: 'Expired' },
  { bnBnr: 'BLM-17-3', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', isBNModified: 'No', status: 'Pending at Consultant', statusDate: '01/10/2019', expCanDate: '', editView: 'Cancelled' },
];

@Component({
  selector: 'kt-manage-modified-bl',
  templateUrl: './manage-modified-bl.component.html',
  styleUrls: ['./manage-modified-bl.component.scss']
})
export class ManageModifiedBlComponent implements OnInit {

  displayedColumnsOU: string[] = ['bnBnr', 'bnAr', 'bnEn', 'submissionDate', 'status', 'statusDate', 'isBNModified', 'editView'];
  dataSourceOU = new MatTableDataSource<OuData>(ELEMENT_OU_DATA);
  @ViewChild('matPaginatorOU', { static: true }) paginatorOU: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortOU: MatSort;
  constructor() {

  }

  ngOnInit() {


    this.dataSourceOU.paginator = this.paginatorOU;
    this.dataSourceOU.sort = this.sortOU;
  }


  applyFilterOU(filterValue: string) {
    this.dataSourceOU.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceOU.paginator) {
      this.dataSourceOU.paginator.firstPage();
    }
  }

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);


}