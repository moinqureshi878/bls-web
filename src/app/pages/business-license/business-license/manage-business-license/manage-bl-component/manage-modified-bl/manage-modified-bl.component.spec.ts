import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageModifiedBlComponent } from './manage-modified-bl.component';

describe('ManageModifiedBlComponent', () => {
  let component: ManageModifiedBlComponent;
  let fixture: ComponentFixture<ManageModifiedBlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageModifiedBlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageModifiedBlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
