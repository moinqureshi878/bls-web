import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCancelledBlComponent } from './manage-cancelled-bl.component';

describe('ManageCancelledBlComponent', () => {
  let component: ManageCancelledBlComponent;
  let fixture: ComponentFixture<ManageCancelledBlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageCancelledBlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCancelledBlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
