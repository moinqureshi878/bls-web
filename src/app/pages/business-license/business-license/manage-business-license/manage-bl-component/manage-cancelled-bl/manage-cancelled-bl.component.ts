
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl } from '@angular/forms';

/* Expired Soon */
export interface OuData {

  bnBnr: string;
  bnAr: string;
  bnEn: string;
  submissionDate: string;
  status: string;
  statusDate: string;
  expCanDate: string;
  editView: string;

}

const ELEMENT_OU_DATA: OuData[] = [
  { bnBnr: 'BLC-10', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Cancelled', statusDate: '12/10/2019', expCanDate: '', editView: 'Rejected' },
  { bnBnr: 'BL-12', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Expired', statusDate: '03/08/2019', expCanDate: '', editView: 'Cancelled' },
  { bnBnr: 'BLR-13', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Rejected', statusDate: '12/10/2019', expCanDate: '', editView: 'Expired' },
  { bnBnr: 'BLC-10', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Cancelled', statusDate: '12/10/2019', expCanDate: '', editView: 'Rejected' },
  { bnBnr: 'BL-12', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Expired', statusDate: '03/08/2019', expCanDate: '', editView: 'Cancelled' },
  { bnBnr: 'BLR-13', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Rejected', statusDate: '12/10/2019', expCanDate: '', editView: 'Expired' },
  { bnBnr: 'BLC-10', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Cancelled', statusDate: '12/10/2019', expCanDate: '', editView: 'Rejected' },
  { bnBnr: 'BL-12', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Expired', statusDate: '03/08/2019', expCanDate: '', editView: 'Cancelled' },
  { bnBnr: 'BLR-13', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Rejected', statusDate: '12/10/2019', expCanDate: '', editView: 'Expired' },
  { bnBnr: 'BLC-10', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Cancelled', statusDate: '12/10/2019', expCanDate: '', editView: 'Rejected' },
  { bnBnr: 'BL-12', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Expired', statusDate: '03/08/2019', expCanDate: '', editView: 'Cancelled' },
  { bnBnr: 'BLR-13', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Rejected', statusDate: '12/10/2019', expCanDate: '', editView: 'Expired' },
];


@Component({
  selector: 'kt-manage-cancelled-bl',
  templateUrl: './manage-cancelled-bl.component.html',
  styleUrls: ['./manage-cancelled-bl.component.scss']
})
export class ManageCancelledBlComponent implements OnInit {
  closeResult: string; //Bootstrap Modal Popup

  displayedColumnsOU: string[] = ['bnBnr', 'bnAr', 'bnEn', 'submissionDate', 'status', 'statusDate', 'editView'];
  dataSourceOU = new MatTableDataSource<OuData>(ELEMENT_OU_DATA);
  @ViewChild('matPaginatorOU', { static: true }) paginatorOU: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortOU: MatSort;
  constructor(private modalService: NgbModal, ) {

  }

  ngOnInit() {


    this.dataSourceOU.paginator = this.paginatorOU;
    this.dataSourceOU.sort = this.sortOU;
  }


  applyFilterOU(filterValue: string) {
    this.dataSourceOU.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceOU.paginator) {
      this.dataSourceOU.paginator.firstPage();
    }
  }


  //..Modal Popup Start
  open(content) {
    
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 


  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);


}