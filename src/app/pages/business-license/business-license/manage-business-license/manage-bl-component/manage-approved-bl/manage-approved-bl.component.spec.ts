import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageApprovedBlComponent } from './manage-approved-bl.component';

describe('ManageApprovedBlComponent', () => {
  let component: ManageApprovedBlComponent;
  let fixture: ComponentFixture<ManageApprovedBlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageApprovedBlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageApprovedBlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
