import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageNewBlComponent } from './manage-new-bl.component';

describe('ManageNewBlComponent', () => {
  let component: ManageNewBlComponent;
  let fixture: ComponentFixture<ManageNewBlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageNewBlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageNewBlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
