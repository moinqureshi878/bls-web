import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRenewBlComponent } from './manage-renew-bl.component';

describe('ManageRenewBlComponent', () => {
  let component: ManageRenewBlComponent;
  let fixture: ComponentFixture<ManageRenewBlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageRenewBlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRenewBlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
