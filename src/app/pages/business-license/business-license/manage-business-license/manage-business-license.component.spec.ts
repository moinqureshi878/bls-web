import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageBusinessLicenseComponent } from './manage-business-license.component';

describe('ManageBusinessLicenseComponent', () => {
  let component: ManageBusinessLicenseComponent;
  let fixture: ComponentFixture<ManageBusinessLicenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageBusinessLicenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageBusinessLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
