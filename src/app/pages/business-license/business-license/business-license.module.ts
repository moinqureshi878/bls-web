import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '../../../views/partials/partials.module';
import { BusinessLicenseComponent } from './business-license.component';
import { ManageBusinessLicenseComponent } from './manage-business-license/manage-business-license.component';
import { ManageApprovedBlComponent } from './manage-business-license/manage-bl-component/manage-approved-bl/manage-approved-bl.component';
import { ManageCancelledBlComponent } from './manage-business-license/manage-bl-component/manage-cancelled-bl/manage-cancelled-bl.component';
import { ManageRenewBlComponent } from './manage-business-license/manage-bl-component/manage-renew-bl/manage-renew-bl.component';
import { ManageModifiedBlComponent } from './manage-business-license/manage-bl-component/manage-modified-bl/manage-modified-bl.component';
import { ManageNewBlComponent } from './manage-business-license/manage-bl-component/manage-new-bl/manage-new-bl.component';

import { BnCommonComponentsModule } from '../bn-common-components/bn-common-components.module';
import {
  MatSortModule,
} from '@angular/material';
import { ReviewBlRequestDetailComponent } from './review-bl-request-detail/review-bl-request-detail.component';
import { ReviewRenewBlRequestDetailComponent } from './review-renew-bl-request-detail/review-renew-bl-request-detail.component';
import { ViewedModifiedBlComponent } from './viewed-modified-bl/viewed-modified-bl.component';


@NgModule({
  entryComponents: [

  ],
  declarations: [
    BusinessLicenseComponent, ManageBusinessLicenseComponent, ManageApprovedBlComponent, ManageCancelledBlComponent, ManageRenewBlComponent, ManageModifiedBlComponent, ManageNewBlComponent, ReviewBlRequestDetailComponent, ReviewRenewBlRequestDetailComponent, ViewedModifiedBlComponent,
  ],
  imports: [
    PartialsModule,
    NgbModule.forRoot(),
    BnCommonComponentsModule,
    MatSortModule,
    RouterModule.forChild([
      {
        path: '',
        component: BusinessLicenseComponent
      },
      {
        path: 'manage-business-license',
        component: ManageBusinessLicenseComponent,
      },
      {
        path: 'review-bl-request',
        component: ReviewBlRequestDetailComponent,
      },
      {
        path: 'review-renew-bl-request-detail',
        component: ReviewRenewBlRequestDetailComponent,
      },
      {
        path: 'viewed-modified-bl',
        component: ViewedModifiedBlComponent,
      },

    ]),
  ]
})
export class BusinessLicenseModule { }