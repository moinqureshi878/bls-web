import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';
// import { RejectedBlModalComponent } from '../rejected-bl-modal/rejected-bl-modal.component';
// import { ViewActionsComponent } from '../view-actions/view-actions.component';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { FwdForActionComponent } from '../fwd-for-action/fwd-for-action.component';
// import { ReturnBackNotesComponent } from "../return-back-notes/return-back-notes.component";
// import { ViewSubmittedBnRequestComponent } from "../view-submitted-bn-request/view-submitted-bn-request.component";
// import { RequestForInspectionComponent } from '../request-for-inspection/request-for-inspection.component';
// import { ViewSubmittedBlRequestComponent } from '../view-submitted-bl-request/view-submitted-bl-request.component';
// import { ViewSubmittedIaRequestComponent } from '../view-submitted-ia-request/view-submitted-ia-request.component';

@Component({
  selector: 'kt-review-bl-request-detail',
  templateUrl: './review-bl-request-detail.component.html',
  styleUrls: ['./review-bl-request-detail.component.scss']
})
export class ReviewBlRequestDetailComponent implements OnInit {
  //Stepper active class
  stepperValue: number = 1;
  //Form Group Stepper
  firstFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  closeResult: string; //Bootstrap Modal Popup

  constructor(
    private _formBuilder: FormBuilder,
    public dialog: MatDialog,
    private modalService: NgbModal, ) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
    });

    this.fourthFormGroup = this._formBuilder.group({
    });
    this.sixthFormGroup = this._formBuilder.group({
    });

  }

  /* Checkbox Value Set to true */
  checkedBLS = true;

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);


  /* begin:: Rejected Pages Popup */
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    // this.dialog.open(RejectedBlModalComponent, dialogConfig);

  }
  /* end:: Rejected Pages Popup */

  /* begin:: Forward for Action Popup */
  openFwdActionDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    // this.dialog.open(FwdForActionComponent, dialogConfig);

  }
  /* end:: Forward for Action Popup */

  /* begin:: Request for Inspection Popup */
  openRequestActionDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    // this.dialog.open(RequestForInspectionComponent, dialogConfig);

  }
  /* end:: Request for Inspection Popup */
 /* begin:: Business Modification */
  openBusinessModifications() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    // this.dialog.open(ViewSubmittedIaRequestComponent, dialogConfig);

  }
  /* end:: Business Modification */

  
  /* begin:: Business Modification */
  openBusinessLicenseSubmit() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    // this.dialog.open(ViewSubmittedBlRequestComponent, dialogConfig);

  }
  /* end:: Business Modification */
  /* begin:: Return Back Notes Popup */
  openReturnBackNotesDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    // this.dialog.open(ReturnBackNotesComponent, dialogConfig);

  }
  /* end:: Return Back Notes Popup */

  /* begin:: View Actions */
  openViewActions() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    // this.dialog.open(ViewActionsComponent, dialogConfig);

  }
  /* end:: View Action */

  /* begin:: Open Business Request */
  openSubmittedBNRequest() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    // this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);

  }
  /* end:: Open Business Request */

  //Stepper Value
  setStepperValue(value: number) {
    this.stepperValue = value;
  }



  //..Modal Popup Start
  open(content) {
    
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 

}