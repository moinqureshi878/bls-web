import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewBlRequestDetailComponent } from './review-bl-request-detail.component';

describe('ReviewBlRequestDetailComponent', () => {
  let component: ReviewBlRequestDetailComponent;
  let fixture: ComponentFixture<ReviewBlRequestDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewBlRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewBlRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
