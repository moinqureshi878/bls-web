import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BusinessLicenseComponent } from './business-license.component';
import { PartialsModule } from 'app/views/partials/partials.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatTableExporterModule } from 'mat-table-exporter';
@NgModule({
  entryComponents: [],
  declarations: [BusinessLicenseComponent],
  exports: [],
  imports: [
    CommonModule,
    PartialsModule,
    MatTableExporterModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: BusinessLicenseComponent
      },
    ]),
  ]
})
export class BusinessLicenseModule { }
