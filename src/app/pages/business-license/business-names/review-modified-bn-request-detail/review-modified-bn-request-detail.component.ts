import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
// import { AddNewActivitiesComponent } from '../../request-business-name/model/add-new-activities/add-new-activities.component';
import { AddStakeholdersModalComponent } from '../../request-business-name/model/add-stakeholders-modal/add-stakeholders-modal.component';
import { TooltipPosition } from '@angular/material/tooltip';
import { RejectedBusinessModalComponent } from '../../bn-common-components/model/rejected-business-modal/rejected-business-modal.component';
import { ViewSubmittedBnRequestComponent } from '../../bn-common-components/model/view-submitted-bn-request/view-submitted-bn-request.component';
import { ViewActionsComponent } from '../../bn-common-components/model/view-actions/view-actions.component';
import { FwdForActionComponent } from '../../bn-common-components/model/fwd-for-action/fwd-for-action.component';
import { BnService } from 'app/core/appServices/bn.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from 'app/core/_base/layout';
export interface UserData {
  isic4: string;
  arDescription: string;
  enDescription: string;
  activityGroup: string;
  editView: string;

}

/** Builds and returns a new User. */

const ELEMENT_DATA: UserData[] = [
  { isic4: '1', arDescription: '1', enDescription: '1', activityGroup: '', editView: 'Deleted' },
  { isic4: '2', arDescription: '2', enDescription: '2', activityGroup: '', editView: 'Added' },
  { isic4: '3', arDescription: '3', enDescription: '3', activityGroup: '', editView: 'Added' }
];



export interface stakeHoldersData {
  orderNo: string;
  type: string;
  relationship: string;
  nameAr: string;
  nameEn: string;
  share: string;
  registered: string;
  editView: string;
}

/** Builds and returns a new User. */

const ELEMENT_STAKEHOLDERS_DATA: stakeHoldersData[] = [
  { orderNo: '1', type: 'Individual', relationship: 'Owner', nameAr: ' Name Ar', nameEn: ' Name En', share: '25%', registered: 'Yes', editView: 'Added' },
  { orderNo: '2', type: 'Govt Entity', relationship: 'Partner', nameAr: ' Name Ar', nameEn: ' Name En', share: '25%', registered: 'No', editView: 'Deleted' },
  { orderNo: '3', type: 'License', relationship: 'Responsible Manager', nameAr: ' Name Ar', nameEn: ' Name En', share: '25%', registered: 'Yes', editView: 'Added' },


];


@Component({
  selector: 'kt-review-modified-bn-request-detail',
  templateUrl: './review-modified-bn-request-detail.component.html',
  styleUrls: ['./review-modified-bn-request-detail.component.scss']
})
export class ReviewModifiedBnRequestDetailComponent implements OnInit {
  closeResult: string; //Bootstrap Modal Popup
  panelOpenState = false;
  radioButtonCondition: number = 0;
  emCopy: string = "ID front side";
  embCopy: string = "ID back side";
  passportCopy: string = "Passport info pages";
  visaCopy: string = "Visa page";
  //Stepper active class
  stepperValue: number = 1;

  //Form Group Stepper
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  seventhFormGroup: FormGroup;
  eighthFormGroup: FormGroup;
  /* Checkbox Value Set to true */
  checkedBLS = true;

  displayedColumns7: string[] = ['isic4', 'arDescription', 'enDescription', 'activityGroup', 'editView'];
  dataSource7 = new MatTableDataSource<UserData>(ELEMENT_DATA);
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild('sort7', { static: true }) sort7: MatSort;

  stakeHoldersdisplayedColumns7: string[] = ['orderNo', 'type', 'relationship', 'nameAr', 'nameEn', 'share', 'registered', 'editView'];
  stakeHoldersdataSource7 = new MatTableDataSource<stakeHoldersData>(ELEMENT_STAKEHOLDERS_DATA);
  @ViewChild('stakeHoldersmatPaginator7', { static: true }) stakeHolderspaginator7: MatPaginator;
  @ViewChild('stakeHolderssort7', { static: true }) stakeHolderssort7: MatSort;

  bnInfoloading: boolean = false;
  constructor(public bnService: BnService,
    private cdr: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    public dialog: MatDialog,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private toast: ToastrService,
    // public businessLicenseService: BusinessLicenseService,
    private translationService: TranslationService,
    private router: Router) { }

  ngOnInit() {

    this.firstFormGroup = this._formBuilder.group({
      // firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      // secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      // thirdCtrl: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      // fourthCtrl: ['', Validators.required]
    });
    this.fifthFormGroup = this._formBuilder.group({
      // fifthCtrl: ['', Validators.required]
    });
    this.sixthFormGroup = this._formBuilder.group({
      // sixthCtrl: ['', Validators.required]
    });

    this.seventhFormGroup = this._formBuilder.group({
      // sixthCtrl: ['', Validators.required]
    });
    this.eighthFormGroup = this._formBuilder.group({
      // sixthCtrl: ['', Validators.required]
    });

    /* Data Table */
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;

    /* Data Table */
    this.stakeHoldersdataSource7.paginator = this.stakeHolderspaginator7;
    this.stakeHoldersdataSource7.sort = this.stakeHolderssort7;
  }

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);

  /* Emirates ID Mask */
  emiratesIDMask = [/[0-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/]



  getEmCopy(event) {
    this.emCopy = event.target.files[0].name.substring(0, 15);
    // + "." + event.target.files[0].name.split('.')[1]
  }
  getEmbId(event) {
    this.embCopy = event.target.files[0].name.substring(0, 15);
  }
  getPassportCopy(event) {
    this.passportCopy = event.target.files[0].name.substring(0, 15);
  }
  getVisa(event) {
    this.visaCopy = event.target.files[0].name.substring(0, 15);
  }


  //Stepper Value
  setStepperValue(value: number) {
    this.stepperValue = value;
  }
  //Radio Button Condtion
  setRadioButtonCondition(value: number) {
    this.radioButtonCondition = value;
  }

  //..Modal Popup Start
  open(content) {
    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();

    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  stakeHoldersapplyFilter7(filterValue: string) {
    this.stakeHoldersdataSource7.filter = filterValue.trim().toLowerCase();

    if (this.stakeHoldersdataSource7.paginator) {
      this.stakeHoldersdataSource7.paginator.firstPage();
    }
  }

  /* begin:: Add Activity */
  openAddNewActivities() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "60%";
    // this.dialog.open(AddNewActivitiesComponent, dialogConfig);

  }
  /* end:: Add Activity */

  /* begin:: Display ExtendGrid */
  openBusinessModifications() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);

  }
  /* end:: Display ExtendGrid */

  /* begin:: View Actions */
  openViewActions() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewActionsComponent, dialogConfig);

  }
  /* end:: View Action */

  /* begin:: Forward for Action Popup */
  openFwdActionDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    this.dialog.open(FwdForActionComponent, dialogConfig);

  }
  /* end:: Forward for Action Popup */

  /* begin:: Rejected Pages Popup */
  openDialog(...dialogName) {
    const dialogConfig = new MatDialogConfig();
    let dialogRef;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    if (dialogName[0] == 'viewBNInfo') {
      debugger
      this.bnInfoloading = true;
      this.bnService.GetBNRequest(6).subscribe(res => {
        this.bnInfoloading = false;
        this.cdr.markForCheck();
        let response = res;
        if (response.code == 200 || response.result.code == 200) {
          let bnData = response.data;
          dialogConfig.data = { bnData };
          this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);
        }
      });
    }
  }
  /* end:: Rejected Pages Popup */

  /* begin:: Add StakeHolders */
  openAddStakeHolders() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "60%";
    dialogConfig.maxWidth = "60%";
    this.dialog.open(AddStakeholdersModalComponent, dialogConfig);

  }
  /* end:: Add StakeHolders */

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

}