import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewModifiedBnRequestDetailComponent } from './review-modified-bn-request-detail.component';

describe('ReviewModifiedBnRequestDetailComponent', () => {
  let component: ReviewModifiedBnRequestDetailComponent;
  let fixture: ComponentFixture<ReviewModifiedBnRequestDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewModifiedBnRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewModifiedBnRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
