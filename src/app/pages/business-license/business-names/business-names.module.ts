import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '../../../views/partials/partials.module';
import { BusinessNamesComponent } from './business-names.component';
// import { NotesHistoryModalComponent } from '../bn-common-components/notes-history-modal/notes-history-modal.component';
import { ManageBusinessNamesComponent } from './manage-business-names/manage-business-names.component';
import { ManageNewBnComponent } from './manage-business-names/manage-bn-component/manage-new-bn/manage-new-bn.component';
import { ManageExtendBnComponent } from './manage-business-names/manage-bn-component/manage-extend-bn/manage-extend-bn.component';
import { ManageModifiedBnComponent } from './manage-business-names/manage-bn-component/manage-modified-bn/manage-modified-bn.component';
import { ManageCancelledBnComponent } from './manage-business-names/manage-bn-component/manage-cancelled-bn/manage-cancelled-bn.component';
import { ManageApprovedBnComponent } from './manage-business-names/manage-bn-component/manage-approved-bn/manage-approved-bn.component';
import { ReviewNewBnRequestDetailComponent } from './review-bn-request-detail/review-bn-request-detail.component';
import { ReviewExtendBnRequestDetailComponent } from './review-extend-bn-request-detail/review-extend-bn-request-detail.component';
import { ReviewModifiedBnRequestDetailComponent } from './review-modified-bn-request-detail/review-modified-bn-request-detail.component';
// import { ViewSubmittedBnRequestComponent } from '../bn-common-components/model/view-submitted-bn-request/view-submitted-bn-request.component';
// import { ViewActionsComponent } from '../bn-common-components/model/view-actions/view-actions.component';
// import { ViewedModifiedCustomerBnComponent } from '../request-business-name/viewed-modified-customer-bn/viewed-modified-customer-bn.component';
// import { FwdForActionComponent } from '../bn-common-components/model/fwd-for-action/fwd-for-action.component';
// import { ReturnBackNotesComponent } from '../bn-common-components/model/return-back-notes/return-back-notes.component';
// import { RejectedBusinessModalComponent } from '../bn-common-components/model/rejected-business-modal/rejected-business-modal.component';
// import { ReviewAttachmentModalComponent } from '../bn-common-components/model/review-attachment-modal/review-attachment-modal.component';
// import { FilterBlComponent } from '../bn-common-components/filter-bl/filter-bl.component';
import { MatTableExporterModule } from 'mat-table-exporter';
import { AuthGuard } from 'app/core/auth';
import { BnResolver } from 'app/core/resolvers/bn.resolver';
import { BnCommonComponentsModule } from '../bn-common-components/bn-common-components.module';
import {
  MatSortModule,
} from '@angular/material';

@NgModule({
  entryComponents: [

  ],
  declarations: [
    BusinessNamesComponent,
    ManageBusinessNamesComponent,
    ManageNewBnComponent,
    ManageExtendBnComponent,
    ManageModifiedBnComponent,
    ManageCancelledBnComponent,
    ManageApprovedBnComponent,
    ReviewNewBnRequestDetailComponent,
    ReviewExtendBnRequestDetailComponent,
    ReviewModifiedBnRequestDetailComponent,

  ],
  imports: [
    PartialsModule,
    MatTableExporterModule,
    NgbModule.forRoot(),
    BnCommonComponentsModule,
    MatSortModule,
    RouterModule.forChild([
      {
        path: '',
        component: BusinessNamesComponent
      },
      {
        path: 'manage-business-names',
        component: ManageBusinessNamesComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'review-bn-request-detail/:id',
        component: ReviewNewBnRequestDetailComponent,
        canActivate: [AuthGuard],
        resolve: { data: BnResolver }
      },
      {
        path: 'review-extend-bn-request-detail',
        component: ReviewExtendBnRequestDetailComponent
      },
      {
        path: 'review-modified-bn-request-detail/:id',
        component: ReviewModifiedBnRequestDetailComponent
      },

    ]),
  ]
})
export class BusinessNamesModule { }
