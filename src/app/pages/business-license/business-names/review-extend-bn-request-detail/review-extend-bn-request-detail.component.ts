import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
import { RejectedBusinessModalComponent } from '../../bn-common-components/model/rejected-business-modal/rejected-business-modal.component';
import { ViewActionsComponent } from '../../bn-common-components/model/view-actions/view-actions.component';
import { ViewSubmittedBnRequestComponent } from '../../bn-common-components/model/view-submitted-bn-request/view-submitted-bn-request.component';
import { FwdForActionComponent } from "../../bn-common-components/model/fwd-for-action/fwd-for-action.component";
import { BnService } from 'app/core/appServices/bn.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from 'app/core/_base/layout';

@Component({
  selector: 'kt-review-extend-bn-request-detail',
  templateUrl: './review-extend-bn-request-detail.component.html',
  styleUrls: ['./review-extend-bn-request-detail.component.scss']
})
export class ReviewExtendBnRequestDetailComponent implements OnInit {

  bnInfoloading: boolean = false;
  constructor(public bnService: BnService,
    private cdr: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    public dialog: MatDialog,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private toast: ToastrService,
    // public businessLicenseService: BusinessLicenseService,
    private translationService: TranslationService,
    private router: Router) { }

  ngOnInit() {

  }

  /* Checkbox Value Set to true */
  checkedBLS = true;

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);

  /* begin:: Rejected Pages Popup */
  // openDialog() {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.minWidth = "30%";
  //   this.dialog.open(RejectedBusinessModalComponent, dialogConfig);

  // }
  /* end:: Rejected Pages Popup */

  /* begin:: Forward for Action Popup */
  openFwdActionDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    this.dialog.open(FwdForActionComponent, dialogConfig);

  }
  /* end:: Forward for Action Popup */

  /* begin:: Business Modification */
  openDialog(...dialogName) {
    const dialogConfig = new MatDialogConfig();
    let dialogRef;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    if (dialogName[0] == 'viewBNInfo') {
      debugger
      this.bnInfoloading = true;
      this.bnService.GetBNRequest(6).subscribe(res => {
        this.bnInfoloading = false;
        this.cdr.markForCheck();
        let response = res;
        if (response.code == 200 || response.result.code == 200) {
          let bnData = response.data;
          dialogConfig.data = { bnData };
          this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);
        }
      });
    }
  }
  /* end:: Business Modification */

  /* begin:: View Actions */
  openViewActions() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewActionsComponent, dialogConfig);

  }
  /* end:: View Action */

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);
}