import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewExtendBnRequestDetailComponent } from './review-extend-bn-request-detail.component';

describe('ReviewExtendBnRequestDetailComponent', () => {
  let component: ReviewExtendBnRequestDetailComponent;
  let fixture: ComponentFixture<ReviewExtendBnRequestDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewExtendBnRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewExtendBnRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
