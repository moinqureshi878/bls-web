import { Component, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { ManageNewBnComponent } from './manage-bn-component/manage-new-bn/manage-new-bn.component';
import { FilterBlComponent } from '../../bn-common-components/filter-bl/filter-bl.component';
import { BnService } from 'app/core/appServices/bn.service';
import { TranslationService } from 'app/core/_base/layout';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'kt-manage-business-names',
	templateUrl: './manage-business-names.component.html',
	styleUrls: ['./manage-business-names.component.scss']
})
export class ManageBusinessNamesComponent implements AfterViewInit {
	tabIndex: number = 0;

	blData: any;
	@ViewChild(ManageNewBnComponent, { read: ManageNewBnComponent, static: false }) manageNewBnComponent: ManageNewBnComponent;
	@ViewChild(FilterBlComponent, { read: FilterBlComponent, static: false }) filterBlComponent: FilterBlComponent;
	constructor(public bnService: BnService,
		private toast: ToastrService,
		private cdr: ChangeDetectorRef,
		private translationService: TranslationService) {

		this.GetAllBNRequest(0);
	}
	ngOnInit() {

	}
	ngAfterViewInit() {

	}
	tabChange(index) {
		this.tabIndex = index;
		if (this.tabIndex == 0) {
			this.filterBlComponent.resetForm();
			this.GetAllBNRequest(index);
		}
		else if (this.tabIndex == 1) {
			this.filterBlComponent.resetForm();
			// this.getUserData(3);
		}
		else if (this.tabIndex == 2) {
			this.filterBlComponent.resetForm();
			this.GetAllBNRequest(index);
		}
		else if (this.tabIndex == 3) {
			this.filterBlComponent.resetForm();
			// this.getUserData(2);
		}
		else if (this.tabIndex == 4) {
			this.filterBlComponent.resetForm();
			this.GetAllBNRequest(index);
		}
	}
	getFilters(filterObj) {
		debugger
		if (typeof filterObj == 'string' && filterObj == 'resetForm') {
			// Filter API
			this.GetAllBNRequest(this.tabIndex);
		}
		else if (typeof filterObj == 'object') {
			setTimeout(() => {
				this.blData = this.blData + 1;
				this.filterBlComponent.closeLoader();
			}, 1000);
		}
		console.log(filterObj);
	}
	GetAllBNRequest(tabId) {
		this.bnService.GetAllBNRequest(tabId + 1).subscribe(data => {
			let response = data;
			if ((response.result && response.result.code == 200) || response.code == 200) {
				response.data.tabIndex = tabId;
				this.blData = response.data;
				this.filterBlComponent.closeLoader();
				this.cdr.markForCheck();
			}
			else {
				this.translationService.getTranslation(String(response.result ? response.result.code : response.code)).subscribe((text: string) => {
					this.toast.error(text);
				});
			}
		},
			error => {
				this.cdr.markForCheck();
			});
	}

	// https://stackoverflow.com/questions/35940984/angular2-call-function-of-parent-component
	// https://stackblitz.com/edit/angular-ad4kf3?file=src%2Fapp%2Fchild%2Fgrandchild%2Fgrandchild.component.ts
}
