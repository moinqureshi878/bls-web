import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageBusinessNamesComponent } from './manage-business-names.component';

describe('ManageBusinessNamesComponent', () => {
  let component: ManageBusinessNamesComponent;
  let fixture: ComponentFixture<ManageBusinessNamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageBusinessNamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageBusinessNamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
