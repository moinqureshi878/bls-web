import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCancelledBnComponent } from './manage-cancelled-bn.component';

describe('ManageCancelledBnComponent', () => {
  let component: ManageCancelledBnComponent;
  let fixture: ComponentFixture<ManageCancelledBnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageCancelledBnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCancelledBnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
