
import { Component, OnInit, ViewChild, Input, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl } from '@angular/forms';
import { TranslationService } from 'app/core/_base/layout';

@Component({
  selector: 'kt-manage-approved-bn',
  templateUrl: './manage-approved-bn.component.html',
  styleUrls: ['./manage-approved-bn.component.scss']
})
export class ManageApprovedBnComponent implements OnInit {
  closeResult: string; //Bootstrap Modal Popup

  displayedColumnsOU: string[] = ['bnBnr', 'bnAr', 'bnEn', 'submissionDate', 'status', 'statusDate', 'daysLeft', 'editView'];
  dataSourceOU = new MatTableDataSource();
  @ViewChild('matPaginatorOU', { static: true }) paginatorOU: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortOU: MatSort;


  @Input('blData') blData: any;
  constructor(private modalService: NgbModal, private translationService: TranslationService,
    private cdr: ChangeDetectorRef) {
  }
  ngOnChanges() {
    if (this.blData && this.blData.tabIndex == 4) {
      this.dataSourceOU = new MatTableDataSource(this.blData ? this.blData : []);
      this.dataSourceOU.paginator = this.paginatorOU;
      this.dataSourceOU.sort = this.sortOU;
      this.cdr.markForCheck();
    }
  }
  ngOnInit() {
    this.dataSourceOU.paginator = this.paginatorOU;
    this.dataSourceOU.sort = this.sortOU;
  }

  applyFilterOU(filterValue: string) {
    this.dataSourceOU.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceOU.paginator) {
      this.dataSourceOU.paginator.firstPage();
    }
  }
  printPDF() {
    const objFra: HTMLIFrameElement = document.getElementById('pdfFile') as HTMLIFrameElement;
    // objFra.contentWindow.focus();
    if (objFra) {
      objFra.contentWindow.print();
    }
  }
  //..Modal Popup Start
  open(content) {
    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 


  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);


}