import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageApprovedBnComponent } from './manage-approved-bn.component';

describe('ManageApprovedBnComponent', () => {
  let component: ManageApprovedBnComponent;
  let fixture: ComponentFixture<ManageApprovedBnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageApprovedBnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageApprovedBnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
