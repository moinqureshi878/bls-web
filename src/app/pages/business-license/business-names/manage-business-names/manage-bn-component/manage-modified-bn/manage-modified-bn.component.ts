
import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl } from '@angular/forms';
import { TranslationService } from 'app/core/_base/layout';

@Component({
  selector: 'kt-manage-modified-bn',
  templateUrl: './manage-modified-bn.component.html',
  styleUrls: ['./manage-modified-bn.component.scss']
})
export class ManageModifiedBnComponent implements OnInit {

  displayedColumnsOU: string[] = ['bnBnr', 'bnAr', 'bnEn', 'submissionDate', 'status', 'statusDate', 'daysLeft', 'editView'];
  dataSourceOU = new MatTableDataSource();
  @ViewChild('matPaginatorOU', { static: true }) paginatorOU: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortOU: MatSort;

  @Input('blData') blData: any;
  constructor(private translationService: TranslationService) {

  }

  ngOnInit() {
    this.dataSourceOU.paginator = this.paginatorOU;
    this.dataSourceOU.sort = this.sortOU;
  }
  ngOnChanges() {
    // debugger
    if (this.blData && this.blData.tabIndex == 2) {
      this.dataSourceOU = new MatTableDataSource(this.blData ? this.blData : []);
      this.dataSourceOU.paginator = this.paginatorOU;
      this.dataSourceOU.sort = this.sortOU;
    }
  }
  applyFilterOU(filterValue: string) {
    this.dataSourceOU.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceOU.paginator) {
      this.dataSourceOU.paginator.firstPage();
    }
  }

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);


}