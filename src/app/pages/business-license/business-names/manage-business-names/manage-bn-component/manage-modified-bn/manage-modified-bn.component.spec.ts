import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageModifiedBnComponent } from './manage-modified-bn.component';

describe('ManageModifiedBnComponent', () => {
  let component: ManageModifiedBnComponent;
  let fixture: ComponentFixture<ManageModifiedBnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageModifiedBnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageModifiedBnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
