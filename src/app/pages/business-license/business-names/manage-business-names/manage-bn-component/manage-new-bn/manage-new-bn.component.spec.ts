import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageNewBnComponent } from './manage-new-bn.component';

describe('ManageNewBnComponent', () => {
  let component: ManageNewBnComponent;
  let fixture: ComponentFixture<ManageNewBnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageNewBnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageNewBnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
