
import { Component, OnInit, ViewChild, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl } from '@angular/forms';
import { TranslationService } from 'app/core/_base/layout';

@Component({
  selector: 'kt-manage-new-bn',
  templateUrl: './manage-new-bn.component.html',
  styleUrls: ['./manage-new-bn.component.scss']
})
export class ManageNewBnComponent implements OnInit {

  displayedColumnsOU: string[] = ['bnBnr', 'bnAr', 'bnEn', 'submissionDate', 'status', 'statusDate', 'editView'];
  dataSourceOU = new MatTableDataSource();
  @ViewChild('matPaginatorOU', { static: true }) paginatorOU: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortOU: MatSort;

  @Input('blData') blData: any;
  constructor(private translationService: TranslationService,
    private cdr: ChangeDetectorRef) {

  }

  ngOnInit() {
    this.dataSourceOU.paginator = this.paginatorOU;
    this.dataSourceOU.sort = this.sortOU;
  }

  ngOnChanges() {
    if (this.blData && this.blData.tabIndex == 0) {
      this.dataSourceOU = new MatTableDataSource(this.blData ? this.blData : []);
      this.dataSourceOU.paginator = this.paginatorOU;
      this.dataSourceOU.sort = this.sortOU;
      this.cdr.markForCheck();
    }
  }
  applyFilterOU(filterValue: string) {
    this.dataSourceOU.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceOU.paginator) {
      this.dataSourceOU.paginator.firstPage();
    }
  }

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

}
