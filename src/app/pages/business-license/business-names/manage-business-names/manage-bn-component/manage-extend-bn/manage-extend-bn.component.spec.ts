import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageExtendBnComponent } from './manage-extend-bn.component';

describe('ManageExtendBnComponent', () => {
  let component: ManageExtendBnComponent;
  let fixture: ComponentFixture<ManageExtendBnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageExtendBnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageExtendBnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
