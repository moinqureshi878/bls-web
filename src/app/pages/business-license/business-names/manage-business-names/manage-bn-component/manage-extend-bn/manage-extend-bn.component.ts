
import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl } from '@angular/forms';

const ELEMENT_OU_DATA = [
  { bnBnr: 'BNE-10-1', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', extendedDays: '10', status: 'Extended BN', statusDate: '12/10/2019', expCanDate: '', editView: '' },
  { bnBnr: 'BNE-12-1', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', extendedDays: '12', status: 'Under Process', statusDate: '03/08/2019', expCanDate: '', editView: '' },
  { bnBnr: 'BNE-13-2', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', extendedDays: '15', status: 'Pending at Customere', statusDate: '12/10/2019', expCanDate: '', editView: '' },
  { bnBnr: 'BNE-15-2', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', extendedDays: '10', status: 'Pending at Manager', statusDate: '16/09/2019', expCanDate: '', editView: '' },
  { bnBnr: 'BNE-17-3', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', extendedDays: '25', status: 'Pending at Consultant', statusDate: '01/10/2019', expCanDate: '', editView: '' },

];
@Component({
  selector: 'kt-manage-extend-bn',
  templateUrl: './manage-extend-bn.component.html',
  styleUrls: ['./manage-extend-bn.component.scss']
})
export class ManageExtendBnComponent implements OnInit {

  displayedColumnsOU: string[] = ['bnBnr', 'bnAr', 'bnEn', 'submissionDate', 'status', 'statusDate', 'extendedDays', 'editView'];
  dataSourceOU = new MatTableDataSource(ELEMENT_OU_DATA);
  @ViewChild('matPaginatorOU', { static: true }) paginatorOU: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortOU: MatSort;

  @Input() blData: any;
  constructor() {

  }

  ngOnInit() {
    this.dataSourceOU.paginator = this.paginatorOU;
    this.dataSourceOU.sort = this.sortOU;
  }
  ngOnChanges() {
    // debugger
    // this.dataSourceOU = new MatTableDataSource(this.blData);
    // this.dataSourceOU.paginator = this.paginatorOU;
    // this.dataSourceOU.sort = this.sortOU;
  }

  applyFilterOU(filterValue: string) {
    this.dataSourceOU.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceOU.paginator) {
      this.dataSourceOU.paginator.firstPage();
    }
  }

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

}

