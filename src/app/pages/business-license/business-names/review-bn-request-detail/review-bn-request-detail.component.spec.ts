import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewNewBnRequestDetailComponent } from './review-bn-request-detail.component';

describe('ReviewNewBnRequestDetailComponent', () => {
  let component: ReviewNewBnRequestDetailComponent;
  let fixture: ComponentFixture<ReviewNewBnRequestDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewNewBnRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewNewBnRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
