import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatTableDataSource } from '@angular/material';
import { RejectedBusinessModalComponent } from '../../bn-common-components/model/rejected-business-modal/rejected-business-modal.component';
import { ReviewAttachmentModalComponent } from '../../bn-common-components/model/review-attachment-modal/review-attachment-modal.component';
import { ViewActionsComponent } from '../../bn-common-components/model/view-actions/view-actions.component';
import { FwdForActionComponent } from '../../bn-common-components/model/fwd-for-action/fwd-for-action.component';
import { ReturnBackNotesComponent } from "../../bn-common-components/model/return-back-notes/return-back-notes.component";
import { emiratesIDMasking, phoneNumberMasking } from 'app/core/validator/form-validators.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BusinessLicenseService } from 'app/core/appServices/business-license/business-license.service';
import { TranslationService } from 'app/core/_base/layout';
import { WorkFlowCode } from "../../../../core/_enum/workflowCodes.enum";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BnService } from 'app/core/appServices/bn.service';
import { ToastrService } from 'ngx-toastr';
import { JwtTokenService } from 'app/core/appServices/index.service';
import * as jwt_decodes from 'jwt-decode';
import { CommonEnum } from 'app/core/_enum/index.enum';
import { WorkflowStatusEnum } from 'app/core/_enum/workflowStatus.enum';

@Component({
  selector: 'kt-review-bn-request-detail',
  templateUrl: './review-bn-request-detail.component.html',
  styleUrls: ['./review-bn-request-detail.component.scss']
})
export class ReviewNewBnRequestDetailComponent implements OnInit {
  //Stepper active class
  stepperValue: number = 1;

  //Form Group Stepper
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;

  stickyNotes: any;
  /* Emirates ID Mask */
  emiratesIDMask = emiratesIDMasking();
  /* Mobile No Mask */
  mobileNoMask = phoneNumberMasking();
  workFlowCode = WorkFlowCode;
  getBNData: any;
  postJson = {
    "workFlow": {
      "code": 0,
      "userId": 0,
      "designation": 0,
      "remarks": "",
      "reasonIds": [0]
    },
    "stepNo": 0,
    "applicationRefNo": "",
    "bnid": 0,
    "stickyNotes": "",
    "applicationDataControls": [],
    "businessActivity": [],
    "businessNamesControls": {
      "id": 0,
      "enId": 0,
      "enChecked": false,
      "arId": 0,
      "arChecked": false
    },
    "shortNames": [],
    "stakeHolders": [],
    "attachments": []
  }

  businessNamesColumns: string[] = ['action', 'BNNameEn', 'BNNameAr'];
  businessNamesGridDataSource = new MatTableDataSource();
  businessNameRadioBtnId: number;
  isDisabledPaymentBtn: boolean = true;
  commonEnum = CommonEnum;
  workflowStatusEnum = WorkflowStatusEnum;
  constructor(private _formBuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    public bnService: BnService,
    private toast: ToastrService,
    public businessLicenseService: BusinessLicenseService,
    private translationService: TranslationService,
    private jwtTokenService: JwtTokenService,
    private router: Router) {
    let BNData = this.route.snapshot.data.data;
    BNData.applicationData.isHide = true;
    BNData.businessActivity ? BNData.businessActivity.isHide = false : null;
    BNData.businessName.isHide = false;
    BNData.stakeHolders.isHide = false;
    BNData.attachments.isHide = true;

    BNData.isManager = this.getUserRoles().some(role => role.Id == this.commonEnum.managerRoleId);
    BNData.isConsultant = this.getUserRoles().some(role => role.Id == this.commonEnum.consultantRoleId);
    BNData.requestStatusId ? BNData.requestStatusId : BNData.requestStatusId=0;
    BNData.isHideActions = !(this.workflowStatusEnum.ReturnedToCustomerWithFeedback == BNData.requestStatusId ||
      this.workflowStatusEnum.SentToonsultant == BNData.requestStatusId ||
      this.workflowStatusEnum.SentToManger == BNData.requestStatusId);

    this.getBNData = BNData;

    this.postJson.applicationRefNo = this.getBNData.applicationRefNo;
    this.postJson.bnid = this.getBNData.bnid;
  }

  private getUserRoles() {
    let userSession = JSON.parse(localStorage.getItem("userSession"));
    let currentSessionToken: string;
    if (userSession && userSession.length > 1) {
      currentSessionToken = userSession[1].token;
    }
    else {
      currentSessionToken = userSession[0].token;
    }
    return JSON.parse(jwt_decodes(currentSessionToken).UserRoles);
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({});
    this.initializeApplicationInfoForm();
    this.thirdFormGroup = this._formBuilder.group({});
    this.initializeBusinessNamesForm();
    this.fifthFormGroup = this._formBuilder.group({});
    this.sixthFormGroup = this._formBuilder.group({});

    this.applicationInfoFormSubmit();
    this.businessActivityFormSubmit();
    this.businessNamesFormSubmit();
    this.stakeholderInfoFormSubmit();
    this.updateAttachmentsData(undefined);
  }
  initializeApplicationInfoForm() {
    this.secondFormGroup = this._formBuilder.group({
      appInfoStatus: this._formBuilder.array([]),
    });
    let control = <FormArray>this.secondFormGroup.controls.appInfoStatus;
    this.getBNData.applicationDataControls.forEach(x => {
      control.push(this._formBuilder.group({
        id: [x.id],
        controlName: [x.controlName],
        rowType: [x.rowType],
        isChecked: [x.isChecked],
      }))
    });
  }
  initializeBusinessNamesForm() {
    this.getBNData.businessName.businessNames = this.getBNData.businessName.businessNames.map((v, index) => ({ ...v, enId: this.getEnData('enId', index), enChecked: false, arId: this.getArData('arId', index), arChecked: false, fieldEn: this.getEnData('fieldEn', index), fieldAr: this.getArData('fieldAr', index) }));
    this.businessNamesGridDataSource = new MatTableDataSource(this.getBNData.businessName.businessNames);
    this.fourthFormGroup = this._formBuilder.group({
      businesNamesStatus: this._formBuilder.array([]),
      shortNamesStatus: this._formBuilder.array([]),
    });
    console.log(this.fourthFormGroup)
    // let control = <FormArray>this.fourthFormGroup.controls.businesNamesStatus;
    // this.getBNData.businessName.businessNames.forEach((x, index) => {
    //   debugger
    //   control.push(this._formBuilder.group({
    //     id: [x.id],
    //     // controlName: [x.controlName],
    //     // rowType: [x.rowType],
    //     isChecked: [x.isChecked],
    //     controlId: [this.businessNameId(index)]
    //   }));
    // });
    // this.getBNData.businessNamesControls.forEach(x => {
    //   control.push(this._formBuilder.group({
    //     id: [x.id],
    //     controlName: [x.controlName],
    //     rowType: [x.rowType],
    //     isChecked: [x.isChecked],
    //     controlId: []
    //   }))
    // });

    let controlShortName = <FormArray>this.fourthFormGroup.controls.shortNamesStatus;
    this.getBNData.shortNamesControls.forEach(x => {
      controlShortName.push(this._formBuilder.group({
        id: [x.id],
        controlName: [x.controlName],
        rowType: [x.rowType],
        isChecked: [x.isChecked],
      }))
    });
  }
  getEnData(key: string, index: any) {
    if (key == 'enId') {
      if (index == 0) {
        return this.getBNData.businessNamesControls[0].id;
      }
      else if (index == 1) {
        return this.getBNData.businessNamesControls[2].id;
      }
      else if (index == 2) {
        return this.getBNData.businessNamesControls[4].id;
      }
    }
    else {
      if (index == 0) {
        return this.getBNData.businessNamesControls[0].fieldEn;
      }
      else if (index == 1) {
        return this.getBNData.businessNamesControls[2].fieldEn;
      }
      else if (index == 2) {
        return this.getBNData.businessNamesControls[4].fieldEn;
      }
    }
  }
  getArData(key: string, index: any) {
    if (key == 'arId') {
      if (index == 0) {
        return this.getBNData.businessNamesControls[1].id;
      }
      else if (index == 1) {
        return this.getBNData.businessNamesControls[3].id;
      }
      else if (index == 2) {
        return this.getBNData.businessNamesControls[5].id;
      }
    }
    else {
      if (index == 0) {
        return this.getBNData.businessNamesControls[1].fieldAr;
      }
      else if (index == 1) {
        return this.getBNData.businessNamesControls[3].fieldAr;
      }
      else if (index == 2) {
        return this.getBNData.businessNamesControls[5].fieldAr;
      }
    }

  }
  businessNameRadioAndCheckboxChecked(elementName, key, id) {
    this.getBNData.businessName.businessNames
    if (elementName == 'checkbox') {
      let index = this.getBNData.businessName.businessNames.findIndex(x => x.id == id);
      let isActive: boolean = this.getBNData.businessName.businessNames[index][key] == true ? false : true;
      this.getBNData.businessName.businessNames[index][key] = isActive;
    }
    if (elementName == 'radio') {
      this.businessNameRadioBtnId = id;
      this.getBNData.businessName.businessNames.forEach(v => { v.enChecked = false; v.arChecked = false });
      this.getBNData.businessName.businessNames.id = this.businessNameRadioBtnId;
    }
    this.postJson.businessNamesControls = this.businessNameRadioBtnId ? (({ id, enId, enChecked, arId, arChecked }) => ({ id, enId, enChecked, arId, arChecked }))(this.getBNData.businessName.businessNames.find(f => f.id == this.businessNameRadioBtnId)) : this.postJson.businessNamesControls;
  }
  updateBusinessActivityData(updatedBusinessActivity) {
    this.getBNData.businessActivity = updatedBusinessActivity;
    this.postJson.businessActivity = this.getBNData.businessActivity.filter(res => res.isChecked == false).map(({ id, rowType }) => ({ id, rowType }));
  }
  // updateBusinessNamesValue(index) {
  //   const controlArray = <FormArray>this.fourthFormGroup.get('businesNamesStatus');

  //   let chkCheked = controlArray.value.find((res, i) => res.isChecked == true && i == index) ? controlArray.value.find((res, i) => res.isChecked == true && i == index).isChecked : false;
  //   controlArray.value.forEach((element, i) => {
  //     controlArray.controls[i].get('isChecked').setValue(false);
  //   });
  //   controlArray.controls[index].get('isChecked').setValue(chkCheked);

  //   if (index == 0 || index == 2 || index == 4) {
  //     if (controlArray.controls[index].get('isChecked').value) {
  //       controlArray.controls[index + 1].get('isChecked').setValue(true);
  //     }
  //     else {
  //       controlArray.controls[index + 1].get('isChecked').setValue(false);
  //     }
  //   }
  //   else {
  //     if (controlArray.controls[index].get('isChecked').value) {
  //       controlArray.controls[index - 1].get('isChecked').setValue(true);
  //     }
  //     else {
  //       controlArray.controls[index - 1].get('isChecked').setValue(false);
  //     }
  //   }
  // }

  updateShortNamesValue(index) {
    // const controlArray = <FormArray>this.fourthFormGroup.get('shortNamesStatus');
    // debugger
    // let chkCheked = controlArray.value.find((res, i) => res.isChecked == true && i == index) ? controlArray.value.find((res, i) => res.isChecked == true && i == index).isChecked : false;
    // if (index == 0) {
    //   if (chkCheked) {
    //     controlArray.controls[index + 1].get('isChecked').setValue(true);
    //   }
    //   else {
    //     controlArray.controls[index + 1].get('isChecked').setValue(false);
    //   }
    // }
    // else {
    //   if (chkCheked) {
    //     controlArray.controls[index - 1].get('isChecked').setValue(true);
    //   }
    //   else {
    //     controlArray.controls[index - 1].get('isChecked').setValue(false);
    //   }
    // }
    this.postJson.shortNames = this.fourthFormGroup.value.shortNamesStatus.filter(res => res.isChecked == false).map(({ id, rowType }) => ({ id, rowType }));
  }
  updateStakeholdersData(stakeHolders) {
    this.getBNData.stakeHolders = stakeHolders;
    this.postJson.stakeHolders = this.getBNData.stakeHolders.filter(res => res.isChecked == false).map(({ id, rowType }) => ({ id, rowType }));
  }
  updateAttachmentsData(updatedAttachments) {
    this.getBNData.attachments = updatedAttachments ? updatedAttachments : this.getBNData.attachments;
    this.postJson.attachments = this.getBNData.attachments.filter(res => res.isChecked == false).map(({ id, rowType }) => ({ id, rowType }));
    this.checkFormValidity();
  }
  // Form Submit
  applicationInfoFormSubmit() {
    this.postJson.applicationDataControls = this.secondFormGroup.value.appInfoStatus.filter(res => res.isChecked == false).map(({ id, rowType }) => ({ id, rowType }));
  }
  businessActivityFormSubmit() {
    this.postJson.businessActivity = this.getBNData.businessActivity.filter(res => res.isChecked == false).map(({ id, rowType }) => ({ id, rowType }));
  }
  businessNamesFormSubmit() {
    this.postJson.businessNamesControls = this.businessNameRadioBtnId ? (({ id, enId, enChecked, arId, arChecked }) => ({ id, enId, enChecked, arId, arChecked }))(this.getBNData.businessName.businessNames.find(f => f.id == this.businessNameRadioBtnId)) : this.postJson.businessNamesControls;
    this.postJson.shortNames = this.fourthFormGroup.value.shortNamesStatus.filter(res => res.isChecked == false).map(({ id, rowType }) => ({ id, rowType }));
  }
  stakeholderInfoFormSubmit() {
    this.postJson.stakeHolders = this.getBNData.stakeHolders.filter(res => res.isChecked == false).map(({ id, rowType }) => ({ id, rowType }));
  }
  attachmentsFormSubmit() {
    this.postJson.workFlow.code = this.workFlowCode.Approved;
    if (!this.isDisabledPaymentBtn) {
      this.submitRequest();
    }
    // 
    // this.router.navigateByUrl('/pages/business-license/business-names/manage-business-names');
  }
  checkFormValidity(): boolean {
    if (this.getBNData.isHideActions &&
      (!this.getBNData.isConsultant && !this.getBNData.isManager) &&
      this.secondFormGroup.value.appInfoStatus.every(x => x.isChecked == true) &&
      this.getBNData.businessActivity.every(x => x.isChecked == true) &&
      this.fourthFormGroup.value.shortNamesStatus.every(x => x.isChecked == true) &&
      this.getBNData.stakeHolders.every(x => x.isChecked == true) &&
      this.getBNData.attachments.every(x => x.isChecked == true) &&
      this.postJson.businessNamesControls.arChecked && this.postJson.businessNamesControls.enChecked) {
      this.isDisabledPaymentBtn = false;
      return this.isDisabledPaymentBtn;
    }
    else {
      this.isDisabledPaymentBtn = true;
      return this.isDisabledPaymentBtn;
    }
  }
  openDialog(...dialogName) {
    const dialogConfig = new MatDialogConfig();
    let dialogRef;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    // dialogConfig.data = { usersList, roleId };
    if (dialogName[0] == 'viewAction') {

      dialogRef = this.dialog.open(ViewActionsComponent, dialogConfig);
    }
    else if (dialogName[0] == 'returnNotes') {
      // dialogConfig.data = this.stickyNotes || '';
      dialogConfig.data = this.postJson;
      dialogRef = this.dialog.open(ReturnBackNotesComponent, dialogConfig);
    }
    else if (dialogName[0] == 'reject') {
      dialogConfig.data = this.postJson;
      dialogRef = this.dialog.open(RejectedBusinessModalComponent, dialogConfig);
    }
    else if (dialogName[0] == 'attachment') {
      dialogConfig.data = dialogName[1];
      dialogRef = this.dialog.open(ReviewAttachmentModalComponent, dialogConfig);
    }
    else if (dialogName[0] == 'fwdAction') {
      let postJson = this.postJson;
      let managerOrConsultant = this.getBNData.isManager ? 'manager' : this.getBNData.isConsultant ? 'consultant' : undefined;
      dialogConfig.data = { postJson, managerOrConsultant };
      dialogRef = this.dialog.open(FwdForActionComponent, dialogConfig);
    }

    // dialogRef.afterClosed().pipe().subscribe(obj => {
    //   debugger
    //   this.postJson = obj.postJson;
    // });
    dialogRef.componentInstance.action.subscribe(resp => {
      this.postJson = resp.postJson;
      this.submitRequest();
    });
  }

  private submitRequest() {
    debugger
    this.bnService.AddWorkFlow(this.postJson).subscribe(data => {
      let response: any = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {
        this.dialog.closeAll();
        this.router.navigateByUrl('pages/business-license/business-names/manage-business-names');
      }
      else {
        this.translationService.getTranslation(String(response.result ? response.result.code : response.code)).subscribe((text: string) => {
          this.toast.error(text);
        });
      }
    });
  }

  notesEvent(value) {
    this.stickyNotes = value;
    this.postJson.stickyNotes = this.stickyNotes;
  }
  public onStepChange(event: any): void {
    if (event.previouslySelectedIndex == 1) {
      this.applicationInfoFormSubmit();
    }
    else if (event.previouslySelectedIndex == 2) {
      this.businessActivityFormSubmit();
    }
    else if (event.previouslySelectedIndex == 3) {
      this.businessNamesFormSubmit();
    }
    else if (event.previouslySelectedIndex == 4) {
      this.stakeholderInfoFormSubmit();
    }
    else if (event.previouslySelectedIndex == 5) {
      this.updateAttachmentsData(undefined);
    }
    this.checkFormValidity();
  }
}