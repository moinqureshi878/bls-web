
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';
// import { ExtendBusinessNamesComponent } from '../model/extend-business-names/extend-business-names.component';
// import { BusinessNamesModificationListComponent } from '../model/business-names-modification-list/business-names-modification-list.component';
// import { CancelBusinessNameComponent } from '../model/cancel-business-name/cancel-business-name.component';
import { ActivatedRoute } from '@angular/router';
import { WorkflowStatusEnum } from 'app/core/_enum/workflowStatus.enum';
import { TranslationService } from '@translateService/translation.service';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';

@Component({
  selector: 'kt-business-names',
  templateUrl: './business-names.component.html',
  styleUrls: ['./business-names.component.scss']
})
export class BusinessNamesComponent implements OnInit {

  workflowStatusEnum = WorkflowStatusEnum;
  RequestBusinessName: any;
  CancelBusinessnames: any;

  isShown: boolean = false; // hidden by default
  isHidden: boolean = true;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  displayedColumns7: string[] = ['bnrefNo', 'businessNameAr', 'businessNameEn', 'ianumber', 'blno', 'status', 'daysLeft', 'editView'];
  dataSource7 = new MatTableDataSource<any>(this.RequestBusinessName);
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;

  closeResult: string; //Bootstrap Modal Popup

  displayedColumnsOU: string[] = ['bnrefNo', 'businessNameAr', 'businessNameEn', 'bnTypeName', 'reservationDate', 'status', 'statusDate', 'editView'];
  dataSourceCancelBusinessNames = new MatTableDataSource<any>(this.CancelBusinessnames);
  @ViewChild('matPaginatorOU', { static: true }) paginatorOU: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortOU: MatSort;
  constructor(
    private _formBuilder: FormBuilder,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private translationService: TranslationService,
    public validationMessages: ValidationMessagesService,
    public dialog: MatDialog) {
    debugger
    this.RequestBusinessName = this.route.snapshot.data.data.businessName;
    this.CancelBusinessnames = this.route.snapshot.data.data.expiredBusinessName;
    this.dataSource7 = new MatTableDataSource<any>(this.RequestBusinessName);
    this.dataSourceCancelBusinessNames = new MatTableDataSource<any>(this.CancelBusinessnames);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;

    this.dataSourceCancelBusinessNames.paginator = this.paginatorOU;
    this.dataSourceCancelBusinessNames.sort = this.sortOU;

  }



  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();

    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  applyFilterOU(filterValue: string) {
    this.dataSourceCancelBusinessNames.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceCancelBusinessNames.paginator) {
      this.dataSourceCancelBusinessNames.paginator.firstPage();
    }
  }




  //..Modal Popup Start
  open(content) {
    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 

 
  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

  toggleShow() {
    this.isHidden = !this.isHidden;
    this.isShown = !this.isShown;

  }

}