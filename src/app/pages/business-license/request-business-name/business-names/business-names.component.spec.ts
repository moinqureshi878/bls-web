import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessNamesComponent } from './business-names.component';

describe('BusinessNamesComponent', () => {
  let component: BusinessNamesComponent;
  let fixture: ComponentFixture<BusinessNamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessNamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessNamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
