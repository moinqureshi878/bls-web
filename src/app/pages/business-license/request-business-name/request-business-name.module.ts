import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '../../../views/partials/partials.module';


import { RequestBusinessNameComponent } from './request-business-name.component';
import { BusinessNamesComponent } from './business-names/business-names.component';
import { ExtendBusinessNamesComponent } from './model/extend-business-names/extend-business-names.component';
import { BusinessNamesModificationListComponent } from './model/business-names-modification-list/business-names-modification-list.component';
import { CancelBusinessNameComponent } from './model/cancel-business-name/cancel-business-name.component';
import { BusinessNameModificationComponent } from './model/business-name-modification/business-name-modification.component';
import { AddStakeholdersModalComponent } from './model/add-stakeholders-modal/add-stakeholders-modal.component';
import { ExtendBusinessExtensionComponent } from './model/extend-business-extension/extend-business-extension.component';
import { AddBusinessNamesComponent } from './add-business-names/add-business-names.component';
import { BusinessActivityComponent } from '../bn-common-components/business-activity/business-activity.component';
// import { BusinessNamesAvailabilityComponent } from '../bn-common-components/business-names-availability/business-names-availability.component';
import { BnCommonComponentsModule } from '../bn-common-components/bn-common-components.module';
import { ViewedModifiedCustomerBnComponent } from './viewed-modified-customer-bn/viewed-modified-customer-bn.component';
import { AuthGuard } from 'app/core/auth';
import { BnResolver } from 'app/core/resolvers/bn.resolver';
import { TranslateModule } from '@ngx-translate/core';
import { MatTableExporterModule } from 'mat-table-exporter';

import {
  MatSortModule,
} from '@angular/material';

@NgModule({
  entryComponents: [
    BusinessNamesModificationListComponent,
    ExtendBusinessNamesComponent,
    CancelBusinessNameComponent,
    BusinessNameModificationComponent,
    ExtendBusinessExtensionComponent,
    AddStakeholdersModalComponent
  ],
  declarations: [
    RequestBusinessNameComponent,
    BusinessNamesComponent,
    ExtendBusinessNamesComponent,
    BusinessNamesModificationListComponent,
    CancelBusinessNameComponent,
    BusinessNameModificationComponent,
    AddStakeholdersModalComponent,
    ExtendBusinessExtensionComponent,
    AddBusinessNamesComponent,
    BusinessActivityComponent,
    // BusinessNamesAvailabilityComponent,
    ViewedModifiedCustomerBnComponent
  ],
  exports: [AddBusinessNamesComponent],
  imports: [
    PartialsModule,
    MatSortModule,
    BnCommonComponentsModule,
    MatTableExporterModule,
    TranslateModule.forChild(),
    NgbModule.forRoot(),
    RouterModule.forChild([
      {
        path: '',
        component: RequestBusinessNameComponent
      },
      {
        path: 'business-names',
        canActivate: [AuthGuard],
        component: BusinessNamesComponent,
        resolve: { data: BnResolver }
      },
      {
        path: 'add-business-names',
        canActivate: [AuthGuard],
        component: AddBusinessNamesComponent
      },
      {
        path: 'viewed-modified-customer-bn',
        component: ViewedModifiedCustomerBnComponent
      },
      {
        path: 'business-name-modification',
        component: BusinessNameModificationComponent
      },
      {
        path: 'business-names-modification-list',
        component: BusinessNamesModificationListComponent
      },
      {
        path: 'extend-business-names',
        component: ExtendBusinessNamesComponent
      },
      {
        path: 'extend-business-extension',
        component: ExtendBusinessExtensionComponent
      },
      {
        path: 'cancel-business-name',
        component: CancelBusinessNameComponent
      },


    ]),
  ],
  schemas: [
    AddBusinessNamesComponent,
  ],
})
export class RequestBusinessNameModule { }




