import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessNamesModificationListComponent } from './business-names-modification-list.component';

describe('BusinessNamesModificationListComponent', () => {
  let component: BusinessNamesModificationListComponent;
  let fixture: ComponentFixture<BusinessNamesModificationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessNamesModificationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessNamesModificationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
