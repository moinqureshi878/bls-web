import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormControl } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';

/* Step Two Date */
export interface stepTwoData {
  applicationNo: string;
  requestStatus: string;
  requestDate: string;
  editView: string;
}

/** Builds and returns a new User. */
const STEP_TWO_DATA: stepTwoData[] = [
  { applicationNo: 'BNM-1', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'BNM-2', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'BNM-3', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'BNM-4', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'BNM-5', requestStatus: 'Submitted', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'BNM-6', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'BNM-7', requestStatus: 'Submitted', requestDate: '12/12/2019', editView: '' }
];

@Component({
  selector: 'kt-business-names-modification-list',
  templateUrl: './business-names-modification-list.component.html',
  styleUrls: ['./business-names-modification-list.component.scss']
})
export class BusinessNamesModificationListComponent implements OnInit {

  /* Step Two */
  displayedColumnsstepTwo: string[] = ['applicationNo', 'requestStatus', 'requestDate', 'editView'];
  stepTwo = new MatTableDataSource<stepTwoData>(STEP_TWO_DATA);

  @ViewChild('matPaginatorstepTwo', { static: true }) paginatorstepTwo: MatPaginator;
  // @ViewChild('sortStepTwo', { static: true }) sortStepTwo: MatSort;
  @ViewChild(MatSort, { static: true }) sortStepTwo: MatSort;
  constructor() {

  }

  ngOnInit() {

    /* Step Two */
    this.stepTwo.paginator = this.paginatorstepTwo;
    this.stepTwo.sort = this.sortStepTwo;
  }

  applyFilterstepTwo(filterValue: string) {
    this.stepTwo.filter = filterValue.trim().toLowerCase();

    if (this.stepTwo.paginator) {
      this.stepTwo.paginator.firstPage();
    }
  }

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

}