import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddStakeholdersModalComponent } from './add-stakeholders-modal.component';

describe('AddStakeholdersModalComponent', () => {
  let component: AddStakeholdersModalComponent;
  let fixture: ComponentFixture<AddStakeholdersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddStakeholdersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddStakeholdersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
