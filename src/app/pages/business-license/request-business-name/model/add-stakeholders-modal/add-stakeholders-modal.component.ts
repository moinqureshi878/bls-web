
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {MatDialog } from '@angular/material';

@Component({
  selector: 'kt-add-stakeholders-modal',
  templateUrl: './add-stakeholders-modal.component.html',
  styleUrls: ['./add-stakeholders-modal.component.scss']
})
export class AddStakeholdersModalComponent implements OnInit {
  closeResult: string; //Bootstrap Modal Popup
  radioButtonCondition: number = 5;
  branchCondition: number = 0;

  constructor(private modalService: NgbModal,
    private dialogRef: MatDialog) { }

  ngOnInit() {

  }
  //Radio Button Condtion
  setRadioButtonCondition(value: number) {
    this.radioButtonCondition = value;
  }
  setBranchCondition(value: number) {
    
    this.branchCondition = value;
  }

  /* Emirates ID Mask */
  emiratesIDMask = [/[0-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/]

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);

  
  //..Modal Popup Start
  open(content) {
    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 

  closeAllMatDialog(){
    this.dialogRef.closeAll();
  }
}

