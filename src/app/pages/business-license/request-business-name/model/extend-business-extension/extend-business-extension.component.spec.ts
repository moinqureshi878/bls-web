import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendBusinessExtensionComponent } from './extend-business-extension.component';

describe('ExtendBusinessExtensionComponent', () => {
  let component: ExtendBusinessExtensionComponent;
  let fixture: ComponentFixture<ExtendBusinessExtensionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtendBusinessExtensionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendBusinessExtensionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
