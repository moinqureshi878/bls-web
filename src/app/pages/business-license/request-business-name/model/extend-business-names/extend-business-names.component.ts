import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import {FormControl} from '@angular/forms';
import {TooltipPosition} from '@angular/material/tooltip';
import {ExtendBusinessExtensionComponent} from '../extend-business-extension/extend-business-extension.component';

/* Step Two Date */
export interface stepTwoData {
  applicationNo: string;
  extendDays: string;
  requestStatus: string;
  requestDate: string;
  editView: string;
}

/** Builds and returns a new User. */
const STEP_TWO_DATA: stepTwoData[] = [
  { applicationNo: 'BNE-1', extendDays: '90', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'BNE-2', extendDays: '30', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'BNE-3', extendDays: '30', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'BNE-4', extendDays: '60', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'BNE-5', extendDays: '30', requestStatus: 'Submitted', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'BNE-6', extendDays: '90', requestStatus: 'Approved', requestDate: '12/12/2019', editView: '' },
  { applicationNo: 'BNE-7', extendDays: '90', requestStatus: 'Submitted', requestDate: '12/12/2019', editView: '' }
];

@Component({
  selector: 'kt-extend-business-names',
  templateUrl: './extend-business-names.component.html',
  styleUrls: ['./extend-business-names.component.scss']
})
export class ExtendBusinessNamesComponent implements OnInit {
 
  /* Step Two */
  displayedColumnsstepTwo: string[] = ['applicationNo', 'extendDays', 'requestStatus', 'requestDate', 'editView'];
  stepTwo = new MatTableDataSource<stepTwoData>(STEP_TWO_DATA);

  @ViewChild('matPaginatorstepTwo', { static: true }) paginatorstepTwo: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortStepTwo: MatSort;
  constructor(public dialog: MatDialog) {

  }

  ngOnInit() {

    /* Step Two */
    this.stepTwo.paginator = this.paginatorstepTwo;
    this.stepTwo.sort = this.sortStepTwo;
  }

  applyFilterstepTwo(filterValue: string) {
    this.stepTwo.filter = filterValue.trim().toLowerCase();

    if (this.stepTwo.paginator) {
      this.stepTwo.paginator.firstPage();
    }
  }  


   /* begin:: Display ExtendGrid */
   openExtendBusinessExtension() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "60%";
    dialogConfig.maxWidth = "60%";
    this.dialog.open(ExtendBusinessExtensionComponent, dialogConfig);

  }
  /* end:: Display ExtendGrid */

   /* Tooltip */
   positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
   position = new FormControl(this.positionOptions[3]);

}
