import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendBusinessNamesComponent } from './extend-business-names.component';

describe('ExtendBusinessNamesComponent', () => {
  let component: ExtendBusinessNamesComponent;
  let fixture: ComponentFixture<ExtendBusinessNamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtendBusinessNamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendBusinessNamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
