import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { termsConditionValidator } from 'app/core/validator/form-validators.service';
import { MatStepper } from '@angular/material';
import { Observable } from 'rxjs';
import { BusinessLicenseService } from 'app/core/appServices/business-license/business-license.service';
import { JwtTokenService } from 'app/core/appServices/index.service';

@Component({
  selector: 'kt-cancel-business-name',
  templateUrl: './cancel-business-name.component.html',
  styleUrls: ['./cancel-business-name.component.scss']
})
export class CancelBusinessNameComponent implements OnInit {
  closeResult: string; //Bootstrap Modal Popup

  firstServiceInfoForm: FormGroup;
  secondFormGroup: FormGroup;

  serviceInfoAndUserInfo: Observable<any>;
  @ViewChild('CancelBusinessNameStepper', { static: true }) CancelBusinessNameStepper: MatStepper;
  loading: boolean = false;
  constructor(private fb: FormBuilder,
    public businessLicenseService: BusinessLicenseService,
    private jwtTokenService: JwtTokenService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.fillUserIDOnModel();

    this.firstServiceInfoForm = this.fb.group({
      termsCondition: [false, Validators.compose([Validators.required, termsConditionValidator])]
    });
    this.secondFormGroup = this.fb.group({
    });
  }

  //..Modal Popup Start
  open(content) {

    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  fillUserIDOnModel() {
    let userSession = JSON.parse(localStorage.getItem("userSession"));
    let sessionID = 0;

    let currentSessionToken: string;
    let behalfUserID: number;
    let UserID: number;
    if (userSession && userSession.length > 1) {
      currentSessionToken = userSession[0].token;
      behalfUserID = +this.jwtTokenService.getJwtTokenValueByToken(userSession[1].token).UserID;
      UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
      // this.JsonModel["bnRequest"].onBehalfUid = behalfUserID;
    }
    else {
      currentSessionToken = userSession[0].token;
      UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
    }

    this.jwtTokenService.getSessionSubject().subscribe(value => {
      if (value) {
        if (value && value.length > 1) {
          currentSessionToken = value[0].token;
          behalfUserID = +this.jwtTokenService.getJwtTokenValueByToken(value[1].token).UserID;
          UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
          //  this.JsonModel["bnRequest"].onBehalfUid = behalfUserID;
        }
        else {
          currentSessionToken = value[0].token;
          UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
        }
      }
    });

    this.getServiceInfo(behalfUserID ? behalfUserID : 0, UserID);

    //this.JsonModel["bnRequest"].userId = UserID;
  }

  getServiceInfo(loginOnBehalfID: number, UserID: number) {
    this.serviceInfoAndUserInfo = this.businessLicenseService.GetServiceInfo(loginOnBehalfID, UserID);
  }

  firstServiceInfoFormSubmit() {
    this.CancelBusinessNameStepper.selectedIndex = 2;
  }
}