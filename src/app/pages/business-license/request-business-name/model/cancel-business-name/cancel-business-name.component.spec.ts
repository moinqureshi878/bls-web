import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelBusinessNameComponent } from './cancel-business-name.component';

describe('CancelBusinessNameComponent', () => {
  let component: CancelBusinessNameComponent;
  let fixture: ComponentFixture<CancelBusinessNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelBusinessNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelBusinessNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
