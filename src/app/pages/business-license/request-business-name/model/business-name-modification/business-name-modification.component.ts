import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { AddNewActivitiesComponent } from '../add-new-activities/add-new-activities.component';
import { AddStakeholdersModalComponent } from '../add-stakeholders-modal/add-stakeholders-modal.component';
import { TooltipPosition } from '@angular/material/tooltip';
export interface UserData {
  isic4: string;
  arDescription: string;
  enDescription: string;
  activityGroup: string;
  editView: string;
}

/** Builds and returns a new User. */

const ELEMENT_DATA: UserData[] = [
  { isic4: '1', arDescription: '1', enDescription: '1', activityGroup: '', editView: 'Remove' },
  { isic4: '2', arDescription: '2', enDescription: '2', activityGroup: '', editView: 'Remove' },
  { isic4: '3', arDescription: '3', enDescription: '3', activityGroup: '', editView: 'Remove' },
  { isic4: '4', arDescription: '4', enDescription: '4', activityGroup: '', editView: 'Remove' },
  { isic4: '5', arDescription: '5', enDescription: '5', activityGroup: '', editView: 'Remove' }
];

export interface stakeHoldersData {
  orderNo: string;
  type: string;
  relationship: string;
  nameAr: string;
  nameEn: string;
  share: string;
  registered: string;
  editView: string;

}

/** Builds and returns a new User. */
const ELEMENT_STAKEHOLDERS_DATA: stakeHoldersData[] = [
  { orderNo: '1', type: 'Individual', relationship: 'Owner', nameAr: ' Name Ar', nameEn: ' Name En', share: '25%', registered: 'Yes', editView: '' },
  { orderNo: '2', type: 'Govt Entity', relationship: 'Partner', nameAr: ' Name Ar', nameEn: ' Name En', share: '25%', registered: 'No', editView: '' },
  { orderNo: '3', type: 'License', relationship: 'Responsible Manager', nameAr: ' Name Ar', nameEn: ' Name En', share: '25%', registered: 'Yes', editView: '' },
  { orderNo: '4', type: 'Inehritance Representative', relationship: 'Partner', nameAr: 'Name Ar', nameEn: ' Name En', share: '25%', registered: 'Yes', editView: '' },

];

@Component({
  selector: 'kt-business-name-modification',
  templateUrl: './business-name-modification.component.html',
  styleUrls: ['./business-name-modification.component.scss']
})
export class BusinessNameModificationComponent implements OnInit {

  isShown: boolean = false; // hidden by default
  isHidden: boolean = true;

  closeResult: string; //Bootstrap Modal Popup
  emCopy: string = "ID front side";
  embCopy: string = "ID back side";
  passportCopy: string = "Passport info pages";
  visaCopy: string = "Visa page";
  //Stepper active class
  stepperValue: number = 1;

  //Form Group Stepper
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  seventhFormGroup: FormGroup;
  eighthFormGroup: FormGroup;


  displayedColumns7: string[] = ['isic4', 'arDescription', 'enDescription', 'activityGroup', 'editView'];
  dataSource7 = new MatTableDataSource<UserData>(ELEMENT_DATA);
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  // @ViewChild('sort7', { static: true }) sort7: MatSort;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;

  stakeHoldersdisplayedColumns7: string[] = ['orderNo', 'type', 'relationship', 'nameAr', 'nameEn', 'share', 'registered', 'editView'];
  stakeHoldersdataSource7 = new MatTableDataSource<stakeHoldersData>(ELEMENT_STAKEHOLDERS_DATA);
  @ViewChild('stakeHoldersmatPaginator7', { static: true }) stakeHolderspaginator7: MatPaginator;
  // @ViewChild('stakeHolderssort7', { static: true }) stakeHolderssort7: MatSort;
  @ViewChild(MatSort, { static: true }) stakeHolderssort7: MatSort;

  constructor(
    private _formBuilder: FormBuilder,
    private modalService: NgbModal,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({

    });
    this.secondFormGroup = this._formBuilder.group({

    });
    this.fourthFormGroup = this._formBuilder.group({

    });
    this.fifthFormGroup = this._formBuilder.group({

    });
    this.sixthFormGroup = this._formBuilder.group({

    });

    this.seventhFormGroup = this._formBuilder.group({

    });
    this.eighthFormGroup = this._formBuilder.group({

    });

    /* Data Table */
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;

    /* Data Table */
    this.stakeHoldersdataSource7.paginator = this.stakeHolderspaginator7;
    this.stakeHoldersdataSource7.sort = this.stakeHolderssort7;
  }

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);

  /* Emirates ID Mask */
  emiratesIDMask = [/[0-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/]

  getEmCopy(event) {
    this.emCopy = event.target.files[0].name.substring(0, 15);
    // + "." + event.target.files[0].name.split('.')[1]
  }
  getEmbId(event) {
    this.embCopy = event.target.files[0].name.substring(0, 15);
  }
  getPassportCopy(event) {
    this.passportCopy = event.target.files[0].name.substring(0, 15);
  }
  getVisa(event) {
    this.visaCopy = event.target.files[0].name.substring(0, 15);
  }

  //Stepper Value
  setStepperValue(value: number) {
    this.stepperValue = value;
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();

    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  stakeHoldersapplyFilter7(filterValue: string) {
    this.stakeHoldersdataSource7.filter = filterValue.trim().toLowerCase();

    if (this.stakeHoldersdataSource7.paginator) {
      this.stakeHoldersdataSource7.paginator.firstPage();
    }
  }

  /* begin:: Add Activity */
  openAddNewActivities() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "60%";
    // this.dialog.open(AddNewActivitiesComponent, dialogConfig);

  }
  /* end:: Add Activity */
  /* begin:: Add StakeHolders */
  openAddStakeHolders() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "60%";
    dialogConfig.maxWidth = "60%";
    this.dialog.open(AddStakeholdersModalComponent, dialogConfig);

  }
  /* end:: Add StakeHolders */


  //..Modal Popup Start
  open(content) {
    
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 

  toggleShow() {
    this.isHidden = !this.isHidden;
    this.isShown = !this.isShown;

  }

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

}
