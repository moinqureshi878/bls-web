import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessNameModificationComponent } from './business-name-modification.component';

describe('BusinessNameModificationComponent', () => {
  let component: BusinessNameModificationComponent;
  let fixture: ComponentFixture<BusinessNameModificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessNameModificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessNameModificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
