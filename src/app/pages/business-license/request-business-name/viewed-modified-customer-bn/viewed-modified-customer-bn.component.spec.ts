import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewedModifiedCustomerBnComponent } from './viewed-modified-customer-bn.component';

describe('ViewedModifiedCustomerBnComponent', () => {
  let component: ViewedModifiedCustomerBnComponent;
  let fixture: ComponentFixture<ViewedModifiedCustomerBnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewedModifiedCustomerBnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewedModifiedCustomerBnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
