import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBusinessNamesComponent } from './add-business-names.component';

describe('AddBusinessNamesComponent', () => {
  let component: AddBusinessNamesComponent;
  let fixture: ComponentFixture<AddBusinessNamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBusinessNamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBusinessNamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
