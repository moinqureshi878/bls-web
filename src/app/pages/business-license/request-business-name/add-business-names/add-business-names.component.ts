import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig, MatStepper } from '@angular/material';
import { AddStakeholdersModalComponent } from '../model/add-stakeholders-modal/add-stakeholders-modal.component';
import { termsConditionValidator, nameArValidator } from '../../../../core/validator/form-validators.service';
import { ValidationMessagesService } from '../../../../core/validator/validation-messages.service';
import { Observable } from 'rxjs';
import { CommonService, BnCommonService, JwtTokenService } from 'app/core/appServices/index.service';
import { BusinessLicenseService } from 'app/core/appServices/business-license/business-license.service';
import { AsyncFormValidatorService } from 'app/core/validator/async-form-validator.service';
import { TranslationService } from 'app/core/_base/layout';
import { ToastrService } from 'ngx-toastr';
import { BnService } from 'app/core/appServices/bn.service';
import { ApplicationTypesEnum } from 'app/core/_enum/applicationTypes.enum';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { fileSize } from '../../../../core/validator/form-validators.service';


@Component({
  selector: 'kt-add-business-names',
  templateUrl: './add-business-names.component.html',
  styleUrls: ['./add-business-names.component.scss']
})
export class AddBusinessNamesComponent implements OnInit, AfterViewInit {

  enumApplicationTypes = ApplicationTypesEnum;
  closeResult: string; //Bootstrap Modal Popup
  panelOpenState = false;

  radioButtonCondition2: number = 5;
  emCopy: string = "ID front side";
  embCopy: string = "ID back side";
  passportCopy: string = "Passport info pages";
  visaCopy: string = "Visa page";
  //Stepper active class
  stepperValue: number = 1;

  //Form Group Stepper
  firstServiceInfoForm: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  seventhFormGroup: FormGroup;
  eighthFormGroup: FormGroup;
  /* Checkbox Value Set to true */

  getAttachmentsType: any;
  isTick = [];
  isLoading = [];
  isDisabled: boolean = false;


  checkedBLS = true;

  loading = false;


  JsonModel: any = {
    "bnRequest": {
      "id": 0,
      "userId": null,
      "onBehalfUid": null,
      "bnrefNo": null,
      "ianumber": null,
      "applicationTypeId": null,
      "bntypeId": null,
      "legalTypeId": null,
      "licenseCategoryId": null,
      "licenseTypeId": null,
      "reservationPeriod": null,
      "holicenseNo": null,
      "blno": null,
      "certificate": null,
      "issueDate": null,
      "expiredDate": null,
      "licenseCertificate": null,
      "licenseIssuanceAuthorityId": null,
      "countryId": null,
      "requestStatusId": null,
      "stepNo": null,
      "createdAt": null,
      "createdBy": null,
      "isActive": null,
      "businessNameActivities": null,
      "businessNameSelection": null,
      "businessNameServices": null,
      "businessNameStakeHolders": null
    },
    "bnActivity": [],
    "bnService": [],
    "bnSelection": [],
    "bnStackHolder": [],
    "bnAttachments": [],
  };
  // steps models
  serviceInfoModel: any[] = [];



  stakeHoldersdisplayedColumns7: string[] = ['orderNo', 'type', 'relationship', 'nameAr', 'nameEn', 'share', 'registered', 'editView'];



  serviceInfoAndUserInfo: Observable<any>;
  termConditions: any;
  @ViewChild('stepperBusinessName', { static: true }) stepperBusinessName: MatStepper;
  constructor(
    private fb: FormBuilder,
    public businessLicenseService: BusinessLicenseService,
    private modalService: NgbModal,
    public validationMessages: ValidationMessagesService,
    private commonService: CommonService,
    public bnCommonService: BnCommonService,
    public asyncFormValidatorService: AsyncFormValidatorService,
    private cdr: ChangeDetectorRef,
    private translationService: TranslationService,
    private toast: ToastrService,
    private bnService: BnService,
    private jwtTokenService: JwtTokenService,
    public router: Router,
    private _formBuilder: FormBuilder,
    private _DomSanitizationService: DomSanitizer,
    public dialog: MatDialog) {
  }

  ngOnInit() {
    this.fillUserIDOnModel();
    this.getAttachments(this.enumApplicationTypes.BNN)

    this.firstServiceInfoForm = this.fb.group({
      termsCondition: [false, Validators.compose([Validators.required, termsConditionValidator])]
    });
    this.secondFormGroup = this.fb.group({
    });
    this.thirdFormGroup = this.fb.group({
    });
    this.fourthFormGroup = this.fb.group({
    });
    this.fifthFormGroup = this.fb.group({
    });
    this.sixthFormGroup = this.fb.group({
    });
    this.seventhFormGroup = this.fb.group({
    });
    this.eighthFormGroup = this.fb.group({
    });
    this.updateDOM();
  }
  fillUserIDOnModel() {
    let userSession = JSON.parse(localStorage.getItem("userSession"));
    let sessionID = 0;

    let currentSessionToken: string;
    let behalfUserID: number;
    let UserID: number;
    if (userSession && userSession.length > 1) {
      currentSessionToken = userSession[0].token;
      behalfUserID = +this.jwtTokenService.getJwtTokenValueByToken(userSession[1].token).UserID;
      UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
      this.JsonModel["bnRequest"].onBehalfUid = behalfUserID;
    }
    else {
      currentSessionToken = userSession[0].token;
      UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
    }

    this.jwtTokenService.getSessionSubject().subscribe(value => {
      if (value) {
        if (value && value.length > 1) {
          currentSessionToken = value[0].token;
          behalfUserID = +this.jwtTokenService.getJwtTokenValueByToken(value[1].token).UserID;
          UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
          this.JsonModel["bnRequest"].onBehalfUid = behalfUserID;
        }
        else {
          currentSessionToken = value[0].token;
          UserID = +this.jwtTokenService.getJwtTokenValueByToken(currentSessionToken).UserID;
        }
      }
    });

    this.getServiceInfo(behalfUserID ? behalfUserID : 0, UserID);

    this.JsonModel["bnRequest"].userId = UserID;
  }
  updateDOM() {
    this.fifthFormGroup.statusChanges.subscribe(() => this.cdr.markForCheck());
  }
  getServiceInfo(loginOnBehalfID: number, UserID: number) {
    this.serviceInfoAndUserInfo = this.businessLicenseService.GetServiceInfo(loginOnBehalfID, UserID);
  }
  getEmCopy(event) {
    this.emCopy = event.target.files[0].name.substring(0, 15);
  }
  getEmbId(event) {
    this.embCopy = event.target.files[0].name.substring(0, 15);
  }
  getPassportCopy(event) {
    this.passportCopy = event.target.files[0].name.substring(0, 15);
  }
  getVisa(event) {
    this.visaCopy = event.target.files[0].name.substring(0, 15);
  }

  setStepperValue(value: number) {
    this.stepperValue = value;
  }

  setRadioButtonCondition2(value: number) {
    this.radioButtonCondition2 = value;
  }

  open(content) {
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngAfterViewInit() {
    //  this.stepperBusinessName.selectedIndex = 2;
    // setTimeout(() => {
    //   this.stepperBusinessName.selectedIndex = 1;
    // }, 0);
    // setTimeout(() => {
    //   this.stepperBusinessName.selectedIndex = 2;
    // }, 100);
    // setTimeout(() => {
    //   this.stepperBusinessName.selectedIndex = 3;
    // }, 200);
    // setTimeout(() => {
    //   this.stepperBusinessName.selectedIndex = 4;
    // }, 200);
    // setTimeout(() => {
    //   this.stepperBusinessName.selectedIndex = 5;
    // }, 200);
    // setTimeout(() => {
    //   this.stepperBusinessName.selectedIndex = 6;
    // }, 200);
  }

  AttachmentsForm() {
    this.seventhFormGroup = this._formBuilder.group({
      attachments: this._formBuilder.array([]),
    });

    let control = <FormArray>this.seventhFormGroup.controls.attachments;
    this.getAttachmentsType.forEach(x => {
      control.push(this._formBuilder.group({
        id: [0],
        userId: [this.JsonModel.bnRequest.userId],
        // bnid: [this.JsonModel.bnRequest.id],
        // bnrefNo: [this.JsonModel.bnRequest.bnrefNo],
        attachmentName: ['', x.isMandatory ? Validators.required : ''],
        attachmentURL: [''],
        attachmentSize: ['', x.isMandatory ? Validators.compose([Validators.required, fileSize]) : Validators.compose([fileSize])],
        attachmentExtension: [''],
        typeCode: [x.valueEn],
        expiryDate: [new Date()],
        isMandatory: [x.isMandatory],
        inputData: ['', x.inputData ? Validators.required : ''],
        attachmentType: [x.id],
      }));
    });
    this.isTick = [];
    this.getAttachmentsType.forEach(att => {
      this.isLoading.push({ loading: false });
      this.isTick.push({ tick: false });
    });
  }

  getAttachments(code) {
    this.commonService.GetAttachmentByUserTypeCode(code).subscribe(res => {
      let response = res;
      if ((response.result && response.result.code == 200) || (response.code == 200)) {
        this.getAttachmentsType = response.result.data;
        this.AttachmentsForm();
      }
    });
  }

  getFile(event, index) {
    const controlArray = <FormArray>this.seventhFormGroup.get('attachments');
    if (event.target.files[0].size > 20913252) {
      controlArray.controls[index].get('attachmentSize').markAsTouched();
      controlArray.controls[index].get('attachmentSize').markAsDirty();
      controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
    }
    else {
      if (event.target.files[0]) {
        this.isLoading[index].loading = true;
        this.isTick[index].tick = true;
        this.isDisabled = true;
        this.commonService.fileUpload(event.target.files[0]).then(resp => {
          var response = resp;
          if ((response.result && response.result.code == 200) || (response.code == 200)) {
            controlArray.controls[index].get('attachmentExtension').setValue(event.target.files[0].type);
            controlArray.controls[index].get('attachmentSize').setValue(event.target.files[0].size);
            controlArray.controls[index].get('attachmentName').setValue(event.target.files[0].name);

            this.isLoading[index].loading = false;
            this.isDisabled = false;

            this.cdr.markForCheck();
            controlArray.controls[index].get('attachmentURL').setValue(response.attachmentURL);
            controlArray.controls[index].get('attachmentSize').markAsTouched();
            controlArray.controls[index].get('attachmentName').markAsTouched();
          }
          else {
            this.isLoading[index].loading = false;
            this.isDisabled = false;
            this.cdr.markForCheck();
            this.translationService.getTranslation(String(response.result.code)).subscribe((text: string) => {
              this.toast.error(text);
            });
          }
        });
      }
    }
  }


  firstServiceInfoFormSubmit() {
    if (this.firstServiceInfoForm.valid) {
      this.loading = true;
      this.serviceInfoAndUserInfo.subscribe(
        data => {
          data["serviceInfo"].forEach(item => {
            this.JsonModel.bnService.push({
              "id": 0,
              "bnid": 0,
              "bnrefNo": null,
              "serviceId": item.id,
              "applicationTypeId": 226,
              "businessNameRequest": null
            })
          })
          this.JsonModel["bnRequest"].stepNo = 1;
          this.AddBusinessNamesAPI(this.JsonModel, 1);
        })
    }
    else {
      this.validationMessages.validateAllFormFields(this.firstServiceInfoForm);
    }
  }

  secondSubmitterInfoFormSubmit() {
    this.JsonModel["bnRequest"].stepNo = 2;
    this.AddBusinessNamesAPI(this.JsonModel, 2);
  }

  thirdApplicationInfoSubmit() {
    if (this.thirdFormGroup.valid) {
      this.JsonModel["bnRequest"].applicationTypeId = this.enumApplicationTypes.BNN;
      this.JsonModel["bnRequest"].bntypeId = this.thirdFormGroup.value.BNTypes;
      this.JsonModel["bnRequest"].legalTypeId = this.thirdFormGroup.value.legalTypeddl;
      this.JsonModel["bnRequest"].licenseCategoryId = this.thirdFormGroup.value.ddlLicenseCategory;
      this.JsonModel["bnRequest"].licenseTypeId = this.thirdFormGroup.value.ddlLicenseType;
      this.JsonModel["bnRequest"].reservationPeriod = this.thirdFormGroup.value.reservationPeriod;
      this.JsonModel["bnRequest"].holicenseNo = this.thirdFormGroup.value.licenceNumber;
      this.JsonModel["bnRequest"].issueDate = this.thirdFormGroup.value.licenceIssueDate;
      this.JsonModel["bnRequest"].expiredDate = this.thirdFormGroup.value.licenceExpiryDate;
      this.JsonModel["bnRequest"].licenseIssuanceAuthorityId = this.thirdFormGroup.value.ddlIssueanceAuthority;
      this.JsonModel["bnRequest"].countryId = this.thirdFormGroup.value.ddlCountry;
      this.JsonModel["bnRequest"].bncategoryId = this.thirdFormGroup.value.businessNamesTypes;
      this.JsonModel["bnRequest"].stepNo = 3;

      this.AddBusinessNamesAPI(this.JsonModel, 3);
    }
    else {
      this.validationMessages.validateAllFormFields(this.thirdFormGroup);
    }
  }

  fourthBusinessActivitySubmit(values: any[]) {
    if (values.length > 0) {
      this.JsonModel.bnActivity = values;
      this.JsonModel["bnRequest"].stepNo = 4;

      this.AddBusinessNamesAPI(this.JsonModel, 4);
    }
  }

  fifthFormGroupSubmit() {
    if (this.fifthFormGroup.valid) {
      var BNRequest = [
        {
          "id": 0,
          "bnid": 1,
          "bnrefNo": null,
          "businessNameAr": this.fifthFormGroup.value.BNArabic1,
          "businessNameEn": this.fifthFormGroup.value.BNEnglish1,
          "shortNameAr": this.fifthFormGroup.value.BNShortNameArabic,
          "shortNameEn": this.fifthFormGroup.value.BNShortNameEnglish,
          "priority": 1,
          "isSelected": false,
          "businessNameRequest": null
        },
        {
          "id": 0,
          "bnid": 1,
          "bnrefNo": null,
          "businessNameAr": this.fifthFormGroup.value.BNArabic2,
          "businessNameEn": this.fifthFormGroup.value.BNEnglish2,
          "shortNameAr": this.fifthFormGroup.value.BNShortNameArabic,
          "shortNameEn": this.fifthFormGroup.value.BNShortNameEnglish,
          "priority": 2,
          "isSelected": false,
          "businessNameRequest": null
        },
        {
          "id": 0,
          "bnid": 1,
          "bnrefNo": null,
          "businessNameAr": this.fifthFormGroup.value.BNArabic3,
          "businessNameEn": this.fifthFormGroup.value.BNEnglish3,
          "shortNameAr": this.fifthFormGroup.value.BNShortNameArabic,
          "shortNameEn": this.fifthFormGroup.value.BNShortNameEnglish,
          "priority": 3,
          "isSelected": false,
          "businessNameRequest": null
        }
      ];
      var valueAr = BNRequest.map(function (item) { return item.businessNameAr });
      var valueEn = BNRequest.map(function (item) { return item.businessNameEn });
      var isDuplicateAr = valueAr.some(function (item, index) {
        return valueAr.indexOf(item) != index
      });
      var isDuplicateEn = valueEn.some(function (item, index) {
        return valueEn.indexOf(item) != index
      });

      if (!isDuplicateAr && !isDuplicateEn) {
        this.JsonModel.bnSelection = BNRequest;
        this.JsonModel["bnRequest"].stepNo = 5;

        this.AddBusinessNamesAPI(this.JsonModel, 5);
      }
      else {
        this.translationService.getTranslation('VALIDATIONMESSAGES.BUSINESSNAMEAVAILABILITYEXISTS').subscribe((text: string) => {
          this.toast.error(text);
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.fifthFormGroup);
    }

  }

  sixthStakeholderInfoSubmit(values: any[]) {
    if (values.length > 0) {
      this.JsonModel.bnStackHolder = values;

      this.JsonModel["bnRequest"].stepNo = 6;
      this.AddBusinessNamesAPI(this.JsonModel, 6);

      console.log(this.JsonModel);
    }
    else {

    }
  }

  seventhAttachmentSubmit() {
    const controlArray = <FormArray>this.seventhFormGroup.get('attachments');
    for (let index = 0; index < controlArray.length; index++) {
      if (!this.getAttachmentsType[index].isMandatory) {
        controlArray.controls[index].get('attachmentSize').reset();
      }
    }
    if (this.seventhFormGroup.valid) {
      let attachmentValues = this.seventhFormGroup.value.attachments;
      attachmentValues.forEach(data => {
        data.bnid = this.JsonModel.bnRequest.id,
          data.bnrefNo = this.JsonModel.bnRequest.bnrefNo
      });
      this.JsonModel.bnAttachments = attachmentValues;
      this.JsonModel["bnRequest"].stepNo = 7;

      this.AddBusinessNamesAPI(this.JsonModel, 7);
    }
    else {
      this.validationMessages.validateAllFormFields(this.seventhFormGroup);
    }
  }

  AddBusinessNamesAPI(model, stepno: number) {
    this.loading = true;
    this.bnService.addBNRequest(model)
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.loading = false;
          this.cdr.markForCheck();
          if (stepno == 1) {
            this.JsonModel["bnRequest"].bnrefNo = data.refNo;
            this.JsonModel["bnRequest"].id = data.id;
            this.stepperBusinessName.selectedIndex = stepno; // redirecting on next step
          }
          else if (stepno == 7) {
            this.openHappinessMeterModel("happinessMeterModal");
          }
          else {
            this.stepperBusinessName.selectedIndex = stepno; // redirecting on next step
          }

        }
        else {
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toast.error(text);
          });
          this.loading = false;
          this.cdr.markForCheck();
        }
      })
  }

  openHappinessMeterModel(modelName: string) {
    this.modalService.open(modelName, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  happinessMeterClick() {

    let model = {
      "Id": this.JsonModel.bnRequest.id,
      "bnrefNo": this.JsonModel.bnRequest.bnrefNo,
      "rating": 2
    }

    this.bnService.addRating(model)
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.router.navigateByUrl('/pages/business-license/request-business-name/business-names');
        }
        else {
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toast.success(text);
          });
        }
      })


  }
}