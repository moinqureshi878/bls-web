import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestBusinessNameComponent } from './request-business-name.component';

describe('RequestBusinessNameComponent', () => {
  let component: RequestBusinessNameComponent;
  let fixture: ComponentFixture<RequestBusinessNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestBusinessNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestBusinessNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
