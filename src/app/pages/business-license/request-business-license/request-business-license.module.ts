import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '../../../views/partials/partials.module';
import { BnCommonComponentsModule } from '../bn-common-components/bn-common-components.module';
import { ViewSubmittedBnRequestComponent } from '../bn-common-components/model/view-submitted-bn-request/view-submitted-bn-request.component';
import { RequestBusinessLicenseComponent } from './request-business-license.component';
import { BusinessLicenseComponent } from './business-license/business-license.component';

import {
  MatSortModule,
} from '@angular/material';
import { RenewBusinessLicenseListComponent } from './model/renew-business-license-list/renew-business-license-list.component';
import { RenewBusinessLicenseComponent } from './model/renew-business-license/renew-business-license.component';
import { CancelBusinessLicenseComponent } from './model/cancel-business-license/cancel-business-license.component';
import { BusinessLicenseModificationListComponent } from './model/business-license-modification-list/business-license-modification-list.component';
import { BusinessLicenseModificationComponent } from './model/business-license-modification/business-license-modification.component';
import { AddBusinessLicenseComponent } from './add-business-license/add-business-license.component';


@NgModule({
  entryComponents: [ViewSubmittedBnRequestComponent],
  declarations: [RequestBusinessLicenseComponent, BusinessLicenseComponent, RenewBusinessLicenseListComponent, RenewBusinessLicenseComponent, CancelBusinessLicenseComponent, BusinessLicenseModificationListComponent, BusinessLicenseModificationComponent, AddBusinessLicenseComponent],
  exports: [],
  imports: [
    PartialsModule,
    MatSortModule,
    BnCommonComponentsModule,
    NgbModule.forRoot(),
    RouterModule.forChild([
      {
        path: '',
        component: RequestBusinessLicenseComponent
      },
      {
        path: 'business-license',
        component: BusinessLicenseComponent,
      },
      {
        path: 'add-business-license',
        component: AddBusinessLicenseComponent,
      },
      {
        path: 'renew-business-license',
        component: RenewBusinessLicenseComponent
      },
      {
        path: 'renew-business-license-list',
        component: RenewBusinessLicenseListComponent
      },
      {
        path: 'business-license-modification-list',
        component: BusinessLicenseModificationListComponent
      },
      {
        path: 'business-license-modification',
        component: BusinessLicenseModificationComponent
      },
      {
        path: 'cancel-business-license',
        component: CancelBusinessLicenseComponent
      },


    ]),
  ],

})
export class RequestBusinessLicenseModule { }
