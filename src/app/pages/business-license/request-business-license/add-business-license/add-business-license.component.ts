import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
// import { ViewSubmittedBnRequestComponent } from '../view-submitted-bn-request/view-submitted-bn-request.component';
// import { ViewSubmittedIaRequestComponent } from '../view-submitted-ia-request/view-submitted-ia-request.component';

export interface BusinessNames {
  iaAppNo: string;
  bnAr: string;
  bnEn: string;
  status: string;
  daysLeft: string;
  editView: string;
}

/** Builds and returns a new User. */

const ELEMENT_BUSINESS_DATA: BusinessNames[] = [
  { iaAppNo: 'IA-1', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'Approved', daysLeft: '10', editView: '', },
  { iaAppNo: 'IA-2', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'Approved', daysLeft: '10', editView: '', },
  { iaAppNo: 'IA-3', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'Approved', daysLeft: '10', editView: '', },
  { iaAppNo: 'IA-4', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'Approved', daysLeft: '60', editView: '', },

];


@Component({
  selector: 'kt-add-business-license',
  templateUrl: './add-business-license.component.html',
  styleUrls: ['./add-business-license.component.scss']
})
export class AddBusinessLicenseComponent implements OnInit {
  closeResult: string; //Bootstrap Modal Popup
  panelOpenState = false;
  radioButtonCondition: number = 1;
  emCopy: string = "ID front side";
  embCopy: string = "ID back side";
  passportCopy: string = "Passport info pages";
  visaCopy: string = "Visa page";
  //Stepper active class
  stepperValue: number = 1;

  //Form Group Stepper
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  eighthFormGroup: FormGroup;
  /* Checkbox Value Set to true */
  checkedBLS = true;


  displayedColumns8: string[] = ['iaAppNo', 'bnAr', 'bnEn', 'status', 'daysLeft', 'editView'];
  dataSource8 = new MatTableDataSource<BusinessNames>(ELEMENT_BUSINESS_DATA);
  @ViewChild('matPaginator8', { static: true }) paginator8: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort8: MatSort;

  constructor(
    private _formBuilder: FormBuilder,
    private modalService: NgbModal,
    public dialog: MatDialog) { }

  ngOnInit() {

    this.firstFormGroup = this._formBuilder.group({
    });
    this.secondFormGroup = this._formBuilder.group({
    });
    this.thirdFormGroup = this._formBuilder.group({
    });
    this.sixthFormGroup = this._formBuilder.group({
    });
    this.eighthFormGroup = this._formBuilder.group({
    });


    /* Data Table */
    this.dataSource8.paginator = this.paginator8;
    this.dataSource8.sort = this.sort8;
  }

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);


  getEmCopy(event) {
    this.emCopy = event.target.files[0].name.substring(0, 15);
  }
  getEmbId(event) {
    this.embCopy = event.target.files[0].name.substring(0, 15);
  }
  getPassportCopy(event) {
    this.passportCopy = event.target.files[0].name.substring(0, 15);
  }
  getVisa(event) {
    this.visaCopy = event.target.files[0].name.substring(0, 15);
  }

  //Stepper Value
  setStepperValue(value: number) {
    this.stepperValue = value;
  }


  //..Modal Popup Start
  open(content) {

    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 

  applyFilter8(filterValue: string) {
    this.dataSource8.filter = filterValue.trim().toLowerCase();
    if (this.dataSource8.paginator) {
      this.dataSource8.paginator.firstPage();
    }
  }

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

  /* begin:: Open Business Request */
  // openSubmittedBNRequest() {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.minWidth = "70%";
  //   dialogConfig.maxWidth = "70%";
  //   this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);

  // }
  /* end:: Open Business Request */

  /* begin:: Business Modification */
  // openBusinessModifications() {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.minWidth = "70%";
  //   dialogConfig.maxWidth = "70%";
  //   this.dialog.open(ViewSubmittedIaRequestComponent, dialogConfig);

  // }
  /* end:: Business Modification */

  //Radio Button Condtion
  setRadioButtonCondition(value: number) {
    this.radioButtonCondition = value;
  }
}