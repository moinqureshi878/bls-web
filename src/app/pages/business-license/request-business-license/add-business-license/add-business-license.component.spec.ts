import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBusinessLicenseComponent } from './add-business-license.component';

describe('AddBusinessLicenseComponent', () => {
  let component: AddBusinessLicenseComponent;
  let fixture: ComponentFixture<AddBusinessLicenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBusinessLicenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBusinessLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
