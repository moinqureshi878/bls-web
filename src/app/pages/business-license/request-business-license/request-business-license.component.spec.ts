import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestBusinessLicenseComponent } from './request-business-license.component';

describe('RequestBusinessLicenseComponent', () => {
  let component: RequestBusinessLicenseComponent;
  let fixture: ComponentFixture<RequestBusinessLicenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestBusinessLicenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestBusinessLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
