import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessLicenseModificationListComponent } from './business-license-modification-list.component';

describe('BusinessLicenseModificationListComponent', () => {
  let component: BusinessLicenseModificationListComponent;
  let fixture: ComponentFixture<BusinessLicenseModificationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessLicenseModificationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessLicenseModificationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
