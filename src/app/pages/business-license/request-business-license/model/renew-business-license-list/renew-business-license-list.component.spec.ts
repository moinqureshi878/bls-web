import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewBusinessLicenseListComponent } from './renew-business-license-list.component';

describe('RenewBusinessLicenseListComponent', () => {
  let component: RenewBusinessLicenseListComponent;
  let fixture: ComponentFixture<RenewBusinessLicenseListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenewBusinessLicenseListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenewBusinessLicenseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
