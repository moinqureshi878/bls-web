import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
import { ViewSubmittedBnRequestComponent } from '../../../bn-common-components/model/view-submitted-bn-request/view-submitted-bn-request.component';
// import { ViewSubmittedIaRequestComponent } from '../view-submitted-ia-request/view-submitted-ia-request.component';

/* Inspection */
export interface UserData {
  reportNo: string;
  relatedDocs: string;
  pending: string;
  action: string;
}

const ELEMENT_DATA: UserData[] = [
  { reportNo: '', relatedDocs: '', pending: '', action: '', },
  { reportNo: '', relatedDocs: '', pending: '', action: '', },
  { reportNo: '', relatedDocs: '', pending: '', action: '', },
  { reportNo: '', relatedDocs: '', pending: '', action: '', },
  { reportNo: '', relatedDocs: '', pending: '', action: '', },
];


/* Violation */
export interface ViolationsData {
  refNo: string;
  type: string;
  descAr: string;
  descEn: string;
  fineAmount: string;
  dueDate: string;
  payLinks: string;
}

const ELEMENT_DATA_VIOLATION: ViolationsData[] = [
  { refNo: '001', type: 'A', descAr: 'عربی تفصیل', descEn: 'English Desc', fineAmount: '100', dueDate: '20-20-2020', payLinks: 'Click for Payment' },
  { refNo: '002', type: 'B', descAr: 'عربی تفصیل', descEn: 'English Desc', fineAmount: '-', dueDate: '-', payLinks: '' },
  { refNo: '003', type: 'C', descAr: 'عربی تفصیل', descEn: 'English Desc', fineAmount: '600', dueDate: '20-20-2020', payLinks: 'Click for Payment' },
  { refNo: '004', type: 'D', descAr: 'عربی تفصیل', descEn: 'English Desc', fineAmount: '-', dueDate: '-', payLinks: '' },
  { refNo: '005', type: 'E', descAr: 'عربی تفصیل', descEn: 'English Desc', fineAmount: '250', dueDate: '20-20-2020', payLinks: 'Click for Payment' },
];

/* Government Entity Name */
export interface EntityData {
  refNo: string;
  govtEntity: string;
  notesAr: string;
  notesEn: string;
  fineAmount: string;
  payLinks: string;
}

const ELEMENT_DATA_ENTITY: EntityData[] = [
  { refNo: '1', govtEntity: 'ABC', notesAr: 'ملاحظات عربية', notesEn: 'Arabic Notes', fineAmount: '20', payLinks: 'Click for Payment' },
  { refNo: '2', govtEntity: 'ABC', notesAr: 'ملاحظات عربية', notesEn: 'Arabic Notes', fineAmount: '-', payLinks: '-' },
  { refNo: '3', govtEntity: 'XYZ', notesAr: 'ملاحظات عربية', notesEn: 'Arabic Notes', fineAmount: '100', payLinks: 'Click for Payment' },
  { refNo: '4', govtEntity: 'XYZ', notesAr: 'ملاحظات عربية', notesEn: 'Arabic Notes', fineAmount: '-', payLinks: '-' },
  { refNo: '5', govtEntity: 'ABC', notesAr: 'ملاحظات عربية', notesEn: 'Arabic Notes', fineAmount: '300', payLinks: 'Click for Payment' },
];

@Component({
  selector: 'kt-renew-business-license',
  templateUrl: './renew-business-license.component.html',
  styleUrls: ['./renew-business-license.component.scss']
})
export class RenewBusinessLicenseComponent implements OnInit {
  closeResult: string; //Bootstrap Modal Popup
  panelOpenState = false;
  radioButtonCondition: number = 1;
  emCopy: string = "ID front side";
  embCopy: string = "ID back side";
  passportCopy: string = "Passport info pages";
  visaCopy: string = "Visa page";
  //Stepper active class
  stepperValue: number = 1;

  partial: number = 0;
  //Form Group Stepper
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  forthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  eighthFormGroup: FormGroup;
  /* Checkbox Value Set to true */
  checkedBLS = true;

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);


  displayedColumns7: string[] = ['reportNo', 'action', 'relatedDocs', 'pending',];
  dataSource7 = new MatTableDataSource<UserData>(ELEMENT_DATA);
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;


  displayedColumns8: string[] = ['refNo', 'type', 'descAr', 'descEn', 'fineAmount', 'dueDate', 'payLinks',];
  dataSource8 = new MatTableDataSource<ViolationsData>(ELEMENT_DATA_VIOLATION);
  @ViewChild('matPaginator8', { static: true }) paginator8: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort8: MatSort;


  displayedColumns9: string[] = ['refNo', 'govtEntity', 'notesAr', 'notesEn', 'fineAmount', 'payLinks',];
  dataSource9 = new MatTableDataSource<EntityData>(ELEMENT_DATA_ENTITY);
  @ViewChild('matPaginator9', { static: true }) paginator9: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort9: MatSort;


  constructor(
    private _formBuilder: FormBuilder,
    private modalService: NgbModal,
    public dialog: MatDialog) { }

  ngOnInit() {

    this.firstFormGroup = this._formBuilder.group({
    });
    this.secondFormGroup = this._formBuilder.group({
    });
    this.thirdFormGroup = this._formBuilder.group({
    });
    this.forthFormGroup = this._formBuilder.group({
    });
    this.fifthFormGroup = this._formBuilder.group({
    });
    this.eighthFormGroup = this._formBuilder.group({
    });


    /* Data Table */

    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;

    this.dataSource8.paginator = this.paginator8;
    this.dataSource8.sort = this.sort8;
  }

  getEmCopy(event) {
    this.emCopy = event.target.files[0].name.substring(0, 15);
  }
  getEmbId(event) {
    this.embCopy = event.target.files[0].name.substring(0, 15);
  }
  getPassportCopy(event) {
    this.passportCopy = event.target.files[0].name.substring(0, 15);
  }
  getVisa(event) {
    this.visaCopy = event.target.files[0].name.substring(0, 15);
  }

  //Stepper Value
  setStepperValue(value: number) {
    this.stepperValue = value;
  }


  //..Modal Popup Start
  open(content) {

    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  applyFilter8(filterValue: string) {
    this.dataSource8.filter = filterValue.trim().toLowerCase();
    if (this.dataSource8.paginator) {
      this.dataSource8.paginator.firstPage();
    }
  }


  applyFilter9(filterValue: string) {
    this.dataSource9.filter = filterValue.trim().toLowerCase();
    if (this.dataSource9.paginator) {
      this.dataSource9.paginator.firstPage();
    }
  }

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

  /* begin:: Open Business Request */
  openSubmittedBNRequest() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);

  }
  /* end:: Open Business Request */

  /* begin:: Business Modification */
  openBusinessModifications() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    // this.dialog.open(ViewSubmittedIaRequestComponent, dialogConfig);

  }
  /* end:: Business Modification */

  //Radio Button Condtion
  setRadioButtonCondition(value: number) {
    this.radioButtonCondition = value;
  }

  partialValue(value: number) {
    this.partial = value;
  }

}