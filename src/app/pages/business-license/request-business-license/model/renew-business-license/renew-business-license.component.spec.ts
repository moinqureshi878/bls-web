import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewBusinessLicenseComponent } from './renew-business-license.component';

describe('RenewBusinessLicenseComponent', () => {
  let component: RenewBusinessLicenseComponent;
  let fixture: ComponentFixture<RenewBusinessLicenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenewBusinessLicenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenewBusinessLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
