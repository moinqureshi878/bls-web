import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelBusinessLicenseComponent } from './cancel-business-license.component';

describe('CancelBusinessLicenseComponent', () => {
  let component: CancelBusinessLicenseComponent;
  let fixture: ComponentFixture<CancelBusinessLicenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelBusinessLicenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelBusinessLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
