import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessLicenseModificationComponent } from './business-license-modification.component';

describe('BusinessLicenseModificationComponent', () => {
  let component: BusinessLicenseModificationComponent;
  let fixture: ComponentFixture<BusinessLicenseModificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessLicenseModificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessLicenseModificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
