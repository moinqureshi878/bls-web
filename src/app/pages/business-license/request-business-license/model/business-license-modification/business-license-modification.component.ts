import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
// import { ViewSubmittedBnRequestComponent } from '../view-submitted-bn-request/view-submitted-bn-request.component';
// import { ViewSubmittedIaRequestComponent } from '../view-submitted-ia-request/view-submitted-ia-request.component';


@Component({
  selector: 'kt-business-license-modification',
  templateUrl: './business-license-modification.component.html',
  styleUrls: ['./business-license-modification.component.scss']
})
export class BusinessLicenseModificationComponent implements OnInit {
  closeResult: string; //Bootstrap Modal Popup
  panelOpenState = false;
  radioButtonCondition: number = 1;
  emCopy: string = "ID front side";
  embCopy: string = "ID back side";
  passportCopy: string = "Passport info pages";
  visaCopy: string = "Visa page";
  //Stepper active class
  stepperValue: number = 1;

  partial: number = 0;
  //Form Group Stepper
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  forthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  seventhFormGroup: FormGroup;
  /* Checkbox Value Set to true */
  checkedBLS = true;

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);


 


  constructor(
    private _formBuilder: FormBuilder,
    private modalService: NgbModal,
    public dialog: MatDialog) { }

  ngOnInit() {

    this.firstFormGroup = this._formBuilder.group({
    });
    this.secondFormGroup = this._formBuilder.group({
    });
    this.thirdFormGroup = this._formBuilder.group({
    });
    this.forthFormGroup = this._formBuilder.group({
    });
    this.fifthFormGroup = this._formBuilder.group({
    });
    this.sixthFormGroup = this._formBuilder.group({
    });
    this.seventhFormGroup = this._formBuilder.group({
    });


   
  }

  //Stepper Value
  setStepperValue(value: number) {
    this.stepperValue = value;
  }


  //..Modal Popup Start
  open(content) {
    
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 

  

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

  /* begin:: Open Business Request */
  // openSubmittedBNRequest() {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.minWidth = "70%";
  //   dialogConfig.maxWidth = "70%";
  //   this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);

  // }
  /* end:: Open Business Request */

  /* begin:: Business Modification */
  // openBusinessModifications() {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.minWidth = "70%";
  //   dialogConfig.maxWidth = "70%";
  //   this.dialog.open(ViewSubmittedIaRequestComponent, dialogConfig);

  // }
  /* end:: Business Modification */



}