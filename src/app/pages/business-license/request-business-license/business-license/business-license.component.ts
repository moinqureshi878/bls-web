
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';
// import { ExtendBusinessLicenseComponent } from '../extend-business-license/extend-business-license.component';
// import { BusinessLicenseModificationListComponent } from '../business-license-modification-list/business-license-modification-list.component';
// import { CancelBusinessLicenseComponent } from '../cancel-business-license/cancel-business-license.component';

export interface UserData {
  iaAppNo: string;
  bnAr: string;
  bnEn: string;
  status: string;
  daysLeft: string;
  iaNo: string;
  editView: string;
}

/** Builds and returns a new User. */

const ELEMENT_DATA: UserData[] = [
  { iaAppNo: 'BLN-1-1', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'Draft', daysLeft: '', iaNo: 'IA-1', editView: 'viewDelete', },
  { iaAppNo: 'BLN-2-1', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'Submitted', daysLeft: '', iaNo: 'IA-2', editView: 'viewDelete', },
  { iaAppNo: 'BL-3', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'Approved', daysLeft: '', iaNo: 'IA-3', editView: 'editDeleteUpdate', },
  { iaAppNo: 'BL-4', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'Approved', daysLeft: '', iaNo: 'IA-4', editView: 'editDeleteUpdate', },
  { iaAppNo: 'BL-5-2', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'Returned', daysLeft: '', iaNo: 'IA-5', editView: 'Returned', },
  { iaAppNo: 'BL-6', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'payment is due', daysLeft: '', iaNo: 'IA-6', editView: 'Process', },
];


/* Expired Soon */
export interface OuData {

  iaAppNo: string;
  bnAr: string;
  bnEn: string;
  iaNo: string;
  status: string;
  statusDate: string;
  expCanDate: string;
  submissionDate: string;
  editView: string;

}

const ELEMENT_OU_DATA: OuData[] = [
  { iaAppNo: 'BLR-10', bnAr: 'الاسم التجاري', bnEn: 'Business Name', iaNo: 'IA-1', submissionDate: '', status: 'Rejected', statusDate: '12/10/2019', expCanDate: '', editView: 'Rejected' },
  { iaAppNo: 'BLC-12', bnAr: 'الاسم التجاري', bnEn: 'Business Name', iaNo: 'IA-2', submissionDate: '', status: 'Cancelled', statusDate: '03/08/2019', expCanDate: '', editView: 'Cancelled' },
  { iaAppNo: 'BL-13', bnAr: 'الاسم التجاري', bnEn: 'Business Name', iaNo: 'IA-3', submissionDate: '', status: 'Expired', statusDate: '12/10/2019', expCanDate: '', editView: 'Expired' },
  { iaAppNo: 'BL-15', bnAr: 'الاسم التجاري', bnEn: 'Business Name', iaNo: 'IA-4', submissionDate: '', status: 'Expired', statusDate: '16/09/2019', expCanDate: '', editView: 'Expired' },
  { iaAppNo: 'BLC-17', bnAr: 'الاسم التجاري', bnEn: 'Business Name', iaNo: 'IA-5', submissionDate: '', status: 'Cancelled', statusDate: '01/10/2019', expCanDate: '', editView: 'Cancelled' },


];

@Component({
  selector: 'kt-business-license',
  templateUrl: './business-license.component.html',
  styleUrls: ['./business-license.component.scss']
})
export class BusinessLicenseComponent implements OnInit {

  isShown: boolean = false; // hidden by default
  isHidden: boolean = true;

  displayedColumns7: string[] = ['iaAppNo', 'bnAr', 'bnEn', 'iaNo', 'status', 'daysLeft', 'editView'];
  dataSource7 = new MatTableDataSource<UserData>(ELEMENT_DATA);
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;

  closeResult: string; //Bootstrap Modal Popup

  displayedColumnsOU: string[] = ['iaAppNo', 'bnAr', 'bnEn', 'iaNo', 'submissionDate', 'status', 'statusDate', 'editView'];
  dataSourceOU = new MatTableDataSource<OuData>(ELEMENT_OU_DATA);
  @ViewChild('matPaginatorOU', { static: true }) paginatorOU: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortOU: MatSort;
  constructor(

    private modalService: NgbModal,
    public dialog: MatDialog) {

  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;

    this.dataSourceOU.paginator = this.paginatorOU;
    this.dataSourceOU.sort = this.sortOU;
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();

    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  applyFilterOU(filterValue: string) {
    this.dataSourceOU.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceOU.paginator) {
      this.dataSourceOU.paginator.firstPage();
    }
  }




  //..Modal Popup Start
  open(content) {
    
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 


  /* begin:: Display ExtendGrid */
  // openExtendInitialApprovals() {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.minWidth = "60%";
  //   dialogConfig.maxWidth = "60%";
  //   this.dialog.open(ExtendBusinessLicenseComponent, dialogConfig);

  // }
  /* end:: Display ExtendGrid */

  /* begin:: Display Cancel */
  // openCancelInitialApprovals() {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.minWidth = "60%";
  //   dialogConfig.maxWidth = "60%";
  //   this.dialog.open(CancelBusinessLicenseComponent, dialogConfig);

  // }
  /* end:: Display Cancel */

  /* begin:: Display ExtendGrid */
  // openModifyInitialApprovals() {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.minWidth = "60%";
  //   dialogConfig.maxWidth = "60%";
  //   this.dialog.open(BusinessLicenseModificationListComponent, dialogConfig);

  // }
  /* end:: Display ExtendGrid */

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

  toggleShow() {
    this.isHidden = !this.isHidden;
    this.isShown = !this.isShown;

  }

}