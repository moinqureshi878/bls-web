import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '../../../views/partials/partials.module';

import { InitialApprovalsComponent } from './initial-approvals.component';
// import { NotesHistoryModalComponent } from '../bn-common-components/notes-history-modal/notes-history-modal.component';
// import { ViewSubmittedBnRequestComponent } from '../bn-common-components/model/view-submitted-bn-request/view-submitted-bn-request.component';
// import { ViewActionsComponent } from '../bn-common-components/model/view-actions/view-actions.component';
// import { FwdForActionComponent } from '../bn-common-components/model/fwd-for-action/fwd-for-action.component';
// import { ReturnBackNotesComponent } from '../bn-common-components/model/return-back-notes/return-back-notes.component';
// import { RejectedBusinessModalComponent } from '../bn-common-components/model/rejected-business-modal/rejected-business-modal.component';
// import { ReviewAttachmentModalComponent } from '../bn-common-components/model/review-attachment-modal/review-attachment-modal.component';
// import { FilterBlComponent } from './manage-business-names/manage-bn-component/filter-bl/filter-bl.component';
// import { MatTableExporterModule } from 'mat-table-exporter';
// import { AuthGuard } from 'app/core/auth';
// import { BnResolver } from 'app/core/resolvers/bn.resolver';
import { BnCommonComponentsModule } from '../bn-common-components/bn-common-components.module';
import { ManageInitialApprovalsComponent } from './manage-initial-approvals/manage-initial-approvals.component';
import { ManageNewIaComponent } from './manage-initial-approvals/manage-ia-component/manage-new-ia/manage-new-ia.component';
import { ManageExtendIaComponent } from './manage-initial-approvals/manage-ia-component/manage-extend-ia/manage-extend-ia.component';
import { ManageModifiedIaComponent } from './manage-initial-approvals/manage-ia-component/manage-modified-ia/manage-modified-ia.component';
import { ManageCancelledIaComponent } from './manage-initial-approvals/manage-ia-component/manage-cancelled-ia/manage-cancelled-ia.component';
import { ManageApprovedIaComponent } from './manage-initial-approvals/manage-ia-component/manage-approved-ia/manage-approved-ia.component';
import { ReviewIaRequestDetailComponent } from './review-ia-request-detail/review-ia-request-detail.component';
import { ReviewExtendIaRequestDetailComponent } from './review-extend-ia-request-detail/review-extend-ia-request-detail.component';
import { ReviewModifiedIaRequestDetailComponent } from './review-modified-ia-request-detail/review-modified-ia-request-detail.component';
import {
  MatSortModule,
} from '@angular/material';

@NgModule({
  entryComponents: [],
  declarations: [
    InitialApprovalsComponent,
    ManageInitialApprovalsComponent,
    ManageNewIaComponent,
    ManageExtendIaComponent,
    ManageModifiedIaComponent,
    ManageCancelledIaComponent,
    ManageApprovedIaComponent,
    ReviewIaRequestDetailComponent,
    ReviewExtendIaRequestDetailComponent,
    ReviewModifiedIaRequestDetailComponent,
  ],
  imports: [
    PartialsModule,
    NgbModule.forRoot(),
    BnCommonComponentsModule,
    MatSortModule,
    RouterModule.forChild([
      {
        path: '',
        component: InitialApprovalsComponent
      },
      {
        path: 'manage-initial-approvals',
        component: ManageInitialApprovalsComponent,

      },
      {
        path: 'review-ia-request-detail',
        component: ReviewIaRequestDetailComponent,

      },
      {
        path: 'review-extend-ia-request-detail',
        component: ReviewExtendIaRequestDetailComponent
      },
      {
        path: 'review-modified-ia-request-detail',
        component: ReviewModifiedIaRequestDetailComponent
      },
    ]),
  ]
})
export class InitialApprovalsModule { }
