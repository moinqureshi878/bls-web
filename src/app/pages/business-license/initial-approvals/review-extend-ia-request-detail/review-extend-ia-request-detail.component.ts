import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
import { RejectedBusinessModalComponent } from '../../bn-common-components/model/rejected-business-modal/rejected-business-modal.component';
import { ViewActionsComponent } from '../../bn-common-components/model/view-actions/view-actions.component';
import { ViewSubmittedIaRequestComponent } from '../../bn-common-components/model/view-submitted-ia-request/view-submitted-ia-request.component';
import { FwdForActionComponent } from '../../bn-common-components/model/fwd-for-action/fwd-for-action.component';
import { RequestForInspectionComponent } from '../../bn-common-components/model/request-for-inspection/request-for-inspection.component';
import { ReturnBackNotesComponent } from "../../bn-common-components/model/return-back-notes/return-back-notes.component";
import { ViewSubmittedBnRequestComponent } from "../../bn-common-components/model/view-submitted-bn-request/view-submitted-bn-request.component";

@Component({
  selector: 'kt-review-extend-ia-request-detail',
  templateUrl: './review-extend-ia-request-detail.component.html',
  styleUrls: ['./review-extend-ia-request-detail.component.scss']
})
export class ReviewExtendIaRequestDetailComponent implements OnInit {

  constructor(private _formBuilder: FormBuilder, public dialog: MatDialog) { }

  ngOnInit() {

  }

  /* Checkbox Value Set to true */
  checkedBLS = true;

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);

  /* begin:: Rejected Pages Popup */
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    this.dialog.open(RejectedBusinessModalComponent, dialogConfig);

  }
  /* end:: Rejected Pages Popup */


  /* begin:: Business Modification */
  openBusinessModifications() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewSubmittedIaRequestComponent, dialogConfig);

  }
  /* end:: Business Modification */

  /* begin:: Open Business Request */
  openSubmittedBNRequest() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);

  }
  /* end:: Open Business Request */

  /* begin:: View Actions */
  openViewActions() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewActionsComponent, dialogConfig);

  }
  /* end:: View Action */

  /* begin:: Forward for Action Popup */
  openFwdActionDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    this.dialog.open(FwdForActionComponent, dialogConfig);

  }
  /* end:: Forward for Action Popup */

  /* begin:: Request for Inspection Popup */
  openRequestActionDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    this.dialog.open(RequestForInspectionComponent, dialogConfig);

  }
  /* end:: Request for Inspection Popup */


  /* begin:: Return Back Notes Popup */
  openReturnBackNotesDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    this.dialog.open(ReturnBackNotesComponent, dialogConfig);

  }
  /* end:: Return Back Notes Popup */


  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);
}