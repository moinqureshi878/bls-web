import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewExtendIaRequestDetailComponent } from './review-extend-ia-request-detail.component';

describe('ReviewExtendIaRequestDetailComponent', () => {
  let component: ReviewExtendIaRequestDetailComponent;
  let fixture: ComponentFixture<ReviewExtendIaRequestDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewExtendIaRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewExtendIaRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
