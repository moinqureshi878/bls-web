import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewModifiedIaRequestDetailComponent } from './review-modified-ia-request-detail.component';

describe('ReviewModifiedIaRequestDetailComponent', () => {
  let component: ReviewModifiedIaRequestDetailComponent;
  let fixture: ComponentFixture<ReviewModifiedIaRequestDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewModifiedIaRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewModifiedIaRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
