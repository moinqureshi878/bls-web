import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
import { ViewActionsComponent } from '../../bn-common-components/model/view-actions/view-actions.component';
import { RejectedBusinessModalComponent } from '../../bn-common-components/model/rejected-business-modal/rejected-business-modal.component';
import { ViewSubmittedIaRequestComponent } from '../../bn-common-components/model/view-submitted-ia-request/view-submitted-ia-request.component';
import { FwdForActionComponent } from '../../bn-common-components/model/fwd-for-action/fwd-for-action.component';
import { ReturnBackNotesComponent } from "../../bn-common-components/model/return-back-notes/return-back-notes.component";
import { ViewSubmittedBnRequestComponent } from "../../bn-common-components/model/view-submitted-bn-request/view-submitted-bn-request.component";
import { RequestForInspectionComponent } from '../../bn-common-components/model/request-for-inspection/request-for-inspection.component';

export interface BusinessNames {
  bnAppNo: string;
  bnAr: string;
  bnEn: string;
  status: string;
  isLinkedIA: string;
  isLinkedBL: string;
  daysLeft: string;
  editView: string;
}

/** Builds and returns a new User. */

const ELEMENT_BUSINESS_DATA: BusinessNames[] = [
  { bnAppNo: 'BN-1', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'Approved', isLinkedIA: 'None', isLinkedBL: 'None', daysLeft: '10', editView: '', },
  { bnAppNo: 'BN-2', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'Approved', isLinkedIA: 'None', isLinkedBL: 'None', daysLeft: '10', editView: '', },
  { bnAppNo: 'BN-3', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'Approved', isLinkedIA: 'None', isLinkedBL: 'None', daysLeft: '10', editView: '', },
  { bnAppNo: 'BN-4', bnAr: 'الاسم التجاري', bnEn: 'Business Name', status: 'Approved', isLinkedIA: 'None', isLinkedBL: 'None', daysLeft: '60', editView: '', },

];


export interface UserData {
  isic4: string;
  arDescription: string;
  enDescription: string;


}

/** Builds and returns a new User. */

const ELEMENT_DATA: UserData[] = [
  { isic4: '1', arDescription: '1', enDescription: '1' },
  { isic4: '2', arDescription: '2', enDescription: '2' },
  { isic4: '3', arDescription: '3', enDescription: '3' }
];



export interface stakeHoldersData {
  orderNo: string;
  type: string;
  relationship: string;
  nameAr: string;
  nameEn: string;
  share: string;
  registered: string;
}

/** Builds and returns a new User. */

const ELEMENT_STAKEHOLDERS_DATA: stakeHoldersData[] = [
  { orderNo: '1', type: 'Individual', relationship: 'Owner', nameAr: ' Name Ar', nameEn: ' Name En', share: '25%', registered: 'Yes', },
  { orderNo: '2', type: 'Govt Entity', relationship: 'Partner', nameAr: ' Name Ar', nameEn: ' Name En', share: '25%', registered: 'No', },
  { orderNo: '3', type: 'License', relationship: 'Responsible Manager', nameAr: ' Name Ar', nameEn: ' Name En', share: '25%', registered: 'Yes', },


];

@Component({
  selector: 'kt-review-modified-ia-request-detail',
  templateUrl: './review-modified-ia-request-detail.component.html',
  styleUrls: ['./review-modified-ia-request-detail.component.scss']
})
export class ReviewModifiedIaRequestDetailComponent implements OnInit {

  closeResult: string; //Bootstrap Modal Popup
  panelOpenState = false;
  radioButtonCondition: number = 0;
  emCopy: string = "ID front side";
  embCopy: string = "ID back side";
  passportCopy: string = "Passport info pages";
  visaCopy: string = "Visa page";
  //Stepper active class
  stepperValue: number = 1;

  //Form Group Stepper
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  seventhFormGroup: FormGroup;
  eighthFormGroup: FormGroup;
  /* Checkbox Value Set to true */
  checkedBLS = true;


  displayedColumns8: string[] = ['bnAppNo', 'bnAr', 'bnEn', 'isLinkedIA', 'isLinkedBL', 'status', 'daysLeft', 'editView'];
  dataSource8 = new MatTableDataSource<BusinessNames>(ELEMENT_BUSINESS_DATA);
  @ViewChild('matPaginator8', { static: true }) paginator8: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort8: MatSort;


  displayedColumns7: string[] = ['isic4', 'arDescription', 'enDescription'];
  dataSource7 = new MatTableDataSource<UserData>(ELEMENT_DATA);
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;

  stakeHoldersdisplayedColumns7: string[] = ['orderNo', 'type', 'relationship', 'nameAr', 'nameEn', 'share', 'registered',];
  stakeHoldersdataSource7 = new MatTableDataSource<stakeHoldersData>(ELEMENT_STAKEHOLDERS_DATA);
  @ViewChild('stakeHoldersmatPaginator7', { static: true }) stakeHolderspaginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) stakeHolderssort7: MatSort;

  constructor(
    private _formBuilder: FormBuilder,
    private modalService: NgbModal,
    public dialog: MatDialog) { }

  ngOnInit() {

    this.firstFormGroup = this._formBuilder.group({
      // firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      // secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      // thirdCtrl: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      // fourthCtrl: ['', Validators.required]
    });
    this.fifthFormGroup = this._formBuilder.group({
      // fifthCtrl: ['', Validators.required]
    });
    this.sixthFormGroup = this._formBuilder.group({
      // sixthCtrl: ['', Validators.required]
    });

    this.seventhFormGroup = this._formBuilder.group({
      // sixthCtrl: ['', Validators.required]
    });
    this.eighthFormGroup = this._formBuilder.group({
      // sixthCtrl: ['', Validators.required]
    });

    /* Data Table */
    this.dataSource8.paginator = this.paginator8;
    this.dataSource8.sort = this.sort8;


    /* Data Table */
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;

    /* Data Table */
    this.stakeHoldersdataSource7.paginator = this.stakeHolderspaginator7;
    this.stakeHoldersdataSource7.sort = this.stakeHolderssort7;
  }

  //Disable SelectBox / Checkbox
  disableSelect = new FormControl(false);

  /* Emirates ID Mask */
  emiratesIDMask = [/[0-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/]



  getEmCopy(event) {
    this.emCopy = event.target.files[0].name.substring(0, 15);
    // + "." + event.target.files[0].name.split('.')[1]
  }
  getEmbId(event) {
    this.embCopy = event.target.files[0].name.substring(0, 15);
  }
  getPassportCopy(event) {
    this.passportCopy = event.target.files[0].name.substring(0, 15);
  }
  getVisa(event) {
    this.visaCopy = event.target.files[0].name.substring(0, 15);
  }


  //Stepper Value
  setStepperValue(value: number) {
    this.stepperValue = value;
  }
  //Radio Button Condtion
  setRadioButtonCondition(value: number) {
    this.radioButtonCondition = value;
  }

  //..Modal Popup Start
  open(content) {
    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End 


  applyFilter8(filterValue: string) {
    this.dataSource8.filter = filterValue.trim().toLowerCase();
    if (this.dataSource8.paginator) {
      this.dataSource8.paginator.firstPage();
    }
  }


  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();

    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  stakeHoldersapplyFilter7(filterValue: string) {
    this.stakeHoldersdataSource7.filter = filterValue.trim().toLowerCase();

    if (this.stakeHoldersdataSource7.paginator) {
      this.stakeHoldersdataSource7.paginator.firstPage();
    }
  }

  /* begin:: View Actions */
  openViewActions() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewActionsComponent, dialogConfig);

  }
  /* end:: View Action */

  /* begin:: Rejected Pages Popup */
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    this.dialog.open(RejectedBusinessModalComponent, dialogConfig);

  }
  /* end:: Rejected Pages Popup */


  /* begin:: Business Modification */
  openBusinessModifications() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewSubmittedIaRequestComponent, dialogConfig);

  }
  /* end:: Business Modification */
  /* begin:: Forward for Action Popup */
  openFwdActionDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    this.dialog.open(FwdForActionComponent, dialogConfig);

  }
  /* end:: Forward for Action Popup */


  /* begin:: Request for Inspection Popup */
  openRequestActionDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    this.dialog.open(RequestForInspectionComponent, dialogConfig);

  }
  /* end:: Request for Inspection Popup */

  /* begin:: Return Back Notes Popup */
  openReturnBackNotesDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "30%";
    this.dialog.open(ReturnBackNotesComponent, dialogConfig);

  }
  /* end:: Return Back Notes Popup */

  /* begin:: Open Business Request */
  openSubmittedBNRequest() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";
    this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);

  }
  /* end:: Open Business Request */
  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);

}