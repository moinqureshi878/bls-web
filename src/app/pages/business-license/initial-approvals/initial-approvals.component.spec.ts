import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitialApprovalsComponent } from './initial-approvals.component';

describe('InitialApprovalsComponent', () => {
  let component: InitialApprovalsComponent;
  let fixture: ComponentFixture<InitialApprovalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitialApprovalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitialApprovalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
