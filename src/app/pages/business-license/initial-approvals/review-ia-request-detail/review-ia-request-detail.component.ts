import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ReturnBackNotesComponent } from "../../bn-common-components/model/return-back-notes/return-back-notes.component";
import { RequestForInspectionComponent } from '../../bn-common-components/model/request-for-inspection/request-for-inspection.component';
import { RejectedBusinessModalComponent } from '../../bn-common-components/model/rejected-business-modal/rejected-business-modal.component';
import { ViewSubmittedBnRequestComponent } from '../../bn-common-components/model/view-submitted-bn-request/view-submitted-bn-request.component';
import { ViewActionsComponent } from '../../bn-common-components/model/view-actions/view-actions.component';
import { FwdForActionComponent } from '../../bn-common-components/model/fwd-for-action/fwd-for-action.component';
import { BnService } from 'app/core/appServices/bn.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from 'app/core/_base/layout';
import { ReviewAttachmentModalComponent } from 'app/pages/admin/review-attachment-modal/review-attachment-modal.component';

@Component({
  selector: 'kt-review-ia-request-detail',
  templateUrl: './review-ia-request-detail.component.html',
  styleUrls: ['./review-ia-request-detail.component.scss']
})
export class ReviewIaRequestDetailComponent implements OnInit {
  //Stepper active class
  stepperValue: number = 1;
  //Form Group Stepper
  firstFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;

  bnInfoloading: boolean = false;
  stickyNotes: any;
  getBNData: any;
  postJson = {
    "workFlow": {
      "code": 0,
      "userId": 0,
      "designation": 0,
      "remarks": "",
      "reasonIds": [0]
    },
    "stepNo": 0,
    "applicationRefNo": "",
    "bnid": 0,
    "stickyNotes": "",
    "applicationDataControls": [],
    "businessActivity": [],
    "businessNamesControls": [],
    "shortNames": [],
    "stakeHolders": [],
    "attachments": []
  }
  constructor(
    public bnService: BnService,
    private cdr: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    public dialog: MatDialog,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private toast: ToastrService,
    // public businessLicenseService: BusinessLicenseService,
    private translationService: TranslationService,
    private router: Router) {

  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({});
    this.fourthFormGroup = this._formBuilder.group({});
    this.sixthFormGroup = this._formBuilder.group({});
  }
  updateAttachmentsData(updatedAttachments) {
    this.getBNData.attachments = updatedAttachments;
  }
  attachmentsFormSubmit() {
    this.router.navigateByUrl('/pages/business-license/initial-approvals/manage-initial-approvals');
  }
  openDialog(...dialogName) {
    const dialogConfig = new MatDialogConfig();
    let dialogRef;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "70%";
    dialogConfig.maxWidth = "70%";

    if (dialogName[0] == 'viewAction') {
      dialogRef = this.dialog.open(ViewActionsComponent, dialogConfig);
    }
    else if (dialogName[0] == 'returnNotes') {
      // dialogConfig.data = this.notes || '';
      dialogRef = this.dialog.open(ReturnBackNotesComponent, dialogConfig);
    }
    else if (dialogName[0] == 'reject') {
      dialogRef = this.dialog.open(RejectedBusinessModalComponent, dialogConfig);
    }
    else if (dialogName[0] == 'attachment') {
      dialogConfig.data = dialogName[1];
      dialogRef = this.dialog.open(ReviewAttachmentModalComponent, dialogConfig);
    }
    else if (dialogName[0] == 'fwdAction') {
      dialogRef = this.dialog.open(FwdForActionComponent, dialogConfig);
    }
    else if (dialogName[0] == 'requestForInspection') {
      this.dialog.open(RequestForInspectionComponent, dialogConfig);
    }
    else if (dialogName[0] == 'viewBNInfo') {
      this.bnInfoloading = true;
      this.bnService.GetBNRequest(6).subscribe(res => {
        this.bnInfoloading = false;
        this.cdr.markForCheck();
        let response = res;
        if (response.code == 200 || response.result.code == 200) {
          let bnData = response.data;
          dialogConfig.data = { bnData };
          this.dialog.open(ViewSubmittedBnRequestComponent, dialogConfig);
        }
      });
    }
    dialogRef.componentInstance.action.subscribe(resp => {
      debugger
      this.postJson = resp.postJson;
      //this.submitRequest();
    });

  }
  private submitRequest() {
    this.bnService.AddWorkFlow(this.postJson).subscribe(data => {
      let response: any = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {
        this.dialog.closeAll();
        this.router.navigateByUrl('/pages/business-license/initial-approvals/manage-initial-approvals');
      }
      else {
        this.translationService.getTranslation(String(response.result ? response.result.code : response.code)).subscribe((text: string) => {
          this.toast.error(text);
        });
      }
    });
  }

  notesEvent(value) {
    this.stickyNotes = value;
    this.postJson.stickyNotes = this.stickyNotes;
  }
}