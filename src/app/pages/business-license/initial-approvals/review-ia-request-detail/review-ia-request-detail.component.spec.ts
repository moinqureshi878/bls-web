import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewIaRequestDetailComponent } from './review-ia-request-detail.component';

describe('ReviewIaRequestDetailComponent', () => {
  let component: ReviewIaRequestDetailComponent;
  let fixture: ComponentFixture<ReviewIaRequestDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewIaRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewIaRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
