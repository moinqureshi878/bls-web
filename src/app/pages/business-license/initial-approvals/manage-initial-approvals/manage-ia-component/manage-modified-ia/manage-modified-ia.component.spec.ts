import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageModifiedIaComponent } from './manage-modified-ia.component';

describe('ManageModifiedIaComponent', () => {
  let component: ManageModifiedIaComponent;
  let fixture: ComponentFixture<ManageModifiedIaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageModifiedIaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageModifiedIaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
