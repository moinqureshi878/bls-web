
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl } from '@angular/forms';

/* Expired Soon */
export interface OuData {

  bnIAN: string;
  bnAr: string;
  bnEn: string;
  submissionDate: string;
  status: string;
  statusDate: string;
  expCanDate: string;
  editView: string;

}

const ELEMENT_OU_DATA: OuData[] = [
  { bnIAN: 'IAN-10-1', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'New IA', statusDate: '12/10/2019', expCanDate: '', editView: 'Rejected' },
  { bnIAN: 'IAN-12-1', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Under Process', statusDate: '03/08/2019', expCanDate: '', editView: 'Cancelled' },
  { bnIAN: 'IAN-13-1', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Pending at Customere', statusDate: '12/10/2019', expCanDate: '', editView: 'Expired' },
  { bnIAN: 'IAN-15-2', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Pending at Manager', statusDate: '16/09/2019', expCanDate: '', editView: 'Expired' },
  { bnIAN: 'IAN-17-3', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', status: 'Pending at Consultant', statusDate: '01/10/2019', expCanDate: '', editView: 'Cancelled' },
];

@Component({
  selector: 'kt-manage-new-ia',
  templateUrl: './manage-new-ia.component.html',
  styleUrls: ['./manage-new-ia.component.scss']
})
export class ManageNewIaComponent implements OnInit {

  displayedColumnsOU: string[] = ['bnIAN', 'bnAr', 'bnEn', 'submissionDate', 'status', 'statusDate', 'editView'];
  dataSourceOU = new MatTableDataSource<OuData>(ELEMENT_OU_DATA);
  @ViewChild('matPaginatorOU', { static: true }) paginatorOU: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortOU: MatSort;
  constructor() {

  }

  ngOnInit() {


    this.dataSourceOU.paginator = this.paginatorOU;
    this.dataSourceOU.sort = this.sortOU;
  }


  applyFilterOU(filterValue: string) {
    this.dataSourceOU.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceOU.paginator) {
      this.dataSourceOU.paginator.firstPage();
    }
  }

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);


}