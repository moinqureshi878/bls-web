import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageNewIaComponent } from './manage-new-ia.component';

describe('ManageNewIaComponent', () => {
  let component: ManageNewIaComponent;
  let fixture: ComponentFixture<ManageNewIaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageNewIaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageNewIaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
