import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageExtendIaComponent } from './manage-extend-ia.component';

describe('ManageExtendIaComponent', () => {
  let component: ManageExtendIaComponent;
  let fixture: ComponentFixture<ManageExtendIaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageExtendIaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageExtendIaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
