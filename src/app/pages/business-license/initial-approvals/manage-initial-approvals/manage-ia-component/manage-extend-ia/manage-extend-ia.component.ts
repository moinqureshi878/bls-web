
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl } from '@angular/forms';

/* Expired Soon */
export interface OuData {

  bnBnr: string;
  bnAr: string;
  bnEn: string;
  submissionDate: string;
  status: string;
  statusDate: string;
  expCanDate: string;
  extendedDays: string;
  editView: string;

}

const ELEMENT_OU_DATA: OuData[] = [
  { bnBnr: 'IAN-10-1', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', extendedDays: '10', status: 'Extended IA', statusDate: '12/10/2019', expCanDate: '', editView: '' },
  { bnBnr: 'IAN-12-1', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', extendedDays: '12', status: 'Under Process', statusDate: '03/08/2019', expCanDate: '', editView: '' },
  { bnBnr: 'IAN-13-2', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', extendedDays: '15', status: 'Pending at Customere', statusDate: '12/10/2019', expCanDate: '', editView: '' },
  { bnBnr: 'IAN-15-2', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', extendedDays: '10', status: 'Pending at Manager', statusDate: '16/09/2019', expCanDate: '', editView: '' },
  { bnBnr: 'IAN-17-3', bnAr: 'الاسم التجاري', bnEn: 'Business Name', submissionDate: '', extendedDays: '25', status: 'Pending at Consultant', statusDate: '01/10/2019', expCanDate: '', editView: '' },

];
@Component({
  selector: 'kt-manage-extend-ia',
  templateUrl: './manage-extend-ia.component.html',
  styleUrls: ['./manage-extend-ia.component.scss']
})
export class ManageExtendIaComponent implements OnInit {

  displayedColumnsOU: string[] = ['bnBnr', 'bnAr', 'bnEn', 'submissionDate', 'status', 'statusDate', 'extendedDays', 'editView'];
  dataSourceOU = new MatTableDataSource<OuData>(ELEMENT_OU_DATA);
  @ViewChild('matPaginatorOU', { static: true }) paginatorOU: MatPaginator;
  @ViewChild(MatSort, { static: true }) sortOU: MatSort;
  constructor() {

  }

  ngOnInit() {


    this.dataSourceOU.paginator = this.paginatorOU;
    this.dataSourceOU.sort = this.sortOU;
  }


  applyFilterOU(filterValue: string) {
    this.dataSourceOU.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceOU.paginator) {
      this.dataSourceOU.paginator.firstPage();
    }
  }

  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);


}