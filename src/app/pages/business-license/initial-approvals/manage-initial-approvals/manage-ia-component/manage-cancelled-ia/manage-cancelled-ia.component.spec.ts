import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCancelledIaComponent } from './manage-cancelled-ia.component';

describe('ManageCancelledIaComponent', () => {
  let component: ManageCancelledIaComponent;
  let fixture: ComponentFixture<ManageCancelledIaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageCancelledIaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCancelledIaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
