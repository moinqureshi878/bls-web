import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageApprovedIaComponent } from './manage-approved-ia.component';

describe('ManageApprovedIaComponent', () => {
  let component: ManageApprovedIaComponent;
  let fixture: ComponentFixture<ManageApprovedIaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageApprovedIaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageApprovedIaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
