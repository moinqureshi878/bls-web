import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageInitialApprovalsComponent } from './manage-initial-approvals.component';

describe('ManageInitialApprovalsComponent', () => {
  let component: ManageInitialApprovalsComponent;
  let fixture: ComponentFixture<ManageInitialApprovalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageInitialApprovalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageInitialApprovalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
