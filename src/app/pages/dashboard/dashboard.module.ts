import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PartialsModule } from 'app/views/partials/partials.module';
import { DashboardComponent } from './dashboard.component';



@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    PartialsModule,
    RouterModule.forChild([
      {
        path: '',
        component: DashboardComponent
      },
    ]),
  ]
})
export class DashboardModule { }