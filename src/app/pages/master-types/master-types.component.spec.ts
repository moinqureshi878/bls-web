import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterTypesComponent } from './master-types.component';

describe('MasterTypesComponent', () => {
  let component: MasterTypesComponent;
  let fixture: ComponentFixture<MasterTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
