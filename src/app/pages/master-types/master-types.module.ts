import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterTypesComponent } from './master-types.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatTableModule,
  MatPaginatorModule,
  MatIconModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatTreeModule,
  MatCheckboxModule,
  MatTooltipModule,
  MatSortModule,
} from '@angular/material';


import { ModuleService, SubModuleService, PagesService, UserService, MasterTypesService } from '../../core/appServices/index.service';
import { AppResolver } from '../../core/resolvers/app.resolver';
import { TextMaskModule } from 'angular2-text-mask';
import { MatTableExporterModule } from 'mat-table-exporter';
import { AddTypesComponent } from './add-types/add-types.component';
import { TranslateModule } from '@ngx-translate/core';
import { AuthGuard } from 'app/core/auth';
import { PartialsModule } from 'app/views/partials/partials.module';

@NgModule({
  declarations: [MasterTypesComponent, AddTypesComponent],

  exports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatTooltipModule,
  ],
  imports: [
    PartialsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatTreeModule,
    MatCheckboxModule,
    MatTooltipModule,
    TextMaskModule,
    MatSortModule,
    MatTableExporterModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: MasterTypesComponent
      },
      {
        path: 'add-types',
        component: AddTypesComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'edit-types/:id',
        component: AddTypesComponent,
        canActivate: [AuthGuard],
        resolve: { data: AppResolver }
      },

    ]),
  ],
  providers: [
    AppResolver,
    ModuleService,
    SubModuleService,
    PagesService,
    UserService,
    MasterTypesService,
  ]
})
export class MasterTypesModule { }
