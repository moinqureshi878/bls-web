import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MasterTypesService, MasterDataService } from '../../core/appServices/index.service';
import { ActivatedRoute, Router } from '@angular/router';
import { mastertypesmodel } from '../../core/appModels/mastertypes.model';
import { TranslationService } from '../../core/_base/layout';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';

@Component({
  selector: 'kt-master-types',
  templateUrl: './master-types.component.html',
  styleUrls: ['./master-types.component.scss']
})
export class MasterTypesComponent implements OnInit {
  ELEMENT_DATA: mastertypesmodel[] = [];
  displayedColumns7: string[] = ['valueEn', 'valueAr', 'editView', 'isActive'];
  dataSource7 = new MatTableDataSource<mastertypesmodel>(this.ELEMENT_DATA);
  closeResult: string; //Bootstrap Modal Popup
  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  masterType: any;
  getMasterType: any;
  constructor(private modalService: NgbModal,
    private masterTypesService: MasterTypesService,
    private route: ActivatedRoute,
    private router: Router,
    private toast: ToastrService,
    public masterDataService: MasterDataService,
    private translationService: TranslationService) {
    this.getMasterType = this.route.snapshot.data.data;
    this.masterType = this.getMasterType;
    this.dataSource7 = new MatTableDataSource(this.masterType);

    this.masterDataService.GetAllMaster().pipe(map(resp => resp["data"])).subscribe(res => {
      this.getMasterType = res;
      this.masterType = this.getMasterType;
      this.dataSource7 = new MatTableDataSource(this.masterType);
      this.dataSource7.paginator = this.paginator7;
      this.dataSource7.sort = this.sort7;
    });
  }

  ngOnInit() {

    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }
  isActiveMasterType(id) {
    this.changeDataSource(id);
    this.masterTypesService.DeleteMasterTypes(id).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {

      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toast.error(text);
        });
      }
    });
  }
  changeDataSource(ID: number) {
    let index = this.masterType.findIndex(x => x.id == ID);
    let isActive: boolean = this.masterType[index].isActive == true ? false : true;
    this.masterType[index].isActive = isActive;
  }
  insertSpaces(string) {
    string = string.replace(/([a-z])([A-Z])/g, '$1 $2');
    string = string.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2')
    return string;
  }
}
