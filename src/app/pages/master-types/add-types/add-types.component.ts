import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AsyncFormValidatorService } from '../../../core/validator/async-form-validator.service';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { MasterTypesService } from '../../../core/appServices/index.service';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { mastertypesmodel } from '../../../core/appModels/mastertypes.model';
import { TranslationService } from '../../../core/_base/layout';
import { nameArValidator } from 'app/core/validator/form-validators.service';

@Component({
  selector: 'kt-add-types',
  templateUrl: './add-types.component.html',
  styleUrls: ['./add-types.component.scss']
})
export class AddTypesComponent implements OnInit {
  id: number;
  MasterTypeEdit: any;

  addMasterType: FormGroup;
  loading: boolean = false;
  constructor(public router: Router,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private toast: ToastrService,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    private masterTypeservice: MasterTypesService,
    private translationService: TranslationService) {
    this.MasterTypeEdit = this.route.snapshot.data.data;
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      if (this.id) {
        this.InitializeForm();
        debugger
        this.addMasterType.patchValue(this.MasterTypeEdit);
      }
      else {
        this.InitializeForm();
      }
    });
  }

  ngOnInit() {
  }

  InitializeForm() {
    this.addMasterType = this.fb.group({
      "id": [0, Validators.required],
      "nameEn": ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      "nameAr": ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
      "isActive": [true, Validators.required],
    });
  }
  masterTypeFormSubmit() {
    if (this.addMasterType.valid) {
      //Api call
      this.loading = true;
      if (this.id) {
        this.masterTypeservice.EditMasterTypes(this.addMasterType.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigateByUrl('/pages/master-types');
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
      else {
        this.masterTypeservice.AddMasterTypes(this.addMasterType.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigateByUrl('/pages/master-data');
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.addMasterType);
    }
  }
}
