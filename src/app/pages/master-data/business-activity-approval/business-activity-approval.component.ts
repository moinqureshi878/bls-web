import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { TranslationService } from 'app/core/_base/layout';
import { CommonService } from 'app/core/appServices/index.service';
import { Observable } from 'rxjs';
import { ConfigService } from 'app/core/appServices/config.service';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';

export interface UserData {
  baName: string;
  applicationType: string;
  govtAuthorities: string;

  isActive: string;
}

/** Builds and returns a new User. */

const ELEMENT_DATA: UserData[] = [
  { baName: '', applicationType: '', govtAuthorities: '', isActive: '' },
  { baName: '', applicationType: '', govtAuthorities: '', isActive: '' },
  { baName: '', applicationType: '', govtAuthorities: '', isActive: '' },
  { baName: '', applicationType: '', govtAuthorities: '', isActive: '' },
  { baName: '', applicationType: '', govtAuthorities: '', isActive: '' },
];

@Component({
  selector: 'kt-business-activity-approval',
  templateUrl: './business-activity-approval.component.html',
  styleUrls: ['./business-activity-approval.component.scss']
})
export class BusinessActivityApprovalComponent implements OnInit {
  ELEMENT_DATA: any[] = [];
  LegalTypeApproval: any;

  displayedColumns7: string[] = ['baName', 'applicationType', 'govtAuthorities', 'isActive'];
  dataSource7 = new MatTableDataSource<UserData>(ELEMENT_DATA);

  applicationTypeddl: Observable<any>;
  businessNamesTypeddl: Observable<any>;

  applicationTypeSelected : any;
  bnTypeSelected : any;
  multipleLookups : Observable<any>;

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;

  constructor(
    private route: ActivatedRoute,
    private translationService: TranslationService,
    private commonService: CommonService,
    public configService: ConfigService,
    private toastrService: ToastrService,
  ) {
    debugger
    this.LegalTypeApproval = this.route.snapshot.data.data;
    this.dataSource7 = new MatTableDataSource<any>(this.LegalTypeApproval);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
    this.getApplicationTypeDdl();
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  getApplicationTypeDdl() {
    this.multipleLookups = this.commonService.getMultipleLookups("?types=ApplicationTypes,BusinessNamesTypes");
    this.applicationTypeddl = this.multipleLookups.pipe(map(resp => resp["ApplicationTypes"]));
    this.businessNamesTypeddl = this.multipleLookups.pipe(map(resp => resp["BusinessNamesTypes"]));
  }

  ddlApplicationTypeChange(applicationTypeSelected: any) {
    this.dataSource7.filter = applicationTypeSelected.valueEn.trim().toLowerCase() || applicationTypeSelected.valueAr.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  ddlBusinessNameTypeChange(businessNamesType: any) {
    debugger
    this.dataSource7.filter = businessNamesType.valueEn.trim().toLowerCase() || businessNamesType.valueAr.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }


  editBusinessActivityApproval(...value) {
    let model = {
      "id": value[0],
      "isMandatory": null,
      "inputData": null,
      "isActive": null
    }

    if (value[2] === "isMandatory") {
      model.isMandatory = value[1];
    }
    else if (value[2] === "inputData") {
      model.inputData = value[1];
    }

    else if (value[2] === "isActive") {
      model.isActive = value[1];
    }

    this.configService.editManageApplicationTypeAttachment(model)
      .subscribe(data => {
        if (data["result"].code === 200) {

        }
        else {
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toastrService.success(text);
          });
        }
      })
  }
}