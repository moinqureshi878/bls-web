import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessActivityApprovalComponent } from './business-activity-approval.component';

describe('BusinessActivityApprovalComponent', () => {
  let component: BusinessActivityApprovalComponent;
  let fixture: ComponentFixture<BusinessActivityApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessActivityApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessActivityApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
