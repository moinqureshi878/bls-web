import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterDataComponent } from './master-data.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatTableModule,
  MatPaginatorModule,
  MatIconModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatTreeModule,
  MatCheckboxModule,
  MatTooltipModule,
  MatSortModule,
} from '@angular/material';
import { IgxTimePickerModule, IgxInputGroupModule } from 'igniteui-angular';

import { ModuleService, SubModuleService, PagesService, UserService, MasterDataService } from '../../core/appServices/index.service';
import { AppResolver } from '../../core/resolvers/app.resolver';
import { TextMaskModule } from 'angular2-text-mask';
import { MatTableExporterModule } from 'mat-table-exporter';
import { AddMasterComponent } from './add-master/add-master.component';
import { TranslateModule } from '@ngx-translate/core';
import { AuthGuard } from '../../core/auth';
import { PartialsModule } from 'app/views/partials/partials.module';
import { ApplicationTypeComponent } from './application-type/application-type.component';
import { AddApplicationTypeComponent } from './add-application-type/add-application-type.component';
import { AddApplicationTypeReasonsComponent } from './add-application-type-reasons/add-application-type-reasons.component';
import { ApplicationTypeReasonsComponent } from './application-type-reasons/application-type-reasons.component';
import { BusinessActivityApprovalComponent } from './business-activity-approval/business-activity-approval.component';
import { AddBusinessActivityApprovalComponent } from './add-business-activity-approval/add-business-activity-approval.component';
import { LegalTypeAttachmentComponent } from './legal-type-attachment/legal-type-attachment.component';
import { LegalTypeApprovalComponent } from './legal-type-approval/legal-type-approval.component';
import { LoginDocValidationComponent } from './login-doc-validation/login-doc-validation.component';
import { AddLegalTypeApprovalComponent } from './add-legal-type-approval/add-legal-type-approval.component';
import { AddLegalTypeAttachmentComponent } from './add-legal-type-attachment/add-legal-type-attachment.component';
import { MasterDataResolver } from 'app/core/resolvers/master-data.resolver';
@NgModule({
  declarations: [MasterDataComponent, AddMasterComponent, ApplicationTypeComponent, AddApplicationTypeComponent, AddApplicationTypeReasonsComponent, ApplicationTypeReasonsComponent, BusinessActivityApprovalComponent, AddBusinessActivityApprovalComponent, LegalTypeAttachmentComponent, LegalTypeApprovalComponent, LoginDocValidationComponent, AddLegalTypeApprovalComponent, AddLegalTypeAttachmentComponent,],// MasterTypesComponent],
  exports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatTooltipModule
  ],
  imports: [
    PartialsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatTreeModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatSortModule,
    IgxTimePickerModule,
    IgxInputGroupModule,
    TextMaskModule,
    MatTableExporterModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: MasterDataComponent,
      },
      {
        path: 'add-master',
        canActivate: [AuthGuard],
        component: AddMasterComponent

      },
      {
        path: 'edit-master/:id',
        canActivate: [AuthGuard],
        component: AddMasterComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'application-type',
        canActivate: [AuthGuard],
        component: ApplicationTypeComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'add-application-type',
        canActivate: [AuthGuard],
        component: AddApplicationTypeComponent,
      },
      {
        path: 'edit-application-type/:id',
        canActivate: [AuthGuard],
        component: AddApplicationTypeComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'application-type-reasons',
        canActivate: [AuthGuard],
        component: ApplicationTypeReasonsComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'add-application-type-reasons',
        canActivate: [AuthGuard],
        component: AddApplicationTypeReasonsComponent,
      },
      {
        path: 'edit-application-type-reasons/:id',
        canActivate: [AuthGuard],
        component: AddApplicationTypeReasonsComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'business-activity-approval',
        canActivate: [AuthGuard],
        component: BusinessActivityApprovalComponent,
        resolve: { data: MasterDataResolver }
      },
      {
        path: 'add-business-activity-approval',
        canActivate: [AuthGuard],
        component: AddBusinessActivityApprovalComponent,
        resolve: { data: MasterDataResolver }
      },
      {
        path: 'legal-type-attachment',
        canActivate: [AuthGuard],
        component: LegalTypeAttachmentComponent,
        resolve: { data: MasterDataResolver }
      },
      {
        path: 'legal-type-approval',
        canActivate: [AuthGuard],
        component: LegalTypeApprovalComponent,
        resolve: { data: MasterDataResolver }
      },
      {
        path: 'add-legal-type-approval',
        canActivate: [AuthGuard],
        component: AddLegalTypeApprovalComponent,
        resolve: { data: MasterDataResolver }
      },
      {
        path: 'add-legal-type-attachment',
        canActivate: [AuthGuard],
        component: AddLegalTypeAttachmentComponent,
        resolve: { data: MasterDataResolver }
      },
      {
        path: 'login-doc-validation',
        canActivate: [AuthGuard],
        component: LoginDocValidationComponent,
        resolve: { data: AppResolver }
      },

    ]),
  ],
  providers: [
    AppResolver,
    ModuleService,
    SubModuleService,
    PagesService,
    UserService,
    MasterDataService
    // {
    // 	provide: HTTP_INTERCEPTORS,
    // 	useClass: JWTInterceptor,
    // 	multi: true
    // },
  ]
})
export class MasterDataModule { }
