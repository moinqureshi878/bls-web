import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLegalTypeAttachmentComponent } from './add-legal-type-attachment.component';

describe('AddLegalTypeAttachmentComponent', () => {
  let component: AddLegalTypeAttachmentComponent;
  let fixture: ComponentFixture<AddLegalTypeAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLegalTypeAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLegalTypeAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
