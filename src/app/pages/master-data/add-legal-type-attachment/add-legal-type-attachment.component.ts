import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'app/core/appServices/index.service';
import { TranslationService } from 'app/core/_base/layout';
import { ConfigService } from 'app/core/appServices/config.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'kt-add-legal-type-attachment',
  templateUrl: './add-legal-type-attachment.component.html',
  styleUrls: ['./add-legal-type-attachment.component.scss']
})
export class AddLegalTypeAttachmentComponent implements OnInit {

  legalTypeAttachmentFormGroup: FormGroup;
  loading = false;
  legalTypeddl: Observable<any>;
  applicationTypeddl: Observable<any>;
  legalTypeAttachmentGrid: any;

  constructor(
    private fb: FormBuilder,
    public validationMessages: ValidationMessagesService,
    public router: Router,
    private commonService: CommonService,
    private translationService: TranslationService,
    private route: ActivatedRoute,
    public configService: ConfigService,
    private toast: ToastrService,
    private cdr: ChangeDetectorRef,
  ) {
    this.legalTypeAttachmentGrid = this.route.snapshot.data.data;
  }

  ngOnInit() {
    this.createFormPages();
    this.getLegalTypeDdl();
    this.getApplicationTypeDdl();
  }

  createFormPages() {
    this.legalTypeAttachmentFormGroup = this.fb.group({
      legalTypeddl: ['', Validators.compose([Validators.required])],
      applicationTypeddl: ['', Validators.compose([Validators.required])]
    });
  }

  getLegalTypeDdl() {
    this.legalTypeddl = this.commonService.getLookUp("?type=LegalTypes");
  }

  getApplicationTypeDdl() {
    this.applicationTypeddl = this.commonService.getLookUp("?type=ApplicationTypes");
  }

  ddlLegalTypeChange() {
    if (this.legalTypeAttachmentFormGroup.valid) {
      this.changelegalTypeAttachmentGrid(this.legalTypeAttachmentFormGroup.value.legalTypeddl.id, this.legalTypeAttachmentFormGroup.value.applicationTypeddl.id);
    }
    else {
      this.validationMessages.validateAllFormFields(this.legalTypeAttachmentFormGroup);
    }
  }


  ddlApplicationTypeChange() {
    if (this.legalTypeAttachmentFormGroup.valid) {
      this.changelegalTypeAttachmentGrid(this.legalTypeAttachmentFormGroup.value.legalTypeddl.id, this.legalTypeAttachmentFormGroup.value.applicationTypeddl.id);
    }
    else {
      this.validationMessages.validateAllFormFields(this.legalTypeAttachmentFormGroup);
    }
  }

  changelegalTypeAttachmentGrid(t1: number, t2: number) {
    this.configService.getTypeDetailByID( t1, t2,"Attachments")
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.legalTypeAttachmentGrid = data["data"];
          this.cdr.markForCheck();
        }
        else {
          this.loading = false;
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toast.success(text);
          });
        }
      })
  }

  submitlegalTypeAttachment() {
    if (this.legalTypeAttachmentFormGroup.valid) {
      let AppTypeAttachmentSelected: [] = this.legalTypeAttachmentGrid.filter(x => x.isExist == true);
      if (AppTypeAttachmentSelected.length > 0) {

        let model = AppTypeAttachmentSelected.map(item => {
          return {
            "id": item["existId"],
            "data3": item["id"],
            "data2": this.legalTypeAttachmentFormGroup.value.applicationTypeddl.id,
            "data1": this.legalTypeAttachmentFormGroup.value.legalTypeddl.id,
            "type3": item["headID"],
            "type2": this.legalTypeAttachmentFormGroup.value.applicationTypeddl.headID,
            "type1": this.legalTypeAttachmentFormGroup.value.legalTypeddl.headID,
            "isMandatory": item["isMandatory"] == true ? true : false,
            "inputData": item["inputData"] == true ? true : false,
            "isActive": item["isActive"] == true ? true : false
          }
        })

        this.configService.addAppTypeAttachment(model)
          .subscribe(data => {
            if (data["result"].code === 200) {
              this.loading = false;
              this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
                this.toast.success(text);
              });
              this.router.navigateByUrl('/pages/master-data/legal-type-attachment');
            }
            else {
              this.loading = false;
              this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
                this.toast.success(text);
              });
            }
          })
      }
      else {
        // this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
        //   this.toastrService.success(text);
        // });

        // no validation provided
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.legalTypeAttachmentFormGroup);
    }
  }

  checkBoxChange(...value) {
    this.legalTypeAttachmentGrid[value[0]].isExist = value[1];
  }

  isMandatory(...value) {
    this.legalTypeAttachmentGrid[value[0]].isMandatory = value[1];
  }

  inputdata(...value) {
    this.legalTypeAttachmentGrid[value[0]].inputData = value[1];
  }

  isActive(...value) {
    this.legalTypeAttachmentGrid[value[0]].isActive = value[1];
  }

}
