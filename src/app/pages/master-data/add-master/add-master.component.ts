import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AsyncFormValidatorService } from '../../../core/validator/async-form-validator.service';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { MasterDataService } from '../../../core/appServices/index.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from '@translateService/translation.service';
import { nameArValidator } from 'app/core/validator/form-validators.service';


@Component({
  selector: 'kt-add-master',
  templateUrl: './add-master.component.html',
  styleUrls: ['./add-master.component.scss']
})
export class AddMasterComponent implements OnInit {
  MasterDataEdit: any;
  addMaster: FormGroup;
  loading: boolean = false;
  typesName: Observable<any>;
  masterData: any;
  typeId: number;

  id: number;
  constructor(//private modalService: NgbModal,
    public router: Router,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    private masterDataservice: MasterDataService,
    private cdr: ChangeDetectorRef,
    private toast: ToastrService,
    private translationService: TranslationService) {
    this.MasterDataEdit = this.route.snapshot.data.data;
    debugger
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      if (this.id) {
        this.InitializeForm();
        debugger
        this.addMaster.patchValue(this.MasterDataEdit);
      }
      else {
        this.InitializeForm();
      }
    });
  }

  ngOnInit() {
    this.getTypes();
  }
  InitializeForm() {
    this.addMaster = this.fb.group({
      "id": [0, Validators.required],
      "nameEn": ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      "nameAr": ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
      "headId": ['', Validators.required],
      "isActive": [true, Validators.required],
      "canEdit": [true, Validators.required],
    });
  }

  getTypes() {
    this.typesName = this.masterDataservice.getMaterByType().pipe(map(resp => resp["data"]));
  }

  addMasterFormSubmit() {
    if (this.addMaster.valid) {
      //Api call
      this.loading = true;
      if (this.id) {
        this.masterDataservice.UpdateMaster(this.addMaster.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigateByUrl('/pages/master-data');
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
      else {
        this.masterDataservice.AddMaster(this.addMaster.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigateByUrl('/pages/master-data');
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.addMaster);
    }
  }

}
