import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MasterDataService, CommonService } from '../../../core/appServices/index.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TranslationService } from '../../../core/_base/layout';
import { ToastrService } from 'ngx-toastr';
import { ConfigService } from "../../../core/appServices/index.service";
@Component({
  selector: 'kt-application-type-reasons',
  templateUrl: './application-type-reasons.component.html',
  styleUrls: ['./application-type-reasons.component.scss']
})
export class ApplicationTypeReasonsComponent implements OnInit {


  displayedColumns7: string[] = ['reasonType', 'applicationType', 'descriptionAr', 'descriptionEn', 'editView', 'isActive'];
  dataSource7 = new MatTableDataSource();

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  typesName: Observable<any>;
  applicationTypeReason: any;
  getApplicationTypeReason: any;
  
  typeId: number;
  id: number;
  constructor(
    private masterDataservice: MasterDataService,
    public commonService: CommonService,
    private toast: ToastrService,
    private route: ActivatedRoute,
    public configService: ConfigService,
    private router: Router,
    private translationService: TranslationService) {
      this.getApplicationTypeReason = this.route.snapshot.data.data;
      this.applicationTypeReason = this.getApplicationTypeReason;
      this.dataSource7 = new MatTableDataSource(this.applicationTypeReason);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
    this.getTypes();
  }
  getTypes() {
    this.typesName = this.commonService.getLookUp("?type=ApplicationTypes");
  }
 
  filterByAppType(id) {
    this.applicationTypeReason = this.getApplicationTypeReason.filter(res => res.appTypeId == id);
    this.dataSource7 = new MatTableDataSource(this.applicationTypeReason);
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }
  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  isActive(id) {
    this.changeDataSource(id);
    this.configService.DeleteAppTypeReason(id).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {

      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toast.error(text);
        });
      }
    });
  }
  changeDataSource(ID: number) {
    let index = this.applicationTypeReason.findIndex(x => x.id == ID);
    let isActive: boolean = this.applicationTypeReason[index].isActive == true ? false : true;
    this.applicationTypeReason[index].isActive = isActive;
  }
}