import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationTypeReasonsComponent } from './application-type-reasons.component';

describe('ApplicationTypeReasonsComponent', () => {
  let component: ApplicationTypeReasonsComponent;
  let fixture: ComponentFixture<ApplicationTypeReasonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationTypeReasonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationTypeReasonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
