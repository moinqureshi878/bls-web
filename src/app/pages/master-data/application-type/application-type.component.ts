import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AsyncFormValidatorService } from '../../../core/validator/async-form-validator.service';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { MasterDataService } from '../../../core/appServices/index.service';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from '@translateService/translation.service';
import { ConfigService } from 'app/core/appServices/config.service';

@Component({
  selector: 'kt-application-type',
  templateUrl: './application-type.component.html',
  styleUrls: ['./application-type.component.scss']
})
export class ApplicationTypeComponent implements OnInit {
  displayedColumns7: string[] = ['applicationType', 'timeKPI', 'totalKPI', 'edit', 'isActive'];
  dataSource7 = new MatTableDataSource();

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  getApplicationTypes: any;
  constructor(public router: Router,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    private masterDataservice: MasterDataService,
    private cdr: ChangeDetectorRef,
    private toast: ToastrService,
    private translationService: TranslationService,
    public configService: ConfigService,
  ) {
    this.getApplicationTypes = this.route.snapshot.data.data;
    
    this.dataSource7 = new MatTableDataSource(this.getApplicationTypes ? this.getApplicationTypes : []);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  isActive(id) {
    this.changeDataSource(id);
    this.configService.DeleteAppTypeKPIs(id).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {

      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toast.error(text);
        });
      }
    });
  }

  changeDataSource(ID: number) {
    let index = this.getApplicationTypes.findIndex(x => x.id == ID);
    let isActive: boolean = this.getApplicationTypes[index].isActive == true ? false : true;
    this.getApplicationTypes[index].isActive = isActive;
  }
}