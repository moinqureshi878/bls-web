import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CommonService, BnCommonService } from 'app/core/appServices/index.service';
import { Observable } from 'rxjs';
import { TranslationService } from 'app/core/_base/layout';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { ConfigService } from 'app/core/appServices/config.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';

@Component({
  selector: 'kt-add-business-activity-approval',
  templateUrl: './add-business-activity-approval.component.html',
  styleUrls: ['./add-business-activity-approval.component.scss']
})
export class AddBusinessActivityApprovalComponent implements OnInit {

  businessActivityApporvalFormGroup: FormGroup;
  ISICBNAuthorityddl: Observable<any>;
  applicationTypeddl: Observable<any>;

  govtAuthorityGrid: any;
  loading = false;

  constructor(
    private fb: FormBuilder,
    private commonService: CommonService,
    private translationService: TranslationService,
    public validationMessages: ValidationMessagesService,
    public configService: ConfigService,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private toast: ToastrService,
    public router: Router,
    public bnCommonService: BnCommonService,
  ) {
    this.govtAuthorityGrid = this.route.snapshot.data.data;
  }

  ngOnInit() {
    this.createFormPages();
    this.getISICeDdl();
    this.getApplicationTypeDdl();
  }

  createFormPages() {
    this.businessActivityApporvalFormGroup = this.fb.group({
      ISICBNAuthorityddl: ['', Validators.compose([Validators.required])],
      applicationTypeddl: ['', Validators.compose([Validators.required])]
    });
  }

  getISICeDdl() {
    // this.ISICBNAuthorityddl = this.commonService.getLookUp("?type=BusinessNamesTypes");
    this.ISICBNAuthorityddl = this.bnCommonService.GetBusinessActivityPages()
      .pipe(
        map(resp => resp["data"])
      );
  }

  getApplicationTypeDdl() {
    this.applicationTypeddl = this.commonService.getLookUp("?type=ApplicationTypes");
  }

  submitBusinessActivityApproval() {
    if (this.businessActivityApporvalFormGroup.valid) {
      this.loading = true;

      let GovtAuthorityChecked: [] = this.govtAuthorityGrid.filter(x => x.isExist == true);
      if (GovtAuthorityChecked.length > 0) {
        let model = GovtAuthorityChecked.map(item => {
          return {
            "id": item["existId"],
            "data3": item["id"],
            "data2": this.businessActivityApporvalFormGroup.value.applicationTypeddl.id,
            "data1": this.businessActivityApporvalFormGroup.value.ISICBNAuthorityddl.id,
            "type3": item["headID"],
            "type2": this.businessActivityApporvalFormGroup.value.applicationTypeddl.headID,
            "type1": this.businessActivityApporvalFormGroup.value.ISICBNAuthorityddl.headID,
            "isActive": item["isExist"] == true ? true : false
          }
        })


        this.configService.addBAApproval(model)
          .subscribe(data => {
            if (data["result"].code === 200) {
              this.loading = false;
              this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
                this.toast.success(text);
              });
              this.loading = false;
              this.router.navigateByUrl('/pages/master-data/business-activity-approval');
            }
            else {
              this.loading = false;
              this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
                this.toast.error(text);
              });
            }
          })
      }
      else {
        this.loading = false;
        this.translationService.getTranslation('VALIDATIONMESSAGES.ATTACHMENTREQUIRED').subscribe((text: string) => {
          this.toast.error(text);
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.businessActivityApporvalFormGroup);
    }
  }

  ddlISICChange() {
    if (this.businessActivityApporvalFormGroup.valid) {
      this.changeGovtAuthorityGrid(this.businessActivityApporvalFormGroup.value.ISICBNAuthorityddl.id, this.businessActivityApporvalFormGroup.value.applicationTypeddl.id);
    }
    else {
      this.validationMessages.validateAllFormFields(this.businessActivityApporvalFormGroup);
    }
  }

  ddlApplicationTypeChange() {
    if (this.businessActivityApporvalFormGroup.valid) {
      this.changeGovtAuthorityGrid(this.businessActivityApporvalFormGroup.value.ISICBNAuthorityddl.id, this.businessActivityApporvalFormGroup.value.applicationTypeddl.id);
    }
    else {
      this.validationMessages.validateAllFormFields(this.businessActivityApporvalFormGroup);
    }
  }

  changeGovtAuthorityGrid(t1: number, t2: number) {
    this.configService.GetBLApprovalDetail(t1, t2, "GovernmentAuthorities")
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.govtAuthorityGrid = data["data"];
          this.cdr.markForCheck();
        }
        else {
          this.loading = false;
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toast.success(text);
          });
        }
      })
  }

  cbGovtAuthorityChange(...value) {
    this.govtAuthorityGrid[value[0]].isExist = value[1];
  }
}
