import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBusinessActivityApprovalComponent } from './add-business-activity-approval.component';

describe('AddBusinessActivityApprovalComponent', () => {
  let component: AddBusinessActivityApprovalComponent;
  let fixture: ComponentFixture<AddBusinessActivityApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBusinessActivityApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBusinessActivityApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
