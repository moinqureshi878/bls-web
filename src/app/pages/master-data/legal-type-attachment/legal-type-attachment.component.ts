import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { TranslationService } from 'app/core/_base/layout';
import { Observable } from 'rxjs';
import { CommonService } from 'app/core/appServices/index.service';
import { ConfigService } from 'app/core/appServices/config.service';
import { ToastrService } from 'ngx-toastr';

export interface UserData {
  legalType: string;
  applicationType: string;
  attachmentAr: string;
  attachmentEn: string;
  isModify: string;
  inputData: string;
  isActive: string;
}

/** Builds and returns a new User. */

const ELEMENT_DATA: UserData[] = [
  { legalType: '', applicationType: '', attachmentAr: '', attachmentEn: '', isModify: '', inputData: '', isActive: '' },
  { legalType: '', applicationType: '', attachmentAr: '', attachmentEn: '', isModify: '', inputData: '', isActive: '' },
  { legalType: '', applicationType: '', attachmentAr: '', attachmentEn: '', isModify: '', inputData: '', isActive: '' },
  { legalType: '', applicationType: '', attachmentAr: '', attachmentEn: '', isModify: '', inputData: '', isActive: '' },
  { legalType: '', applicationType: '', attachmentAr: '', attachmentEn: '', isModify: '', inputData: '', isActive: '' },
];

@Component({
  selector: 'kt-legal-type-attachment',
  templateUrl: './legal-type-attachment.component.html',
  styleUrls: ['./legal-type-attachment.component.scss']
})
export class LegalTypeAttachmentComponent implements OnInit {
  ELEMENT_DATA: any[] = [];
  LegalTypeAttachment: any;

  displayedColumns7: string[] = ['legalType', 'applicationType', 'attachmentAr', 'attachmentEn', 'isModify', 'inputData', 'isActive'];
  dataSource7 = new MatTableDataSource<UserData>(ELEMENT_DATA);

  closeResult: string; //Bootstrap Modal Popup

  applicationTypeddl: Observable<any>;

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;

  applicationTypeSelected: any;
  constructor(
    private route: ActivatedRoute,
    private translationService: TranslationService,
    private commonService: CommonService,
    public configService: ConfigService,
    private toastrService: ToastrService,
  ) {
    this.LegalTypeAttachment = this.route.snapshot.data.data;
    this.dataSource7 = new MatTableDataSource<any>(this.LegalTypeAttachment);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;

    this.getApplicationTypeDdl();
  }

  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  getApplicationTypeDdl() {
    this.applicationTypeddl = this.commonService.getLookUp("?type=ApplicationTypes");
  }

  ddlApplicationTypeChange(applicationTypeSelected: any) {
    this.dataSource7.filter = applicationTypeSelected.valueEn.trim().toLowerCase() || applicationTypeSelected.valueAr.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  editLegalTypeAttachment(...value) {
    let model = {
      "id": value[0],
      "isMandatory": null,
      "inputData": null,
      "isActive": null
    }

    if (value[2] === "isMandatory") {
      model.isMandatory = value[1];
    }
    else if (value[2] === "inputData") {
      model.inputData = value[1];
    }

    else if (value[2] === "isActive") {
      model.isActive = value[1];
    }

    this.configService.editManageApplicationTypeAttachment(model)
      .subscribe(data => {
        if (data["result"].code === 200) {

        }
        else {
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toastrService.success(text);
          });
        }
      })
  }

}