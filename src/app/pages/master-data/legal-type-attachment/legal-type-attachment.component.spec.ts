import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalTypeAttachmentComponent } from './legal-type-attachment.component';

describe('LegalTypeAttachmentComponent', () => {
  let component: LegalTypeAttachmentComponent;
  let fixture: ComponentFixture<LegalTypeAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegalTypeAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalTypeAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
