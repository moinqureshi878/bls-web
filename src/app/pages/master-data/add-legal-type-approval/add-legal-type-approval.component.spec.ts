import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLegalTypeApprovalComponent } from './add-legal-type-approval.component';

describe('AddLegalTypeApprovalComponent', () => {
  let component: AddLegalTypeApprovalComponent;
  let fixture: ComponentFixture<AddLegalTypeApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLegalTypeApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLegalTypeApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
