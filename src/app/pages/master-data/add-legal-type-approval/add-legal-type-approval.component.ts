import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'app/core/appServices/index.service';
import { Observable } from 'rxjs';
import { TranslationService } from 'app/core/_base/layout';
import { ConfigService } from 'app/core/appServices/config.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'kt-add-legal-type-approval',
  templateUrl: './add-legal-type-approval.component.html',
  styleUrls: ['./add-legal-type-approval.component.scss']
})
export class AddLegalTypeApprovalComponent implements OnInit {

  legalTypeApporvalFormGroup: FormGroup;
  loading = false;
  legalTypeddl: Observable<any>;
  applicationTypeddl: Observable<any>;
  govtAuthorityGrid: any;

  constructor(
    private fb: FormBuilder,
    public validationMessages: ValidationMessagesService,
    public router: Router,
    private commonService: CommonService,
    private translationService: TranslationService,
    private route: ActivatedRoute,
    public configService: ConfigService,
    private toast: ToastrService,
    private cdr: ChangeDetectorRef,
  ) {
    this.govtAuthorityGrid = this.route.snapshot.data.data;
  }

  ngOnInit() {
    this.createFormPages();
    this.getLegalTypeDdl();
    this.getApplicationTypeDdl();
    // this.getGovernmentAuthorityGrid();
  }

  createFormPages() {
    this.legalTypeApporvalFormGroup = this.fb.group({
      legalTypeddl: ['', Validators.compose([Validators.required])],
      applicationTypeddl: ['', Validators.compose([Validators.required])]
    });
  }

  submitlegalTypeApproval() {
    if (this.legalTypeApporvalFormGroup.valid) {
      this.loading = true;

      let GovtAuthorityChecked: [] = this.govtAuthorityGrid.filter(x => x.isExist == true);
      if (GovtAuthorityChecked.length > 0) {
        let model = GovtAuthorityChecked.map(item => {
          return {
            "id": item["existId"],
            "data3": item["id"],
            "data2": this.legalTypeApporvalFormGroup.value.applicationTypeddl.id,
            "data1": this.legalTypeApporvalFormGroup.value.legalTypeddl.id,
            "type3": item["headID"],
            "type2": this.legalTypeApporvalFormGroup.value.applicationTypeddl.headID,
            "type1": this.legalTypeApporvalFormGroup.value.legalTypeddl.headID,
            "isActive": item["isExist"] == true ? true : false
          }
        })


        this.configService.addAppTypeAttachment(model)
          .subscribe(data => {
            if (data["result"].code === 200) {
              this.loading = false;
              this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
                this.toast.success(text);
              });
              this.loading = false;
              this.router.navigateByUrl('/pages/master-data/legal-type-approval');
            }
            else {
              this.loading = false;
              this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
                this.toast.error(text);
              });
            }
          })
      }
      else {
        this.loading = false;
        this.translationService.getTranslation('VALIDATIONMESSAGES.ATTACHMENTREQUIRED').subscribe((text: string) => {
          this.toast.error(text);
        });
      }
      console.log(GovtAuthorityChecked);
    }
    else {
      this.validationMessages.validateAllFormFields(this.legalTypeApporvalFormGroup);
    }
  }

  getLegalTypeDdl() {
    this.legalTypeddl = this.commonService.getLookUp("?type=LegalTypes");
  }

  getApplicationTypeDdl() {
    this.applicationTypeddl = this.commonService.getLookUp("?type=ApplicationTypes");
  }

  cbGovtAuthorityChange(...value) {
    this.govtAuthorityGrid[value[0]].isExist = value[1];
  }

  ddlLegalTypeChange() {
    if (this.legalTypeApporvalFormGroup.valid) {
      this.changeGovtAuthorityGrid(this.legalTypeApporvalFormGroup.value.legalTypeddl.id, this.legalTypeApporvalFormGroup.value.applicationTypeddl.id);
    }
    else {
      this.validationMessages.validateAllFormFields(this.legalTypeApporvalFormGroup);
    }
  }

  ddlApplicationTypeChange() {
    if (this.legalTypeApporvalFormGroup.valid) {
      this.changeGovtAuthorityGrid(this.legalTypeApporvalFormGroup.value.legalTypeddl.id, this.legalTypeApporvalFormGroup.value.applicationTypeddl.id);
    }
    else {
      this.validationMessages.validateAllFormFields(this.legalTypeApporvalFormGroup);
    }
  }

  changeGovtAuthorityGrid(t1: number, t2: number) {
    this.configService.getTypeDetailByID( t1, t2,"GovernmentAuthorities")
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.govtAuthorityGrid = data["data"];
          this.cdr.markForCheck();
        }
        else {
          this.loading = false;
          this.translationService.getTranslation(String(data["result"].code)).subscribe((text: string) => {
            this.toast.success(text);
          });
        }
      })
  }

}
