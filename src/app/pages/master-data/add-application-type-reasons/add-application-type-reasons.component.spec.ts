import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddApplicationTypeReasonsComponent } from './add-application-type-reasons.component';

describe('AddApplicationTypeReasonsComponent', () => {
  let component: AddApplicationTypeReasonsComponent;
  let fixture: ComponentFixture<AddApplicationTypeReasonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddApplicationTypeReasonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddApplicationTypeReasonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
