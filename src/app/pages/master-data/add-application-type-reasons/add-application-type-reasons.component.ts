import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AsyncFormValidatorService } from '../../../core/validator/async-form-validator.service';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { MasterDataService, ConfigService, CommonService } from '../../../core/appServices/index.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from '@translateService/translation.service';
import { nameArValidator } from 'app/core/validator/form-validators.service';
@Component({
  selector: 'kt-add-application-type-reasons',
  templateUrl: './add-application-type-reasons.component.html',
  styleUrls: ['./add-application-type-reasons.component.scss']
})
export class AddApplicationTypeReasonsComponent implements OnInit {

  getAppReasonData: any;
  addAppReasonForm: FormGroup;
  loading: boolean = false;
  reasonTypesName: Observable<any>;
  appTypesName: Observable<any>;
  masterData: any;
  typeId: number;

  id: number;
  constructor(//private modalService: NgbModal,
    public router: Router,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    public commonService: CommonService,
    private masterDataservice: MasterDataService,
    private cdr: ChangeDetectorRef,
    private toast: ToastrService,
    public configService: ConfigService,
    private translationService: TranslationService) {
    this.getAppReasonData = this.route.snapshot.data.data;
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      if (this.id) {
        this.InitializeForm();
        debugger
        this.addAppReasonForm.patchValue(this.getAppReasonData[0]);
        this.addAppReasonForm.patchValue({ appTypeIdReason: this.getAppReasonData[0].appTypeId });
      }
      else {
        this.InitializeForm();
      }
    });
  }

  ngOnInit() {
    this.getTypes();
  }
  InitializeForm() {
    this.addAppReasonForm = this.fb.group({
      "id": [0, Validators.required],
      "reasonTypeId": [null, Validators.required],
      "appTypeIdReason": [null, Validators.required],
      "reasonEn": ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      "reasonAr": ['', Validators.compose([Validators.required, Validators.maxLength(50), nameArValidator])],
      "isActive": [true]
    });
  }

  getTypes() {
    this.reasonTypesName = this.commonService.getLookUp("?type=ReasonTypes");
    this.appTypesName = this.commonService.getLookUp("?type=ApplicationTypes");
  }

  addAppReasonFormSubmit() {
    if (this.addAppReasonForm.valid) {
      //Api call
      this.loading = true;
      this.addAppReasonForm.value.appTypeId = this.addAppReasonForm.value.appTypeIdReason;
      if (this.id) {
        this.configService.AppTypeReason(this.addAppReasonForm.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigateByUrl('/pages/master-data/application-type-reasons');
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
      else {
        this.configService.AppTypeReason(this.addAppReasonForm.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigateByUrl('/pages/master-data/application-type-reasons');
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.addAppReasonForm);
    }
  }

}
