import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginDocValidationComponent } from './login-doc-validation.component';

describe('LoginDocValidationComponent', () => {
  let component: LoginDocValidationComponent;
  let fixture: ComponentFixture<LoginDocValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginDocValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginDocValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
