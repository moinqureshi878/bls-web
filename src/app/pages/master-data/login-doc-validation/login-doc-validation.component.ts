import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { TranslationService } from 'app/core/_base/layout';
import { ToastrService } from 'ngx-toastr';
import { ValidationMessagesService } from 'app/core/validator/validation-messages.service';
import { Observable } from 'rxjs';
import { CommonService, AuthenticationService } from 'app/core/appServices/index.service';

@Component({
  selector: 'kt-login-doc-validation',
  templateUrl: './login-doc-validation.component.html',
  styleUrls: ['./login-doc-validation.component.scss']
})
export class LoginDocValidationComponent implements OnInit {
  getLoginDoc: any;
  loginDocForm: FormGroup;
  loading: boolean = false;
  labels: any[] = [
    { labelEn: 'Check User Emirate ID', labelAr: 'تأكد من الهوية الإماراتية', beforeExpiryLabelEn: 'Notify before expiry (Days)', beforeExpiryLabelAr: 'تنبيه قبل الانتهاء 0(بالأيام)', afterExpiryLabelEn: 'Lock account after expiry (Days)', afterExpiryLabelAr: 'إغلاق الحساب بعد الانتهاء' },
    { labelEn: 'Check User Passport', labelAr: 'تأكد من جواز السفر', beforeExpiryLabelEn: 'Notify before expiry (Days)', beforeExpiryLabelAr: 'تنبيه قبل الانتهاء 0(بالأيام)', afterExpiryLabelEn: 'Lock account after expiry (Days)', afterExpiryLabelAr: 'إغلاق الحساب بعد الانتهاء' },
    { labelEn: 'Check User Visa Expiry', labelAr: 'تأكد من انتهاء التاشيرة', beforeExpiryLabelEn: 'Notify before expiry (Days)', beforeExpiryLabelAr: 'تنبيه قبل الانتهاء 0(بالأيام)', afterExpiryLabelEn: 'Lock account after expiry (Days)', afterExpiryLabelAr: 'إغلاق الحساب بعد الانتهاء' },
    { labelEn: 'Check Document Clearance Company License', labelAr: 'تأكد من رخصة شركة تخليص المعاملات', beforeExpiryLabelEn: 'Notify before expiry (Days)', beforeExpiryLabelAr: 'تنبيه قبل الانتهاء 0(بالأيام)', afterExpiryLabelEn: 'Lock account after expiry (Days)', afterExpiryLabelAr: 'إغلاق الحساب بعد الانتهاء' },
  ];
  getDocs: Observable<any>;

  constructor(private route: ActivatedRoute,
    private fb: FormBuilder,
    private translationService: TranslationService,
    public validationMessages: ValidationMessagesService,
    private router: Router,
    private toast: ToastrService,
    public commonService: CommonService,
    public authenticationService: AuthenticationService,
    private cdr: ChangeDetectorRef) {
    this.getLoginDoc = this.route.snapshot.data.data;
    // this.getLoginDoc = [];
    if (this.getLoginDoc && this.getLoginDoc.length > 0) {
      this.InitializeForm();
      this.updateDocFormArray();
    }
    else {
      this.InitializeForm();
      this.commonService.getLookUp("?type=LoginDoc").subscribe(res => {
        this.addDocFormArray(res);
      });
    }
  }

  ngOnInit() {
  }
  InitializeForm() {
    this.loginDocForm = this.fb.group({
      loginDoc: this.fb.array([]),
    });
  }
  addDocFormArray(values) {
    let control = <FormArray>this.loginDocForm.controls.loginDoc;
    values.forEach(x => {
      control.push(this.fb.group({
        id: [null],
        lockAfter: [null, Validators.compose([Validators.required, Validators.min(1)])],
        notifyBefore: [null, Validators.compose([Validators.required, Validators.min(1)])],
        validationType: [x.id],
      }))
    });
  }
  updateDocFormArray() {
    debugger
    let control = <FormArray>this.loginDocForm.controls.loginDoc;
    this.getLoginDoc.forEach(x => {
      control.push(this.fb.group({
        id: [x.id],
        lockAfter: [x.lockAfter, Validators.compose([Validators.required, Validators.min(1)])],
        notifyBefore: [x.notifyBefore, Validators.compose([Validators.required, Validators.min(1)])],
        validationType: [x.validationType],
      }))
    });
  }
  loginDocFormSubmit() {
    if (this.loginDocForm.valid) {
      //Api call
      this.loading = true;
      debugger
      if (this.getLoginDoc && this.getLoginDoc.length > 0) {
        this.authenticationService.LoginValidation(this.loginDocForm.value.loginDoc).subscribe(res => {
          this.loading = false;
          this.cdr.markForCheck();
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
              this.toast.success(text);
            });
            // this.router.navigate(["/pages/admin/login-doc-validation"]);
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
      else {
        this.authenticationService.LoginValidation(this.loginDocForm.value.loginDoc).subscribe(res => {
          this.loading = false;
          this.cdr.markForCheck();
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
            // this.router.navigate(["/pages/admin/login-doc-validation"]);
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.loginDocForm);
    }
  }

}
