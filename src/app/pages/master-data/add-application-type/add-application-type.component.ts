import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { timeLessThanWhen, dateLessThanWhenRequired } from '../../../core/validator/form-validators.service';
import { TranslationService } from '@translateService/translation.service';
import { CommonService, UserService } from '../../../core/appServices/index.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ConfigService } from 'app/core/appServices/config.service';

@Component({
  selector: 'kt-add-application-type',
  templateUrl: './add-application-type.component.html',
  styleUrls: ['./add-application-type.component.scss']
})
export class AddApplicationTypeComponent implements OnInit {

  applicationType: Observable<any>;
  setLeave: number;
  applicationTypeForm: FormGroup;
  loading: boolean = false;
  getApplicationData: any;
  id: number;
  constructor(
    public commonService: CommonService,
    public userService: UserService,
    private fb: FormBuilder,
    public translationService: TranslationService,
    public validationMessages: ValidationMessagesService,
    private router: Router,
    public toast: ToastrService,
    public route: ActivatedRoute,
    public configService: ConfigService,
    private cdr: ChangeDetectorRef) {
    if (this.route.snapshot.data.data) {
      this.getApplicationData = this.route.snapshot.data.data[0];
    }
    debugger
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      if (this.id) {
        this.InitializeForm();
        this.applicationTypeForm.patchValue({
          "id": this.getApplicationData.id,
          "appTypeId": this.getApplicationData.appTypeId,
          "attendingTime": new Date(this.getApplicationData.attendingTime),
          "customerReview": new Date(this.getApplicationData.customerReview),
          "managerReview": new Date(this.getApplicationData.managerReview),
          "consultation": new Date(this.getApplicationData.consultation),
          "customerReSubmit": new Date(this.getApplicationData.customerReSubmit),
          "customerPayment": new Date(this.getApplicationData.customerPayment),
          "govtEntityApproval": new Date(this.getApplicationData.govtEntity),
          "totalKpis": new Date(this.getApplicationData.totalKpis),
          "isActive": this.getApplicationData.isActive
        });
      }
      else {
        this.InitializeForm();
      }
    });
  }

  ngOnInit() {
    this.getApplicationType();
  }
  InitializeForm() {
    this.applicationTypeForm = this.fb.group(
      {
        "id": [0, Validators.compose([Validators.required])],
        "appTypeId": [null, Validators.compose([Validators.required])],
        "attendingTime": ['', Validators.compose([Validators.required])],
        "customerReview": ['', Validators.compose([Validators.required])],
        "managerReview": ['', Validators.compose([Validators.required])],
        "consultation": ['', Validators.compose([Validators.required])],
        "customerReSubmit": ['', Validators.compose([Validators.required])],
        "customerPayment": ['', Validators.compose([Validators.required])],
        "govtEntityApproval": ['', Validators.compose([Validators.required])],
        "totalKpis": ['', Validators.compose([Validators.required])],
        "isActive": [true]
      });
  }

  getApplicationType() {
    this.applicationType = this.commonService.getLookUp("?type=ApplicationTypes");
  }
  applicationTypeFormSubmit() {
    debugger
    if (this.applicationTypeForm.valid) {
      this.applicationTypeForm.value.attendingTime = this.validationMessages.convertUTCDateToLocalDate(this.applicationTypeForm.value.attendingTime);
      this.applicationTypeForm.value.customerReview = this.validationMessages.convertUTCDateToLocalDate(this.applicationTypeForm.value.customerReview);
      this.applicationTypeForm.value.managerReview = this.validationMessages.convertUTCDateToLocalDate(this.applicationTypeForm.value.managerReview);
      this.applicationTypeForm.value.consultation = this.validationMessages.convertUTCDateToLocalDate(this.applicationTypeForm.value.consultation);
      this.applicationTypeForm.value.customerReSubmit = this.validationMessages.convertUTCDateToLocalDate(this.applicationTypeForm.value.customerReSubmit);
      this.applicationTypeForm.value.customerPayment = this.validationMessages.convertUTCDateToLocalDate(this.applicationTypeForm.value.customerPayment);
      this.applicationTypeForm.value.govtEntity = this.validationMessages.convertUTCDateToLocalDate(this.applicationTypeForm.value.govtEntityApproval);
      this.applicationTypeForm.value.totalKpis = this.validationMessages.convertUTCDateToLocalDate(this.applicationTypeForm.value.totalKpis);
      //Api call
      this.loading = true;
      if (this.id) {
        this.configService.AppTypeKPIs(this.applicationTypeForm.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigate(["/pages/master-data/application-type"]);
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
      else {
        this.configService.AppTypeKPIs(this.applicationTypeForm.value).subscribe(res => {
          this.loading = false;
          let response = res;
          if ((response.result && response.result.code == 200) || response.code == 200) {
            this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
              this.toast.success(text);
            });
            this.router.navigate(["/pages/master-data/application-type"]);
          }
        }, error => {
          this.loading = false;
          this.cdr.markForCheck();
        });
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.applicationTypeForm);
    }
  }
}
