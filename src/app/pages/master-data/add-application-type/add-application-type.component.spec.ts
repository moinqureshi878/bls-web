import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddApplicationTypeComponent } from './add-application-type.component';

describe('AddApplicationTypeComponent', () => {
  let component: AddApplicationTypeComponent;
  let fixture: ComponentFixture<AddApplicationTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddApplicationTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddApplicationTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
