import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MasterDataService } from '../../core/appServices/index.service';
import { ActivatedRoute, Router } from '@angular/router';
import { masterdata } from '../../core/appModels/masterdata.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TranslationService } from '../../core/_base/layout';
import { ToastrService } from 'ngx-toastr';


/* Select Box */
export interface Food {
  value: string;
  viewValue: string;
}

/* Table */
export interface UserData {
  ValueAr: string;
  ValueEn: string;
  TableName: string;
  isActive: string;

}
@Component({
  selector: 'kt-master-data',
  templateUrl: './master-data.component.html',
  styleUrls: ['./master-data.component.scss']
})

export class MasterDataComponent implements OnInit {
  ELEMENT_DATA: masterdata[] = [];

  displayedColumns7: string[] = ['ValueAr', 'ValueEn', 'editView', 'isActive'];
  dataSource7 = new MatTableDataSource<masterdata>(this.ELEMENT_DATA);

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  typesName: Observable<any>;
  masterData: any;
  getMasterData: any;
  typeId: number;
  id: number;
  constructor(
    private masterDataservice: MasterDataService,
    private toast: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private translationService: TranslationService) {

    this.getMasterData = this.masterDataservice.GetAllLookups().pipe(map(resp => resp["data"])).subscribe(res=>{
      this.getMasterData=res;
      this.masterData = this.getMasterData;
      this.dataSource7 = new MatTableDataSource(this.masterData);
      this.dataSource7.paginator = this.paginator7;
      this.dataSource7.sort = this.sort7;
    });
   
  }

  ngOnInit() {
    this.getTypes();
  }
  getTypes() {
    this.typesName = this.masterDataservice.getMaterByType().pipe(map(resp => resp["data"]))
  }

  filterByMasterType(id) {
    this.masterData = this.getMasterData.filter(res => res.headID == id);
    this.dataSource7 = new MatTableDataSource(this.masterData);
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
  }
  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  isActiveMasterData(id) {
    this.changeDataSource(id);
    this.masterDataservice.DeleteMaster(id).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {

      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toast.error(text);
        });
      }
    });
  }
  changeDataSource(ID: number) {
    let index = this.masterData.findIndex(x => x.id == ID);
    let isActive: boolean = this.masterData[index].isActive == true ? false : true;
    this.masterData[index].isActive = isActive;
  }
  insertSpaces(string) {
    string = string.replace(/([a-z])([A-Z])/g, '$1 $2');
    string = string.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2')
    return string;
  }
}
