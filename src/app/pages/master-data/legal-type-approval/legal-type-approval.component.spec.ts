import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalTypeApprovalComponent } from './legal-type-approval.component';

describe('LegalTypeApprovalComponent', () => {
  let component: LegalTypeApprovalComponent;
  let fixture: ComponentFixture<LegalTypeApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegalTypeApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalTypeApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
