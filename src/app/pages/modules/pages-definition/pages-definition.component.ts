
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';
import { PagesService } from '../../../core/appServices/pages.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PagesModels } from '../../../core/appModels/pages.model';
import { map } from 'rxjs/operators';
import { ModuleService, SubModuleService } from '../../../core/appServices/index.service';
import { Observable } from 'rxjs';
import { TranslationService } from '@translateService/translation.service';
import { ToastrService } from 'ngx-toastr';

/* Select Box */
export interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'kt-pages-definition',
  templateUrl: './pages-definition.component.html',
  styleUrls: ['./pages-definition.component.scss']
})
export class PagesDefinitionComponent implements OnInit {
  ELEMENT_DATA: PagesModels[] = [];

  displayedColumns7: string[] = ['nameAr', 'nameEn', 'modulesRoot', 'isDefault', 'remarks', 'pageUrl', 'editView', 'isActive'];
  dataSource7 = new MatTableDataSource<PagesModels>(this.ELEMENT_DATA);


  closeResult: string; //Bootstrap Modal Popup

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  typeModuleName: Observable<any>;
  typeSubModuleName: Observable<any>;
  pages: any;
  getPages: any;
  moduleId: number;
  subModuleId: number[];
  filterModulePages: any;
  filterSubModulePages = [];

  constructor(
    private modalService: NgbModal,
    private pagesService: PagesService,
    private route: ActivatedRoute,
    private router: Router,
    public moduleService: ModuleService,
    public subModuleService: SubModuleService,
    private translationService: TranslationService,
    public toastrService: ToastrService,
  ) {
    this.pages = this.route.snapshot.data.data;
    this.getPages = this.pages;
    this.dataSource7 = new MatTableDataSource<PagesModels>(this.getPages);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
    this.getModule();
    //this.getSubModule();
  }
  getModule() {
    this.typeModuleName = this.moduleService.getModules().pipe(map(resp => resp["data"]));
  }
  getSubModule(ID: number) {
    //this.typeSubModuleName = this.subModuleService.getSubModules().pipe(map(resp => resp["data"]));map
    this.typeSubModuleName = this.subModuleService.GetSubModulesByModuleID(ID).pipe(map(resp => resp["data"]));
  }
  filterByModuleName(moduleId) {
    this.moduleId = moduleId;
    if (!this.moduleId) {
      this.dataSource7 = new MatTableDataSource<PagesModels>(this.getPages);
      this.dataSource7.paginator = this.paginator7;
      this.dataSource7.sort = this.sort7;
      this.applyFilter7((<HTMLInputElement>document.getElementById('managePagesDefinition')).value);
    }
    else {
      this.filterModulePages = this.pages.filter(element => {
        return element.moduleId == this.moduleId
      });
      if (this.subModuleId && this.subModuleId.length > 0) {
        this.filterBySubModuleName(this.subModuleId);
      }
      else {
        this.getPages = this.filterModulePages;
        this.dataSource7 = new MatTableDataSource<PagesModels>(this.getPages);
        this.dataSource7.paginator = this.paginator7;
        this.dataSource7.sort = this.sort7;
        this.applyFilter7((<HTMLInputElement>document.getElementById('managePagesDefinition')).value);
      }
    }
    this.getSubModule(moduleId);

  }
  filterBySubModuleName(subModuleId: number[]) {
    this.subModuleId = subModuleId;
    if (this.moduleId) {
      if (this.subModuleId.length > 0) {
        this.filterSubModulePages = [];
        this.subModuleId.forEach(ids => {
          let subModulePages = this.filterModulePages.filter(element => {
            return ids == element.subModuleId
          });
          subModulePages.forEach(pages => {
            this.filterSubModulePages.push(pages);
          });
        });
        this.getPages = this.filterSubModulePages;
        this.dataSource7 = new MatTableDataSource<PagesModels>(this.getPages);
        this.dataSource7.paginator = this.paginator7;
        this.dataSource7.sort = this.sort7;
        this.applyFilter7((<HTMLInputElement>document.getElementById('managePagesDefinition')).value);
      }
      else {
        this.filterByModuleName(this.moduleId);
      }
    }
    else {
      if (this.subModuleId.length > 0) {
        this.filterSubModulePages = [];
        this.subModuleId.forEach(ids => {
          let subModulePages = this.pages.filter(element => {
            return ids == element.subModuleId
          });
          subModulePages.forEach(pages => {
            this.filterSubModulePages.push(pages);
          });
        });
        this.getPages = this.filterSubModulePages;
        this.dataSource7 = new MatTableDataSource<PagesModels>(this.getPages);
        this.dataSource7.paginator = this.paginator7;
        this.dataSource7.sort = this.sort7;
        this.applyFilter7((<HTMLInputElement>document.getElementById('managePagesDefinition')).value);
      }
      else {
        this.filterByModuleName(this.moduleId);
      }
    }
  }
  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  onEditClick(id) {
    this.router.navigate(['/pages/modules/edit-pages/' + id]);
  }
  CheckboxChange(id) {
    this.changeDataSource(id);
    
    this.pagesService.DeletePages(id).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {
       
      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toastrService.error(text);
        });
      }
    });
  }
  changeDataSource(ID: number) {
    let indexPages = this.pages.findIndex(x => x.id == ID);
    let indexGetPages = this.getPages.findIndex(x => x.id == ID);
    let isActive: boolean = this.pages[indexPages].isActive == true ? false : true;
    this.pages[indexPages].isActive = isActive;
    this.getPages[indexGetPages].isActive = isActive;
  }
  /* Tooltip */
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[2]);
}
