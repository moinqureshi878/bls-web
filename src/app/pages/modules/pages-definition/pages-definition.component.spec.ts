import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagesDefinitionComponent } from './pages-definition.component';

describe('PagesDefinitionComponent', () => {
  let component: PagesDefinitionComponent;
  let fixture: ComponentFixture<PagesDefinitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagesDefinitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagesDefinitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
