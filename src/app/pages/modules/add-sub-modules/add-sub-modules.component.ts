import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AsyncFormValidatorService } from '../../../core/validator/async-form-validator.service';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { commentValidator, nameArValidator } from '../../../core/validator/form-validators.service';
import { ModuleService, SubModuleService } from '../../../core/appServices/index.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { SubModulesModels } from '../../../core/appModels/submodules.model';
import { TranslationService } from '@translateService/translation.service';

@Component({
  selector: 'kt-add-sub-modules',
  templateUrl: './add-sub-modules.component.html',
  styleUrls: ['./add-sub-modules.component.scss']
})
export class AddSubModulesComponent implements OnInit {
  ID: number;
  SubModuleDataEdit: SubModulesModels;

  addSubmodule: FormGroup;

  moduleddl: Observable<any>;

  loading = false;
  //lastIndex: number;
  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    public toastrService: ToastrService,
    private moduleService: ModuleService,
    private subModuleService: SubModuleService,
    private translationService: TranslationService
  ) {
    this.SubModuleDataEdit = this.route.snapshot.data.data;

    this.route.params.subscribe(params => {
      this.ID = +params['id'];
    });
  }

  ngOnInit() {
    this.moduleDDl();
    this.createSubModuleForm();
    if (this.ID) {
      this.onEdit();
    }
  }

  moduleDDl() {
    this.moduleddl = this.moduleService.getActiveModules()
      .pipe(
        map(resp => resp["data"])
      )
  }

  createSubModuleForm() {
    this.addSubmodule = this.fb.group({
      moduleName: ['', Validators.required],
      subModuleNameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100),nameArValidator])],
      subModuleNameEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      orderSequenceSubModule : [null],
      remarks: ['', Validators.compose([Validators.maxLength(500), commentValidator])],
      isActive: [true, Validators.required],
    });
    //this.lastIndex = this.addSubmodule.value.devices.length - 1;
  }
  addSubmoduleFormSubmit() {
    if (this.addSubmodule.valid) {
      this.loading = true;

      let SubModuleJson = {
        "moduleCode": this.addSubmodule.value.moduleName,
        "nameAr": this.addSubmodule.value.subModuleNameAr,
        "nameEn": this.addSubmodule.value.subModuleNameEn,
        "remarks": this.addSubmodule.value.remarks,
        "isActive": this.addSubmodule.value.isActive,
        "ordersequence" : this.addSubmodule.value.orderSequenceSubModule,
      }

      if (this.ID) {
        this.updateMethod(SubModuleJson);

      }
      else {
        this.addMethod(SubModuleJson);
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.addSubmodule);
    }
  }

  addMethod(subModuleJson: any) {
    this.subModuleService.AddSubModules(subModuleJson)
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.loading = false;
          this.addSubmodule.reset();
          setTimeout(() => {
            Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
              item.classList.remove("mat-form-field-invalid");
            });
          }, 50);
          this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
            this.toastrService.success(text);
          });
          this.router.navigateByUrl('/pages/modules/sub-modules-definition');
        }
        else {
          this.loading = false;
          this.toastrService.error('Something went wrong. Please try again');
        }
      })
  }

  updateMethod(subModuleJson: any) {
    this.subModuleService.UpdateSubModules(subModuleJson, this.ID)
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.loading = false;
          this.addSubmodule.reset();
          setTimeout(() => {
            Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
              item.classList.remove("mat-form-field-invalid");
            });
          }, 50);
          this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
            this.toastrService.success(text);
          });
          this.router.navigateByUrl('/pages/modules/sub-modules-definition');
        }
        else {
          this.loading = false;
          this.toastrService.error('Something went wrong. Please try again');
        }
      })
  }

  onEdit() {
    if (this.SubModuleDataEdit) {
      this.addSubmodule.patchValue({
        moduleName: this.SubModuleDataEdit.moduleCode,
        subModuleNameAr: this.SubModuleDataEdit.nameAr,
        subModuleNameEn: this.SubModuleDataEdit.nameEn,
        remarks: this.SubModuleDataEdit.remarks,
        isActive: this.SubModuleDataEdit.isActive,
        orderSequenceSubModule : this.SubModuleDataEdit.ordersequence
      })
    }
  }
}
