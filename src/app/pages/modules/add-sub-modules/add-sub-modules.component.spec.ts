import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubModulesComponent } from './add-sub-modules.component';

describe('AddSubModulesComponent', () => {
  let component: AddSubModulesComponent;
  let fixture: ComponentFixture<AddSubModulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSubModulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSubModulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
