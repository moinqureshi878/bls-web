import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubModulesDefinitionComponent } from './sub-modules-definition.component';

describe('SubModulesDefinitionComponent', () => {
  let component: SubModulesDefinitionComponent;
  let fixture: ComponentFixture<SubModulesDefinitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubModulesDefinitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubModulesDefinitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
