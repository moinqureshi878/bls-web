
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { SubModuleService, ModuleService } from '../../../core/appServices/index.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SubModulesModels } from '../../../core/appModels/submodules.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TranslationService } from '@translateService/translation.service';
import { ToastrService } from 'ngx-toastr';

/* Select Box */
export interface Food {
  value: string;
  viewValue: string;
}

/* Table */
export interface UserData {
  subModuleNameAr: string;
  subModuleNameEn: string;
  remarks: string;
  type: string;
  linkedwithModule: string;
  editView: string;
  isActive: string;

}

@Component({
  selector: 'kt-sub-modules-definition',
  templateUrl: './sub-modules-definition.component.html',
  styleUrls: ['./sub-modules-definition.component.scss'],
})
export class SubModulesDefinitionComponent implements OnInit {
  ELEMENT_DATA: SubModulesModels[] = [];

  displayedColumns7: string[] = ['nameAr', 'nameEn', 'remarks', 'moduleNameEn', 'typeNameEn', 'ordersequence', 'editView', 'isActive'];
  dataSource7 = new MatTableDataSource<SubModulesModels>(this.ELEMENT_DATA);

  closeResult: string; //Bootstrap Modal Popup

  @ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort7: MatSort;
  typeModuleName: Observable<any>;
  moduleId: number;
  subModules: any;
  getSubModules: any;
  constructor(
    private modalService: NgbModal,
    private subModuleService: SubModuleService,
    private moduleService: ModuleService,
    private route: ActivatedRoute,
    private router: Router,
    private translationService: TranslationService,
    public toastrService: ToastrService,
  ) {
    this.subModules = this.route.snapshot.data.data;
    this.getSubModules = this.subModules;
    this.dataSource7 = new MatTableDataSource<SubModulesModels>(this.getSubModules);
  }

  ngOnInit() {
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
    this.getModule();
  }
  getModule() {
    this.typeModuleName = this.moduleService.getModules().pipe(map(resp => resp["data"]))
  }
  filterByModuleName(moduleId) {
    let filterSubModule = this.subModules.filter(element => {
      return element.moduleId == moduleId
    });
    this.getSubModules = filterSubModule;
    this.dataSource7 = new MatTableDataSource<SubModulesModels>(filterSubModule);
    this.dataSource7.paginator = this.paginator7;
    this.dataSource7.sort = this.sort7;
    this.applyFilter7((<HTMLInputElement>document.getElementById('subModuleDefinition')).value);
  }
  applyFilter7(filterValue: string) {
    this.dataSource7.filter = filterValue.trim().toLowerCase();
    if (this.dataSource7.paginator) {
      this.dataSource7.paginator.firstPage();
    }
  }

  //..Modal Popup Start
  open(content) {
    //const modalRef = this.modalService.open({ size: 'lg', backdrop: 'static' });
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //..Modal Popup End

  onEditClick(row) {
    this.router.navigate(['/pages/modules/edit-sub-modules/' + row.id]);
  }
  CheckboxChange(id) {
    this.changeDataSource(id);

    this.subModuleService.DeleteSubModules(id).subscribe(data => {
      let response = data;
      if ((response.result && response.result.code == 200) || response.code == 200) {

      }
      else {
        this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
          this.toastrService.error(text);
        });
      }
    });
  }
  changeDataSource(ID: number) {
    let indexSubModule = this.subModules.findIndex(x => x.id == ID);
    let indexGetSubModule = this.getSubModules.findIndex(x => x.id == ID);
    let isActive: boolean = this.subModules[indexSubModule].isActive == true ? false : true;
    this.subModules[indexSubModule].isActive = isActive;
    this.getSubModules[indexGetSubModule].isActive = isActive;
  }
}
