import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulesDefinitionComponent } from './modules-definition.component';

describe('ModulesDefinitionComponent', () => {
  let component: ModulesDefinitionComponent;
  let fixture: ComponentFixture<ModulesDefinitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModulesDefinitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModulesDefinitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
