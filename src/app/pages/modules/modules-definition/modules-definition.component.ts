
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ModuleService } from '../../../core/appServices/modules.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ModulesModels } from '../../../core/appModels/modules.model';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from '@translateService/translation.service';

@Component({
	selector: 'kt-modules-definition',
	templateUrl: './modules-definition.component.html',
	styleUrls: ['./modules-definition.component.scss']
})
export class ModulesDefinitionComponent implements OnInit {
	ELEMENT_DATA: ModulesModels[] = [];

	displayedColumns7: string[] = ['nameAr', 'nameEn', 'remarks', 'typeNameEn', 'applicationEn','ordersequence', 'moduleId', 'isActive'];
	dataSource7 = new MatTableDataSource<ModulesModels>(this.ELEMENT_DATA);
	closeResult: string; //Bootstrap Modal Popup

	@ViewChild('matPaginator7', { static: true }) paginator7: MatPaginator;
	@ViewChild(MatSort, { static: true }) sort7: MatSort;
	modules: any;
	constructor(
		private modalService: NgbModal,
		private moduleService: ModuleService,
		private route: ActivatedRoute,
		private router: Router,
		private translationService: TranslationService,
		public toastrService: ToastrService,
	) {
		this.modules = this.route.snapshot.data.data;
		this.dataSource7 = new MatTableDataSource<ModulesModels>(this.modules);
	}

	ngOnInit() {
		this.dataSource7.paginator = this.paginator7;
		this.dataSource7.sort = this.sort7;
	}

	applyFilter7(filterValue: string) {
		this.dataSource7.filter = filterValue.trim().toLowerCase();

		if (this.dataSource7.paginator) {
			this.dataSource7.paginator.firstPage();
		}
	}
	open(content) {
		this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	}
	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}

	onEditClick(row) {
		this.router.navigate(['/pages/modules/edit-modules/' + row.id]);
	}
	CheckboxChange(id) {
		this.changeDataSource(id);
		
		this.moduleService.DeleteModules(id).subscribe(data => {
			let response = data;
			if ((response.result && response.result.code == 200) || response.code == 200) {
			
			}
			else {
				this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
					this.toastrService.error(text);
				});
			}
		});
	}
	changeDataSource(ID: number) {
		let index = this.modules.findIndex(x => x.id == ID);
		let isActive: boolean = this.modules[index].isActive == true ? false : true;
		this.modules[index].isActive = isActive;
	}
}
