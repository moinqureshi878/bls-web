import { Component, OnInit } from '@angular/core';

import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import {
  matchingPasswords, emailValidator,
  phoneNumberValidator, nameValidator,
  passwordValidator, commentValidator,
  nameArValidator
} from '../../../core/validator/form-validators.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AsyncFormValidatorService } from '../../../core/validator/async-form-validator.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ModuleService, SubModuleService, PagesService, CommonService } from '../../../core/appServices/index.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { PagesModels } from '../../../core/appModels/pages.model';
import { TranslationService } from '@translateService/translation.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'kt-add-pages',
  templateUrl: './add-pages.component.html',
  styleUrls: ['./add-pages.component.scss']
})
export class AddPagesComponent implements OnInit {

  ID: number;
  PagesDataEdit: PagesModels;

  pagesFormGroup: FormGroup;

  moduleddl: Observable<any>;
  submoduleddl: Observable<any>;
  pagesddl: Observable<any>;

  loading = false;
  private _pageURL = 'assets/pageURL.json';
  constructor(
    private fb: FormBuilder,
    public router: Router,
    private http: HttpClient,
    public route: ActivatedRoute,
    public validationMessages: ValidationMessagesService,
    public asyncFormValidatorService: AsyncFormValidatorService,
    private moduleService: ModuleService,
    private subModuleService: SubModuleService,
    private pagesService: PagesService,
    public toastrService: ToastrService,
    private commonService: CommonService,
    private translationService: TranslationService
  ) {
    this.PagesDataEdit = this.route.snapshot.data.data;

    if (this.PagesDataEdit) {
      this.subModuleDDl(this.PagesDataEdit.moduleId);
    }

    this.route.params.subscribe(params => {
      this.ID = +params['id'];
    });
    this.pagesddl = this.getPagesURL();
  }
  public getPagesURL(): Observable<any> {
    return this.http.get(this._pageURL);
  }
  ngOnInit() {
    this.moduleDDl();
    this.createFormPages();
    if (this.ID) {
      this.onEdit();
    }
  }

  moduleDDl() {
    this.moduleddl = this.moduleService.getActiveModules()
      .pipe(
        map(resp => resp["data"])
      )
  }

  subModuleDDl(ID: number) {
    this.submoduleddl = this.subModuleService.GetSubModulesByModuleID(ID)
      .pipe(
        map(resp => resp["data"])
      )
  }

  createFormPages() {
    this.pagesFormGroup = this.fb.group({
      moduleDdl: ['', Validators.compose([Validators.required])],
      submoduleDdl: ['', Validators.compose([Validators.required])],
      pageNameEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      pageNameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100), nameArValidator])],
      pageUrl: ['', Validators.compose([Validators.required, Validators.maxLength(500)])],
      remarks: ['', Validators.compose([Validators.maxLength(500), commentValidator])],
      IsDefault: [true],
      isActive: [true, Validators.required]
    });
  }

  onEdit() {
    if (this.PagesDataEdit) {
      this.pagesFormGroup.patchValue({
        moduleDdl: this.PagesDataEdit.moduleId,
        submoduleDdl: this.PagesDataEdit.subModuleId,
        pageNameEn: this.PagesDataEdit.nameEn,
        pageNameAr: this.PagesDataEdit.nameAr,
        pageUrl: this.PagesDataEdit.pageUrl,
        remarks: this.PagesDataEdit.remarks,
        isActive: this.PagesDataEdit.isActive,
        IsDefault: this.PagesDataEdit.isDefault,
      })
    }
  }

  submitPages() {
    if (this.pagesFormGroup.valid) {
      this.loading = true;

      let PagesJson = {
        "moduleId": this.pagesFormGroup.value.moduleDdl,
        "subModuleId": this.pagesFormGroup.value.submoduleDdl,
        "nameEn": this.pagesFormGroup.value.pageNameEn,
        "nameAr": this.pagesFormGroup.value.pageNameAr,
        "pageUrl": this.pagesFormGroup.value.pageUrl,
        "remarks": this.pagesFormGroup.value.remarks,
        "isActive": this.pagesFormGroup.value.isActive,
        "IsDefault": this.pagesFormGroup.value.IsDefault
      }

      if (this.ID) {
        this.updateMethod(PagesJson);

      }
      else {
        this.addMethod(PagesJson);
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.pagesFormGroup);
    }
  }

  addMethod(pagesJson: any) {
    this.pagesService.AddPages(pagesJson)
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.loading = false;
          this.pagesFormGroup.reset();
          setTimeout(() => {
            Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
              item.classList.remove("mat-form-field-invalid");
            });
          }, 50);
          this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
            this.toastrService.success(text);
          });
          this.router.navigateByUrl('/pages/modules/pages-definition');
        }
        else {
          this.loading = false;
          this.toastrService.error('Something went wrong. Please try again');
        }
      })
  }

  updateMethod(pagesJson: any) {
    this.pagesService.UpdatePages(pagesJson, this.ID)
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.loading = false;
          this.pagesFormGroup.reset();
          setTimeout(() => {
            Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
              item.classList.remove("mat-form-field-invalid");
            });
          }, 50);
          this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
            this.toastrService.success(text);
          });
          this.router.navigateByUrl('/pages/modules/pages-definition');
        }
        else {
          this.loading = false;
          this.toastrService.error('Something went wrong. Please try again');
        }
      })
  }

  onCancel() {
    this.router.navigateByUrl('/pages/modules/pages-definition');
  }

  onModuleChange() {
    this.subModuleDDl(this.pagesFormGroup.value.moduleDdl)
  }
}
