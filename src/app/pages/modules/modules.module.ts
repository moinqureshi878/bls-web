import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '../../views/partials/partials.module';

import { ModulesComponent } from './modules.component';
import { ModulesDefinitionComponent } from './modules-definition/modules-definition.component';
import { SubModulesDefinitionComponent } from './sub-modules-definition/sub-modules-definition.component';
import { PagesDefinitionComponent } from './pages-definition/pages-definition.component';
import { AddModulesComponent } from './add-modules/add-modules.component';
import { AddSubModulesComponent } from './add-sub-modules/add-sub-modules.component';
import { AddPagesComponent } from './add-pages/add-pages.component';


import { ModuleService, SubModuleService, PagesService, UserService } from '../../core/appServices/index.service';
import { AppResolver } from '../../core/resolvers/app.resolver';

import { MatTableExporterModule } from 'mat-table-exporter';
import { AuthGuard } from '../../core/auth';
import { TranslateModule } from '@ngx-translate/core';
import {
  MatSortModule,
} from '@angular/material';
@NgModule({
  declarations: [ModulesComponent, ModulesDefinitionComponent, SubModulesDefinitionComponent, PagesDefinitionComponent, AddModulesComponent, AddSubModulesComponent, AddPagesComponent],
 
  imports: [
    PartialsModule,
   
    NgbModule.forRoot(),
    MatTableExporterModule,
    MatSortModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: ModulesComponent
      },
      {
        path: 'modules-definition',
        canActivate: [AuthGuard],
        component: ModulesDefinitionComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'add-modules',
        canActivate: [AuthGuard],
        component: AddModulesComponent
      },
      {
        path: 'edit-modules/:id',
        canActivate: [AuthGuard],
        component: AddModulesComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'sub-modules-definition',
        canActivate: [AuthGuard],
        component: SubModulesDefinitionComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'add-sub-modules',
        canActivate: [AuthGuard],
        component: AddSubModulesComponent
      },
      {
        path: 'edit-sub-modules/:id',
        canActivate: [AuthGuard],
        component: AddSubModulesComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'pages-definition',
        canActivate: [AuthGuard],
        component: PagesDefinitionComponent,
        resolve: { data: AppResolver }
      },
      {
        path: 'add-pages',
        canActivate: [AuthGuard],
        component: AddPagesComponent
      },
      {
        path: 'edit-pages/:id',
        canActivate: [AuthGuard],
        component: AddPagesComponent,
        resolve: { data: AppResolver }
      }
    ]),
  ],
  providers: [
    AppResolver,
    ModuleService,
    SubModuleService,
    PagesService,
    UserService
    // {
    // 	provide: HTTP_INTERCEPTORS,
    // 	useClass: JWTInterceptor,
    // 	multi: true
    // },
  ]
})
export class ModulesModule { }
