import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AsyncFormValidatorService, } from '../../../core/validator/async-form-validator.service';
import { commentValidator, nameArValidator } from '../../../core/validator/form-validators.service';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { ModuleService, CommonService } from '../../../core/appServices/index.service';
import { ModulesModels } from '../../../core/appModels/modules.model';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from '@translateService/translation.service';

@Component({
  selector: 'kt-add-modules',
  templateUrl: './add-modules.component.html',
  styleUrls: ['./add-modules.component.scss']
})
export class AddModulesComponent implements OnInit {
  ID: number;
  ModulesDataEdit: ModulesModels;

  addModule: FormGroup;


  module: ModulesModels;
  typeddl: Observable<any>;
  applicationsddl: Observable<any>;

  loading = false;

  constructor(
    public router: Router,
    private fb: FormBuilder,
    public route: ActivatedRoute,
    public asyncFormValidatorService: AsyncFormValidatorService,
    public validationMessages: ValidationMessagesService,
    public toastrService: ToastrService,
    private moduleService: ModuleService,
    private commonService: CommonService,
    private translationService: TranslationService
  ) {
    this.ModulesDataEdit = this.route.snapshot.data.data;

    this.route.params.subscribe(params => {
      this.ID = +params['id'];
    });
    this.module = new ModulesModels;
  }

  ngOnInit() {
    this.getModuleTypeDDl();
    this.getApplicationsDDl();
    this.createModuleForm();
    if (this.ID) {
      this.onEdit();
    }
  }

  getModuleTypeDDl() {
    this.typeddl = this.commonService.getLookUp("?type=moduleType");
  }

  getApplicationsDDl() {
    this.applicationsddl = this.commonService.getLookUp("?type=applications");
  }

  createModuleForm() {
    this.addModule = this.fb.group({
      moduleNameAr: ['', Validators.compose([Validators.required, Validators.maxLength(100),nameArValidator])],
      moduleNameEn: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      orderSequenceModule: [null],
      icon: [''],
      type: ['', Validators.required],
      application: ['', Validators.required],
      remarks: ['', Validators.compose([Validators.maxLength(500), commentValidator])],
      isActive: [true, Validators.required],
    });

  }

  addModuleFormSubmit() {
    if (this.addModule.valid) {

      this.loading = true;

      // Creating custom json because form control name is not same as add API accept.
      let ModuleJson = {
        "nameEn": this.addModule.value.moduleNameEn,
        "nameAr": this.addModule.value.moduleNameAr,
        "typeCode": this.addModule.value.type,
        "applicationCode": +this.addModule.value.application,
        "remarks": this.addModule.value.remarks,
        "isActive": this.addModule.value.isActive,
        "ordersequence" : this.addModule.value.orderSequenceModule,
        "icon": this.addModule.value.icon
      }


      if (this.ID) {
        this.updateMethod(ModuleJson);

      }
      else {
        this.addMethod(ModuleJson);
      }
    }
    else {
      this.validationMessages.validateAllFormFields(this.addModule);
    }
  }

  addMethod(moduleJson: any) {
    this.moduleService.AddModules(moduleJson)
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.loading = false;
          this.addModule.reset();
          setTimeout(() => {
            Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
              item.classList.remove("mat-form-field-invalid");
            });
          }, 50);
          this.translationService.getTranslation('FormCRUD.ADD').subscribe((text: string) => {
            this.toastrService.success(text);
          });
          this.router.navigateByUrl('/pages/modules/modules-definition');
        }
        else {
          this.loading = false;
          this.toastrService.error('Something went wrong. Please try again');
        }
      })
  }

  updateMethod(moduleJson: any) {
    this.moduleService.UpdateModules(moduleJson, this.ID)
      .subscribe(data => {
        if (data["result"].code === 200) {
          this.loading = false;
          this.addModule.reset();
          setTimeout(() => {
            Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
              item.classList.remove("mat-form-field-invalid");
            });
          }, 50);
          this.translationService.getTranslation('FormCRUD.UPDATE').subscribe((text: string) => {
            this.toastrService.success(text);
          });
          this.router.navigateByUrl('/pages/modules/modules-definition');
        }
        else {
          this.loading = false;
          this.toastrService.error('Something went wrong. Please try again');
        }
      })
  }

  onEdit() {
    if (this.ModulesDataEdit) {
      
      this.addModule.patchValue({
        moduleNameAr: this.ModulesDataEdit.nameAr,
        moduleNameEn: this.ModulesDataEdit.nameEn,
        type: this.ModulesDataEdit.typeCode,
        application: +this.ModulesDataEdit.applicationCode,
        remarks: this.ModulesDataEdit.remarks,
        isActive: this.ModulesDataEdit.isActive,
        icon : this.ModulesDataEdit.icon,
        orderSequenceModule : this.ModulesDataEdit.ordersequence
      })
    }
  }
  createDeviceFormArray() {
    return this.fb.group({
      name: ['', Validators.required],
      macAddress: ['', Validators.required]
    });
  }
}
