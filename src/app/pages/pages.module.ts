import { NgModule } from '@angular/core';
import { PagesRoutingModule } from '../common-template/pages-routing.module';
import { PartialsModule } from '../views/partials/partials.module';
import { CoreModule } from '../core/core.module';

//session timeout
import { UserIdleModule } from 'angular-user-idle';
import { MaterialPreviewModule } from '../views/partials/content/general/material-preview/material-preview.module';

import { JwtTokenService } from '../core/appServices/index.service';
import { AuthGuard } from '../core/auth/_guards/auth.guard';
//import { NationalitiesComponent } from './nationalities/nationalities.component';
//import { MasterTypesComponent } from './master-types/master-types.component';
import { TranslateModule } from '@ngx-translate/core';
//import { WorkingHoursComponent } from './working-hours/working-hours.component';
//import { MasterDataComponent } from './master-data/master-data.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@NgModule({
	declarations:[] ,//[WorkingHoursComponent],
//	MasterDataComponent//NationalitiesComponent
//MasterTypesComponent
	imports: [
		MatSlideToggleModule,
		CoreModule,
		PagesRoutingModule,
		PartialsModule,
		MaterialPreviewModule,
		TranslateModule.forChild(),
		// HttpClientModule,

		UserIdleModule.forRoot({ idle: 5, timeout: 5, ping: 5 })
	],
	providers: [AuthGuard, JwtTokenService]
})
export class PagesModule {
	constructor() {
		console.log('PagesModule');
	}
}
