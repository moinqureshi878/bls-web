import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ActivationLinkComponent } from './activation-link/activation-link.component';
import { AccountActivationComponent } from './account-activation/account-activation.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { LoginVisitorComponent } from './login-visitor/login-visitor.component';
import { authDeactivateGuard } from '../core/auth/index';
import { LoginGuard } from '../core/auth/_guards/login.guard';



const routes: Routes = [{
  path: '',
  component: AuthComponent,

  // canDeactivate: [authDeactivateGuard],
  canActivate: [LoginGuard],
  children: [
    {
      path: '',
      redirectTo: 'login',
      component: LoginComponent,
    },
    {
      path: 'login',
      component: LoginComponent,
    },
    {
      path: 'login-visitor',
      component: LoginVisitorComponent,
    },

    {
      path: 'forgot-password',
      component: ForgotPasswordComponent,
    },
    {
      path: 'reset-password',
      component: ResetPasswordComponent,
    },
    {
      path: 'activation-link',
      component: ActivationLinkComponent,
    },
    {
      path: 'account-activation',
      component: AccountActivationComponent,
    },

  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
