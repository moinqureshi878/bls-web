import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService, CommonService, JwtTokenService } from '../../core/appServices/index.service';
import { ReCaptcha2Component, ReCaptchaV3Service } from 'ngx-captcha';
import { EnvService } from '../../core/environment/env.service';
import { ValidationMessagesService } from './../../core/validator/validation-messages.service';
import { passwordValidator } from './../../core/validator/form-validators.service';
import { localStorageSession } from '../../core/appModels/localStorageSessions.model';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from '@translateService/translation.service';
import { PreviousRouteService } from '../../core/previous_route/previous-root.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '@env/environment';
import { CommonEnum } from '../../core/_enum/index.enum';

@Component({
  selector: 'kt-login-visitor',
  templateUrl: './login-visitor.component.html',
  styleUrls: ['./login-visitor.component.scss']
})
export class LoginVisitorComponent implements OnInit {

  PinFormPanel: boolean = false; // hidden by default
  LoginFormPanel: boolean = true;

  loginForm: FormGroup;
  TokenForm: FormGroup;
  loading = false;

  statusCode: string;
  lang: string;

  pin: any;

  localstorageSessionArray: localStorageSession[] = [];
  localstorageSession: localStorageSession = new localStorageSession();

  @ViewChild('captchaElem', { static: false }) captchaElem: ReCaptcha2Component;
  @ViewChild('noPages', { static: false }) content: ElementRef;
  @ViewChild("tb1", { static: false }) tb1: ElementRef;

  siteKey: string;
  blockAsyncCall: boolean = false;
  closeResult: string; //Bootstrap Modal Popup

  sessionTypeDiv: boolean = false;
  CommonEnum = CommonEnum;
  constructor(
    private reCaptchaV3Service: ReCaptchaV3Service,
    private fb: FormBuilder,
    private router: Router,
    private authentication: AuthenticationService,
    public validationMessages: ValidationMessagesService,
    private cdr: ChangeDetectorRef,
    public commonService: CommonService,
    public translationService: TranslationService,
    public toast: ToastrService,
    public previousRouteService: PreviousRouteService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private jwtTokenService: JwtTokenService,
    private env: EnvService) {
    this.siteKey = this.env.captchaKey;
    this.lang = localStorage.getItem("language");

    this.route.queryParams.subscribe(params => {
      this.sessionTypeDiv = params['sessionType'] == "sessiontimeout" ? true : false;
    });
  }

  ngOnInit() {
    this.initLoginForm();
    this.initTokenForm();
  }

  changeCaptcha(eve) {
    if (localStorage.getItem("language") == "en") {
      document.getElementById('langen').addEventListener('click', (eve) => this.changeCaptcha(eve));
    }
    else {
      document.getElementById('langar').addEventListener('click', (eve) => this.changeCaptcha(eve));
    }

    this.lang = localStorage.getItem("language");
    this.captchaElem.resetCaptcha();
  }

  ngAfterContentInit() {
    if (localStorage.getItem("language") == "en") {
      document.getElementById('langen').addEventListener('click', (eve) => this.changeCaptcha(eve));
    }
    else {
      document.getElementById('langar').addEventListener('click', (eve) => this.changeCaptcha(eve));
    }
  }

  regeneratePin() {
    this.statusCode = "";
    if (this.loginForm.valid) {
      let LoginBody = {
        "UserEmail": this.loginForm.value.usernameoremail,
        "Password": this.loginForm.value.password,
        "UserType": +CommonEnum.userTypeLoginVisitorPage
      }

      this.authentication.getOTP(LoginBody).subscribe(data => {
        if (data.code === 200) {
          this.loading = false;
          this.LoginFormPanel = false;
          this.PinFormPanel = true;
          console.log(data["data"]);
          // For Development
          if (!environment.production) {
            this.pin = data["data"].otp;
            this.verifyPin();
          }
        }
        else {
          this.statusCode = String(data.code);
        }
        this.cdr.markForCheck();
      })
    }
    else {
      this.validationMessages.validateAllFormFields(this.loginForm);
    }
  }

  initLoginForm() {
    this.loginForm = this.fb.group({
      usernameoremail: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required, Validators.maxLength(30), passwordValidator])],
      UserType: ['admin'],
      // recaptcha: ['', Validators.required],
      recaptcha: [''],
    });
  }

  initTokenForm() {
    this.TokenForm = this.fb.group({
      tb1: ['', Validators.compose([Validators.required])],
      tb2: ['', Validators.compose([Validators.required])],
      tb3: ['', Validators.compose([Validators.required])],
      tb4: ['', Validators.compose([Validators.required])]
    });
  }

  loginFormSubmit() {
    this.statusCode = "";
    if (this.loginForm.valid) {

      this.loading = true;
      let LoginBody = {
        "UserEmail": this.loginForm.value.usernameoremail,
        "Password": this.loginForm.value.password,
        "UserType": +49
      }

      this.authentication.getOTP(LoginBody)
        .subscribe(data => {
          if (data.code === 200) {
            this.loading = false;
            this.LoginFormPanel = false;
            this.PinFormPanel = true;
            console.log(data["data"]);
            if (!environment.production) {
              this.pin = data["data"].otp;
              this.verifyPin();
            }
            this.cdr.markForCheck();
          }
          else {
            this.statusCode = String(data.code);
            this.loading = false;
          }
          this.cdr.markForCheck();
        })
    }
  }
  goBacktoLogin() {
    this.LoginFormPanel = true;
    this.PinFormPanel = false;
  }
  verifyPin() {
    if (this.TokenForm.valid || !environment.production) {
      this.toast.clear();
      this.loading = true;
      let token;
      // For Development
      if (environment.production) {
        token = this.TokenForm.value.tb1 + this.TokenForm.value.tb2 + this.TokenForm.value.tb3 + this.TokenForm.value.tb4;
      }
      else {
        token = this.pin;
      }
      let verifyTokenJson = {
        "UserEmail": this.loginForm.value.usernameoremail,
        "OTP": token
      }
      if (!this.blockAsyncCall) {
        this.blockAsyncCall = true;
        this.authentication.VerifyToken(verifyTokenJson).subscribe(data => {
          if (data.code === 200) {
            this.loading = false;

            this.localstorageSession.token = data["data"].token;
            this.localstorageSession.pages = data["data"].pages;
            this.localstorageSession.sideMenu = data["data"].items;

            this.localstorageSession.sessionID = 0;

            this.localstorageSessionArray.push(this.localstorageSession);

            localStorage.setItem('userSession', JSON.stringify(this.localstorageSessionArray));
            // set menu in subject
            this.commonService.UpdateMenuFromLocalStorage();
            this.jwtTokenService.updateSessionFromLocalStorage();

            // localStorage.setItem('token', data["data"].token);
            // this.router.navigate(['/pages/admin/add-role'])
            localStorage.setItem('token', data["data"].token);
            localStorage.setItem('sideMenu', JSON.stringify(data["data"].items));
            localStorage.setItem('pages', JSON.stringify(data["data"].pages));
            this.router.navigate(["/pages/" + data["data"].pages[0].pageUrl]);
          }
          else if (data.code == 603) {
            this.modalService.open(this.content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
              this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
              this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
          }
          else {
            this.blockAsyncCall = false;
            this.translationService.getTranslation(String(data.code)).subscribe((text: string) => {
              this.toast.error(text);
            });
            this.tb1.nativeElement.focus();
            this.TokenForm.reset();
            this.loading = false;
          }
          this.loading = false;
          this.cdr.markForCheck();
        }
        )
      }
    }
  }

  processKeyUp(e, next, prev) {
    if (e.code === "Backspace") {
      if (prev) {
        prev.focus();
      }
    }
    else if (next == undefined) {
      this.verifyPin();
    }
    else {
      next.focus();
    }
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  closeNoPagesModal() {
    this.modalService.dismissAll();
    this.LoginFormPanel = true;
    this.PinFormPanel = false;
  }
}
