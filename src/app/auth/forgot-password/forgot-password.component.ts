import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ValidationMessagesService } from '../../core/validator/validation-messages.service';
import { UserService } from '../../core/appServices/user.service';
import { Router, NavigationExtras } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslationService } from '@translateService/translation.service';
import { emailValidator, phoneNumberValidator, phoneNumberMasking } from '../../core/validator/form-validators.service';
import { PreviousRouteService } from '../../core/previous_route/previous-root.service';

@Component({
  selector: 'kt-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  mobileNoMask = phoneNumberMasking();

  forgotPasswordForm: FormGroup;
  loading = false;

  constructor(
    public validationMessages: ValidationMessagesService,
    public userService: UserService,
    private cdr: ChangeDetectorRef,
    public toastrService: ToastrService,
    public router: Router,
    private translateService: TranslationService,
    public previousRouteService: PreviousRouteService,
    private fb: FormBuilder) {
  }
  ngOnInit() {
    this.initRegistrationForm();
  }
  initRegistrationForm() {
    this.forgotPasswordForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, emailValidator])],
      mobileNo: ['', Validators.compose([Validators.required, phoneNumberValidator])],
    });
  }

  forgotPasswordFormSubmit() {
    if (this.forgotPasswordForm.valid) {
      this.loading = true;
      // let navigationExtras: NavigationExtras = {
      //   queryParams: {
      //     user: JSON.stringify(this.forgotPasswordForm.value)
      //   }
      // };
      // this.router.navigate(['/notifications/reset-password'], navigationExtras);
      let obj = {
        "userEmail": this.forgotPasswordForm.value.email,
        "mobileNo": this.forgotPasswordForm.value.mobileNo
      }
      this.userService.ChangePassword(obj).subscribe(res => {
        this.loading = false;
        let response = res;
        if ((response.result && response.result.code == 200) || response.code == 200) {
          //this.router.navigateByUrl('/notifications/reset-password', { state: this.forgotPasswordForm.value });
          this.toastrService.success(this.translateService.getSelectedLanguage() == 'en' ? 'Email send to your registered email address' : 'البريد الالكتروني تم ارساله لبريدك الالكتروني المسجل');
          this.forgotPasswordForm.reset();
          setTimeout(() => {
            Array.from(document.getElementsByClassName('mat-form-field-invalid')).forEach(function (item) {
              item.classList.remove("mat-form-field-invalid");
            });
          }, 50);
        }
        else {
          this.loading = false;
          this.cdr.markForCheck();
          this.translateService.getTranslation(String(response.code)).subscribe((text: string) => {
            this.toastrService.error(text);
          });
        }
      });
    }
    else {
      this.validationMessages.validateAllFormFields(this.forgotPasswordForm);
    }
  }
}
