import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationMessagesService } from '../../core/validator/validation-messages.service';
import { TranslationService } from '@translateService/translation.service';
import { UserService } from '../../core/appServices/user.service';
import { matchingPasswords, passwordValidator } from '../../core/validator/form-validators.service';

@Component({
  selector: 'kt-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  resetPasswordForm: FormGroup;
  Verify: string;
  loading: boolean = false;
  constructor(public validationMessages: ValidationMessagesService,
    public userService: UserService,
    public router: Router,
    public toast: ToastrService,
    private cdr: ChangeDetectorRef,
    private translationService: TranslationService,
    private route: ActivatedRoute,
    private fb: FormBuilder) {
    // this.route.queryParams.subscribe(params => {
    //   if (params && params.user) {
    //      
    //     this.data = JSON.parse(params.user);
    //     console.log(this.data);
    //   }
    // });
    this.route.queryParams.subscribe(params => {
      this.Verify = params['Verify'];
    });
  }

  ngOnInit() {
    this.resetPasswordForm = this.fb.group({
      'securityToken': [this.Verify, Validators.required],
      'newPassword': ['', Validators.compose([Validators.required, Validators.maxLength(30), passwordValidator])],
      'confirmPassword': ['', Validators.compose([Validators.required])]
    }, { validator: matchingPasswords('newPassword', 'confirmPassword') });
  }
  resetPasswordFormSubmit() {
    this.resetPasswordForm.value.securityToken = this.Verify;
    if (this.resetPasswordForm.valid) {
      this.loading = true;
      this.userService.UpdatePassword(this.resetPasswordForm.value).subscribe(res => {
        this.loading = false;
        let response = res;
        if ((response.result && response.result.code == 200) || response.code == 200) {
          this.toast.success(this.translationService.getSelectedLanguage() == 'en' ? 'Password reset Successfully' : 'Password reset Successfully');
          this.router.navigateByUrl('/auth/login');
          this.resetPasswordForm.reset();
        }
        else{
          this.loading = false;
          this.cdr.markForCheck();
          this.translationService.getTranslation(String(response.code)).subscribe((text: string) => {
            this.toast.error(text);
          });
        }
      });
    }
    else {
      this.validationMessages.validateAllFormFields(this.resetPasswordForm);
    }
  }
}
