// Angular
import { Component, ElementRef, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';
// Layout
import { LayoutConfigService, SplashScreenService, TranslationService } from '../core/_base/layout';



interface LanguageFlag {
	lang: string;
	name: string;
	flag: string;
	active?: boolean;
}

@Component({
	selector: 'kt-auth',
	styleUrls: ['auth.component.scss'],
	templateUrl: './auth.component.html',
	encapsulation: ViewEncapsulation.None
})
export class AuthComponent implements OnInit {
	// Public properties
	today: number = Date.now();
	headerLogo: string;

	cssLtr: string;
	cssLoginLtr: string;
	cssLang: string;
	lang: string;
	language: LanguageFlag;
	languages: LanguageFlag[] = [
		{
			lang: 'en',
			name: 'English',
			flag: './assets/media/flags/012-uk.svg'
		},
		{
			lang: 'ar',
			name: 'Arabic',
			flag: './assets/media/flags/021-uae.svg'
		}
	];
	/**
	 * Component constructor
	 *
	 * @param el
	 * @param render
	 * @param layoutConfigService: LayoutConfigService
	 * @param authNoticeService: authNoticeService
	 * @param translationService: TranslationService
	 * @param splashScreenService: SplashScreenService
	 */
	constructor(
		private el: ElementRef,
		private render: Renderer2,
		private layoutConfigService: LayoutConfigService,
		private translationService: TranslationService,
		private splashScreenService: SplashScreenService) {
		this.lang = this.translationService.getSelectedLanguage();
	}
	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */
	/**
	 * On init
	 */
	ngOnInit(): void {
		this.translationService.setLanguage(this.translationService.getSelectedLanguage());
		this.headerLogo = this.layoutConfigService.getLogo();

		this.splashScreenService.hide();
		this.translationCss(this.translationService.getSelectedLanguage());
	}
	/**
	 * Load CSS for this specific page only, and destroy when navigate away
	 * @param styleUrl
	 */

	/*::begin Translation CSS*/
	private loadCSS(styleUrl: string) {
		return new Promise((resolve, reject) => {
			const styleElement = document.createElement('link');
			styleElement.href = styleUrl;
			styleElement.type = 'text/css';
			styleElement.rel = 'stylesheet';
			styleElement.id = 'loadCss';
			styleElement.onload = resolve;
			this.render.appendChild(this.el.nativeElement, styleElement);
		});
	}
	private loginCSS(styleUrl: string) {
		return new Promise((resolve, reject) => {
			const styleElement = document.createElement('link');
			styleElement.href = styleUrl;
			styleElement.type = 'text/css';
			styleElement.rel = 'stylesheet';
			styleElement.id = 'loginCss';
			styleElement.onload = resolve;
			this.render.appendChild(this.el.nativeElement, styleElement);
		});
	}

	private langCSS(styleUrl: string) {
		return new Promise((resolve, reject) => {
			const styleElement = document.createElement('link');
			styleElement.href = styleUrl;
			styleElement.type = 'text/css';
			styleElement.rel = 'stylesheet';
			styleElement.id = 'langCss';
			styleElement.onload = resolve;
			this.render.appendChild(this.el.nativeElement, styleElement);
		});
	}

	private removeloadCSS() {
		return new Promise((resolve, reject) => {
			const styleElement = document.getElementById('loadCss');
			this.render.removeChild(this.el.nativeElement, styleElement);
		});
	}

	private removeLoginCSS() {
		return new Promise((resolve, reject) => {
			const styleElement = document.getElementById('loginCss');
			this.render.removeChild(this.el.nativeElement, styleElement);
		});
	}

	private removelangCSS() {
		return new Promise((resolve, reject) => {
			const styleElement = document.getElementById('langCss');
			this.render.removeChild(this.el.nativeElement, styleElement);
		});
	}


	translationCss(lang) {
		
		if (lang == 'ar') {
			this.cssLtr = '../../assets/css/demo1/style.bundle.rtl.min.css';
			this.cssLoginLtr = '../../assets/css/demo1/pages/login/login-1.rtl.min.css';
			this.cssLang = '../../assets/css/demo1/langar/ar.css';
		}
		else {
			this.cssLtr = '../../assets/css/demo1/style.bundle.min.css';
			this.cssLoginLtr = '../../assets/css/demo1/pages/login/login-1.min.css';
			this.cssLang = '../../assets/css/demo1/langen/en.css';
		}
		this.loadCSS(this.cssLtr);
		this.loginCSS(this.cssLoginLtr);
		this.langCSS(this.cssLang);
	}
	/*::end Translation CSS*/
	setLanguage(lang) {
		this.lang = lang;
		this.removeloadCSS();
		this.removeLoginCSS();
		this.removelangCSS();
		this.translationCss(lang);
		this.languages.forEach((language: LanguageFlag) => {
			if (language.lang === lang) {
				language.active = true;
				this.language = language;
			} else {
				language.active = false;
			}
		});
		this.translationService.setLanguage(lang);
	}
	/**
	 * Set selected language
	 */
	setSelectedLanguage(): any {
		this.setLanguage(this.translationService.getSelectedLanguage());
	}
}