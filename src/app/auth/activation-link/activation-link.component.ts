import { Component, OnInit } from '@angular/core';
import { TranslationService } from '@translateService/translation.service';

@Component({
  selector: 'kt-activation-link',
  templateUrl: './activation-link.component.html',
  styleUrls: ['./activation-link.component.scss']
})
export class ActivationLinkComponent implements OnInit {

  constructor(public translationService: TranslationService,
  ) { }

  ngOnInit() {
  }

}
