import { Component, OnInit } from '@angular/core';
import { phoneNumberMasking } from '../../core/validator/form-validators.service';
import { TranslationService } from '@translateService/translation.service';

@Component({
  selector: 'kt-account-activation',
  templateUrl: './account-activation.component.html',
  styleUrls: ['./account-activation.component.scss']
})
export class AccountActivationComponent implements OnInit {

  constructor(public translationService: TranslationService,
  ) { }

  ngOnInit() {
  }
  /* Mobile No Mask */
  mobileNoMask = phoneNumberMasking();

}
