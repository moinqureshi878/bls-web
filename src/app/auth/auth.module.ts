import { ModuleWithProviders, NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { ActivationLinkComponent } from './activation-link/activation-link.component';
import { AccountActivationComponent } from './account-activation/account-activation.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { LoginVisitorComponent } from './login-visitor/login-visitor.component';
import { JwtTokenService, UserService, CommonService } from '../core/appServices/index.service';
import { authDeactivateGuard } from '../core/auth';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxCaptchaModule } from 'ngx-captcha';

import { AuthenticationService } from '../core/appServices/auth.service';
import { PartialsModule } from '../views/partials/partials.module';
import { TextMaskModule } from 'angular2-text-mask';
import { TranslateModule } from '@ngx-translate/core';
import { LoginGuard } from '../core/auth/_guards/login.guard';

const routes: Routes = [
	{
		path: '',
		component: AuthComponent,
		children: [
			{
				path: '',
				redirectTo: 'login',
				pathMatch: 'full'
			},
			{
				path: 'login',
				component: LoginComponent
			},
			{
				path: 'login-visitor',
				component: LoginVisitorComponent,
				data: { returnUrl: window.location.pathname }
			},
			{
				path: 'forgot-password',
				component: ForgotPasswordComponent,
			},
			{
				path: 'reset-password',
				component: ResetPasswordComponent,
			},
			{
				path: 'activation-link',
				component: ActivationLinkComponent,
			},
			{
				path: 'account-activation',
				component: AccountActivationComponent,
			}
		]
	}
];

@NgModule({
	imports: [
		AuthRoutingModule,
		NgbModule.forRoot(),
		OverlayModule,
		NgxCaptchaModule,
		PartialsModule,
		TextMaskModule,
		TranslateModule.forChild(),
	],
	providers: [
		AuthenticationService,
		JwtTokenService,
		authDeactivateGuard,
		UserService,
		LoginGuard,
		CommonService
	],
	exports: [AuthComponent],
	declarations: [
		AuthComponent,
		LoginComponent,
		ForgotPasswordComponent,
		ResetPasswordComponent,
		ActivationLinkComponent,
		AccountActivationComponent,
		LoginVisitorComponent,
	],
})

export class AuthenticationModule {
	constructor(){
		console.log('Auth Module');
	}
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: AuthenticationModule,
			providers: [
			]
		};
	}
}