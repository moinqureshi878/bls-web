import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ValidationMessagesService } from '../../core/validator/validation-messages.service';
import { TranslationService } from '../../core/_base/layout';
import { CommonService } from '../../core/appServices/index.service';

@Component({
  selector: 'kt-email-confirmation',
  templateUrl: './email-confirmation.component.html',
  styleUrls: ['./email-confirmation.component.scss']
})
export class EmailConfirmationComponent implements OnInit {
  token: string;
  response: any;
  constructor(public validationMessages: ValidationMessagesService,
    public commonService: CommonService,
    public toastrService: ToastrService,
    public router: Router,
    private translateService: TranslationService,
    private route: ActivatedRoute, ) {
    this.route.queryParams.subscribe(params => {
      
      this.token = params['Verify'];
       
      this.commonService.EmailConfirmation(this.token).subscribe(res => {
         
        this.response = res;
        if ((this.response.result && this.response.result.code == 200) || (this.response.code == 200)) {
           
        }
      });
    });
  }

  ngOnInit() {
  }

}
