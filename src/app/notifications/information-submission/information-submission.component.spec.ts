import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationSubmissionComponent } from './information-submission.component';

describe('InformationSubmissionComponent', () => {
  let component: InformationSubmissionComponent;
  let fixture: ComponentFixture<InformationSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
