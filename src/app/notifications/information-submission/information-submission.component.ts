import { Component, OnInit } from '@angular/core';
import { TranslationService } from '@translateService/translation.service';

@Component({
  selector: 'kt-information-submission',
  templateUrl: './information-submission.component.html',
  styleUrls: ['./information-submission.component.scss']
})
export class InformationSubmissionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
