import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslationService } from '@translateService/translation.service';

@Component({
  selector: 'kt-activation-successful',
  templateUrl: './activation-successful.component.html',
  styleUrls: ['./activation-successful.component.scss']
})
export class ActivationSuccessfulComponent implements OnInit {

  refID: any;
  constructor(
    public route: ActivatedRoute,
  ) {

    this.route.params.subscribe(params => {
      this.refID = +params['id'];
    });
  }

  ngOnInit() {
  }


}
