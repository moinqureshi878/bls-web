import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivationSuccessfulComponent } from './activation-successful.component';

describe('ActivationSuccessfulComponent', () => {
  let component: ActivationSuccessfulComponent;
  let fixture: ComponentFixture<ActivationSuccessfulComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivationSuccessfulComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivationSuccessfulComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
