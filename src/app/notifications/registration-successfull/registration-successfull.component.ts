import { Component, OnInit } from '@angular/core';
import { TranslationService } from '@translateService/translation.service';

@Component({
  selector: 'kt-registration-successfull',
  templateUrl: './registration-successfull.component.html',
  styleUrls: ['./registration-successfull.component.scss']
})
export class RegistrationSuccessfullComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
