// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NotificationsComponent } from './notifications.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { HtmlClassService } from './html-class.service';
import { RegistrationSuccessfullComponent } from './registration-successfull/registration-successfull.component';
import { InitialApprovalComponent } from './initial-approval/initial-approval.component';
import { InformationSubmissionComponent } from './information-submission/information-submission.component';
import { ActivationSuccessfulComponent } from './activation-successful/activation-successful.component';
import { EmailConfirmationComponent } from './email-confirmation/email-confirmation.component';
// import { OverlayModule } from '@angular/cdk/overlay';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { CommonTemplateModule } from '../common-template/common-template.module';
// import { PartialsModule } from '../views/partials/partials.module';

const routes: Routes = [{
  path: '',
  component: NotificationsComponent,
  children: [
    {
      path: 'notifications',
      component: NotificationsComponent,
    },
    {
      path: 'reset-password',
      component: ResetPasswordComponent,
    },
    {
      path: 'registration-successfull',
      component: RegistrationSuccessfullComponent,
    },
    {
      path: 'initial-approval',
      component: InitialApprovalComponent,
    },
    {
      path: 'information-submission',
      component: InformationSubmissionComponent,
    },
    {
      path: 'activation-successful/:id',
      component: ActivationSuccessfulComponent,
    },
    {
      path: 'email-confirmation',
      component: EmailConfirmationComponent,
    },

  ],
}];

@NgModule({
  declarations: [NotificationsComponent, ResetPasswordComponent, RegistrationSuccessfullComponent, InitialApprovalComponent, InformationSubmissionComponent, ActivationSuccessfulComponent, EmailConfirmationComponent],
  exports: [RouterModule],
  providers: [HtmlClassService],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    // CommonTemplateModule,
    // PartialsModule,
    // NgbModule.forRoot(),
    // OverlayModule,

  ]
})
export class NotificationsModule { }