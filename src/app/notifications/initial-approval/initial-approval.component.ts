import { Component, OnInit } from '@angular/core';
import { TranslationService } from '@translateService/translation.service';

@Component({
  selector: 'kt-initial-approval',
  templateUrl: './initial-approval.component.html',
  styleUrls: ['./initial-approval.component.scss']
})
export class InitialApprovalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
