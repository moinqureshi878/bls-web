import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { EnvService } from '../environment/env.service';
import { Observable } from 'rxjs';
import * as jwt_decode from 'jwt-decode';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class BnService {
  private ApiUrl: string;
  ControllerName = 'BN';
  FullPath: string;
  constructor(
    private httpclient: HttpClient,
    private router: Router,
    private env: EnvService) {
    this.ApiUrl = this.env.apiUrl;
    this.FullPath = this.env.apiUrl + this.ControllerName;
  }

  addBNRequest(Obj: any): Observable<any> {
    return this.httpclient.post(this.FullPath + "/addBNRequest", Obj);
  }
  GetBNRequest(id: number): Observable<any> {
    return this.httpclient.get(this.FullPath + `/getBNRequest?id=${id}`);
  }
  GetAllBNRequest(tab: number): Observable<any> {
    return this.httpclient.get(this.FullPath + `/getAllBNRequest?tab=${tab}`);
  }
  addRating(model: any) {
    return this.httpclient.post(this.FullPath + "/addRating", model);
  }
  GetBusinessNameRequest() {
    return this.httpclient.get(this.FullPath + `/GetBusinessNameRequest`);
  }
  AddWorkFlow(obj: any) {
    return this.httpclient.post(this.FullPath + "/AddWorkFlow", obj);
  }
}