import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvService } from '../environment/env.service';
import { Observable } from 'rxjs';


@Injectable({
	providedIn: "root"
})
export class UserService {

    ApiUrl: string;
    ControllerName = 'user';
    FullPath: string;

    constructor(
        private httpclient: HttpClient,
        private env: EnvService) {
        this.ApiUrl = this.env.apiUrl;
        this.FullPath = this.env.apiUrl + this.ControllerName;
    }
    AddUser(tdaUserObj: any): Observable<any> {
        return this.httpclient.post(this.FullPath, tdaUserObj);
    }
    EditUser(tdaUserObj: any): Observable<any> {
        return this.httpclient.put(this.FullPath, tdaUserObj);
    }
    GetUser(id: number): any {
        return this.httpclient.get(this.FullPath + `?id=${id}`);
    }
    GetUsers(): any {
        return this.httpclient.get(this.FullPath);
    }
    GetRoleByGovernment(): Observable<any> {
        return this.httpclient.get(this.FullPath + `/GetRoleByGovernment`);
    }
    GetMasterUsers(): any {
        return this.httpclient.get(this.FullPath + "/GetGovMastertUser");
    }
    GetInternalUser(): any {
        return this.httpclient.get(this.FullPath + "/GetInternalUser");
    }
    GetGovtUser(): any {
        return this.httpclient.get(this.FullPath + "/GetGovtUser");
    }
    getLinkedRoles(userID: number): any {
        return this.httpclient.get(this.FullPath + "/getLinkedRoles" + `?userId=${userID}`);
    }
    getNotLinkedRoles(userID: number): any {
        return this.httpclient.get(this.FullPath + "/getNotLinkedRoles" + `?userId=${userID}`);
    }
    ActiveDeactiveUsers(userID: number): Observable<any> {
        return this.httpclient.delete<any>(this.FullPath + `?userId=${userID}`);
    }

    GetLinkedUserFromMasterUser(userID: number): any {
        return this.httpclient.get(this.FullPath + "/getLinkedUsers" + `?userId=${userID}`);
    }

    GetGovermentRole() {
        return this.httpclient.get(this.FullPath + "/GetRoleByGovernment");
    }

    addRolesSubUsers(rolesObj: any): Observable<any> {
        return this.httpclient.post(this.FullPath + "/addRolesSubUsers", rolesObj);
    }
    GetGovtEntity() {
        return this.httpclient.get(this.FullPath + "/getGovtEntity");
    }
    ChangePassword(obj: any): Observable<any> {
        return this.httpclient.post(this.FullPath + "/ChangePassword", obj);
    }
    UpdatePassword(obj: any): Observable<any> {
        return this.httpclient.put(this.FullPath + "/UpdatePassword", obj);
    }
    GetCustomers(): Observable<any> {
        return this.httpclient.get(this.FullPath + "/GetCustomers");
    }

    SignUpUsers(obj: any): Observable<any> {
        return this.httpclient.post(this.FullPath + "/Singup", obj);
    }
    AddLeave(Obj: any): Observable<any> {
        return this.httpclient.post(this.FullPath + "/addLeave", Obj);
    }
    GetLeaves(): Observable<any> {
        return this.httpclient.get(this.FullPath + "/getLeaves");
    }
    GetLeavesById(id: number): Observable<any> {
        return this.httpclient.get(this.FullPath + "/getLeavesById" + `?id=${id}`);
    }
    UpdateLeave(obj: any): Observable<any> {
        return this.httpclient.put(this.FullPath + "/updateLeave", obj);
    }
    GetUserByID(ID , mobileNo): Observable<any> {
        return this.httpclient.get(this.FullPath + `/getUserByID?id=${ID}&mobileNo=${mobileNo}`);
    }
    AddUserAsStakeHolder(userId: number): Observable<any> {
        return this.httpclient.post(this.FullPath + `/addUserAsStakeHolder?userId=${userId}`,{});
    }
    GetStakeHolderByType(userType: number): Observable<any> {
        return this.httpclient.get(this.FullPath + `/getStakeHolderByType?userType=${userType}`);
    }   
}