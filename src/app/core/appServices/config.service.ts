import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvService } from '../environment/env.service';
import { Observable, from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  ApiUrl: string;
  ControllerName = 'config';
  FullPath: string;

  constructor(
    private httpclient: HttpClient,
    private env: EnvService
  ) {
    this.ApiUrl = this.env.apiUrl;
    this.FullPath = this.env.apiUrl + this.ControllerName;
  }

  getAllAppTypeAttachment(type1: string, type2): Observable<any> {
    return this.httpclient.get(this.FullPath + `/getLookupDetail?t1=${type1}&t2=${type2}`);
  }

  addAppTypeAttachment(model: any): Observable<any> {
    return this.httpclient.post(this.FullPath + "/addLookupDetail", model);
  }

  addBAApproval(model: any): Observable<any> {
    return this.httpclient.post(this.FullPath + "/addBAApproval", model);
  }

  editManageApplicationTypeAttachment(model: any): Observable<any> {
    return this.httpclient.put(this.FullPath + "/manageAppType", model);
  }

  getLookupDetailByID(id: number, type: string): Observable<any> {
    return this.httpclient.get(this.FullPath + `/getLookupDetailByID?userType=${id}&type=${type}`);
  }

  GetTypeDetail(t1: string, t3: string): Observable<any> {
    return this.httpclient.get(this.FullPath + `/GetTypeDetail?type1=${t1}&type3=${t3}`);
  }
  getTypeDetailByID(t1: number, t2: number, t3: string): Observable<any> {
    return this.httpclient.get(this.FullPath + `/getTypeDetailByID?type1=${t1}&type2=${t2}&type3=${t3}`);
  }

  GetBLApprovalDetail(t1: number, t2: number, t3: string): Observable<any> {
    return this.httpclient.get(this.FullPath + `/GetBLApprovalDetail?Data1=${t1}&Data2=${t2}&type3=${t3}`);
  }

  GetAllBAApproval(t1: string, t3: string): Observable<any> {
    return this.httpclient.get(this.FullPath + `/GetAllBAApproval?type1=${t1}&type3=${t3}`);
  }

  AppTypeKPIs(model: any): Observable<any> {
    return this.httpclient.post(this.FullPath + "/AppTypeKPIs", model);
  }
  GetAppTypeKPIs(id: number): Observable<any> {
    return this.httpclient.get(this.FullPath + `/GetAppTypeKPIs?AppTypeID=${id}`);
  }
  DeleteAppTypeKPIs(ID: number): any {
    return this.httpclient.delete(this.FullPath + `/DeleteAppTypeKPIs?ID=${ID}`);
  }

  AppTypeReason(model: any): Observable<any> {
    return this.httpclient.post(this.FullPath + "/AppTypeReason", model);
  }
  GetAppTypeReason(id: number): Observable<any> {
    return this.httpclient.get(this.FullPath + `/GetAppTypeReason?AppTypeID=${id}`);
  }
  DeleteAppTypeReason(ID: number): any {
    return this.httpclient.delete(this.FullPath + `/DeleteAppTypeReason?ID=${ID}`);
  }
}