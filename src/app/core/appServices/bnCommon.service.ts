import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { EnvService } from '../environment/env.service';
import { Observable } from 'rxjs';
import * as jwt_decode from 'jwt-decode';
import { map } from 'rxjs/operators';
import { ApplicationTypesEnum } from '../_enum/applicationTypes.enum';
import { JwtTokenService } from './index.service';
@Injectable({
  providedIn: 'root'
})
export class BnCommonService {
  private ApiUrl: string;
  ControllerName = 'bnCommon';
  FullPath: string;
  constructor(
    private httpclient: HttpClient,
    private router: Router,
    private env: EnvService) {
    this.ApiUrl = this.env.apiUrl;
    this.FullPath = this.env.apiUrl + this.ControllerName;
  }

  getAppTypesCatalogByID(id: number): Observable<any[]> {
    return this.httpclient.get<any[]>(this.ApiUrl + `bnCommon/getAppTypesCatalogByID?id=${id}`);
  }

  AddAppTypesCatalog(model: any[]): Observable<any[]> {
    return this.httpclient.post<any[]>(this.ApiUrl + "bnCommon/AddAppTypesCatalog", model);
  }

  UpdateppTypesCatalog(model: any[]): Observable<any[]> {
    return this.httpclient.put<any[]>(this.ApiUrl + "bnCommon/updateAppTypesCatalog", model);
  }
  // Business Activity Group
  AddBusinessActivity(Obj: any): Observable<any> {
    return this.httpclient.post(this.FullPath + "/addBusinessActivity", Obj);
  }
  GetBusinessActivity(): Observable<any> {
    return this.httpclient.get(this.FullPath + "/getBusinessActivity");
  }
  GetActiveBusinessActivity(): Observable<any> {
    return this.httpclient.get(this.FullPath + "/getActiveBusinessActivity");
  }
  GetBusinessActivitygByID(id: number): Observable<any> {
    return this.httpclient.get(this.FullPath + `/getBusinessActivitygByID?id=${id}`);
  }
  UpdateBusinessActivity(obj: any): Observable<any> {
    return this.httpclient.put(this.FullPath + "/updateBusinessActivity", obj);
  }
  DeleteBusinessActivity(id: number): Observable<any> {
    return this.httpclient.delete(this.FullPath + `/deleteBusinessActivity?id=${id}`);
  }
  // Business Activity Pages
  AddBusinessActivityPages(Obj: any): Observable<any> {
    return this.httpclient.post(this.FullPath + "/addBusinessActivityPages", Obj);
  }
  GetBusinessActivityPages(): Observable<any> {
    return this.httpclient.get(this.FullPath + "/getBusinessActivityPages");
  }
  GetBusinessActivityPagesByID(id: number): Observable<any> {
    return this.httpclient.get(this.FullPath + `/getBusinessActivityPagesByID?id=${id}`);
  }
  UpdateBusinessActivityPages(obj: any): Observable<any> {
    return this.httpclient.put(this.FullPath + "/updateBusinessActivityPages", obj);
  }
  DeleteBusinessActivityPages(id: number): Observable<any> {
    return this.httpclient.delete(this.FullPath + `/deleteBusinessActivityPages?id=${id}`);
  }

  AddBusinessLicenseParameters(model: any): Observable<any[]> {
    return this.httpclient.post<any[]>(this.ApiUrl + "bnCommon/addBusinessLicenseParameter", model);
  }
  GetBusinessLicenseParameters(id: Number): Observable<any[]> {
    return this.httpclient.get<any[]>(this.ApiUrl + `bnCommon/getBusinessLicenseParameter?id=${id}`);
  }

  // Public Holiday
  AddPublicHoliday(Obj: any): Observable<any> {
    return this.httpclient.post(this.FullPath + "/addPublicHoliday", Obj);
  }
  GetPublicHoliday(): Observable<any> {
    return this.httpclient.get(this.FullPath + "/getPublicHoliday");
  }
  GetPublicHolidayByID(id: number): Observable<any> {
    debugger
    return this.httpclient.get(this.FullPath + `/getPublicHolidayById?id=${id}`);
  }
  UpdatePublicHoliday(obj: any): Observable<any> {
    return this.httpclient.put(this.FullPath + "/updatePublicHoliday", obj);
  }
  DeletePublicHoliday(id: number): Observable<any> {
    return this.httpclient.delete(this.FullPath + `/deletePublicHoliday?id=${id}`);
  }

  // ServiceInfo
  getServiceInfo(loginOnBehalfID: number, UserID: number): Observable<any> {
    let applicationTypeEnum = ApplicationTypesEnum;
    let appType: number = applicationTypeEnum.BNN;

    // let loginOnBehalfID: number, UserID: number;

    // let userSession = JSON.parse(localStorage.getItem("userSession"));
    // if (userSession) {
    //   debugger
    //   UserID = Number(jwt_decode(userSession[0].token).UserID);
    //   loginOnBehalfID = userSession[1] ? Number(jwt_decode(userSession[1].token).UserID) : 0;
    // }
    // this.jwtTokenService.getSessionSubject().subscribe(value => {
    // 	if (value) {
    // 		loginOnBehalfID = userSession[1] ? Number(jwt_decode(userSession[1].token).UserID) : 0;
    // 	}
    // });
    return this.httpclient.get(this.FullPath + `/getServiceInfo?appType=${appType}&loginOnBehalfID=${loginOnBehalfID}&UserID=${UserID}`).pipe(map(resp => resp["data"]));
  }
  // ServiceInfo
  getServiceInfobyTypeID(loginOnBehalfID: number, UserID: number, appType: number): Observable<any> {
    return this.httpclient.get(this.FullPath + `/getServiceInfo?appType=${appType}&loginOnBehalfID=${loginOnBehalfID}&UserID=${UserID}`).pipe(map(resp => resp["data"]));
  }
  GetReservationPeriod(): Observable<any> {
    return this.httpclient.get(this.FullPath + `/getReservationPeriod?businessLicensePeriod=${124}`).pipe(map(resp => resp["data"]));
  }
  GetLicenseDetail(): Observable<any> {
    return this.httpclient.get(this.FullPath + `/getLicenseDetail?HolicenseNo=${112}`).pipe(map(resp => resp["data"]));
  }
  GetBusinessActivityPagesByBAGID(id: number): Observable<any> {
    return this.httpclient.get(this.FullPath + `/getBusinessActivityPagesByBAGID?id=${id}`);
  }


  getLicenseDetail(id: number): Observable<any> {
    return this.httpclient.get(this.FullPath + `/getLicenseDetail?HolicenseNo=${id}`);
  }
  GetCancelReason(id: number) {
    return this.httpclient.get(this.FullPath + `/GetCancelReason?ApptypeID=${id}`).pipe(map(resp => resp["data"]));
  }
}