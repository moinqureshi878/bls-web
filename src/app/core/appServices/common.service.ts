import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { EnvService } from '../environment/env.service';
import { Observable, Subject, BehaviorSubject, from } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { ModulesModels } from '../appModels/modules.model';

@Injectable({
    providedIn: "root"
})
export class CommonService {

    private ApiUrl: string;
    private ApiOrigin: string;
    private GetModulesUrl = 'Modules';
    private GetLookUps = 'common/GetLookups';
    private getMultipleLookupPath = 'common/getMultipleLookups';
    public ClientIpAddress: BehaviorSubject<any> = new BehaviorSubject<any>('');
    public menuSubject: BehaviorSubject<any> = new BehaviorSubject<any>('');
    public GetCustomLookups: BehaviorSubject<any> = new BehaviorSubject<any>([]);
    public CLookups: any[];

    constructor(
        private httpclient: HttpClient,
        private router: Router,
        private env: EnvService) {
        this.ApiUrl = this.env.apiUrl;
        this.ApiOrigin = this.env.apiOrigin;

    }

    getModules(): Observable<any[]> {
        return this.httpclient.get<ModulesModels[]>(this.GetModulesUrl);
        // .pipe(
        //     map(resp => resp["data"])
        // );
    }

    fileUpload(file): any {
        return new Promise(resolve => {
            let body = [
                {
                    "UserId": 1,
                    "UserName": "atifWaqarOwaisOO",
                    "AttachmentUrl": "",
                    "AttachmentName": "",
                    "filebase64": "",
                    "AttachmentExtension": "",
                    "TypeCode": "001"
                }
            ];
            var resp;
            var getFile: File = file;
            body[0].AttachmentName = getFile.name;
            body[0].AttachmentExtension = getFile.type;
            var myReader: FileReader = new FileReader();
            myReader.onloadend = (e) => {
                let streamData = myReader.result;
                if (streamData) {
                    body[0].filebase64 = String(streamData).split("base64,")[1];
                    this.httpclient.post(this.ApiUrl + 'common/Upload', body).toPromise().then(res => {
                        resp = res;
                        if (resp.result.code == 200 || resp.code == 200) {
                            resp = { "attachmentURL": resp.data[0].attachmentUrl, "attachmentName": resp.data[0].attachmentName, code: 200 };
                            resolve(resp);
                        }
                        else {
                            resolve(res);
                        }
                    });
                }
            }
            myReader.readAsDataURL(file);
        });
    }

    logout() {
        localStorage.clear();
        this.router.navigateByUrl('/auth/login');
    }

    getLookUp(type: string) {
        return this.httpclient.get<any[]>(this.ApiUrl + this.GetLookUps + type).pipe(map(resp => resp["data"]));
    }
    getMultipleLookups(type: string) {
        return this.httpclient.get<any[]>(this.ApiUrl + this.getMultipleLookupPath + type);
    }
    getRegLookups(type: string) {
        return this.httpclient.get<any[]>(this.ApiUrl + type).pipe(map(resp => resp["data"]));
    }

    Nationalities() {
        return this.httpclient.get<any[]>(this.ApiUrl + `common/Nationalities`)
            .pipe(
                map(resp => resp["data"])
            );
    }

    GetAttachmentByUserTypeCode(code: number): Observable<any> {
        return this.httpclient.get<ModulesModels[]>(this.ApiUrl + `common/GetAttachmentsByUserTypeCode?userTypeCode=${code}`);
    }
    SessionHistory(userType: number): Observable<any> {
        return this.httpclient.get(this.ApiUrl + `common/SessionHistory?userType=${userType}`);
    }

    GetIPAddress() {
        this.httpclient.get('https://cors-anywhere.herokuapp.com/http://api.ipify.org/?format=json').subscribe(data => {
            this.ClientIpAddress.next(data["ip"]);
        })
    }

    getClientIpAddress() {
        return this.ClientIpAddress;
    }

    UpdateMenuFromLocalStorage() {
        let userItems;
        let userSession = JSON.parse(localStorage.getItem("userSession"));
        if (userSession) {
            if (userSession.length > 1) {
                userItems = userSession[1];
            }
            else {
                userItems = userSession[0];
            }
        }
        else {
            userItems = null;
        }

        this.menuSubject.next(userItems);
    }

    getMenuItems() {
        return this.menuSubject;
    }


    ManageUserSessionHistory(model: any): Observable<any> {
        return this.httpclient.post(this.ApiUrl + "common/SessionHistory", model);
    }

    ManageUserSessionHistoryDetail(model: any): Observable<any> {
        return this.httpclient.post(this.ApiUrl + "common/SessionHistoryDetail", model);
    }
    // Organizational Structure
    GetOrganizationalStructure(): Observable<any> {
        return this.httpclient.get(this.ApiUrl + `common/getOrganizationalStructure`);
    }
    AddOrganizationalStructure(postJson: any): Observable<any> {
        return this.httpclient.post(this.ApiUrl + `common/addOrganizationalStructure`, postJson);
    }
    UpdateOrganizationalStructure(postJson: any): Observable<any> {
        return this.httpclient.put(this.ApiUrl + `common/updateOrganizationalStructure`, postJson);
    }
    DeleteOrganizationalStructure(id: number): Observable<any> {
        return this.httpclient.delete(this.ApiUrl + `common/deleteOrganizationalStructure?id=${id}`);
    }
    EmailConfirmation(token: string): Observable<any> {
        return this.httpclient.get(this.ApiUrl + `common/emailConfirmation?token=${token}`);
    }
    FilterHistory(postJson: any): Observable<any> {
        return this.httpclient.post(this.ApiUrl + `common/filterHistory`, postJson);
    }
    CustomLookups() {
        this.httpclient.get(this.ApiOrigin + `wwwroot/Lookups.json`).subscribe(res => {
            this.GetCustomLookups.next(res);
        });
    }
    GetCustomLookup(filterName, index): Number {
        let value;
        this.GetCustomLookups.subscribe(res => { value = res.filter((arr) => arr.TableName == filterName).filter((arr, i) => i == index)[0].Code });
        return Number(value);
    }
    GetCustomLookupID(filterName, index): Number {
        let value;
        this.GetCustomLookups.subscribe(res => { value = res.filter((arr) => arr.TableName == filterName).filter((arr, i) => i == index)[0].ID });
        return Number(value);
    }

    GetCustomLookupIDAsync(filterName, index): Observable<any> {
        let value;
        this.GetCustomLookups.subscribe(res => { value = res.filter((arr) => arr.TableName == filterName).filter((arr, i) => i == index)[0].ID });

        return from([value]);
    }
}