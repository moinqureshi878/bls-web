import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { tap, retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { CommonService } from './common.service';
import { EnvService } from '../environment/env.service';
import { TranslationService } from '../_base/layout';
import { localStorageSession } from '../appModels/localStorageSessions.model';
import { AuthenticationService } from './index.service';

@Injectable()
export class JWTInterceptor implements HttpInterceptor {

  AuthToken: string;
  constructor(public toastrService: ToastrService,
    public commonService: CommonService,
    private router: Router,
    private env: EnvService,
    private translateService: TranslationService,
    public authenticationService: AuthenticationService,
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let token;//= localStorage.getItem("token");
    let userSession: localStorageSession[] = JSON.parse(localStorage.getItem("userSession"));
    if (userSession) {
      if (userSession.length > 1) {
        token = userSession[1].token;
      }
      else {
        token = userSession[0].token;
      }
    }

    const authReq = req.clone({
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'ApiKey': this.env.apiKey,
        'Authorization': `Bearer ${token}`
      })
    });
    if (window.location.origin.match('http://localhost') || window.location.origin.match('neusol.com')) {
      if ((authReq.method == "POST" || authReq.method == "PUT") && (!authReq.url.match('assets/media/icons'))) {
        console.group(authReq.url);
        console.info(JSON.stringify(authReq.body, null, 2));
        console.groupEnd();
      }
    }
    return next.handle(authReq).pipe(
      retry(1),
      tap(event => {
        //logging the http response to browser's console in case of a success
        if (event instanceof HttpResponse && (!event.url.match('assets/media/icons'))) {
          if (window.location.origin.match('http://localhost') || window.location.origin.match('neusol.com')) {
            console.group(event.url);
            console.info(event);
            console.groupEnd();
          }
        }
      }),
      catchError((error: HttpErrorResponse) => {
        let errorMessage = '';
        if (error.status == 401) {
          this.authenticationService.logout("logout");
        }
        else {
          if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
          } else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
          }
        }

        this.translateService.getTranslation(String(error.statusText)).subscribe((text: string) => {
          this.toastrService.error(text);
        });
        console.warn(error)
        return throwError(errorMessage);
      })
    )
  }
}
