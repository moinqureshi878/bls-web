import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EnvService } from "../environment/env.service";
import { Observable } from "rxjs";
@Injectable({
	providedIn: "root"
})
export class MasterDataService {
	ApiUrl: string;
	ControllerName = "Common";
	FullPath: string;

	constructor(private httpclient: HttpClient, private env: EnvService) {
		this.ApiUrl = this.env.apiUrl;
		this.FullPath = this.env.apiUrl + this.ControllerName;
	}
	GetAllMaster(): Observable<any> {
		return this.httpclient.get(this.FullPath + "/getMasterTypes");
	}
	GetAllLookups(): Observable<any> {
		return this.httpclient.get(this.FullPath + "/getAllLookups");
	}
	AddMaster(masterModules: any): any {
		return this.httpclient.post(this.FullPath, masterModules);
	}
	UpdateMaster(masterModules: any): any {
		return this.httpclient.put(this.FullPath, masterModules);
	}
	DeleteMaster(ID: number): any {
		return this.httpclient.delete(this.FullPath + `?ID=${ID}`);
	}
	GetLookupsById(id: number): Observable<any> {
		return this.httpclient.get(this.FullPath + `/getLookupsById?id=${id}`);
	}



	getMasterDataByType(TypeName: string): any {
		return this.httpclient.get(
			this.FullPath + "/getLookups" + `?type=${TypeName}`
		);
	}
	getMaterByType(): any {
		return this.httpclient.get(this.FullPath + "/GetMasterTypes");
	}


	
}
