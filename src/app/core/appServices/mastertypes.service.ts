import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { EnvService } from '../environment/env.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { mastertypesmodel } from '../appModels/mastertypes.model';
import { stringify } from 'querystring';
 

@Injectable({
	providedIn: "root"
})
export class MasterTypesService {

    ApiUrl: string;
    ControllerName = 'Common';
    FullPath: string;

    constructor(
        private httpclient: HttpClient,
        private env: EnvService
    ) {
        this.ApiUrl = this.env.apiUrl;
        this.FullPath = this.env.apiUrl + this.ControllerName;
    }
	AddMasterTypes(obj: any): any {
		return this.httpclient.post(this.ApiUrl + 'MasterTypes', obj);
	}
	EditMasterTypes(obj: any): any {
		return this.httpclient.put(this.ApiUrl + 'MasterTypes', obj);
	}
	DeleteMasterTypes(ID: number): any {
		return this.httpclient.delete(this.ApiUrl + `MasterTypes?id=${ID}`);
	}
	GetMasterTypesById(id: number): Observable<any> {
		return this.httpclient.get(this.ApiUrl + `MasterTypes/GetTypeById?id=${id}`);
    }
    


    getMasterDataByType(TypeName: string): any {
        return this.httpclient.get<mastertypesmodel[]>(this.FullPath+"/getLookups"+`?type=${TypeName}`);
    }

    getMasterTypeById(id: number): Observable<any>{
        return this.httpclient.get<mastertypesmodel>(this.ApiUrl+`MasterTypes/GetTypeById?id=${id}`);
    }
    
    getAllTypes(): Observable<any>{
      return this.httpclient.get<mastertypesmodel[]>(this.FullPath+"/GetMasterTypes");
    }

    getMaterByType(): any {
     
      return this.httpclient.get<mastertypesmodel[]>(this.FullPath+"/GetMasterTypes");
    }

    AddMaster(masterModules: mastertypesmodel): any {
        return this.httpclient.post<mastertypesmodel>(this.ApiUrl+"MasterTypes", masterModules);
    }

    UpdateMaster(masterModules: mastertypesmodel, ID: number): any {
      masterModules.id = ID;
        return this.httpclient.put<mastertypesmodel>(this.ApiUrl+"MasterTypes", masterModules);
    }

    DeleteMaster(ID: number): any {
        return this.httpclient.delete<mastertypesmodel>(this.ApiUrl+"MasterTypes"+ `?ID=${ID}`);
    }

    
}