import { Injectable } from '@angular/core';
import { BnCommonService, CommonService } from '../index.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BusinessLicenseService {
  serviceInfoAndUserInfo: Observable<any>;
  businessNameTypes: Observable<any>;
  constructor(
    public bnCommonService: BnCommonService,
    private commonService: CommonService
  ) {
    // this.GetServiceInfo();
    this.BusinessNameTypes();

  }

  GetServiceInfo(loginOnBehalfID: number, UserID: number): Observable<any> {
    this.serviceInfoAndUserInfo = this.bnCommonService.getServiceInfo(loginOnBehalfID, UserID);
    return this.serviceInfoAndUserInfo;
  }
  BusinessNameTypes(): Observable<any> {
    this.businessNameTypes = this.commonService.getLookUp("?type=BNCategorries");
    return this.businessNameTypes;
  }

  getServiceInfobyTypeID(loginOnBehalfID: number, UserID: number, appType: number): Observable<any> {
    return this.bnCommonService.getServiceInfobyTypeID(loginOnBehalfID, UserID, appType);
  }
  // Business Name Types
}