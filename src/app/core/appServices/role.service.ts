import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvService } from '../environment/env.service';
import { Observable } from 'rxjs';
import { RolesModel } from '../appModels/roles.model';
// import { TreeviewItem } from './../_treeView/treeview-item';

@Injectable({
    providedIn: 'root'
})
export class RoleService {

    ApiUrl: string;
    ControllerName = 'roles';
    FullPath: string;

    constructor(
        private httpclient: HttpClient,
        private env: EnvService) {
        this.ApiUrl = this.env.apiUrl;
        this.FullPath = this.env.apiUrl + this.ControllerName;
    }

    GetRoles(): Observable<any> {
        return this.httpclient.get(this.FullPath);
    }


    DeleteRoles(ID: number): any {
        return this.httpclient.delete(this.FullPath + `?ID=${ID}`);
    }

    SetUserRole(role: any): Observable<any> {
        return this.httpclient.put(this.FullPath + `/SetUserRole`, role);
    }
    NotLinkedUsers(ID: number): Observable<any> {
        return this.httpclient.get(this.FullPath + `/notLinkedUsers?roleId=${ID}`);
    }
    LinkedUsers(ID: number): Observable<any> {
        return this.httpclient.get(this.FullPath + `/linkedUsers?roleId=${ID}`);
    }
    AssignRole(role): Observable<any> {
        return this.httpclient.post(this.FullPath + `/assignRole`, role);
    }

    AddRoles(roles: RolesModel): Observable<any> {
        return this.httpclient.post<RolesModel>(this.FullPath, roles);
    }

    GetAccessRights(roleId: number): Observable<any> {
        return this.httpclient.get(this.FullPath + `/getAccessrights?roleId=${roleId}`);
    }
    AddRolePermission(permission: any): any {
        return this.httpclient.post(this.FullPath + "/addRolePermission", permission);
    }
    // getBooks(): TreeviewItem[] {
    //     const childrenCategory = new TreeviewItem({
    //         text: 'Children', value: 1, collapsed: true, children: [
    //             { text: 'Baby 3-5', value: 11 },
    //             { text: 'Baby 6-8', value: 12 },
    //             { text: 'Baby 9-12', value: 13 }
    //         ]
    //     });
    //     const itCategory = new TreeviewItem({
    //         text: 'IT', value: 9, children: [
    //             {
    //                 text: 'Programming', value: 91, children: [{
    //                     text: 'Frontend', value: 911, children: [
    //                         { text: 'Angular 1', value: 9111 },
    //                         { text: 'Angular 2', value: 9112 },
    //                         { text: 'ReactJS', value: 9113, disabled: true }
    //                     ]
    //                 }, {
    //                     text: 'Backend', value: 912, children: [
    //                         { text: 'C#', value: 9121 },
    //                         { text: 'Java', value: 9122, checked: false },
    //                         { text: 'Python', value: 9123, checked: false, disabled: true }
    //                     ]
    //                 }]
    //             },
    //             {
    //                 text: 'Networking', value: 92, children: [
    //                     { text: 'Internet', value: 921 },
    //                     { text: 'Security', value: 922 }
    //                 ]
    //             }
    //         ]
    //     });
    //     const teenCategory = new TreeviewItem({
    //         text: 'Teen', value: 2, collapsed: true, disabled: true, children: [
    //             { text: 'Adventure', value: 21 },
    //             { text: 'Science', value: 22 }
    //         ]
    //     });
    //     const othersCategory = new TreeviewItem({ text: 'Others', value: 3, checked: false, disabled: true });
    //     return [childrenCategory, itCategory, teenCategory, othersCategory];
    // }

}
