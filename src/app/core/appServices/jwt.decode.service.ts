import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { JWTModel } from '../appModels/jwt.model';
import { BehaviorSubject } from 'rxjs';
@Injectable({
    providedIn: "root"
})
export class JwtTokenService {
    private jwtModel: JWTModel;
    public tokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>('');
    getJwtTokenValue(): JWTModel {

        var token = localStorage.getItem("token");
        if (token) {
            this.jwtModel = jwt_decode(token);
        }
        else {
            this.jwtModel = new JWTModel();
        }
        return this.jwtModel;
    }

    getJwtTokenValueByToken(token: string): JWTModel {
        // var token = localStorage.getItem("token");
        if (token) {
            this.jwtModel = jwt_decode(token);
        }
        else {
            this.jwtModel = new JWTModel();
        }
        return this.jwtModel;
    }

    getJwtToken(): string {
        return localStorage.getItem("token");
    }

    updateSessionFromLocalStorage() {
        let userSession = JSON.parse(localStorage.getItem("userSession"));
        this.tokenSubject.next(userSession);
    }

    getSessionSubject(){
       return this.tokenSubject;
    }

}