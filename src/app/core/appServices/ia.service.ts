import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { EnvService } from '../environment/env.service';
import { Observable } from 'rxjs';
import * as jwt_decode from 'jwt-decode';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class IAService {
  private ApiUrl: string;
  ControllerName = 'IA';
  FullPath: string;
  constructor(
    private httpclient: HttpClient,
    private router: Router,
    private env: EnvService) {
    this.ApiUrl = this.env.apiUrl;
    this.FullPath = this.env.apiUrl + this.ControllerName;
  }

  getInitialApprovalRequest(): Observable<any> {
    return this.httpclient.get(this.FullPath + `/getInitialApprovalRequest`);
  }

  addIARequest(Obj: any): Observable<any> {
    return this.httpclient.post(this.FullPath + "/addIARequest", Obj);
  }

  GetApproveBusinessName(): Observable<any> {
    return this.httpclient.get(this.FullPath + `/GetApproveBusinessName`);
  }
}