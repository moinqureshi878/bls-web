import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { EnvService } from '../environment/env.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Nationalitymodel } from '../appModels/Nationalities.model';
import { stringify } from 'querystring';
 

@Injectable({
	providedIn: "root"
})
export class NationalitiesService {

    ApiUrl: string;
    ControllerName = 'Nationalities';
    FullPath: string;

    constructor(
        private httpclient: HttpClient,
        private env: EnvService
    ) {
        this.ApiUrl = this.env.apiUrl;
        this.FullPath = this.env.apiUrl + this.ControllerName;
    }

    
    getNatById(id: number): Observable<any>{
      debugger;
        return this.httpclient.get<Nationalitymodel>(this.FullPath+`/GetNationalitiesById?id=${id}`);
    }
    
    getAllNats(): Observable<any>{
     
      return this.httpclient.get<Nationalitymodel[]>(this.FullPath+"/getAllNationalities");
  
    }

    AddNat(natModules: Nationalitymodel): any {
        return this.httpclient.post<Nationalitymodel>(this.FullPath, natModules);
    }

    UpdateNat(natModules: Nationalitymodel, ID: number): any {
        natModules.id = ID;
        return this.httpclient.put<Nationalitymodel>(this.FullPath, natModules);
    }

    DeleteNat(ID: number): any {
        return this.httpclient.delete<Nationalitymodel>(this.FullPath+ `?ID=${ID}`);
    }
}