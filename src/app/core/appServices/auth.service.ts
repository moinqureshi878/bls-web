import { Injectable } from '@angular/core';
import { EnvService } from '../environment/env.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { localStorageSession } from '../appModels/localStorageSessions.model';
import { JwtRolesModel } from '../appModels/jwt.model';
import * as jwt_decodes from 'jwt-decode';
import { CommonService } from './common.service';
import { ValidationMessagesService } from '../validator/validation-messages.service';
import { SessionTypeEnum, CommonEnum } from '../../core/_enum/index.enum';
import { map } from 'rxjs/operators';


@Injectable({
    providedIn: "root"
})
export class AuthenticationService {
    ApiUrl: string;
    ControllerName = 'Auth';
    FullPath: string;

    constructor(
        private httpclient: HttpClient,
        private env: EnvService,
        private commonService: CommonService,
        public validationMessages: ValidationMessagesService,
        private router: Router) {
        this.ApiUrl = this.env.apiUrl;
        this.FullPath = this.env.apiUrl + this.ControllerName;
    }

    // login(loginObj: any):Observable<any> {
    //     return this.httpclient.post(this.FullPath+`/userValidation`, loginObj);
    // }
    getOTP(loginObj: any): Observable<any> {
        return this.httpclient.post(this.FullPath + "/getOTP", loginObj);
    }

    VerifyToken(tokenJson: any): Observable<any> {
        return this.httpclient.post(this.FullPath + "/verifyOTP", tokenJson);
    }

    // login on behalf
    verifyOnBehalfOTP(model: any): Observable<any> {
        return this.httpclient.post(this.FullPath + "/verifyOnBehalfOTP", model);
    }

    getOnBehalfOTP(model: any): Observable<any> {
        return this.httpclient.post(this.FullPath + "/getOnBehalfOTP", model);
    }

    login() {
        let userObj = { Token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBdGlmIiwiZW1haWwiOiJhdGlmQG5ldXNvbC5jb20iLCJ1RW1haWwiOiJhdGlmQG5ldXNvbC5jb20iLCJVc2VySUQiOiJhNzQ3MTlhMS1mNDQyLTRkMDEtOTRjNy1kZDA4YjRhMzJhZmQiLCJGdWxsTmFtZUFyIjoi2LnYp9i32ZDZgeKArOKAjiIsIkZ1bGxOYW1lRW4iOiJBdGlmIiwiUm9sZUNvZGUiOiJBRE1JTiIsImp0aSI6IjM4ZWUzNmZjLTJkZDUtNGZjNC05ZjcxLWQyNjhkZDcxZWFlMiIsImV4cCI6MTU3MjYxMjY2MSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo1MDAwLyIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6NTAwMC8ifQ.AbzMogY0eglRscu7L_I91T5OMsnAFTXNiyn2_tqlTgc" };
        localStorage.setItem('token', JSON.stringify(userObj));
    }
    logout(logoutType: string) {
        
        //localStorage.clear();

        if (!this.router.url.includes('auth/login')) {


            let userSession = JSON.parse(localStorage.getItem("userSession"));
            let sessionID = 0;

            let currentSessionToken: string;
            if (userSession && userSession.length > 1) {
                sessionID = userSession[1].sessionID;
                currentSessionToken = userSession[1].token;
            }
            else {
                sessionID = userSession[0].sessionID;
                currentSessionToken = userSession[0].token;
            }


            let IPAddress: string = "";
            this.commonService.getClientIpAddress().subscribe(value => {
                IPAddress = value;
            });


            let sessiontype;
            let customerID = null;
            var benificaryType = null;
            let jwtToken = jwt_decodes(userSession[0].token);
            if (userSession.length > 1) {
                let jwtTokenValue = jwt_decodes(userSession[1].token);
                customerID = +jwtTokenValue.UserID;

                let beneficiaryTypes = localStorage.getItem("beneficaryType");
                benificaryType = +beneficiaryTypes;
            }

            if (logoutType == "session timeout") {
                sessiontype = SessionTypeEnum.sessionTimeOut;
            }
            else {
                sessiontype = SessionTypeEnum.LogOut;
            }

            let model = {
                "userId": +jwtToken.UserID,
                "sessionType": sessiontype,  // 42 means single
                "customerId": customerID,
                "beneficiaryType": benificaryType,
                "deviceCode": "Web",
                "ip": IPAddress,
                "macaddress": "255.255.255.255",
                "dateTime": this.validationMessages.convertUTCDateToLocalDate(new Date()),
                "sessionhistoryDetailDto": [
                    {
                        "pageId": null, //state.url.split('/')[3],
                        "dateTime": this.validationMessages.convertUTCDateToLocalDate(new Date()),
                    }
                ]
            }

            this.commonService.ManageUserSessionHistory(model).subscribe(data => {
                // set session ID on user array
            });


            if (userSession && userSession.length > 1) {
                userSession.splice(1, 1);
                // userSession[0].sessionID = 0;

                localStorage.setItem('userSession', JSON.stringify(userSession))

                let navigateURL = "/pages/" + userSession[0].pages[0].pageUrl;
                this.router.navigate([navigateURL]);
            }
            else {
                localStorage.removeItem("userSession");

                localStorage.removeItem("token");
                localStorage.removeItem("sideMenu");
                localStorage.removeItem("pages");
                localStorage.removeItem("SessionID");


                let value = JSON.parse(jwt_decodes(currentSessionToken).UserRoles);
                
                if ((value.some(x => x.Id == CommonEnum.Individual)) || (value.some(x => x.Id == CommonEnum.DocumentCleranceCompany))) { // means didn't find 23 ID 
                    if (logoutType == "session timeout") {
                        this.router.navigate(['/auth/login-visitor'], { queryParams: { sessionType: 'sessiontimeout' } });
                    }
                    else {
                        this.router.navigateByUrl('/auth/login-visitor');
                    }
                }
                else {
                    if (logoutType == "session timeout") {
                        this.router.navigate(['/auth/login'], { queryParams: { sessionType: 'sessiontimeout' } });
                    }
                    else {
                        this.router.navigateByUrl('/auth/login');
                    }
                }

            }
        }
        // this.router.navigate(['/auth/login']);
    }
    GetLoginValidation(): Observable<any> {
        return this.httpclient.get<any[]>(this.FullPath + '/GetLoginValidation');
    }
    LoginValidation(obj: any): Observable<any> {
        return this.httpclient.post(this.FullPath + "/LoginValidation", obj);
    }
}