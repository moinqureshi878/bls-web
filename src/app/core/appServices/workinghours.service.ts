import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { EnvService } from '../environment/env.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { workinghoursmodel } from '../appModels/workinghours.model';
import { stringify } from 'querystring';
 

@Injectable()
export class WorkingHoursService {

    ApiUrl: string;
    ControllerName = 'WorkingHours';
    FullPath: string;

    constructor(
        private httpclient: HttpClient,
        private env: EnvService
    ) {
        this.ApiUrl = this.env.apiUrl;
        this.FullPath = this.env.apiUrl + this.ControllerName;
    }
    getById(id: number): Observable<any>{
      debugger;
        return this.httpclient.get<workinghoursmodel>(this.FullPath+`/GetById?id=${id}`);
    }    
    getAll(): Observable<any>{
      debugger;
      return this.httpclient.get<workinghoursmodel[]>(this.FullPath+"/GetAll");  
    }   
    AddNew(masterModules: workinghoursmodel): any {
        return this.httpclient.post<workinghoursmodel>(this.FullPath, masterModules);
    }
    UpdateRecord(masterModules: workinghoursmodel, ID: number): any {
      masterModules.id = ID;
        return this.httpclient.put<workinghoursmodel>(this.FullPath, masterModules);
    }
    DeleteRecord(ID: number): any {
        return this.httpclient.delete<workinghoursmodel>(this.FullPath+ `?ID=${ID}`);
    }
}