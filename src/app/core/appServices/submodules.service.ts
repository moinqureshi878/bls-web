import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { EnvService } from '../environment/env.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SubModulesModels } from '../appModels/submodules.model';


@Injectable({
	providedIn: "root"
})
export class SubModuleService {

    ApiUrl: string;
    ControllerName = 'submodules';
    FullPath: string;

    constructor(
        private httpclient: HttpClient,
        private env: EnvService
    ) {
        this.ApiUrl = this.env.apiUrl;
        this.FullPath = this.env.apiUrl + this.ControllerName;
    }

    getSubModules(): Observable<any[]> {
        return this.httpclient.get<SubModulesModels[]>(this.FullPath);
    }

    getActiveSubModules(): Observable<any[]> {
        return this.httpclient.get<SubModulesModels[]>(this.FullPath + "/GetActiveSubModules");
    }

    getSubModuleByID(ID: number): any {
        return this.httpclient.get<SubModulesModels>(this.FullPath + `?ID=${ID}`);
    }

    AddSubModules(subModules: SubModulesModels): any {
        return this.httpclient.post<SubModulesModels>(this.FullPath, subModules);
    }

    UpdateSubModules(subModules: SubModulesModels, ID: number): any {
        subModules.id = ID;
        return this.httpclient.put<SubModulesModels>(this.FullPath, subModules);
    }

    DeleteSubModules(ID: number): any {
        return this.httpclient.delete<SubModulesModels>(this.FullPath + `?ID=${ID}`);
    }

    GetSubModulesByModuleID(ID: number): Observable<any> {
        return this.httpclient.get<SubModulesModels>(this.FullPath + "/getSubModuleByModuleId" + `?moduleId=${ID}`);
    }
}