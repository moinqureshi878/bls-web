import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { EnvService } from '../environment/env.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModulesModels } from '../appModels/modules.model';


@Injectable({
	providedIn: "root"
})
export class ModuleService {

    ApiUrl: string;
    ControllerName = 'Modules';
    FullPath: string;

    constructor(
        private httpclient: HttpClient,
        private env: EnvService
    ) {
        this.ApiUrl = this.env.apiUrl;
        this.FullPath = this.env.apiUrl + this.ControllerName;
    }

    getModules(): Observable<any[]> {

        // let headers = new HttpHeaders({
        //     'ApiKey' : '123'
        //   }); { headers: headers }
        return this.httpclient.get<ModulesModels[]>(this.FullPath);
        // .pipe(
        //     map(resp => resp["data"])
        // );
    }

    // this api is used to get active modules for displaying in dropdown
    getActiveModules(): Observable<any[]> {
        return this.httpclient.get<ModulesModels[]>(this.FullPath + "/GetActiveModules");
    }

    getModuleByID(ID: number): any {
        return this.httpclient.get<ModulesModels>(this.FullPath + `?ID=${ID}`);
    }

    getApplications(): any {
        return this.httpclient.get<ModulesModels>(this.FullPath + "/Applications")
            .pipe(
                map(resp => resp["data"])
            );
    }

    AddModules(modules: ModulesModels): Observable<ModulesModels> {
        return this.httpclient.post<ModulesModels>(this.FullPath, modules);
    }

    UpdateModules(modules: ModulesModels, ID: number): any {
        modules.id = ID;
        return this.httpclient.put<ModulesModels>(this.FullPath, modules);
    }

    DeleteModules(ID: number): any {
        return this.httpclient.delete<ModulesModels>(this.FullPath + `?ID=${ID}`);
    }
}