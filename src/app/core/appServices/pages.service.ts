import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvService } from '../environment/env.service';
import { Observable } from 'rxjs';
import { PagesModels } from '../appModels/pages.model';


@Injectable({
	providedIn: "root"
})
export class PagesService {

    ApiUrl: string;
    ControllerName = 'Pages';
    FullPath: string;

    constructor(
        private httpclient: HttpClient,
        private env: EnvService
    ) {
        this.ApiUrl = this.env.apiUrl;
        this.FullPath = this.env.apiUrl + this.ControllerName;
    }

    getPages(): Observable<any[]> {
        return this.httpclient.get<PagesModels[]>(this.FullPath);
        // .pipe(
        //     map(resp => resp["data"])
        // );
    }

    getPagesByID(ID: number): Observable<PagesModels> {
        return this.httpclient.get<PagesModels>(this.FullPath + `?ID=${ID}`);
    }

    AddPages(pages: PagesModels): any {
        return this.httpclient.post<PagesModels>(this.FullPath, pages);
    }

    UpdatePages(pages: PagesModels, ID: number): any {
        pages.id = ID;
        return this.httpclient.put<PagesModels>(this.FullPath, pages);
    }

    DeletePages(ID: number): any {
        return this.httpclient.delete<PagesModels>(this.FullPath + `?ID=${ID}`);
    }
}