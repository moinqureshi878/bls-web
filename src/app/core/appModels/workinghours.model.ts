export interface workinghoursmodel {
  id: number;    
  nameEn: string;
  nameAr: string;
  timeFrom: string;  
  timeTo: string;  
  year:number;
  isWeekend:boolean;
  isActive: boolean
}