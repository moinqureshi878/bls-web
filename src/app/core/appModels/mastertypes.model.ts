export interface mastertypesmodel {
    id: number;    
    nameEn: string;
    nameAr: string;
    tableName: string;  
    isActive: boolean
  }