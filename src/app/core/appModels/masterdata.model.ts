export interface masterdata {
  id: number;
  Code:string;
  ValueEn: string;
  ValueAr: string;
  TableName: string;  
  isActive: boolean
}