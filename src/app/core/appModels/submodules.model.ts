export interface SubModulesModels {
    id: number;
    nameEn: string;
    nameAr: string;
    remarks: string;
    moduleCode: number;
    moduleNameEn: string;
    moduleNameAr: string;
    typeCode: number;
    typeNameEn: string;
    typeNameAr: string;
    isActive: boolean;
    ordersequence : number;
}