export class RolesModel {
    roleId: number;
    userId: number;
    nameAr: string;
    nameEn: string;
    remarks: string;
    Type: number;
    typeNameEn: number;
    typeNameAr: string;
    isActive: boolean;
}