export interface Nationalitymodel {
    id: number;    
    code: string;
    nationalityAr: string;
    nationalityEn: string;  
    countryAr: string;
    countryEn: string;  
    isActive: boolean
  }