export interface PagesModels {
    id: number;
    nameAr: string,
    nameEn: string,
    moduleId: number;
    remarks: string,
    subModuleId: number;
    isActive: string,
    createdBy: number;
    createdAt: Date;
    modifiedBy: number;
    modifiedAt: Date;
    pageUrl: string,
    isDefault: boolean;
}