export class ModulesModels {
    id: number;
    nameEn: string;
    nameAr: string;
    remarks: string;
    typeCode: number;
    typeNameEn: string;
    typeNameAr: string;
    applicationEn: string;
    applicationAr: string;
    applicationCode: string;
    isActive: boolean;
    icon : string;
    ordersequence : number;
}


