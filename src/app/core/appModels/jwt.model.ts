export class JWTModel {
    email: string;
    UserID: number;
    UserName: string;
    FullNameEn: string;
    FullNameAr: string;
    TypeNameEn: string;
    TypeNameAr: string;
    UserTypeEn: string;
    UserTypeAr: string;
    StepCode: string;
    Counter: string;
    GovtEntityNameEn: string;
    GovtEntityNameAr: string;
    UserRoles: JwtRolesModel[];
    nbf: number;
    exp: number;
    iat: number;
}

export interface JwtRolesModel {
    id: number,
    NameEn: string;
    NameAr: string;
}