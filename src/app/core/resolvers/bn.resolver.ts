import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BnService } from '../appServices/index.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: "root"
})

export class BnResolver implements Resolve<any> {
  constructor(
    public bnService: BnService,
    public router: Router) {
  }
  resolve(route: ActivatedRouteSnapshot) {
    
    if (route.url[0].path == 'review-bn-request-detail' && route.url[1].path) {
      let id = route.url[1].path;
      return this.bnService.GetBNRequest(Number(id)).pipe(map(resp => resp["data"]));
    }

    else if (route.url[0].path == 'business-names') {
      return this.bnService.GetBusinessNameRequest()
        .pipe(
          map(resp => resp["data"])
        );
    }
  }
}