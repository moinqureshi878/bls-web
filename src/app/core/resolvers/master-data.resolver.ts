import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService, CommonService } from '../appServices/index.service';
import { map } from 'rxjs/operators';
import { UserTypeEnum } from '../_enum/user-type.enum';
import { ConfigService } from '../appServices/config.service';


@Injectable({
  providedIn: "root"
})

export class MasterDataResolver implements Resolve<any> {
  constructor(
    public commonService: CommonService,
    public configService: ConfigService,
    public router: Router) {

  }
  resolve(route: ActivatedRouteSnapshot) {

    if (route.url[0].path == 'add-legal-type-approval') {
      return this.commonService.getLookUp("?type=GovernmentAuthorities");
    }

    else if (route.url[0].path == 'legal-type-approval') {
      return this.configService.GetTypeDetail("LegalTypes", "GovernmentAuthorities")
        .pipe(map(data => data["data"]));
    }

    else if (route.url[0].path == 'add-business-activity-approval') {
      return this.commonService.getLookUp("?type=GovernmentAuthorities");
    }

    else if (route.url[0].path == 'business-activity-approval') {
      return this.configService.GetAllBAApproval("BusinessNamesTypes", "GovernmentAuthorities")
        .pipe(map(data => data["data"]));
    }

    else if (route.url[0].path == 'add-legal-type-attachment') {
      return this.commonService.getLookUp("?type=Attachments");
    }

    else if (route.url[0].path == 'legal-type-attachment') {
      return this.configService.GetTypeDetail("LegalTypes", "Attachments")
        .pipe(map(data => data["data"]));
    }

  }
}