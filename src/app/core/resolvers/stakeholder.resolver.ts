import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../appServices/index.service';
import { UserTypeEnum } from '../../core/_enum/user-type.enum';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: "root"
})

export class StakeholderResolver implements Resolve<any> {
  constructor(
    public userService: UserService,
    public router: Router) {
  }
  resolve(route: ActivatedRouteSnapshot) {
    if (route.url[0].path == 'individual-stakeholders' || route.url[0].path == 'representative-stakeholders') {
      let userType;
      if (route.url[0].path == 'individual-stakeholders') {
        userType = UserTypeEnum.stakeholderIndividual;
      }
      else {
        userType = UserTypeEnum.stakeholderInheritanceRepresentative;
      }
      return this.userService.GetStakeHolderByType(userType).pipe(map(resp => resp["data"]));
    }
    else if (route.url[0].path == 'edit-stakeholders-individual' || route.url[0].path == 'edit-representative-stakeholders' && route.url[1].path) {
      let ID = route.url[1].path;
      return this.userService.GetUser(+ID).pipe(
        map(resp => resp["data"])
      );
    }
  }
}