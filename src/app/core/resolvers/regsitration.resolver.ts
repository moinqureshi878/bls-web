import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../appServices/index.service';

@Injectable({
  providedIn: "root"
})

export class RegistrationResolver implements Resolve<any> {
  constructor(
    public commonService: CommonService,
    public router: Router) {

  }
  resolve(route: ActivatedRouteSnapshot) {
    if (route.url[0].path == 'register' || route.url[0].path == 'add-stakeholders-individual' || route.url[0].path == 'edit-stakeholders-individual' || route.url[0].path == 'add-representative-stakeholders' || route.url[0].path == 'edit-representative-stakeholders') {
      return this.commonService.getRegLookups("common/getRegLookups?type=ApplicationTypes");
    }
  }
}