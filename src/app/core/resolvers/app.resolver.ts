import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { ModuleService, SubModuleService, PagesService, RoleService, UserService, CommonService, BnCommonService, MasterDataService, MasterTypesService, AuthenticationService } from '../appServices/index.service';
import { map } from 'rxjs/operators';
import { ConfigService } from '../appServices/config.service';

@Injectable({
    providedIn: "root"
})
export class AppResolver implements Resolve<any> {
    constructor(
        public moduleService: ModuleService,
        public submoduleService: SubModuleService,
        public pagesService: PagesService,
        public roleService: RoleService,
        public userService: UserService,
        public commonService: CommonService,
        public bnCommonService: BnCommonService,
        public configService: ConfigService,
        public masterDataService: MasterDataService,
        public masterTypesService: MasterTypesService,
        public authenticationService: AuthenticationService,
        public router: Router) {

    }
    resolve(route: ActivatedRouteSnapshot) {
        if (route.url[0].path == 'modules-definition') {
            return this.moduleService.getModules()
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'sub-modules-definition') {
            return this.submoduleService.getSubModules()
                .pipe(
                    map(resp => resp["data"])
                );
        }

        else if (route.url[0].path == 'pages-definition') {
            return this.pagesService.getPages()
                .pipe(
                    map(resp => resp["data"])
                );
        }

        else if (route.url[0].path == 'edit-pages' && route.url[1].path) {
            let ID = route.url[1].path;
            return this.pagesService.getPagesByID(+ID)
                .pipe(
                    map(resp => resp["data"])
                );
        }

        else if (route.url[0].path == 'edit-sub-modules' && route.url[1].path) {
            let ID = route.url[1].path;
            return this.submoduleService.getSubModuleByID(+ID)
                .pipe(
                    map(resp => resp["data"])
                );
        }

        else if (route.url[0].path == 'edit-modules' && route.url[1].path) {
            let ID = route.url[1].path;
            return this.moduleService.getModuleByID(+ID)
                .pipe(
                    map(resp => resp["data"])
                );
        }

        else if (route.url[0].path == 'roles-definition') {
            return this.roleService.GetRoles()
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'add-sub-users' || route.url[0].path == 'edit-sub-users') {
            return this.userService.GetRoleByGovernment()
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'edit-user') {
            return this.userService.GetUser(Number(route.params.id))
                .pipe(
                    map(resp => resp["data"])
                );
        }

        // else if (route.url[0].path == 'manage-users') {
        //     return this.userService.GetUser(1)
        // }
        // Admin Resolver

        else if (route.url[0].path == 'tda-users-profile') {
            return this.userService.GetInternalUser()
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'organization-structure') {
            return this.commonService.GetOrganizationalStructure()
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'registration-request') {
            return this.userService.GetCustomers()
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'review-registration-request') {
            return this.userService.GetUser(Number(route.params.id))
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'my-leaves-management') {
            return this.userService.GetLeaves()
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'edit-leaves' && route.url[1].path) {
            let ID = route.url[1].path;
            return this.userService.GetLeavesById(+ID)
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'business-activity-groups') {
            return this.bnCommonService.GetBusinessActivity()
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'edit-business-activity-groups' && route.url[1].path) {
            let ID = route.url[1].path;
            return this.bnCommonService.GetBusinessActivitygByID(+ID)
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'business-activity-pages') {
            return this.bnCommonService.GetBusinessActivityPages()
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'edit-business-activity-pages' && route.url[1].path) {
            let ID = route.url[1].path;
            return this.bnCommonService.GetBusinessActivityPagesByID(+ID)
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'public-holidays') {

            return this.bnCommonService.GetPublicHoliday()
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'edit-public-holidays' && route.url[1].path) {
            let ID = route.url[1].path;
            return this.bnCommonService.GetPublicHolidayByID(+ID)
                .pipe(
                    map(resp => resp["data"])
                );
        }
        // app type attachment resolvers start
        else if (route.url[0].path == 'app-type-attachment') {
            return this.configService.getAllAppTypeAttachment("ApplicationTypes", "Attachments")
                .pipe(
                    map(resp => resp["data"])
                );
        }

        else if (route.url[0].path == 'add-app-type-attachment') {
            return this.commonService.getLookUp("?type=attachments");
        }
        // app type attachment resolvers end

        // Manage Nationalities resolvers start
        else if (route.url[0].path == 'manage-nationalities') {
            return this.configService.getAllAppTypeAttachment("Country", "Nationality")
                .pipe(
                    map(resp => resp["data"])
                );
        }

        else if (route.url[0].path == 'add-nationalities') {
            return this.commonService.getLookUp("?type=nationality");
        }
        // Manage Nationalities resolvers end

        else if (route.url[0].path == 'edit-types' && route.url[1].path) {
            let ID = route.url[1].path;
            return this.masterTypesService.GetMasterTypesById(+ID)
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'edit-master' && route.url[1].path) {
            let ID = route.url[1].path;
            return this.masterDataService.GetLookupsById(+ID)
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'login-doc-validation') {
            return this.authenticationService.GetLoginValidation()
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'application-type') {
            return this.configService.GetAppTypeKPIs(0)
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'edit-application-type' && route.url[1].path) {
            let ID = route.url[1].path;
            return this.configService.GetAppTypeKPIs(+ID)
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'application-type-reasons') {
            return this.configService.GetAppTypeReason(0)
                .pipe(
                    map(resp => resp["data"])
                );
        }
        else if (route.url[0].path == 'edit-application-type-reasons' && route.url[1].path) {
            let ID = route.url[1].path;
            return this.configService.GetAppTypeReason(+ID)
                .pipe(
                    map(resp => resp["data"])
                );
        }
        // else if (route.url[0].path == 'master-data') {
        //     return this.masterDataService.GetAllLookups()
        //         .pipe(
        //             map(resp => resp["data"])
        //         );
        // }

        // else if (route.url[0].path == 'master-types') {
        //     return this.masterDataService.GetAllMaster()
        //         .pipe(
        //             map(resp => resp["data"])
        //         );
        // }
    }
}