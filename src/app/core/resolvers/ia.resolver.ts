import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BnService } from '../appServices/index.service';
import { map } from 'rxjs/operators';
import { IAService } from '../appServices/ia.service';

@Injectable({
  providedIn: "root"
})

export class IAResolver implements Resolve<any> {
  constructor(
    private iaService: IAService,
    public router: Router) {
  }
  resolve(route: ActivatedRouteSnapshot) {
    if (route.url[0].path == 'initial-approvals') {
      return this.iaService.getInitialApprovalRequest()
        .pipe(
          map(resp => resp["data"])
        );
    }
  }
}