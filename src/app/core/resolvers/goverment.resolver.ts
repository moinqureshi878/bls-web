import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService, CommonService } from '../appServices/index.service';
import { map } from 'rxjs/operators';
import { UserTypeEnum } from '../_enum/user-type.enum';

@Injectable()
export class GovermentResolver implements Resolve<any> {
  constructor(
    public userService: UserService,
    public commonService: CommonService,
    public router: Router) {

  }
  resolve(route: ActivatedRouteSnapshot) {

    if (route.url[0].path == 'manage-users') {
      return this.userService.GetMasterUsers().pipe(
        map(resp => resp["data"])
      );
    }
    else if (route.url[0].path == 'add-govt-users' || route.url[0].path == 'edit-govt-users' || route.url[0].path == 'edit-sub-users') {
      return this.userService.GetUser(Number(route.url[1].path)).pipe(
        map(resp => resp["data"])
      );
    }
    else if (route.url[0].path == 'edit-govt-users' && route.url[1].path) {
      let ID = route.url[1].path;
      return this.userService.GetUser(+ID).pipe(
        map(resp => resp["data"])
      );
    }

    else if (route.url[0].path == 'manage-sub-users') {
      return this.userService.GetGovtUser().pipe(
        map(resp => resp["data"])
      );
    }
  }
}