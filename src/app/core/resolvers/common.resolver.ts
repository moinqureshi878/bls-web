import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService, CommonService } from '../appServices/index.service';
import { map } from 'rxjs/operators';
import { UserTypeEnum } from '../_enum/user-type.enum';

@Injectable()
export class CommonResolver implements Resolve<any> {
  constructor(
    public userService: UserService,
    public commonService: CommonService,
    public router: Router) {

  }
  resolve(route: ActivatedRouteSnapshot) {
    if (route.url[0].path == 'add-govt-users' || route.url[0].path == 'edit-govt-users') {
      return this.commonService.GetAttachmentByUserTypeCode(UserTypeEnum.Govt_User).pipe(
        map(resp => resp.result["data"])
      );
    }
    else if (route.url[0].path == 'add-sub-users' || route.url[0].path == 'edit-sub-users') {
      return this.commonService.GetAttachmentByUserTypeCode(UserTypeEnum.Govt_Sub_User).pipe(
        map(resp => resp.result["data"])
      );
    }
  }
}