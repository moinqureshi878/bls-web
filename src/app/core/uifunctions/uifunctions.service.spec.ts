import { TestBed } from '@angular/core/testing';

import { UifunctionsService } from './uifunctions.service';

describe('UifunctionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UifunctionsService = TestBed.get(UifunctionsService);
    expect(service).toBeTruthy();
  });
});
