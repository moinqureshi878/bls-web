import { Injectable } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationMessagesService {

  constructor() { }
  setTime(getTime) {
    let date = new Date(getTime);
    let time = new Date(date.getTime() - (date.getTimezoneOffset() * 60000));
    return time;
  }
  setDate(getDate) {
    var date = new Date(getDate);
    date.setDate(date.getDate() + 1);
    date = new Date(date);
    return date;
  }
  convertUTCDateToLocalDate(date) {
    var oldDate = new Date(date);
    var newDate = new Date(date);
    newDate.setMinutes(oldDate.getMinutes() - oldDate.getTimezoneOffset());
    return newDate;
  }
  validation_messages = {
    'email': [
      { type: 'required', message: 'Email Address required.', messageAr: 'البريد الالكتروني إجباري' },
      { type: 'invalidEmail', message: 'Invalid Email address.', messageAr: "خطا في صيغة البريد الالكتروني، يرجى التصحيح" },
      { type: 'emailExists', message: 'Email Address already exists.', messageAr: 'البريد الالكتروني مستخدم سابقاً' },
    ],
    'usernameoremail': [
      { type: 'required', message: 'Username/Email Address required.', messageAr: 'اسم المستخدم / البريد الالكتروني إجباري' }
    ],
    'password': [
      { type: 'required', message: 'Password required.', messageAr: 'كلمة المرور إجبارية، يجب إدخالها' },
      { type: 'minlength', message: 'Enter password with min 6 chars or max 30 chars.', messageAr: 'يجب أن تكون كلمة المرور مؤلفة من ست أحرف على الأقل ولا تزيد عن 30 حرفا' },
      { type: 'maxlength', message: 'Enter password with min 6 chars or max 30 chars.', messageAr: 'يجب أن تكون كلمة المرور مؤلفة من ست أحرف على الأقل ولا تزيد عن 30 حرفا' },
      { type: 'invalidPassword', message: 'Password should contain atleast 6 letters and 1 number.', messageAr: 'يجب ان تحتوي كلمة المرور على ست حروف ورقم واحد على الأقل' },
    ],
    'newPassword': [
      { type: 'required', message: 'Password required.', messageAr: 'كلمة المرور إجبارية، يجب إدخالها' },
      { type: 'minlength', message: 'Enter password with min 6 chars or max 30 chars.', messageAr: 'يجب أن تكون كلمة المرور مؤلفة من ست أحرف على الأقل ولا تزيد عن 30 حرفا' },
      { type: 'maxlength', message: 'Enter password with min 6 chars or max 30 chars.', messageAr: 'يجب أن تكون كلمة المرور مؤلفة من ست أحرف على الأقل ولا تزيد عن 30 حرفا' },
      { type: 'invalidPassword', message: 'Password should contain atleast 6 letters and 1 number.', messageAr: 'يجب ان تحتوي كلمة المرور على ست حروف ورقم واحد على الأقل' },
    ],
    'confirmPassword': [
      { type: 'required', message: 'Re-Type Password required.', messageAr: 'يجب إعادة إدخال وتأكيد كلمة المرور' },
      { type: 'mismatchedPasswords', message: 'Passwords and Re-Type Password does not match.', messageAr: 'كلمة المرور غير متطابقة مع تأكيد كلمة المرور' },
    ],
    'fullNameEn': [
      { type: 'required', message: 'Full Name required in English.', messageAr: 'الاسم الكامل باللغة الإنجليزية إجباري' },
      { type: 'maxlength', message: 'Full Name cannot exceed 100 characters.', messageAr: 'الاسم الكامل ابللغة الإنجليزية يجب أن لا يزيد عن 100 حرف' },
      { type: 'fullNameEnExists', message: 'Full Name already exists.', messageAr: 'الاسم الكامل باللغة الإنجليزية مستخدم سابقاً' },
    ],
    'fullNameAr': [
      { type: 'required', message: 'Full Name required in Arabic.', messageAr: 'الاسم الكامل اللغة العربية إجباري' },
      { type: 'maxlength', message: 'Full Name cannot exceed 100 characters.', messageAr: 'لا يمكن أن يتجاوز الاسم الكامل 100 حرف' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
      { type: 'fullNameArExists', message: 'Full Name already exists.', messageAr: 'الاسم الكامل موجود ومستخدم سابقاً' }
    ],
    'nationality': [
      { type: 'required', message: 'Select Nationality.', messageAr: 'يرجى اختيار الجنسية' },
    ],
    'username': [
      { type: 'required', message: 'Username required.', messageAr: 'اسم المستخدم إجباري، يجب إدخاله' },
      { type: 'maxlength', message: 'Name cannot exceed 100 characters.', messageAr: 'لا يمكن أن يتجاوز الاسم 100 حرف' },
      { type: 'usernameExists', message: 'Username already exists.', messageAr: 'اسم المستخدم موجود مسبقاً، يرجى تغييره إلى اسم آخر' },
      { type: 'nameValidator', message: 'Special characters and spacing not allowed in Username.', messageAr: 'غير مسموح بالحروف الخاصة والفراغات ضمن اسم المستخدم' }
    ],
    'employeeName': [
      { type: 'maxlength', message: 'Employee Name cannot exceed 100 characters.', messageAr: 'لا يمكن أن يتجاوز الاسم 100 حرف' },
      { type: 'nameValidator', message: 'Special characters and spacing not allowed.', messageAr: 'غير مسموح بالحروف الخاصة والفراغات ضمن اسم المستخدم' }
    ],
    'businessUnitCode': [
      { type: 'required', message: 'Select Business Unit.', messageAr: 'يرجى اختيار الوحدة الإدارية' },
    ],
    'designation': [
      { type: 'required', message: 'Select Designation.', messageAr: 'يرجى اختيار المسمى الوظيفي' },
    ],
    'hrCode': [
      { type: 'hrCodeExists', message: 'HR code already exists.', messageAr: 'الرقم الوظيفي مستخدم سابقاً' },
      { type: 'maxlength', message: 'HR code can not exceed 50 characters.', messageAr: 'لا يمكن أن يتجاوز الرقم الوظيفي 50 حرف' },
    ],
    'mobileNo': [
      { type: 'required', message: 'Mobile no. required.', messageAr: 'رقم الموبايل إجباري' },
      { type: 'mobileNoExists', message: 'Mobile no. already exists.', messageAr: 'رقم الموبايل مستخدم سابقاً' },
      { type: 'phoneNumberValidator', message: 'Please enter a valid mobile number (+971 or 00971xxxxxxxx)', messageAr: '(+971 or 009715xxxxxxxx)يرجى ادخال رقم هاتف صحيح' }
    ],
    'customerMobileNo': [
      { type: 'required', message: 'Mobile no. required.', messageAr: 'رقم الموبايل إجباري' },
      { type: 'customerMobileNoExists', message: 'Mobile no. already exists.', messageAr: 'رقم الموبايل مستخدم سابقاً' },
      { type: 'phoneNumberValidator', message: 'Please enter a valid mobile number (+971 or 00971xxxxxxxx)', messageAr: '(+971 or 009715xxxxxxxx)يرجى ادخال رقم هاتف صحيح' }
    ],
    'landlineNo': [
      { type: 'required', message: 'Landline no. required.', messageAr: 'رقم الهاتف الأرضي إجباري' },
      { type: 'landlineNoExists', message: 'Landline no. already exists.', messageAr: 'رقم الهاتف الأرضي مستخدم سابقاً' },
    ],
    'deviceName': [
      { type: 'required', message: 'Physical Computer Name required.', messageAr: 'اسم الكمبيوتر إجباري' },
      { type: 'deviceNameExists', message: 'Physical Computer Name already exists.', messageAr: 'اسم الكمبيوتر مستخدم سابقاً' },
    ],
    'macAddress': [
      { type: 'macAddressExists', message: 'Mac addres already exists.', messageAr: 'رقم MAC مستخدم سابقاً' },
    ],
    'remarks': [
      { type: 'commentValidator', message: 'Remarks cannot be started with space.', messageAr: 'لا يمكن أن تبدأ الملاحظات بفراغ' },
      { type: 'maxlength', message: 'Remarks cannot exceed 500 characters.', messageAr: 'لا يمكن أن يزيد حقل الملاحظات عن 500 حرف' },
    ],
    'moduleNameAr': [
      { type: 'required', message: 'Module Name required in Arabic.', messageAr: 'اسم المكون الرئيسيى باللغة العربية إجباري' },
      { type: 'maxlength', message: 'Module Name cannot exceed 100 characters.', messageAr: 'اسم المكون الرئيسيى باللغة العربية لا يمكن أن يتجاوز 100 حرف' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' }
    ],
    'moduleNameEn': [
      { type: 'required', message: 'Module Name required in English.', messageAr: 'اسم المكون الرئيسي باللغة الإنجليزية إجباري' },
      { type: 'maxlength', message: 'Module Name cannot exceed 100 characters.', messageAr: 'اسم المكون الرئيسي باللغة الإنجليزية لا يكمن أن يتجاوز 100 حرف' },
    ],
    'type': [
      { type: 'required', message: 'Select Type.', messageAr: 'يرجى اختيار النوع' },
    ],
    'application': [
      { type: 'required', message: 'Select Application.', messageAr: 'يرجى اختيار النظام' },
    ],
    'subModuleNameAr': [
      { type: 'required', message: 'Sub Module Name required in Arabic.', messageAr: 'اسم المكون الفرعي باللغة العربية إجباري' },
      { type: 'maxlength', message: 'Sub Module Name cannot exceed 100 characters.', messageAr: 'اسم المكون الفرعي باللغة العربية لا يمكن أن يتجاوز 100 حرف' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },

    ],
    'subModuleNameEn': [
      { type: 'required', message: 'Sub Module Name required in English.', messageAr: 'اسم المكون الفرعي باللغة الانجليزية إجباري' },
      { type: 'maxlength', message: 'Sub Module Name cannot exceed 100 characters.', messageAr: 'اسم المكون الفرعي باللغة العربية لا يمكن أن يتجاوز 100 حرف' },
    ],
    'moduleName': [
      { type: 'required', message: 'Select linked Module Name.', messageAr: 'يرجى اختيار المكون الام' },
    ],
    'roleNameEn': [
      { type: 'required', message: 'Role Name required in English.', messageAr: 'اسم الدور باللغة الإنجليزية اجباري' },
      { type: 'maxlength', message: 'Role Name cannot exceed 100 characters.', messageAr: 'لا يمكن أن يتجاوز اسم الدور 100 حرف' },
      { type: 'roleNameEnExists', message: 'Role name already exists.', messageAr: 'اسم الدور مستخدم سابقاً' },
    ],
    'roleNameAr': [
      { type: 'required', message: 'Role Name required in Arabic.', messageAr: 'اسم الدور باللغة االعربية إجباري' },
      { type: 'maxlength', message: 'Role Name cannot exceed 100 characters.', messageAr: 'لا يمكن أن يتجاوز اسم الدور 100 حرف' },
      { type: 'roleNameArExists', message: 'Role name already exists.', messageAr: 'اسم الدور مستخدم سابقاً' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
    ],
    'moduleDdl': [
      { type: 'required', message: 'Select Module Name.', messageAr: 'يرجى اختيار اسم المكون الرئيسي' },
    ],
    'submoduleDdl': [
      { type: 'required', message: 'Select Sub module Name.', messageAr: 'يرجى اختيار اسم المكون الفرعي' },
    ],
    'pageNameEn': [
      { type: 'required', message: 'Page Name required in English.', messageAr: 'اسم الصفحة باللغة الانجليزية إجباري ' },
      { type: 'maxlength', message: 'Page Name cannot exceed 100 characters.', messageAr: 'لا يمكن ان يتجاوز اسم الصفحة 100 حرف' },
      { type: 'pageNameEnExists', message: 'Page Name already exists.', messageAr: 'اسم الصفحة موجود سابقاً' }
    ],
    'pageNameAr': [
      { type: 'required', message: 'Page Name required in Arabic.', messageAr: 'اسم الصفحة باللغة العربية إجباري' },
      { type: 'maxlength', message: 'Page Name cannot exceed 100 characters.', messageAr: 'لا يمكن ان يتجاوز اسم الصفحة 100 حرف' },
      { type: 'pageNameArExists', message: 'Page Name already exists.', messageAr: 'اسم الصفحة موجود سابقاً' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' }
    ],
    'pageUrl': [
      { type: 'required', message: 'Page URL required.', messageAr: 'رايط الصفحة إجباري' },
      { type: 'maxlength', message: 'Page Url cannot exceed 500 characters.', messageAr: 'Page Url cannot exceed 500 characters.' }
    ],
    'govtEntity': [
      { type: 'required', message: ' Select Govt entity.', messageAr: 'يرجى اختيار الجهة الحكومية' },
    ],
    'nameAr': [
      { type: 'required', message: 'Name required in Arabic.', messageAr: 'الاسم باللغة العربية إجباري.' },
      { type: 'maxlength', message: 'Name in Arabic cannot exceed 100 characters.', messageAr: 'يجب أن لا يزيد الاسم باللغة العربية عن 100 حرف' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' }
    ],
    'nameEn': [
      { type: 'required', message: 'Name required in English.', messageAr: 'الاسم باللغة الإنجليزية إجباري' },
      { type: 'maxlength', message: 'Name in English cannot exceed 100 characters.', messageAr: 'يجب أن لا يزيد الاسم باللغة الإنجليزية عن 100 حرف' }
    ],
    'representativeRole': [
      { type: 'maxlength', message: 'Representative Role cannot exceed 100 characters.', messageAr: 'حقل صفة التمثيل لا يمكن أن يزيد عن 100 حرف' },
    ],
    'department': [
      { type: 'maxlength', message: 'Department cannot exceed 100 characters.', messageAr: 'اسم الإدارة لا يمكن أن يزيد عن 100 حرف' },
    ],
    'attachmentSize': [
      { type: 'fileSize', message: 'The file size cannot exceed  2 MB.', messageAr: 'The file size cannot exceed  2 MB.' },
    ],
    'attachmentName': [
      { type: 'required', message: 'Attachment Required.', messageAr: 'المرفقات إجبارية' },
    ],
    'userRole': [
      { type: 'required', message: 'At least select 1 Role', messageAr: 'لا بد من اختيار دور واحد على الأقل لهذا المستخدم' },
    ],
    'fromDate': [
      { type: 'required', message: 'Please select `from Date`', messageAr: 'من فضلك اختر " من تاريخ"' },
    ],
    'toDate': [
      { type: 'required', message: 'Please select `to date`', messageAr: 'من فضلك اختر " الى تاريخ"' },
      { type: 'dateLessThan', message: '`from Date` should be less than `to Date`', messageAr: 'ـ"من تاريخ" يجب أن يكون أقل من " الى تاريخ"' }
    ],
    'fromTime': [
      { type: 'required', message: 'Please select `from time`', messageAr: 'يرجى اختيار "الوقت من"' },
    ],
    'toTime': [
      { type: 'required', message: 'Please select `to time`', messageAr: 'Please select `to time`' },
      { type: 'timeLessThan', message: '`from time` should be less than `to time`', messageAr: '"الوقت من" يجب أن يكون أصغر من "الوقت إلى"' }
    ],
    'date': [
      { type: 'required', message: 'Date required.', messageAr: 'التاريخ إجباري' },
    ],
    'citizenID': [
      { type: 'required', message: 'Emirates ID required.', messageAr: 'رقم الهوية الإماراتية إجباري' },
      { type: 'CitizenIDExists', message: 'Emirates ID already exists.', messageAr: 'رقم الهوية الإماراتية مستخدم سابقاً' },
    ],
    'beneficiaryType': [
      { type: 'required', message: 'select beneficiary type', messageAr: 'select beneficiary type' }
    ],
    'fatherNameAr': [
      { type: 'required', message: 'Father Name required in Arabic.', messageAr: 'اسم الأب باللغة لعربية إجباري' },
      { type: 'maxlength', message: 'Father Name cannot exceed 100 characters.', messageAr: 'لا يمكن أن يتجاوز اسم الأب 100 حرف' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' }
    ],
    'grandFatherNameAr': [
      { type: 'maxlength', message: 'Grandfather Name cannot exceed 100 characters.', messageAr: 'لا يمكن أن يتجاوز اسم الجد 100 حرف' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' }
    ],
    'familyNameAr': [
      { type: 'required', message: 'Family Name required in Arabic.', messageAr: 'اسم العائلة باللغة العربية إجباري' },
      { type: 'maxlength', message: 'Family Name cannot exceed 100 characters.', messageAr: 'اسم العائلة باللغة العربية لا يمكن أن يتجاوز 100 حرف' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' }
    ],
    'passportNo': [
      { type: 'required', message: 'Passport No required.', messageAr: 'رقم جواز السفر إجباري' }
    ],
    'passportExpiryDate': [
      { type: 'required', message: 'Passport Expiry Date required.', messageAr: 'تاريخ انتهاء جواز السفر إجباري' }
    ],
    'occupation': [
      { type: 'maxlength', message: 'Occupation cannot exceed 100 characters.', messageAr: 'الوظيفة لا يمكن ن تتجاوز 100 حرف' }
    ],
    'SecondaryPhone': [
      { type: 'required', message: 'Mobile no. required.', messageAr: 'رقم الموبايل إجباري' },
      { type: 'SecondarymobileNoExists', message: 'Mobile no. already exists.', messageAr: 'رقم الموبايل مستخدم سابقاً' },
      { type: 'phoneNumberValidator', message: 'Please enter a valid mobile number (+971 or 00971xxxxxxxx)', messageAr: '(+971 or 009715xxxxxxxx)يرجى ادخال رقم هاتف صحيح' }
    ],
    'licenceNumber': [
      { type: 'required', message: 'License number required.', messageAr: 'رقم الرخصة إجباري' }
    ],
    'licenceExpiryDate': [
      { type: 'required', message: 'License expiry date required.', messageAr: 'تاريخ انتهار الرخصة إجباري' }
    ],
    'licenceIssueDate': [
      { type: 'required', message: 'License issue date required.', messageAr: 'تاريخ انتهار الرخصة إجباري' }
    ],
    'IssuanceEmirates': [
      { type: 'required', message: 'Please select issuance entity.', messageAr: 'يرجى اختيار الإمارة - جهة الإصدار' }
    ],
    'BNnameAr': [
      { type: 'required', message: 'Business Name in Arabic required.', messageAr: 'الاسم الاقتصادي باللغة العربية إجباري' },
      { type: 'maxlength', message: 'Business Name cannot exceed 200 characters.', messageAr: 'لا يمكن ان يتجاوز الاسم الاقتصادي 200 حرف' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' }
    ],
    'BNnameEn': [
      { type: 'required', message: 'BN name in English required.', messageAr: 'الاسم الاقتصادي باللغة الانجليزي إجباري' },
      { type: 'maxlength', message: 'BN cannot exceed 200 characters.', messageAr: 'لا يمكن ان يتجاوز الاسم الاقتصادي 200 حرف' }
    ],
    'EmiratesExpiryDate': [
      { type: 'required', message: 'Emirates expiry date required.', messageAr: 'تاريخ انتهاء هوية الإمارات إجباري' }
    ],
    'designationTextBox': [
      { type: 'maxlength', message: 'Designation cannot exceed  100 characters.', messageAr: 'المسمى الوظيفي لا يمكن أن يتجاوز 100 حرف' }
    ],
    'sponsorname': [
      { type: 'maxlength', message: 'Sponsor name cannot exceed 100 characters.', messageAr: 'اسم الكفيل لا يمكن أن يتجاوز 100 حرف' }
    ],
    'title': [
      { type: 'maxlength', message: 'Title cannot exceed 100 characters.', messageAr: 'المسمى لا يمكن أن يزيد على 100 حرف' }
    ],
    'subArea': [
      { type: 'required', message: 'please select sub area', messageAr: 'please select sub area' },
      { type: 'maxlength', message: 'Sub Area cannot exceed 100 characters.', messageAr: 'لا يمكن أن تجاوز اسم المنطقة الفرعية 100 حرف' }
    ],
    'mainArea': [
      { type: 'required', message: 'please select main area', messageAr: 'please select main area' }
    ],
    'street': [
      { type: 'maxlength', message: 'Street cannot exceed 100 characters.', messageAr: 'لا يمكن أن يتجاوز اسم الشارع 100 حرف' }
    ],
    'address': [
      { type: 'required', message: 'address required', messageAr: 'address required' }
    ],
    'parcelID': [
      { type: 'required', message: 'parcel ID required', messageAr: 'parcel ID required' }
    ],
    'building': [
      { type: 'required', message: 'building required', messageAr: 'building required' },
      { type: 'maxlength', message: 'Building cannot exceed 100 characters.', messageAr: 'لا يمكن أن يتجاوز اسم المبنى 100 حرف' }
    ],
    'poBox': [
      { type: 'maxlength', message: 'P.O Box cannot exceed 100 characters.', messageAr: 'لا يمكن أن يتجاوز رقم صندوق البريد 100 حرف' }
    ],
    'UID': [
      { type: 'required', message: 'UID required.', messageAr: 'الرقم الموحد إجباري' },
      { type: 'CitizenIDExists', message: 'UID already exists.', messageAr: 'الرقم الموحد مستخدم سابقاً' }
    ],
    'UIDExpiryDate': [
      { type: 'required', message: 'UID Expiry date required.', messageAr: 'تاريخ انتهاء الرقم الموحد إجباري' }
    ],
    'emiratesIDExpiry': [
      { type: 'required', message: 'Emirates ID Expiry Date required.', messageAr: 'تاريخ انتهاء هوية الإمارات إجباري' }
    ],
    'PPEEperiodBLP': [
      { type: 'required', message: 'Period per renewal required.', messageAr: 'مدة تجديد الرخصة حقل إجباري' }
    ],
    'IssuancePeriodBLP': [
      { type: 'required', message: 'Issuance period required.', messageAr: 'مدة الإصدار حقل إجباري' }
    ],

    'businessTypesddl': [
      { type: 'required', message: 'Select Application Type.', messageAr: 'يرجى اختيار نوع المعاملة' }
    ],
    'grp1TitleEn': [
      { type: 'required', message: 'Title required in English.', messageAr: 'الوصف باللغة الإنجليزية إجباري' },
      { type: 'maxlength', message: 'Title in English cannot exceed 100 characters.', messageAr: 'لا يمكن أن يكون الوصف باللغة الانجليزية أكثر من 100 حرف' },
    ],
    'grp1TitleAr': [
      { type: 'required', message: 'Title required in Arabic.', messageAr: 'الوصف باللغة العربية إجباري' },
      { type: 'maxlength', message: 'Title in Arabic cannot exceed 100 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة العربية أكثر من 100 حرف' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' }
    ],
    'grp1DescriptionEn': [
      { type: 'required', message: 'Description required in English.', messageAr: 'الوصف باللغة الانجليزية إجباري' },
      { type: 'maxlength', message: 'Description In English cannot exceed 2500 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة الانجليزي أكثر من 2500 حرف' },
    ],
    'grp1DescriptionAr': [
      { type: 'required', message: 'Description required in Arabic.', messageAr: 'الوصف باللغة العربية إجباري' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
      { type: 'maxlength', message: 'Description in Arabic cannot exceed 2500 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة العربية أكثر من 2500 حرف' },
    ],
    'grp2TitleEn': [
      { type: 'required', message: 'Title required in English.', messageAr: 'الوصف باللغة الإنجليزية إجباري' },
      { type: 'maxlength', message: 'Title in English cannot exceed 100 characters.', messageAr: 'لا يمكن أن يكون الوصف باللغة الانجليزية أكثر من 100 حرف' },
    ],
    'grp2TitleAr': [
      { type: 'required', message: 'Title required in Arabic.', messageAr: 'الوصف باللغة العربية إجباري' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
      { type: 'maxlength', message: 'Title in Arabic cannot exceed 100 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة العربية أكثر من 100 حرف' },
    ],
    'grp2DescriptionEn': [
      { type: 'required', message: 'Description required in English.', messageAr: 'الوصف باللغة الانجليزية إجباري' },
      { type: 'maxlength', message: 'Description In English cannot exceed 2500 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة الانجليزي أكثر من 2500 حرف' },
    ],
    'grp2DescriptionAr': [
      { type: 'required', message: 'Description required in Arabic.', messageAr: 'الوصف باللغة العربية إجباري' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
      { type: 'maxlength', message: 'Description in Arabic cannot exceed 2500 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة العربية أكثر من 2500 حرف' },
    ],
    'grp3TitleEn': [
      { type: 'required', message: 'Title required in English.', messageAr: 'الوصف باللغة الإنجليزية إجباري' },
      { type: 'maxlength', message: 'Title in English cannot exceed 100 characters.', messageAr: 'لا يمكن أن يكون الوصف باللغة الانجليزية أكثر من 100 حرف' },
    ],
    'grp3TitleAr': [
      { type: 'required', message: 'Title required in Arabic.', messageAr: 'الوصف باللغة العربية إجباري' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
      { type: 'maxlength', message: 'Title in Arabic cannot exceed 100 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة العربية أكثر من 100 حرف' },
    ],
    'grp3DescriptionEn': [
      { type: 'required', message: 'Description required in English.', messageAr: 'الوصف باللغة الانجليزية إجباري' },
      { type: 'maxlength', message: 'Description In English cannot exceed 2500 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة الانجليزي أكثر من 2500 حرف' },
    ],
    'grp3DescriptionAr': [
      { type: 'required', message: 'Description required in Arabic.', messageAr: 'الوصف باللغة العربية إجباري' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
      { type: 'maxlength', message: 'Description in Arabic cannot exceed 2500 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة العربية أكثر من 2500 حرف' },
    ],
    'grp4TitleEn': [
      { type: 'required', message: 'Title En is required.', messageAr: 'الوصف باللغة الإنجليزية إجباري' },
      { type: 'maxlength', message: 'Title in English cannot exceed 100 characters.', messageAr: 'لا يمكن أن يكون الوصف باللغة الانجليزية أكثر من 100 حرف' },
    ],
    'grp4TitleAr': [
      { type: 'required', message: 'Title Ar is required.', messageAr: 'الوصف باللغة العربية إجباري' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
      { type: 'maxlength', message: 'Title in Arabic cannot exceed 100 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة العربية أكثر من 100 حرف' },
    ],
    'grp4DescriptionEn': [
      { type: 'required', message: 'Description En is required.', messageAr: 'الوصف باللغة الانجليزية إجباري' },
      { type: 'maxlength', message: 'Description In English cannot exceed 2500 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة الانجليزي أكثر من 2500 حرف' },
    ],
    'grp4DescriptionAr': [
      { type: 'required', message: 'Description Ar is required.', messageAr: 'الوصف باللغة العربية إجباري' },
      { type: 'maxlength', message: 'Description in Arabic cannot exceed 2500 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة العربية أكثر من 2500 حرف' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
    ],
    'allowedExtension': [
      { type: 'required', message: 'No. of allowed extensions required.', messageAr: 'عدد مرات تمديد الاسم حقل إجباري' }
    ],
    'autoCancellationPeriodReturned': [
      { type: 'required', message: 'Auto cancellation period required.', messageAr: 'مدة الإلغاء الاآلية للمعاملة حقل إجباري' }
    ],
    'autoCancellationPeriodPayments': [
      { type: 'required', message: 'Auto cancellation period required.', messageAr: 'مدة الإلغاء الاآلية للمعاملة حقل إجباري' }
    ],
    'termsCondition': [
      { type: 'termsConditionValidator', message: 'Please accept terms and conditons', messageAr: 'Please accept terms and conditons' }
    ],
    'applicationTypeddl': [
      { type: 'required', message: 'Select Application Type.', messageAr: 'يرجى اختيار نوع المعاملة' }
    ],
    'organizationUnitNameEn': [
      { type: 'required', message: 'Organization unit name required in English.', messageAr: 'اسم الوحدة الإدارية باللغة الانجليزية إجباري' },
      { type: 'maxlength', message: 'Organization unit name cannot exceed 100 characters.', messageAr: 'اسم الوحدة الإدارية باللغة الانجليزية لا يمكن أن يتجاوز 100 حرف' },
    ],
    'organizationUnitNameAr': [
      { type: 'required', message: 'Organization unit name required in Arabic.', messageAr: 'اسم الوحدة الإدارية باللغة العربية إجباري' },
      { type: 'maxlength', message: 'Organization unit name cannot exceed 100 characters.', messageAr: 'اسم الوحدة الإدارية باللغة العربية لا يمكن أن يتجاوز 100 حرف' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' }
    ],
    'nationalitiesddl': [
      { type: 'required', message: 'Nationalities Required.', messageAr: 'Nationalities Required.' }
    ],

    'code': [
      { type: 'required', message: 'Code required.', messageAr: 'Code required.' },
      { type: 'maxlength', message: 'Code cannot exceed 100 characters.', messageAr: 'Code cannot exceed 100 characters.' },
      { type: 'alphaNumeric', message: 'Code format is incorrect.', messageAr: 'Code format is incorrect.' },
      { type: 'codeExists', message: 'Code already exists.', messageAr: 'Code already exists.' }
    ],
    'descriptionEn': [
      { type: 'required', message: 'Description required in English.', messageAr: 'الوصف باللغة الانجليزية إجباري' },
      { type: 'maxlength', message: 'Description In English cannot exceed 2500 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة الانجليزي أكثر من 2500 حرف' },
    ],
    'descriptionAr': [
      { type: 'required', message: 'Description required in Arabic.', messageAr: 'الوصف باللغة العربية إجباري' },
      { type: 'maxlength', message: 'Description in Arabic cannot exceed 2500 characters.', messageAr: 'لا يمكن أن يكون الوصف باللعة العربية أكثر من 2500 حرف' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' }
    ],
    'isic4': [
      { type: 'required', message: 'ISIC4 #. required.', messageAr: 'رقم ISIC4 إجباري' },
      { type: 'maxlength', message: 'ISIC4 #. cannot exceed 100 characters.', messageAr: 'رقم ISIC4  لا يمكن أن يزيد عن 100 حرف' },
      { type: 'alphaNumeric', message: 'ISIC4 #. format is incorrect.', messageAr: 'ISIC4 #. format is incorrect.' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' }
    ],
    'activityUsageEn': [
      { type: 'required', message: 'Activity Usage required in English.', messageAr: 'شرح استخدام النشاط باللغة الانجليزية إجباري' },
      { type: 'maxlength', message: 'Activity Usage cannot exceed 2500 characters.', messageAr: 'إن شرح النشاط باللغة الانجليزية لا يمكن أن يزيد عن 2500 حرف' },
    ],
    'activityUsageAr': [
      { type: 'required', message: 'Activity Usage required in Arabic.', messageAr: 'شرح استخدام النشاط باللغة العربية إجباري' },
      { type: 'maxlength', message: 'Activity Usage cannot exceed 2500 characters.', messageAr: 'إن شرح النشاط باللغة العربية لا يمكن أن يزيد عن 2500 حرف' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' }
    ],
    'purpose': [
      { type: 'required', message: 'Purpose required.', messageAr: 'Purpose required.' },
      { type: 'maxlength', message: 'Purpose cannot exceed 1000 characters.', messageAr: 'Purpose cannot exceed 1000 characters.' },
    ],
    'headId': [
      { type: 'required', message: 'Select Master Type', messageAr: 'يرجى اختيار نوع البيانات الرئيسية' },
    ],
    'titleId': [
      { type: 'required', message: 'Select Title', messageAr: 'Select Title' },
    ],
    // 'titleAr': [
    //   { type: 'required', message: 'Name required in Arabic.', messageAr: 'الاسم باللغة العربية إجباري.' },
    //   { type: 'maxlength', message: 'Name in Arabic cannot exceed 100 characters.', messageAr: 'يجب أن لا يزيد الاسم باللغة العربية عن 100 حرف' },
    //   { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' }
    // ],
    // 'titleEn': [
    //   { type: 'required', message: 'Name required in English.', messageAr: 'الاسم باللغة الإنجليزية إجباري' },
    //   { type: 'maxlength', message: 'Name in English cannot exceed 100 characters.', messageAr: 'يجب أن لا يزيد الاسم باللغة الإنجليزية عن 100 حرف' }
    // ],
    'notifyBefore': [
      { type: 'required', message: 'No. of days required for notification before expiry.', messageAr: 'عدد مرات تمديد الاسم حقل إجباري' },
      { type: 'min', message: 'Enter correct value.', messageAr: 'يرجى إدخال بينات صحيحة' },
    ],
    'lockAfter': [
      { type: 'required', message: 'No. of days required to lock account after expiry.', messageAr: 'عدد الأيام لإيقاف الحساب بعد انتهاء الوثائق' },
      { type: 'min', message: 'Enter correct value.', messageAr: 'يرجى إدخال بينات صحيحة' },
    ],
    'legalTypeddl': [
      { type: 'required', message: 'Select Legal Type.', messageAr: 'يرجى اختيار الشكل القانوني' }
    ],
    'ISICBNAuthorityddl': [
      { type: 'required', message: 'Select Business Activity', messageAr: 'يرجى اختيار نشاط اقتصادي' }
    ],

    'appTypeId': [
      { type: 'required', message: 'Select Legal Type.', messageAr: 'يرجى اختيار الشكل القانوني' }
    ],
    'attendingTime': [
      { type: 'required', message: 'Attending Time KPI required.', messageAr: 'زمن بداية مراجعة المعاملة KPI اجباري' }
    ],
    'customerReview': [
      { type: 'required', message: 'Customer service review KPI required.', messageAr: 'زمن مراجعة قسم خدمة العملاء للمعاملة KPI بالساعات اجباري' }
    ],
    'managerReview': [
      { type: 'required', message: 'Manager review KPI required.', messageAr: 'زمن مراجعة المدير للمعاملة KPI بالساعات اجباري' }
    ],
    'consultation': [
      { type: 'required', message: 'Consultation KPI required.', messageAr: 'زمن مراجعة الاستشاري للمعاملة KPI بالساعات اجباري' }
    ],
    'customerReSubmit': [
      { type: 'required', message: 'Customer re-submit KPI required.', messageAr: 'الزمن اللازم لرد العميل على إعادة المعاملة KPI بالساعات اجباري' }
    ],
    'customerPayment': [
      { type: 'required', message: 'Customer Payment KPI required.', messageAr: 'الزمن اللازم لدفع الرسوم من العميل KPI بالساعات اجباري' }
    ],
    'govtEntityApproval': [
      { type: 'required', message: 'Government Entity Approval/ Noc KPI required.', messageAr: 'الزمن الازم لرد الجهة الحكومية على الموافقة أو عدم الممانعة KPI بالساعات اجباري' }
    ],
    'totalKpis': [
      { type: 'required', message: 'Total KPI Timeframe Required.', messageAr: 'إجمالي تنفيذ المعاملة KPI  إجباري' }
    ],

    'reasonTypeId': [
      { type: 'required', message: 'Select Reason Type.', messageAr: 'يرجى اختيار نوع السبب' }
    ],
    'appTypeIdReason': [
      { type: 'required', message: 'Select Application Type.', messageAr: 'يرجى اختيار نوع المعاملة' }
    ],
    'reasonEn': [
      { type: 'required', message: 'Reason in English required.', messageAr: 'السبب باللغة الإنجليزية إجباري' },
      { type: 'maxlength', message: 'Reason in English cannot exceed upto 50 characters.', messageAr: 'لا يمكن أن يكون السبب باللغة الانجليزية يزيد عن 50 حرفا' }
    ],
    'reasonAr': [
      { type: 'required', message: 'Reason in Arabic required.', messageAr: 'السبب باللغة العربية إجباري' },
      { type: 'maxlength', message: 'Reason in arabic cannot exceed upto 50 characters.', messageAr: 'لا يمكن أن يكون السبب باللغة العربية يزيد عن 50 حرفا' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
    ],
    'BNArabic1': [
      { type: 'required', message: 'BN 1 in Arabic required.', messageAr: 'BN 1 in Arabic required.' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
      { type: 'maxlength', message: 'BN 1 in Arabic cannot be exceed upto 50 characters.', messageAr: 'BN 1 in Arabic cannot be exceed upto 50 characters.' },
      { type: 'BNArabic1Exists', message: 'BN 1 in Arabic is already exist.', messageAr: 'BN 1 in Arabic is already exist.' },
    ],
    'BNArabic2': [
      { type: 'required', message: 'BN 2 in Arabic required.', messageAr: 'BN 2 in Arabic required.' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
      { type: 'maxlength', message: 'BN 2 in Arabic cannot be exceed upto 50 characters.', messageAr: 'BN 2 in Arabic cannot be exceed upto 50 characters.' },
      { type: 'BNArabic1Exists', message: 'BN 2 in Arabic is already exist.', messageAr: 'BN 2 in Arabic is already exist.' },
    ],
    'BNArabic3': [
      { type: 'required', message: 'BN 3 in Arabic required.', messageAr: 'BN 3 in Arabic required.' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
      { type: 'maxlength', message: 'BN 3 in Arabic cannot be exceed upto 50 characters.', messageAr: 'BN 3 in Arabic cannot be exceed upto 50 characters.' },
      { type: 'BNArabic1Exists', message: 'BN 3 in Arabic is already exist.', messageAr: 'BN 3 in Arabic is already exist.' },
    ],
    'BNEnglish1': [
      { type: 'required', message: 'BN 1 in English required.', messageAr: 'BN 1 in English required.' },
      { type: 'maxlength', message: 'BN 1 in English cannot be exceed upto 50 characters.', messageAr: 'BN 1 in English cannot be exceed upto 50 characters.' },
      { type: 'BNEnglish1Exists', message: 'BN 1 in English is already exist.', messageAr: 'BN 1 in English is already exist.' },
    ],
    'BNEnglish2': [
      { type: 'required', message: 'BN 2 in English required.', messageAr: 'BN 2 in English required.' },
      { type: 'maxlength', message: 'BN 2 in English cannot be exceed upto 50 characters.', messageAr: 'BN 2 in English cannot be exceed upto 50 characters.' },
      { type: 'BNEnglish1Exists', message: 'BN 2 in English is already exist.', messageAr: 'BN 2 in English is already exist.' },
    ],
    'BNEnglish3': [
      { type: 'required', message: 'BN 3 in English required.', messageAr: 'BN 3 in English required.' },
      { type: 'maxlength', message: 'BN 3 in English cannot be exceed upto 50 characters.', messageAr: 'BN 3 in English cannot be exceed upto 50 characters.' },
      { type: 'BNEnglish1Exists', message: 'BN 3 in English is already exist.', messageAr: 'BN 3 in English is already exist.' },
    ],
    'BNShortNameEnglish': [
      { type: 'maxlength', message: 'short name En cannot be exceed upto 100 characters.', messageAr: 'short name En cannot be exceed upto 100 characters.' },
    ],
    'BNShortNameArabic': [
      { type: 'maxlength', message: 'short name Ar cannot be exceed upto 100 characters.', messageAr: 'short name Ar cannot be exceed upto 100 characters.' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
    ],
    'BNTypes': [
      { type: 'required', message: 'Select BN Type.', messageAr: 'Select BN Type.' },
    ],
    'ddlLicenseCategory': [
      { type: 'required', message: 'Select License Category.', messageAr: 'Select License Category.' },
    ],
    'ddlLicenseType': [
      { type: 'required', message: 'Select License Type.', messageAr: 'Select License Type' },
    ],
    'reservationPeriod': [
      { type: 'required', message: 'Reservation Period Required', messageAr: 'Reservation Period Required' },
    ],
    'ddlIssueanceAuthority': [
      { type: 'required', message: 'Select Issuannce Authority.', messageAr: 'Select Issuannce Authority.' },
    ],
    'ddlCountry': [
      { type: 'required', message: 'Select Issuannce Authority.', messageAr: 'Select Issuannce Authority.' },
      { type: 'nameArValidator', message: 'Text should be in arabic.', messageAr: 'يجب أن تكون الحروف  باللغة العربية' },
    ],
    'ddlStakeHolderRelationshipType': [
      { type: 'required', message: 'Select Relationship Type.', messageAr: 'Select Relationship Type.' },
    ],
    'shares': [
      { type: 'required', message: 'Share required.', messageAr: 'Share required.' },
    ],
    'individualEntities': [
      { type: 'required', message: 'select your individual.', messageAr: 'select your individual.' },
    ],
    'InheritanceRepresentative': [
      { type: 'required', message: 'Select Inheritance Representative', messageAr: 'Select Inheritance Representative' },
    ],
    'requestDateFrom': [
      { type: 'required', message: 'Please select `from Date`', messageAr: 'من فضلك اختر " من تاريخ"' },
    ],
    'requestDateTo': [
      { type: 'required', message: 'Please select `to date`', messageAr: 'من فضلك اختر " الى تاريخ"' },
      { type: 'dateLessThan', message: '`from Date` should be less than `to Date`', messageAr: 'ـ"من تاريخ" يجب أن يكون أقل من " الى تاريخ"' }
    ],
    'statusDateFrom': [
      { type: 'required', message: 'Please select `from Date`', messageAr: 'من فضلك اختر " من تاريخ"' },
    ],
    'statusDateTo': [
      { type: 'required', message: 'Please select `to date`', messageAr: 'من فضلك اختر " الى تاريخ"' },
      { type: 'dateLessThan', message: '`from Date` should be less than `to Date`', messageAr: 'ـ"من تاريخ" يجب أن يكون أقل من " الى تاريخ"' }
    ],
    'tdaUser': [
      { type: 'required', message: 'Select TDA User.', messageAr: 'Select TDA User.' },
    ],
    'notes': [
      { type: 'required', message: 'Notes required.', messageAr: 'Notes required.' },
      { type: 'maxlength', message: 'Notes cannot be exceed upto 2500 characters.', messageAr: 'Notes cannot be exceed upto 2500 characters.' },
    ],
    'reason': [
      { type: 'required', message: 'Select reason.', messageAr: 'Select reason.' }
    ],
    'branchNameAr': [
      { type: 'required', message: 'branch arabic required.', messageAr: 'branch arabic required.' }
    ],
    'branchNameEn': [
      { type: 'required', message: 'branch english required.', messageAr: 'branch english required.' }
    ],
    'latitude': [
      { type: 'required', message: 'latitude required.', messageAr: 'latitude required.' }
    ],
    'longitude': [
      { type: 'required', message: 'longitude required.', messageAr: 'longitude required.' }
    ],
    'makhniNo': [
      { type: 'required', message: 'makhni No required.', messageAr: 'makhni No required.' }
    ],
    'unit': [
      { type: 'required', message: 'unit required.', messageAr: 'unit required.' }
    ]
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      let control = formGroup.get(field);
      if (control.value && control.value.length > 0 && control.value instanceof Array) {
        this.validateArray((control as FormArray));
      }
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  validateArray(formGroup: FormArray) {
    if (formGroup.controls) {
      for (let index = 0; index < formGroup.controls.length; index++) {
        this.validateArrayFields(formGroup.controls[index] as FormArray);
      }
    }
  }
  validateArrayFields(formGroup: FormArray) {
    Object.keys(formGroup.controls).forEach(fields => {
      const controls = formGroup.get(fields);
      if (controls instanceof FormControl) {
        controls.markAllAsTouched();
      }
    });
  }
  allowAr(event) {
    var nameArRegexp = /^[\u0600-\u06FF 0-9~`!@#$%^&*()_+{}|":?><;'/\]/[[,-./\\=\t]+$/;
    if (!nameArRegexp.test(event.key)) {
      return false;
    }
  }
  allowEn(event) {
    var nameArRegexp = /^[a-zA-Z 0-9~`!@#$%^&*()_+{}|":?><;'/\]/[[,-./\\=\t]+$/;
    if (!nameArRegexp.test(event.key)) {
      return false;
    }
  }
}
