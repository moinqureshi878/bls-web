import { FormGroup, FormControl } from '@angular/forms';


export function phoneNumberMasking(): any {
    return [0, 0, 9, 7, 1, /[0-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
}

export function emiratesIDMasking(): any {
    return [/[0-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/];
}
export function macAddressMasking(): any {
    return [/[0-9a-zA-Z]/, /[0-9a-zA-Z]/, '-', /[0-9a-zA-Z]/, /[0-9a-zA-Z]/, '-', /[0-9a-zA-Z]/, /[0-9a-zA-Z]/, '-', /[0-9a-zA-Z]/, /[0-9a-zA-Z]/, '-', /[0-9a-zA-Z]/, /[0-9a-zA-Z]/, '-', /[0-9a-zA-Z]/, /[0-9a-zA-Z]/];
}

export function emailValidator(control: FormControl): { [key: string]: any } {
    var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if (control.value && !emailRegexp.test(control.value)) {
        return { invalidEmail: true };
    }
}
export function passwordValidator(control: FormControl): { [key: string]: any } {
    var passwordRegexp = /^(?=(.*\d.*){1,})(?=(.*[a-zA-Z].*){6,}).{7,30}$/;
    if (control.value && !passwordRegexp.test(control.value)) {
        return { invalidPassword: true };
    }
}
export function nameValidator(control: FormControl): { [key: string]: any } {
    var nameRegexp = /^[^-\s][a-zA-Z_\s-]+$/;
    if (control.value && !nameRegexp.test(control.value)) {
        return { nameValidator: true };
    }
}
export function alphaNumeric(control: FormControl): { [key: string]: any } {
    var nameRegexp = /^([^-\s][a-zA-Z0-9 _-]+)$/;
    if (control.value && !nameRegexp.test(control.value)) {
        return { alphaNumeric: true };
    }
}
export function commentValidator(control: FormControl): { [key: string]: any } {
    var nameRegexp = /^[^\s].*$/;
    if (control.value && !nameRegexp.test(control.value)) {
        return { commentValidator: true };
    }
}
export function nameArValidator(control: FormControl): { [key: string]: any } {
    // var nameArRegexp = /^[^a-zA-Z0-9~`!@#$%^&*()_+{}|":?><;',.][\u0600-\u06FF]+[^a-zA-Z0-9~`!@#$%^&*()_+{}|":?><;',.]+$/;
    var nameArRegexp = /^[\u0600-\u06FF 0-9~`!@#$%^&*()_+{}|":?><;'/\]/[[,-./\\=\t]+$/;
    if (control.value && !nameArRegexp.test(control.value)) {
        return { nameArValidator: true };
    }
}
export function phoneNumberValidator(control: FormControl): { [key: string]: any } {
    // var phoneNumberRegexp = /^(?:\+971|00971|0)?(?:50|51|52|55|56|57|58|2|3|4|6|7|9)\d{7}$/;
    var phoneNumberRegexp = /^(?:\+971|00971|0)?()\d{9}$/;
    if (control.value && !phoneNumberRegexp.test(control.value)) {
        return { phoneNumberValidator: true };
    }
}
export function termsConditionValidator(control: FormControl): { [key: string]: any } {
    if (!control.value) {
        return { termsConditionValidator: true };
    }
}

export function matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
        let password = group.controls[passwordKey];
        let passwordConfirmation = group.controls[passwordConfirmationKey];
        if (password.value !== passwordConfirmation.value) {
            return passwordConfirmation.setErrors({ mismatchedPasswords: true })
        }
    }
}
export function dateLessThan(from: string, to: string): { [key: string]: any } {
    return (group: FormGroup) => {
        let f = group.controls[from];
        let fromDate = f.value;
        let fromDateConverted = new Date(fromDate);

        let t = group.controls[to];
        let toDate = t.value;
        let toDateConverted = new Date(toDate);
        if (fromDateConverted > toDateConverted) {
            return t.setErrors({ dateLessThan: true });
        }
        else if (fromDateConverted < toDateConverted) {
            f.setErrors(null);
            t.setErrors(null);
        }
        else if (!fromDate && !toDate) {
            f.setErrors(null);
            t.setErrors(null);
        }
        else {
            return null;
        }
    }
}
export function dateLessThanWhenRequired(from: string, to: string): { [key: string]: any } {
    return (group: FormGroup) => {
        let f = group.controls[from];
        let fromDate = f.value;
        let fromDateConverted = new Date(fromDate);

        let t = group.controls[to];
        let toDate = t.value;
        let toDateConverted = new Date(toDate);
        if (fromDate && toDate) {
            if (fromDateConverted > toDateConverted) {
                return t.setErrors({ dateLessThan: true });
            }
            else if (fromDateConverted < toDateConverted) {
                f.setErrors(null);
                t.setErrors(null);
            }
            else if (!fromDate && !toDate) {
                f.setErrors(null);
                t.setErrors(null);
            }
            else {
                return null;
            }
        }
    }
}
export function timeLessThan(from: string, to: string): { [key: string]: any } {
    return (group: FormGroup) => {
        let f = group.controls[from];
        let fromDate = f.value;
        let fromDateConverted = new Date(fromDate);

        let t = group.controls[to];
        let toDate = t.value;
        let toDateConverted = new Date(toDate);
        if (fromDateConverted > toDateConverted) {
            return t.setErrors({ timeLessThan: true });
        }
        else if (fromDateConverted < toDateConverted) {
            f.setErrors(null);
            t.setErrors(null);
        }
        else if (!fromDate && !toDate) {
            f.setErrors(null);
            t.setErrors(null);
        }
        else {
            return null;
        }
    }
}
export function timeLessThanWhen(from: string, to: string): { [key: string]: any } {
    return (group: FormGroup) => {
        let f = group.controls[from];
        let fromDate = f.value;
        let fromDateConverted = new Date(fromDate);

        let t = group.controls[to];
        let toDate = t.value;
        let toDateConverted = new Date(toDate);
        if (toDate && fromDate) {
            if (fromDateConverted > toDateConverted) {
                return t.setErrors({ timeLessThan: true });
            }
            else if (fromDateConverted < toDateConverted) {
                f.setErrors(null);
                t.setErrors(null);
            }
            else if (!fromDate && !toDate) {
                f.setErrors(null);
                t.setErrors(null);
            }
            else {
                return null;
            }
        }
    }
}
// export function requiredFileType( type: string ) {
//     return function (control: FormControl) {
//       const file = control.value;
//        
//       if ( file ) {
//         const extension = file.name.split('.')[1].toLowerCase();
//         if ( type.toLowerCase() !== extension.toLowerCase() ) {
//           return {
//             requiredFileType: true
//           };
//         }

//         return null;
//       }

//       return null;
//     };
//   }
export function fileSize(control: FormControl): { [key: string]: any } {
    const file = control.value;
    if (file > 2097152) {
        return { fileSize: true };
    }
    return null;
}
