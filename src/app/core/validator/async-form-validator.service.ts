import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { Observable, timer } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { EnvService } from '../environment/env.service';

@Injectable({
  providedIn: 'root'
})
export class AsyncFormValidatorService {
  constructor(private http: HttpClient, private env: EnvService) { }
  searchExistance(key, value, id) {
    return timer(1000)
      .pipe(
        switchMap(() => {
          // Check if username is available
          return this.http.get<any>(`${this.env.apiUrl}common/ValidateFields?UserId=${id}&key=${key}&Value=${value}`)
        })
      );
  }

  emailValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'UserEmail';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              return { 'emailExists': true };
            }
          })
        );
    };
  }
  fullNameEnValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'FullNameEn';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              return { 'fullNameEnExists': true };
            }
          })
        );
    };
  }
  usernameValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'UserName';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              return { 'usernameExists': true };
            }
          })
        );
    };
  }
  fullNameArValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'FullNameAr';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              return { 'fullNameArExists': true };
            }
          })
        );
    };
  }
  hrCodeValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'HRCode';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              return { 'hrCodeExists': true };
            }
          })
        );
    };
  }
  mobileNoValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'MobileNo';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              return { 'mobileNoExists': true };
            }
          })
        );
    };
  }
  SecondarymobileNoValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'SecondaryPhone';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              return { 'SecondarymobileNoExists': true };
            }
          })
        );
    };
  }
  landlineNoValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'landlineNo';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              return { 'landlineNoExists': true };
            }
          })
        );
    };
  }
  deviceNameValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'DeviceName';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              return { 'deviceNameExists': true };
            }
          })
        );
    };
  }
  macAddressValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'MacAddress';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              return { 'macAddressExists': true };
            }
          })
        );
    };
  }

  pageNameEnValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'PageNameEn';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              return { 'pageNameEnExists': true };
            }
          })
        );
    };
  }

  pageNameArValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'PageNameAr';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              return { 'pageNameArExists': true };
            }
          })
        );
    };
  }
  roleNameEnValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'RoleNameEn';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            // if username is already taken
            if (res.isValidate) {
              // return error
              return { 'roleNameEnExists': true };
            }
          })
        );
    };

  }
  roleNameArValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'RoleNameAr';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            // if username is already taken
            if (res.isValidate) {
              // return error
              return { 'roleNameArExists': true };
            }
          })
        );
    };
  }
  citizenIDValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'CitizenID';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              return { 'CitizenIDExists': true };
            }
          })
        );
    };
  }

  BNArabicValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'BusinessNameAr';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              debugger
              return { 'BNArabic1Exists': true };
            }
          })
        );
    };
  }

  BNEnglishValidator(id): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      var key = 'BusinessNameEn';
      return this.searchExistance(key, control.value, id)
        .pipe(
          map(res => {
            if (res.isValidate) {
              debugger
              return { 'BNEnglish1Exists': true };
            }
          })
        );
    };
  }
}
