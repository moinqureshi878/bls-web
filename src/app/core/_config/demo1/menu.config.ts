export class MenuConfig {
	public defaults: any = {
		aside: {
			self: {},
			items: [
				// {
				// 	title: 'Dashboard',
				// 	root: true,
				// 	icon: 'flaticon2-architecture-and-city',
				// 	page: 'dashboard',
				// 	translate: 'MENU.DASHBOARD',
				// 	bullet: 'dot',
				// },

				{
					title: 'Governments',
					root: true,
					bullet: 'dot',
					translate: 'MENU.GOVERNMENT',
					icon: 'flaticon2-architecture-and-city',
					submenu: [
						{
							title: 'Master Users',
							page: 'government/manage-users',
						},
						{
							title: 'Sub Users',
							page: 'government/manage-sub-users',
						},


					]
				},
				{
					title: 'Management',
					root: true,
					bullet: 'dot',
					icon: 'flaticon-user',
					submenu: [
						{
							title: 'TDA Users',
							page: 'admin/tda-users-profile',
						},
						{
							title: 'Roles Definition',
							page: 'admin/roles-definition',
						},
						{
							title: 'Login on behalf',
							page: 'admin/login-on-behalf',
						},
						{
							title: 'Login Logout History',
							page: 'admin/login-logout-history',
						},
						{
							title: 'Organization Structure',
							page: 'admin/organization-structure',
						},
						
						// {
						// 	title: 'Users Definition',
						// 	page: 'users-definition-management',
						// },
						// {
						// 	title: 'System Modules',
						// 	page: 'system-modules',
						// },
						// {
						// 	title: 'Access Rights',
						// 	page: 'access-rights-management',
						// },

					

					]
				},
				{
					title: 'Business Licensing',
					root: true,
					bullet: 'dot',
					icon: 'flaticon-light',
					submenu: [
						{
							title: 'Request Business Name',
							page: 'admin/business-names',
						},
						// {
						// 	title: 'Business Names',
						// 	page: 'business-licensing/manage-business-names',
						// },
					]
				},
				{
					title: 'Registration',
					root: true,
					bullet: 'dot',
					icon: 'flaticon-user',
					submenu: [
						{
							title: 'Registered Customer',
							page: 'admin/registration-request',
						},
					
					]
				},
				{
					title: 'Modules',
					root: true,
					bullet: 'dot',
					icon: 'flaticon-file',
					submenu: [
						{
							title: 'Modules Definition',
							page: 'modules/modules-definition',
						},
						{
							title: 'Sub Modules Definition',
							page: 'modules/sub-modules-definition',
						},
						{
							title: 'Pages Definition',
							page: 'modules/pages-definition',
						},

					]
				},

				// {
				// 	title: 'BLS Screen',
				// 	root: true,
				// 	bullet: 'dot',
				// 	icon: 'flaticon2-digital-marketing',
				// 	submenu: [
				// 		{
				// 			title: 'TDA Organization Structure',
				// 			page: 'tda-os',
				// 		},
				// 	]
				// },


			]
		},
	};

	public get configs(): any {
		return this.defaults;
	}
}
