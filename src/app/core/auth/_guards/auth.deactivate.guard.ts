import { Injectable } from '@angular/core';
import { CanDeactivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { JwtTokenService } from '../../appServices/index.service';
import { JWTModel } from '../../appModels/jwt.model';
import { Observable } from 'rxjs';
import {HostListener} from "@angular/core";

// export abstract class authDeactivateGuard {
//   abstract  canDeactivate(): boolean;



//     @HostListener('window:beforeunload', ['$event'])
//     unloadNotification($event: any) {
//        
//         if (!this.canDeactivate()) {
//             $event.returnValue =true;
//         }
//     }
// }
export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}
@Injectable({
  providedIn: 'root'
})

export class authDeactivateGuard implements CanDeactivate<CanComponentDeactivate>{

  @HostListener('window:onbeforeunload', ['$event'])
  unloadNotification($event: any) {
    alert('close')
  }
  canDeactivate(component: CanComponentDeactivate) {
    return component.canDeactivate ? component.canDeactivate() : true;
  }
}