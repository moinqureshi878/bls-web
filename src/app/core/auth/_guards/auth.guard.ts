// Angular
import { Injectable, NgZone } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { JwtTokenService, CommonService } from '../../appServices/index.service';
import { JWTModel } from '../../appModels/jwt.model';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { localStorageSession } from '../../appModels/localStorageSessions.model';
import { ValidationMessagesService } from '../../../core/validator/validation-messages.service';
import { SessionTypeEnum } from '../../../core/_enum/index.enum';
@Injectable()
export class AuthGuard implements CanActivate {

    AuthToken: string;
    AuthTokenModel: JWTModel;
    subscription: Subscription;

    constructor(
        private router: Router,
        private jwtService: JwtTokenService,
        private toastrService: ToastrService,
        public commonService: CommonService,
        public validationMessages: ValidationMessagesService,
    ) {

        this.AuthToken = this.jwtService.getJwtToken();
        this.AuthTokenModel = this.jwtService.getJwtTokenValue();
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        this.AuthToken = this.jwtService.getJwtToken();
        this.AuthTokenModel = this.jwtService.getJwtTokenValue();
        if (this.AuthToken) {
            var userSession: localStorageSession[] = JSON.parse(localStorage.getItem("userSession"));
            if (userSession) {
                let pages;
                if (userSession.length > 1) {
                    pages = userSession[1].pages;
                }
                else {
                    pages = userSession[0].pages;
                }

                if (pages) {

                    let pagesItem = pages;
                    if (pagesItem.find(x => x.pageUrl.split('/')[1] === state.url.split('/')[3])) {
                        // calling API to maintain session history

                        if ((userSession.length == 2 && userSession[1].sessionID == 0) || (userSession.length == 1 && userSession[0].sessionID == 0)) {
                            // get IP Address
                            let IPAddress: string = "";
                            this.commonService.getClientIpAddress().subscribe(value => {
                                IPAddress = value;
                            });


                            let sessiontype;
                            let customerID = null;
                            var benificaryType = null;
                            if (userSession.length > 1) {
                                sessiontype = SessionTypeEnum.Login;
                                let jwtTokenValue = this.jwtService.getJwtTokenValueByToken(userSession[1].token);
                                customerID = +jwtTokenValue.UserID;

                                let beneficiaryTypes = localStorage.getItem("beneficaryType");
                                benificaryType = +beneficiaryTypes;
                            }
                            else {
                                sessiontype = SessionTypeEnum.Login;
                            }

                            let model = {
                                "userId": +this.AuthTokenModel.UserID,
                                "sessionType": sessiontype,  // 42 means single
                                "customerId": customerID,
                                "beneficiaryType": benificaryType,
                                "deviceCode": "Web",
                                "ip": IPAddress,
                                "macaddress": "255.255.255.255",
                                "dateTime": this.validationMessages.convertUTCDateToLocalDate(new Date()),
                                "sessionhistoryDetailDto": [
                                    {
                                        "pageId": pagesItem.find(x => x.pageUrl.split('/')[1] === state.url.split('/')[3]).id, //state.url.split('/')[3]
                                        "dateTime": this.validationMessages.convertUTCDateToLocalDate(new Date()),
                                    }
                                ]
                            }

                            this.commonService.ManageUserSessionHistory(model).subscribe(data => {
                                // set session ID on user array

                                if (userSession.length > 1) {
                                    userSession[1].sessionID = data.data.id;
                                }
                                else {
                                    userSession[0].sessionID = data.data.id;
                                }
                                localStorage.setItem('userSession', JSON.stringify(userSession))
                            });
                        }
                        else {
                            let sessionID = 0;
                            if (userSession.length > 1) {
                                sessionID = userSession[1].sessionID;
                            }
                            else {
                                sessionID = userSession[0].sessionID;
                            }

                            let model = {
                                "sessionId": sessionID,
                                //"pageName": state.url.split('/')[3]
                                "pageId": pagesItem.find(x => x.pageUrl.split('/')[1] === state.url.split('/')[3]).id,
                                "dateTime": this.validationMessages.convertUTCDateToLocalDate(new Date()),
                            }


                            this.commonService.ManageUserSessionHistoryDetail(model).subscribe(data => {

                            });
                        }

                        return true;
                    }
                    else {
                        this.toastrService.error('You are not authorize to access');
                        //return false;
                    }
                    // var found = pagesItem.find(function (element) {
                    //     return element.find(x => x.pageUrl.split('/')[1] === state.url.split('/')[3]);
                    // });
                    // let pagesItem = JSON.parse(localStorage.getItem("sideMenu"));
                    //  
                    // var found = pagesItem.find(function (element) {
                    //     return element.submenu.find(x => x.page.split('/')[1] === state.url.split('/')[3]);
                    // });

                    // if (found) {
                    //     return true;
                    // }
                    // else {
                    //     this.toastrService.error('You are not authorize to access');
                    // }
                }
                else {
                    this.toastrService.error('You are not authorize to access');
                }


                // check if route is restricted by role
                // if (route.data.roles && route.data.roles.indexOf(this.AuthTokenModel.RoleCode) === -1) {
                // role not authorised so redirect to home page
                // this.router.navigate(['/']);
                // return false;
                //  }

                // authorised so return true
            }
            else {
                this.router.navigateByUrl('/auth/login');
            }

        }
        else {
            // not logged in so redirect to login page with the return url
            this.router.navigateByUrl('/auth/login');
            //return false;
        }
    }
}
