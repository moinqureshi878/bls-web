// Angular
import { Injectable, NgZone } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { JwtTokenService, CommonService } from '../../appServices/index.service';
import { JWTModel } from '../../appModels/jwt.model';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Injectable()
export class LoginGuard implements CanActivate {

  AuthToken: string;
  AuthTokenModel: JWTModel;
  subscription: Subscription;

  constructor(
    private router: Router,
    private jwtService: JwtTokenService,
    private toastrService: ToastrService,
    public commonService: CommonService,
  ) {

    this.AuthToken = this.jwtService.getJwtToken();
    this.AuthTokenModel = this.jwtService.getJwtTokenValue();
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    
    if (this.AuthToken) {
      let userSession = JSON.parse(localStorage.getItem("userSession"));
      if (userSession) {
        let pages;
        if (userSession && userSession.length > 1) {
          pages = userSession[1].pages;
        }
        else {
          pages = userSession[0].pages;
        }

        if (pages) {
          this.router.navigate(["/pages/" + pages[0].pageUrl])
        }
      }
      else {
        return true;
      }

    }
    else {
      // not logged in so redirect to login page with the return url
      //this.router.navigate(['/auth/login']);
      return true;
    }
  }
}
