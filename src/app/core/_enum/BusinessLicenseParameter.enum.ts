export enum BusinessLicenseParameterEnum {
  BusinessNames = 199,
  InitialApprovals = 200,
  BusinessLicense = 201,
}