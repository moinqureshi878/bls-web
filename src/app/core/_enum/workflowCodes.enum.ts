export enum WorkFlowCode {
  ToManager = 1,
  ToCustomer = 2,
  ToConsultant = 3,
  Approved = 4,
  Rejected = 5,
  ManagerReplied = 6,
  ConsultantReplied = 7
}