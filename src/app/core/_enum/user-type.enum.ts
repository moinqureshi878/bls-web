export enum UserTypeEnum {
  UAE_Citizen = 131,
  GCC_Citizen = 132,
  Resident_in_UAE = 133,
  Visitor_with_UID = 134,
  Document_Clearance_company = 135,
  TDA_User = 136,
  Govt_User = 130,
  Govt_Sub_User = 219,
  stakeholderIndividual = 92,
  stakeholderInheritanceRepresentative = 95
}
