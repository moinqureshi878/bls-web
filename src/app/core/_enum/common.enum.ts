export enum CommonEnum {
  masterUserId = 1,
  managerRoleId = 10,
  consultantRoleId = 11,
  nationality = 214,
  Individual = 2,
  DocumentCleranceCompany = 3,
  userTypeLoginPage = 48,
  userTypeLoginVisitorPage = 49,
  superUserRoleID = 1,
}