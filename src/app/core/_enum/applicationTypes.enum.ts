export enum ApplicationTypesEnum {
  BNN = 137,
  BNE = 138,
  BNM = 139,
  BNC = 140,
  IAN = 141,
  IAE = 142,
  IAM = 143,
  IAC = 144,
  BLN = 145,
  BLE = 146,
  BLM = 147,
  BLC = 148
}