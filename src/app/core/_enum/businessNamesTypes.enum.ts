export enum BusinessNamesTypesEnum {
  BranchForAForeignCompany = 188,
  BranchForAGCCCompany = 189,
  BranchForRAKFreeZoneCompany = 190,
  LocalTDABranch = 186,
  New = 185,
  OtherEmiratesBranch = 187,
}