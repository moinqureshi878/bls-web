import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseComponent } from './base/base.component';
import { AppResolver } from 'app/core/resolvers/app.resolver';


const routes: Routes = [
  {
    path: '',
    component: BaseComponent,
    // canActivate: [AuthGuard],
    //  data: { roles: [Role.User, Role.Admin, Role.Module] },
    children: [
      {
        path: 'dashboard',
        // canActivate: [AuthGuard],
        // data: { roles: [Role.Goverment] },
        loadChildren: () => import('../pages/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'government',
        // canActivate: [AuthGuard],
        // data: { roles: [Role.Goverment] },
        loadChildren: () => import('../pages/government/government.module').then(m => m.GovernmentModule)
      },
      {
        path: 'admin',
        //canActivate: [AuthGuard],
        // data: { roles: [Role.Admin] },
        loadChildren: () => import('../pages/admin/admin.module').then(m => m.AdminModule)
      },
      {
        path: 'modules',
        // canActivate: [AuthGuard],
        // data: { roles: [Role.Module] },
        loadChildren: () => import('../pages/modules/modules.module').then(m => m.ModulesModule)
      },
      {
        path: 'business-license',
        loadChildren: () => import('../pages/business-license/business-license.module').then(m => m.BusinessLicenseModule),

        children: [
          {
            path: 'request-business-name',
            loadChildren: () => import('../pages/business-license/request-business-name/request-business-name.module').then(m => m.RequestBusinessNameModule)
          },
          {
            path: 'business-names',
            loadChildren: () => import('../pages/business-license/business-names/business-names.module').then(m => m.BusinessNamesModule)
          },
          {
            path: 'request-initial-approval',
            loadChildren: () => import('../pages/business-license/request-initial-approval/request-initial-approval.module').then(m => m.RequestInitialApprovalModule)
          },
          {
            path: 'initial-approvals',
            loadChildren: () => import('../pages/business-license/initial-approvals/initial-approvals.module').then(m => m.InitialApprovalsModule)
          },
          {
            path: 'request-business-license',
            loadChildren: () => import('../pages/business-license/request-business-license/request-business-license.module').then(m => m.RequestBusinessLicenseModule)
          },
          {
            path: 'business-license',
            loadChildren: () => import('../pages/business-license/business-license/business-license.module').then(m => m.BusinessLicenseModule)
          },

        ]
      },
      {
        path: 'master-data',
        // canActivate: [AuthGuard],
        // resolve: { data: AppResolver },
        loadChildren: () => import('../pages/master-data/master-data.module').then(m => m.MasterDataModule)
      },
      {
        path: 'master-types',
        // canActivate: [AuthGuard],
        // resolve: { data: AppResolver },
        loadChildren: () => import('../pages/master-types/master-types.module').then(m => m.MasterTypesModule)
      },
      {
        path: 'nationalities',
        // canActivate: [AuthGuard],
        // data: { roles: [Role.Module] },
        loadChildren: () => import('../pages/nationalities/nationalties.module').then(m => m.NationaltiesModule)
      },

      // {
      //   path: 'request-business-name',
      //   // loadChildren: () => import('../pages/business-license/request-business-name/request-business-name.module').then(m => m.RequestBusinessNameModule)
      // },
      {
        path: 'stakeholders',
        loadChildren: () => import('../pages/stakeholders/stakeholders.module').then(m => m.StakeholdersModule)
      },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
