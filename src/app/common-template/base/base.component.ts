// Angular
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
// RxJS
import { Observable, Subscription } from 'rxjs';
// Object-Path
import * as objectPath from 'object-path';
// Layout
import { LayoutConfigService, MenuConfigService, PageConfigService } from '../../core/_base/layout';
import { HtmlClassService } from '../html-class.service';

import { MenuConfig } from '../../core/_config/demo1/menu.config';
// import { PageConfig } from '../../core/_config/demo1/page.config';
// User permissions
import { NgxPermissionsService } from 'ngx-permissions';
import { LayoutConfig } from '../../core/_config/demo1/layout.config';


import { UserIdleService } from 'angular-user-idle';
import { AuthenticationService } from '../../core/appServices/auth.service';
import { EnvService } from '../../core/environment/env.service';

@Component({
	selector: 'kt-base',
	templateUrl: './base.component.html',
	styleUrls: ['./base.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class BaseComponent implements OnInit, OnDestroy {
	// Public variables
	selfLayout: string;
	asideDisplay: boolean;
	asideSecondary: boolean;
	subheaderDisplay: boolean;
	desktopHeaderDisplay: boolean;
	fitTop: boolean;
	fluid: boolean;

	// for timer session
	seconds = 15;
	timeStart = false;
	idleTimeOfSession: number;

	// Private properties
	private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/


	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 * @param menuConfigService: MenuConfifService
	 * @param pageConfigService: PageConfigService
	 * @param htmlClassService: HtmlClassService
	 * @param store
	 * @param permissionsService
	 */
	constructor(
		private layoutConfigService: LayoutConfigService,
		private menuConfigService: MenuConfigService,
		private pageConfigService: PageConfigService,
		private htmlClassService: HtmlClassService,
		private permissionsService: NgxPermissionsService,
		// developer use services
		private userIdle: UserIdleService,
		private authService: AuthenticationService,
		private env: EnvService
	) {

		// register configs by demos
		this.layoutConfigService.loadConfigs(new LayoutConfig().configs);
		this.menuConfigService.loadConfigs(new MenuConfig().configs);
		// this.pageConfigService.loadConfigs(new PageConfig().configs);

		// setup element classes
		this.htmlClassService.setConfig(this.layoutConfigService.getConfig());

		const subscr = this.layoutConfigService.onConfigUpdated$.subscribe(layoutConfig => {
			// reset body class based on global and page level layout config, refer to html-class.service.ts
			document.body.className = '';
			this.htmlClassService.setConfig(layoutConfig);
		});
		this.unsubscribe.push(subscr);

		
		// fill idle time from env.js file
		if (this.env.idleTimeOfSession) {
			this.idleTimeOfSession = this.env.idleTimeOfSession;
		}
		else {
			this.idleTimeOfSession = 600; // means 10 minutes
		}
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		const config = this.layoutConfigService.getConfig();
		this.selfLayout = objectPath.get(config, 'self.layout');
		this.asideDisplay = objectPath.get(config, 'aside.self.display');
		this.subheaderDisplay = objectPath.get(config, 'subheader.display');
		this.desktopHeaderDisplay = objectPath.get(config, 'header.self.fixed.desktop');
		this.fitTop = objectPath.get(config, 'content.fit-top');
		this.fluid = objectPath.get(config, 'content.width') === 'fluid';

		// let the layout type change
		const subscr = this.layoutConfigService.onConfigUpdated$.subscribe(cfg => {
			setTimeout(() => {
				this.selfLayout = objectPath.get(cfg, 'self.layout');
			});
		});
		this.unsubscribe.push(subscr);
		
		this.userIdle.setConfigValues({ idle: this.idleTimeOfSession, timeout: 15, ping: 15 });
		// start watching IDLE time
		this.userIdle.startWatching();

		// Start watching when user idle is starting.
		this.userIdle.onTimerStart().subscribe(count => {
			console.log(count + "start timer")
			var eventList = ["click", "mouseover", "keydown", "DOMMouseScroll", "mousewheel",
				"mousedown", "touchstart", "touchmove", "scroll", "keyup"];
			for (let event of eventList) {
				document.body.addEventListener(event, () => this.userIdle.resetTimer());
			}
		});

		// Start watch when time is up.
		this.userIdle.onTimeout().subscribe(() => {
			this.authService.logout("session timeout");
		});
	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		this.unsubscribe.forEach(sb => sb.unsubscribe());
	}

	
}
