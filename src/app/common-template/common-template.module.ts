
import { NgxPermissionsModule } from 'ngx-permissions';
// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PartialsModule } from '../views/partials/partials.module';

// NgBootstrap
import { NgbProgressbarModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
// Translation
import { TranslateModule } from '@ngx-translate/core';
// Loading bar
import { LoadingBarModule } from '@ngx-loading-bar/core';
// Ngx DatePicker
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
// Perfect Scrollbar
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
// SVG inline
import { InlineSVGModule } from 'ng-inline-svg';
// Core Module
import { CoreModule } from '../core/core.module';
import { HeaderComponent } from './header/header.component';
import { AsideLeftComponent } from './aside/aside-left.component';
import { FooterComponent } from './footer/footer.component';
import { BrandComponent } from './brand/brand.component';
import { TopbarComponent } from './header/topbar/topbar.component';
import { MenuHorizontalComponent } from './header/menu-horizontal/menu-horizontal.component';

import { BaseComponent } from './base/base.component';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesModule } from '../pages/pages.module';
import { HtmlClassService } from './html-class.service';
import { HeaderMobileComponent } from './header/header-mobile/header-mobile.component';

// import { PermissionEffects, permissionsReducer, RoleEffects, rolesReducer } from '../core/auth';
import { AuthenticationService } from '../core/appServices/auth.service';
import{ShareableModalComponent} from '../pages/admin/shareable-modal/shareable-modal.component';
@NgModule({
	entryComponents: [ShareableModalComponent],
	declarations: [
		BaseComponent,
		FooterComponent,
		ShareableModalComponent,
		// headers
		HeaderComponent,
		BrandComponent,
		HeaderMobileComponent,
	
		// topbar components
		TopbarComponent,
		// aside left menu components
		AsideLeftComponent,
		// horizontal menu components
		MenuHorizontalComponent,
	
	],
	exports: [
		BaseComponent,
		FooterComponent,
		// headers
		HeaderComponent,
		BrandComponent,
		HeaderMobileComponent,
		// topbar components
		TopbarComponent,
		// aside left menu components
		AsideLeftComponent,
		// horizontal menu components
		MenuHorizontalComponent,

	],
	providers: [
		HtmlClassService,
		AuthenticationService
	],
	imports: [

		RouterModule,
		NgxPermissionsModule.forChild(),
		// StoreModule.forFeature('roles', rolesReducer),
		// StoreModule.forFeature('permissions', permissionsReducer),
		// EffectsModule.forFeature([PermissionEffects, RoleEffects]),
		PagesRoutingModule,
		PagesModule,
		PartialsModule,
		CoreModule,
		PerfectScrollbarModule,
		TranslateModule.forChild(),
		LoadingBarModule,
		NgxDaterangepickerMd,
		InlineSVGModule,
		// ng-bootstrap modules
		NgbProgressbarModule,
		NgbTooltipModule,
	]
})
export class CommonTemplateModule {
}



























// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { BaseComponent } from './base/base.component';
// import { PagesRoutingModule } from './pages-routing.module';
// import { PagesModule } from '../pages/pages.module';



// @NgModule({
//   declarations: [BaseComponent],
//   imports: [
//     CommonModule,
//     PagesRoutingModule,
//     PagesModule
//   ]
// })
// export class CommonTemplateModule { }
