import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

const routes: Routes = [
	
	{ path: '', redirectTo: 'auth', pathMatch: 'full' },

	{ path: 'auth', loadChildren: () => import('./auth/auth.module').then(m => m.AuthenticationModule) },
	{ path: 'notifications', loadChildren: () => import('./notifications/notifications.module').then(m => m.NotificationsModule) },
	{ path: 'pages', loadChildren: () => import('../app/common-template/common-template.module').then(m => m.CommonTemplateModule) },
	{ path: 'registration', loadChildren: () => import('./registration/registration.module').then(m => m.RegistrationModule) },

	{ path: '**', redirectTo: 'demo1/error/403', pathMatch: 'full' },
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {
			useHash: true,
			preloadingStrategy: PreloadAllModules
		})
	],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
