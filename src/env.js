(function (window) {
    window.__env = window.__env || {};

    // API url
    // window.__env.apiUrl = 'https://bls-api-dev.neusol.com/api/';
    // window.__env.apiUrl = 'https://localhost:44352/api/';
    window.__env.apiUrl = 'https://qa-bls-api.neusol.com/api/';
    // window.__env.apiUrl = 'https://172.16.0.124/api/';
    // window.__env.apiUrl = 'https://25554794.ngrok.io/api/';
    window.__env.apiOrigin = 'https://qa-bls-api.neusol.com/';

    window.__env.apiKey = '799dda717c0d4a2eb44bf2edbfbbcccc'
    window.__env.captchaKey = '6LdMLbYUAAAAAOoFzmEyRoWhZlx4-jvYgz0iXLdi';
    // window.__env.idleTimeOfSession = (5*60); // (minutes*seconds)
    window.__env.idleTimeOfSession = (5 * 60); // (minutes*seconds)

    // Whether or not to enable debug mode
    // Setting this to false will disable console output
    window.__env.enableDebug = true;
}(this));