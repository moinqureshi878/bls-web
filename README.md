
### Reactive Form Array 
https://stackblitz.com/edit/angular-5qc37d?file=app%2Fapp.component.html
https://stackoverflow.com/questions/41727013/setting-angular-2-formarray-value-in-reactiveform

### Reactive Form File Upload
https://netbasal.com/how-to-implement-file-uploading-in-angular-reactive-forms-89a3fffa1a03
https://itnext.io/valid-and-invalid-in-angular-forms-61cfa3f2a0cd

### treeview support issue
https://github.com/angular/components/issues/13056

### Change Detection strategy
https://stackoverflow.com/questions/56311396/angular-dom-not-updating-on-changes

### Textbox focus change
https://stackoverflow.com/questions/31582113/how-can-i-set-focus-to-another-input
https://stackoverflow.com/questions/52523909/angular-change-focus-on-enter-in-reactive-forms

### export to excel mat-table
npm install --save mat-table-exporter

### Map Filter Combination
let usersList = this.route.snapshot.data.data.filter(role => role.roleId == roleId).map(user => user['users']);

### treeView JSON
https://www.google.com/search?sxsrf=ACYBGNRwOBGz3VFb5yckiW8WsR35G-6YjQ%3A1574240648862&ei=iAHVXY2wNKzylwTRjprACw&q=treeview+json+using+parent+id&oq=treeview+json+using+parent+id&gs_l=psy-ab.3...7520.9423..10751...0.3..0.291.1930.0j1j7......0....1..gws-wiz.......0i71.HFsDH-W2fu0&ved=0ahUKEwiNlKq1t_jlAhUs-YUKHVGHBrgQ4dUDCAs&uact=5
https://stackoverflow.com/questions/15376251/hierarchical-json-from-flat-with-parent-id

### Mat Stepper Custom Selection
https://stackblitz.com/edit/ng-mat-beta-12-luhe7n?file=app%2Fapp.component.ts

 console.log(event.currentTarget.offsetTop);
    this.coordinates.top = `${event.currentTarget.offsetTop}px`;
    this.coordinates.left = `${event.currentTarget.offsetLeft}px`;
 [ngStyle]="{'top':this.coordinates.top,'left': this.coordinates.left}"
 https://stackblitz.com/edit/angular-x49gwi?file=src/app/another.component.ts


### Handle Right Click
https://stackoverflow.com/questions/49619887/how-to-handle-right-click-event-in-angular-app
https://jsfiddle.net/j4aLmgw2/5/
https://stackblitz.com/edit/angular-x49gwi?file=src/app/another.component.ts

### Recursive Example
findNode(nodeName) {
var trail = [];
var found = false;
function recurse(categoryAry) {
  for (var i = 0; i < categoryAry.length; i++) {
    trail.push(categoryAry[i].item);
    // Found the category!
    if ((categoryAry[i].item == nodeName)) {
      found = true;
      break;
      // Did not match...
    } else {
      // Are there children / sub-categories? YES
      if (categoryAry[i].children && categoryAry[i].children.length > 0) {
        recurse(categoryAry[i].children);
        if (found) {
          break;
        }
      }
    }
    trail.pop();
  }
}
recurse(this.a);
return found;
}

## Filter array with another array.
  filterArray = [23, 22];
  let results = this.pages.filter(array => this.filterArray.includes(array.moduleId)).filter(name => name.nameEn.match('add')).filter(id => id.id == 23);

### UTC Time To Local Time
  https://stackoverflow.com/questions/6525538/convert-utc-date-time-to-local-date-time
 
### 
https://stackblitz.com/edit/angular-fesawc

### Access the previous route
https://community.wia.io/d/22-access-the-previous-route-in-your-angular-5-app

### Browser and tab close event
https://stackoverflow.com/questions/47199361/logout-when-closing-window-in-angular-4

### if-my-website-is-open-in-another-tab
https://stackoverflow.com/questions/23690666/check-if-my-website-is-open-in-another-tab

### debug app in chrome 
https://automationpanda.com/2018/01/12/debugging-angular-apps-through-visual-studio-code/


this.commonService.GetCustomLookup('gender', 0);

### Business Licencing
https://neusolllc.sharepoint.com/:w:/s/BLS/EQ8VtnVaCU5JlH8o2r9lYNUBTxJpDuiPV14S1ociQ5ToPA?e=hUuTCK

### Print
https://stackblitz.com/edit/angular-printing-solution-example-3?file=src%2Fapp%2Fmain-page%2Fmain-page.component.html
https://stackoverflow.com/questions/50567852/how-to-print-a-page-in-angular-4-with-ts
https://www.npmjs.com/package/ngx-print

### Parent Child Form
https://medium.com/@joshblf/using-child-components-in-angular-forms-d44e60036664

### angular-use-component-from-another-module
https://stackoverflow.com/questions/39601784/angular-use-component-from-another-module

### Date Pipe Locale Change
https://stackoverflow.com/questions/34904683/how-to-set-locale-in-datepipe-in-angular-2

### Mat Stepper Prevent
https://stackoverflow.com/questions/47219903/angular-material-prevent-mat-stepper-from-navigating-between-steps?rq=1

###  Dublicate value in Array
https://stackoverflow.com/questions/30735465/how-can-i-check-if-the-array-of-objects-have-duplicate-property-values